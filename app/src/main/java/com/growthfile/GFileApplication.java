package com.growthfile;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.growthfile.utility.Foreground;

import simplifii.framework.utility.Preferences;

/**
 * Created by canbrand on 5/10/15.
 */

public class GFileApplication extends Application {
    public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
//        Fabric.with(this, new Crashlytics());
        Foreground.init(this);
        Preferences.initSharedPreferences(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}