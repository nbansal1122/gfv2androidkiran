package com.growthfile;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.widget.RemoteViews;
import com.amulyakhare.textdrawable.TextDrawable;
import com.growthfile.services.NotificationActionService;
import com.growthfile.ui.MainActivity;
import com.google.android.gms.gcm.GcmListenerService;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MyFirebaseMessagingService extends GcmListenerService {

    //This method will be called on every new message received
    private TextDrawable.IBuilder drawable1;
    @Override
    public void onMessageReceived(String from, Bundle data) {

        //Getting the message from the bundle
        String message = data.getString("message");
        String type= data.getString("notification_type");
        //Displaying a notiffication with the message
        System.out.println("message--"+message);
        System.out.println("type--"+type);
        startNotification(message,type);
    }


    private void startNotification(String message,String type){
        if(true){
            return;
        }

   /*     if(type.equals("feeds")) {
            JSONObject jsonResultData = null;
            try {
                jsonResultData = new JSONObject(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("From list activity", "nofication come from list actiity");
            NotificationManager mNotificationManager;
            mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            int		NOTIFICATION_ID	= 1;

            Intent intent;
            intent= new Intent(getApplicationContext(), MainActivity.class);
           *//* Bundle bundle = new Bundle();
            bundle.putString("FROM","notification");
            intent.putExtras(bundle);*//*
        //    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
        //    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.spalsh_logo)
                    .setLargeIcon(((BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.spalsh_logo)).getBitmap())
                    .setContentTitle("Growth File")
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(jsonResultData.optString("name")+" "+jsonResultData.optString("action_label")))
                    .setContentText(jsonResultData.optString("name")+" "+jsonResultData.optString("action_label"));
            mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
            mBuilder.setAutoCancel(true);
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }*/

       if(type.equals("feeds")) {
            JSONObject jsonResultData = null;
            try {
                jsonResultData = new JSONObject(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int entityId = jsonResultData.optInt("application_id");
            String entityType = jsonResultData.optString("application_type");
            System.out.println("entityType--" + entityType);
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(ns);

            RemoteViews notificationView = new RemoteViews(getPackageName(),
                    R.layout.theft_alert_popup);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.spalsh_logo)
                    .setContent(notificationView).setAutoCancel(true);

            Notification notification = mBuilder.build();
            notification.flags = Notification.FLAG_AUTO_CANCEL;

            String sourceString = "<b>" + jsonResultData.optString("name") + "</b> " + jsonResultData.optString("action_label");

            String test = jsonResultData.optString("name");
            char first = test.charAt(0);
            notificationView.setTextViewText(R.id.tv_noti_title, Html.fromHtml(sourceString));
            notificationView.setTextViewText(R.id.img_noti_userdp, String.valueOf(first));

            notification.contentView = notificationView;
            //the intent that is started when the notification is clicked (works)
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            notification.contentIntent = pendingNotificationIntent;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

   /*         if (Build.VERSION.SDK_INT >= 16) {
                RemoteViews expandedView = new RemoteViews(getPackageName(), R.layout.theft_alert_popup);
                notification.bigContentView = expandedView;

                mBuilder.setAutoCancel(true);
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                expandedView.setTextViewText(R.id.img_noti_userdp, String.valueOf(first));
                expandedView.setTextViewText(R.id.tv_noti_title, Html.fromHtml(sourceString));
                String dt = jsonResultData.optString("timestamp");
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date theDate = simpleDateFormat.parse(dt);
                    String localDateTime = getDateInTimeZone(theDate, TimeZone.getDefault().getID());

                    if (localDateTime.endsWith("PM")) {
                        String output = localDateTime.replaceAll("PM", "pm");
                        expandedView.setTextViewText(R.id.tv_noti_time, output);
                    } else if (localDateTime.endsWith("AM")) {
                        String output = localDateTime.replaceAll("AM", "am");
                        expandedView.setTextViewText(R.id.tv_noti_time, output);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (jsonResultData.optString("comment").equals("")) {
                    expandedView.setViewVisibility(R.id.lin_notif_comm, View.GONE);
                } else {
                    expandedView.setViewVisibility(R.id.lin_notif_comm, View.VISIBLE);
                    expandedView.setTextViewText(R.id.tv_noti_descrp, jsonResultData.optString("comment"));
                }
                int day = 0;
                String from = "", to = "";
                try {
                    day = jsonResultData.optJSONObject("meta_data").optInt("days");
                    from = jsonResultData.optJSONObject("meta_data").optString("from");
                    to = jsonResultData.optJSONObject("meta_data").optString("to");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (day == 1 || day == 1.0) {
                    expandedView.setViewVisibility(R.id.linear_notify_calSingledate, View.VISIBLE);
                    expandedView.setViewVisibility(R.id.rel_noti_calmultipledate, View.GONE);

                    String[] sepSingle = from.split("-");
                    expandedView.setTextViewText(R.id.tv_noti_singleday, "" + day);
                    expandedView.setTextViewText(R.id.tv_noti_single_leavedate, sepSingle[2]);

                    String leaveTYpe = jsonResultData.optString("application_type");

                    if (leaveTYpe.equals("EL")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    } else if (leaveTYpe.equals("CL")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    } else if (leaveTYpe.equals("SL")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    } else if (leaveTYpe.equals("PM")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    } else if (leaveTYpe.equals("AM")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    } else if (leaveTYpe.equals("LWP")) {
                        expandedView.setTextViewText(R.id.tv_noti_sinle_leavecond, leaveTYpe);
                    }

                    String yr = Character.toString(sepSingle[0].charAt(2)) + Character.toString(sepSingle[0].charAt(3));
                    expandedView.setTextViewText(R.id.tv_noti_single_leaveyear, getMonth(Integer.parseInt(sepSingle[1])) + "'" + yr);
                } else {
                    expandedView.setViewVisibility(R.id.linear_notify_calSingledate, View.GONE);
                    expandedView.setViewVisibility(R.id.rel_noti_calmultipledate, View.VISIBLE);
                    expandedView.setTextViewText(R.id.tv_noti_multiple_days, "" + day);

                    String leaveTYpe = jsonResultData.optString("application_type");

                    if (leaveTYpe.equals("EL")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    } else if (leaveTYpe.equals("CL")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    } else if (leaveTYpe.equals("SL")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    } else if (leaveTYpe.equals("PM")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    } else if (leaveTYpe.equals("AM")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    } else if (leaveTYpe.equals("LWP")) {
                        expandedView.setTextViewText(R.id.tv_noti_multiple_leavetype, leaveTYpe);
                    }

                    String[] sepStart = from.split("-");
                    expandedView.setTextViewText(R.id.tv_noti_multiple_statdate, sepStart[2]);

                    String[] sepEnd = to.split("-");
                    expandedView.setTextViewText(R.id.tv_noti_multiple_enddate, sepEnd[2]);

                    String yrStr = Character.toString(sepStart[0].charAt(2)) + Character.toString(sepStart[0].charAt(3));
                    expandedView.setTextViewText(R.id.tv_noti_multiple_startmon, getMonth(Integer.parseInt(sepStart[1])) + "'" + yrStr);

                    String yrEnd = Character.toString(sepEnd[0].charAt(2)) + Character.toString(sepEnd[0].charAt(3));
                    expandedView.setTextViewText(R.id.tv_noti_multiple_endmon, getMonth(Integer.parseInt(sepEnd[1])) + "'" + yrEnd);
                }
                notification.contentView = expandedView;

                Intent switchIntent = new Intent(this, StopSessionBroadcast.class);
                switchIntent.putExtra("actionId", 1);
                switchIntent.putExtra("entityId", entityId);
                switchIntent.putExtra("entityType", entityType);
                switchIntent.putExtra("notification_id", 0);
                PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 0,
                        switchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
                expandedView.setOnClickPendingIntent(R.id.closeOnFlash,
                        pendingSwitchIntent);
                notification.contentIntent = pendingSwitchIntent;
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                Intent switchIntent1 = new Intent(this, StopSessionBroadcast.class);
                switchIntent1.putExtra("actionId", 0);
                switchIntent1.putExtra("entityId", entityId);
                switchIntent1.putExtra("entityType", entityType);
                switchIntent.putExtra("notification_id", 1);
                PendingIntent pendingSwitchIntent1 = PendingIntent.getBroadcast(this, 1,
                        switchIntent1, PendingIntent.FLAG_UPDATE_CURRENT);
                sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
                expandedView.setOnClickPendingIntent(R.id.but_cross, pendingSwitchIntent1);
                notification.contentIntent = pendingSwitchIntent1;
                notification.flags |= Notification.FLAG_AUTO_CANCEL;
            }*/

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);
        }else if(type.equals("")){



        }
    }
    /*
	 * Broadcast to activate the feeds event.........................
	 */

    public static class StopSessionBroadcast extends BroadcastReceiver {
        private int id;
        @Override
        public void onReceive(Context context, Intent intent) {

            System.out.println("Received Cancelled Event");
            int entityId = intent.getIntExtra("entityId",0);
            int actionId = intent.getIntExtra("actionId",0);
            String entityType = intent.getStringExtra("entityType");
            id = intent.getIntExtra("notification_id", 0);

            Intent i = new Intent(context, NotificationActionService.class);
            i.putExtra("entityId", entityId);
            i.putExtra("actionId", actionId);
            i.putExtra("entityType",entityType);
            context.startService(i);

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(id);

        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getShortMonths()[month-1];
    }

    public String getDateInTimeZone(Date currentDate, String timeZoneId) {

        //String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";
        DateFormat targetFormat = new SimpleDateFormat("MMM dd, hh:mm a");
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);

        Date localDateTime = new Date(currentDate.getTime() + timeZone.getOffset(currentDate.getTime()));
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(localDateTime.getTime());
        if (timeZone.useDaylightTime()) {
            // time zone uses Daylight Saving
            cal.add(Calendar.MILLISECOND, timeZone.getDSTSavings() * -1);// in milliseconds
        }
        return targetFormat.format(cal.getTime());
    }
}