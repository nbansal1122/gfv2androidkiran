package com.growthfile.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewWorkSansLight extends TextView {

	public TextViewWorkSansLight(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TextViewWorkSansLight(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextViewWorkSansLight(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"WorkSans-Light.otf");
		setTypeface(tf);		

	}
	


}
