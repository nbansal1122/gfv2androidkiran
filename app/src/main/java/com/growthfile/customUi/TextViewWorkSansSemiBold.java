package com.growthfile.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewWorkSansSemiBold extends TextView {

	public TextViewWorkSansSemiBold(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TextViewWorkSansSemiBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextViewWorkSansSemiBold(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"WorkSans-SemiBold.otf");
		setTypeface(tf);		

	}
	


}
