package com.growthfile.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewWorkSansMedium extends TextView {

	public TextViewWorkSansMedium(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TextViewWorkSansMedium(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextViewWorkSansMedium(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"WorkSans-Medium.otf");
		setTypeface(tf);		

	}
	


}
