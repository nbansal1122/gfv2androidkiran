package com.growthfile.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewBenchNineBold extends TextView {

	public TextViewBenchNineBold(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TextViewBenchNineBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextViewBenchNineBold(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"BenchNine-Bold.otf");
		setTypeface(tf);		

	}
	


}
