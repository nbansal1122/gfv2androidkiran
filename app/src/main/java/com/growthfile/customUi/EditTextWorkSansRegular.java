package com.growthfile.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextWorkSansRegular extends EditText {

	public EditTextWorkSansRegular(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	public EditTextWorkSansRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public EditTextWorkSansRegular(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"WorkSans-Regular.otf");
		setTypeface(tf);
	}

}