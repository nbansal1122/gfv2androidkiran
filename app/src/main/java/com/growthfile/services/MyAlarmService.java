package com.growthfile.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.growthfile.R;
import com.growthfile.ui.PunchInOutActivity;

public class MyAlarmService extends Service {

	private NotificationManager mManager;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@SuppressWarnings("static-access")
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		NotificationManager mNotificationManager;
		 /* Invoking the default notification service */
		NotificationCompat.Builder  mBuilder = new NotificationCompat.Builder(this);

		mBuilder.setContentTitle("Growth File");
		mBuilder.setContentText("Please mark your attendance");
		//mBuilder.setTicker("New Message Alert!");
		mBuilder.setSmallIcon(R.drawable.spalsh_logo);


  		 /* Add Big View Specific Configuration */
		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
		// Sets a title for the Inbox style big view
		inboxStyle.setBigContentTitle("Big Title Details:");
		mBuilder.setStyle(inboxStyle);

   		/* Creates an explicit intent for an Activity in your app */
		Intent resultIntent = new Intent(this, PunchInOutActivity.class);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(PunchInOutActivity.class);

  		 /* Adds the Intent that starts the Activity to the top of the stack */
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

		mBuilder.setContentIntent(resultPendingIntent);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /* notificationID allows you to update the notification later on. */
		mNotificationManager.notify(0, mBuilder.build());

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}