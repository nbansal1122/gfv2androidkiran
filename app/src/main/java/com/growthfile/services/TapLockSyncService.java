package com.growthfile.services;

import android.app.IntentService;
import android.content.Intent;

import com.growthfile.Pojo.TapLogSyncModel;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import simplifii.framework.utility.Prefs;
import com.growthfile.database.DbHelper;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.growthfile.webservices.TapLogSyncRawDeviceData;
import com.growthfile.webservices.TapLogSyncRawModel;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by kundan on 9/14/2016.
 */
public class TapLockSyncService extends IntentService {

    DbHelper dbHelper;
    Prefs prefs;
    TapLogSyncRawDeviceData rawDeviceData;

    public TapLockSyncService() {
        super("TapLockSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        prefs=new Prefs();
        dbHelper = DbHelper.getInstance(TapLockSyncService.this);
        rawDeviceData=new TapLogSyncRawDeviceData();
        rawDeviceData.app_id= prefs.getPreferencesString(TapLockSyncService.this,"token");
        rawDeviceData.os_type="ANDROID";
        rawDeviceData.os_version="6.1";
        TapLogSyncRawModel tapLogSyncRawModel = new TapLogSyncRawModel();
        tapLogSyncRawModel.taplog = dbHelper.getAllTapLogData();
        tapLogSyncRawModel.device=rawDeviceData;

        if (Helper.checkInternetConnection(this)) {
            callTapLogSyncApi(tapLogSyncRawModel);
        }

    }

    void callTapLogSyncApi(TapLogSyncRawModel logSyncRawModel) {

        String auth_token = prefs.getPreferencesString(TapLockSyncService.this, "AUTH_TOKEN");
        System.out.println(auth_token);
        String content_type="application/json";
        Retrofit retrofit=new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis apiInterface = retrofit.create(RestApis.class);

        Call<TapLogSyncModel> call=apiInterface.syncTapLog("bearer " +auth_token,content_type,logSyncRawModel);
        call.enqueue(new Callback<TapLogSyncModel>() {
            @Override
            public void onResponse(Call<TapLogSyncModel> call, Response<TapLogSyncModel> response) {
                if (response.raw().code() == 200) {
                   dbHelper.updateSyncStatus();
                } else {

                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();

                        JSONObject reader = new JSONObject(errorMsg);
                        name  = reader.getString("msg");
                        // name = sys.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TapLogSyncModel> call, Throwable t) {

            }
        });

    }

}
