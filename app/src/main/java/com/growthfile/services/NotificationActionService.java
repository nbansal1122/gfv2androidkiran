package com.growthfile.services;

import android.app.IntentService;
import android.content.Intent;

import com.growthfile.database.DbHelper;
import com.growthfile.ui.MainActivity;

import simplifii.framework.utility.Prefs;

import com.growthfile.webservices.TapLogSyncRawDeviceData;

/**
 * Created by kundan on 9/14/2016.
 */
public class NotificationActionService extends IntentService {

    DbHelper dbHelper;
    Prefs prefs;
    TapLogSyncRawDeviceData rawDeviceData;

    // Service for halding Notification custom click ie.. Accept and Reject
    public NotificationActionService() {
        super("NotificationActionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        System.out.println("inside NotificationActionService");

        int entityId = intent.getIntExtra("entityId",0);
        int actionId = intent.getIntExtra("actionId",0);
        String entityType = intent.getStringExtra("entityType");

        System.out.println("entityId-"+entityId+",entityType="+entityType+",actionId="+actionId);

        Intent notificationIntent = new Intent(NotificationActionService.this, MainActivity.class);
        notificationIntent.putExtra("entityId", entityId);
        notificationIntent.putExtra("actionId", actionId);
        notificationIntent.putExtra("entityType",entityType);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(notificationIntent);



    }




}
