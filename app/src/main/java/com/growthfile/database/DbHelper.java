package com.growthfile.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import simplifii.framework.utility.Prefs;
import com.growthfile.data.PunchLogData;
import com.growthfile.webservices.TapLogSyncRawData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kundan on 9/5/2016.
 */
public class DbHelper extends SQLiteOpenHelper {


    private static DbHelper mDbHelper;
    private static final int DATABASE_VERSION = 1;
    public static final String DB_NAME = "GFile.sqlite";
    private static final String TAG = "DbHelper";
    private String DB_FULL_PATH = "",PUNCH_LOG="punch_log";
    public static String DB_PATH;
    public static volatile SQLiteDatabase db;
    private Context context;
    private Prefs prefs;


    /**
     * method to get instance of DbHelper class
     *
     * @param context
     * @return
     */
    public static DbHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.

        if (mDbHelper == null) {
            mDbHelper = new DbHelper(context.getApplicationContext());
        }
        return mDbHelper;
    }

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        DB_PATH = "/data/data/" + context.getPackageName() + "/db/";
        DB_FULL_PATH = DB_PATH + DB_NAME;
        this.context = context;

    }


    /**
     * method to delete data base
     */
    public void deleteDb() {

        boolean boolDeleteStatus;
        try {
            File file = new File(DB_PATH + DB_NAME);
            boolDeleteStatus = file.delete();
            Log.d("Database :", "Old Data Base Deleted" + boolDeleteStatus);
        } catch (Exception e) {
            Log.e("Error:", e.getMessage());
        }

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*
    method to copy database
     */
    public void copyRawDataBase() {
        copyDataBase();
    }

    /**
     * method to copy db to project
     */
    public void createDb() {
        boolean chkverres = checkversion();
        if (!checkDb() || chkverres) {
            // If do does not Exist or
            //    version match needs to be done.
            //          => we have to copy the database
            copyDataBase();
        }
        db = openDataBase();
    }

    /**
     * method to check version of db
     *
     * @return
     */
    public boolean checkversion() {
        String MISC_PREFS = "MiscPrefsFile";
        int currentVersion = 1;

        prefs = new Prefs();
        currentVersion = prefs.getPreferencesInt(context, "DBVERSION");

        try {

            if (DATABASE_VERSION == currentVersion) {
                return false;
            } else {
                prefs.setPreferencesInt(context, "DBVERSION", DATABASE_VERSION);
                return true;
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            return false;
        }
    }


    /**
     * method to check db exist or not
     *
     * @return
     */
    public boolean checkDb() {
        db = null;
        try {
            db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
        return (db == null) ? false : true;
    }


    /**
     * method to open database
     *
     * @return
     * @throws SQLException
     */
    public SQLiteDatabase openDataBase() throws SQLException {
        try {
            if (db == null) {
                db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
                        SQLiteDatabase.OPEN_READWRITE | SQLiteDatabase.CREATE_IF_NECESSARY
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } else if (!db.isOpen()) {
                db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.CREATE_IF_NECESSARY
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return db;
    }


    /**
     * method to copy database
     */
    private void copyDataBase() {
        InputStream in;
        OutputStream os;
        byte arrByte[] = new byte[1024];

        try {
            in = context.getAssets().open(DB_NAME);

            // Making directory
            File dbFolder = new File(DB_PATH);
            if (!dbFolder.exists())
                dbFolder.mkdir();

            os = new FileOutputStream(DB_PATH + DB_NAME);
            int length;

            while ((length = in.read(arrByte)) > 0) {
                os.write(arrByte, 0, length);
            }
            // Closing the streams
            os.flush();
            in.close();
            os.close();

            prefs = new Prefs();
            prefs.setPreferencesBoolean(context, "DB_CREATED", true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * method to copy db to sd card
     */
    public void copyDBToPhoneSD1() {
        InputStream in;
        OutputStream os;
        byte arrByte[] = new byte[1024];

        try {
            in = new FileInputStream(DB_PATH + DB_NAME);

            os = new FileOutputStream(Environment.getExternalStorageDirectory()
                    + "/" + DB_NAME);
            int length;

            while ((length = in.read(arrByte)) > 0) {
                os.write(arrByte, 0, length);
            }

            // Closing the streams
            os.flush();
            in.close();
            os.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * method to insert punch log or app lock log into db
     * @param logData
     */
    public void insertPunchLog(PunchLogData logData) {

        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put("current_lat",logData.current_lat);
            contentValues.put("current_lon",logData.current_lon);
            contentValues.put("timestamp",logData.timestamp);
            contentValues.put("sync_status",logData.sync_status);
            contentValues.put("is_attendence", logData.is_attendence);

            db = openDataBase();
            try {
                db.insert(PUNCH_LOG, null, contentValues);
            } catch (Exception exception) {
            } finally {
                if (db != null)
                    db.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();

    }

    /**
     * method to get all offline data
     * @return
     */
    public List<TapLogSyncRawData> getAllTapLogData(){
        List<TapLogSyncRawData> allOfflineData=new ArrayList<>();
        String dbQuery="SELECT *  FROM punch_log where sync_status=0";
        db = openDataBase();
        Cursor cursor=null;

        try {
            cursor=db.rawQuery(dbQuery,null);

            while (cursor.moveToNext()) {
                TapLogSyncRawData syncRawData=new TapLogSyncRawData();
                syncRawData.logged_on=cursor.getString(cursor.getColumnIndex("timestamp"));
                syncRawData.lat=cursor.getDouble(cursor.getColumnIndex("current_lat"));
                syncRawData.lng=cursor.getDouble(cursor.getColumnIndex("current_lon"));
                syncRawData.is_attendance=cursor.getInt(cursor.getColumnIndex("is_attendence"));
                syncRawData.notify_manager=0;
                allOfflineData.add(syncRawData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();

            if (cursor != null)
                cursor.close();
        }

        return allOfflineData;
    }

    public void updateSyncStatus(){

        try {
            db = openDataBase();
            db.execSQL("UPDATE punch_log SET sync_status=1 WHERE sync_status=0");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public  ArrayList<String> getAttendance(){

        ArrayList<String> att= new ArrayList<>();
        String dbQuery= "SELECT timestamp FROM punch_log where is_attendence = 1 and sync_status=1";
        db = openDataBase();
        Cursor cursor=null;

        try {
            cursor=db.rawQuery(dbQuery,null);

            while (cursor.moveToNext()) {
                att.add(cursor.getString(cursor.getColumnIndex("timestamp")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();

            if (cursor != null)
                cursor.close();
        }

        return att;
    }
}
