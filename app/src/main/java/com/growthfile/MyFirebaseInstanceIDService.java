package com.growthfile;


import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class MyFirebaseInstanceIDService extends InstanceIDListenerService {

    //If the token is changed registering the device again
    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, GCMIntentService.class);
        startService(intent);
    }
}