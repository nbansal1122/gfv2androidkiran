package com.growthfile.webservices;

import java.util.List;

/**
 * Created by kundan on 9/14/2016.
 */
public class TapLogSyncRawModel {
    public List<TapLogSyncRawData> taplog;
    public TapLogSyncRawDeviceData device;
}
