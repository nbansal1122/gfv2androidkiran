package com.growthfile.webservices;


import com.growthfile.Pojo.ApplySelfLeavePojo;
import com.growthfile.Pojo.FeedListAcceptPojo;
import com.growthfile.Pojo.FeedListModel;
import com.growthfile.Pojo.OtpPojo;
import com.growthfile.Pojo.SelfCalendarModel;
import com.growthfile.Pojo.SelfLeaveBalPojo;
import com.growthfile.Pojo.SelfProfilePojo;
import com.growthfile.Pojo.TapLogSyncModel;
import simplifii.framework.rest.responses.GetTeamResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;


/**
 * Created by canbrand on 9/5/2016.
 */
public interface RestApis {

    @Headers( "Content-Type: application/json" )
    @POST("/api/v1/generateotp")
    Call<OtpPojo> getOTP(@Body SendOtpData sendOtpData);


    /**
     * Method to login using otp and number
     * @param loginRawData
     * @return
     */
    @Headers( "Content-Type: application/json" )
    @POST("/api/v1/user/authenticate")
    Call<SelfProfilePojo> verifyOTP(@Body LoginRawData loginRawData);


    /**
     * Method to sync Tap log and punch data
     * @param
     * @return
     */


    @PATCH("api/v1/taplogsync")
    Call<TapLogSyncModel> syncTapLog( @Header("Authorization")String auth,
                                      @Header("Content-Type")String content_type,
                                        @Body TapLogSyncRawModel logSyncModel);


    /**
     * Method to get Self calendar info
     * @param auth
     * @return
     */
    @GET("/api/v1/usercalendar?")
    Call<SelfCalendarModel> getSelfCalendarInfo(
            @Header("Authorization")String auth,
            @Query("from_date") String from_date,
            @Query("to_date")String to_date);



    /**
     * Method to get other calendar info
     * @param auth
     * @return
     */
    @GET
    Call<SelfCalendarModel> getOthersCalendarInfo(
            @Url String url,
            @Header("Authorization")String auth);
           // @Query("from_date") String from_date,
            //@Query("to_date")String to_date);


    /**
     * Method to get Self profile info
     * @param auth
     * @return
     */
    @GET("/api/v1/userprofile")
    Call<SelfProfilePojo> getSelfProfileInfo(
            @Header("Authorization")String auth
    );

    /**
     * Method to get team info
     * @param auth
     * @return
     */
    @GET("/api/v1/myteam")
    Call<GetTeamResponse> getTeamInfo(@Header("Authorization")String auth);



    /**
     * Method to get other profile info
     * @param auth
     * @return
     */
    @GET()
    Call<SelfProfilePojo> getOtherProfileInfo(
            @Url String url,
            @Header("Authorization")String auth
    );


    /**
     * Method to get self leave bal info
     * @param auth
     * @return
     */
    @GET("/api/v1/userleaves")
    Call<SelfLeaveBalPojo> getSelfLeaveInfo(
            @Header("Authorization")String auth
    );

    /**
     * Method to apply self leave info
     * @param applySelfLeaveRawData
     * @return
     */
    @Headers( "Content-Type: application/json" )
    @POST()
    Call<ApplySelfLeavePojo> applySelfLeave(@Url String url,@Body ApplySelfLeaveRawData applySelfLeaveRawData,
                                            @Header("Authorization")String auth);


    /**,
     * Method to get other leave bal info
     * @param auth
     * @return
     */
    @Headers( "Content-Type: application/json" )
    @GET
    Call<SelfLeaveBalPojo> getOtherLeaveInfo(
            @Url String url,
            @Header("Authorization")String auth
    );

    /**
     * Method to get feed list data
     * @param auth
     * @return
     */
    @GET("")
    Call<FeedListModel> getFeedListData(
            @Url String url,
            @Header("Authorization")String auth
    );


    @Headers( "Content-Type: application/json" )
    @PATCH
    Call<FeedListAcceptPojo> applyFeedAcept(
            @Url String url,
            @Header("Authorization")String auth
    );



}
