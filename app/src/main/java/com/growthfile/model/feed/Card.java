package com.growthfile.model.feed;

import simplifii.framework.model.BaseRecyclerModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.rest.responses.FeedCardResponse;
import simplifii.framework.rest.responses.Initiator;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 20/11/16.
 */
public class Card extends BaseRecyclerModel{

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.USER_FEED;
    }

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("actions")
    @Expose
    private List<Action> actions = new ArrayList<Action>();
    @SerializedName("calendar")
    @Expose
    private Calendar calendar;
    @SerializedName("card_icon")
    @Expose
    private String cardIcon;
    @SerializedName("initiator")
    @Expose
    private Initiator initiator;
    @SerializedName("responses")
    @Expose
    private List<FeedCardResponse> responses = new ArrayList<>();
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("card_title")
    @Expose
    private String cardTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("application_type")
    @Expose
    private String applicationType;
    @SerializedName("application_id")
    @Expose
    private int applicationId;
    @SerializedName("card_id")
    @Expose
    private Integer cardId;

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplication_id(int applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The actions
     */
    public List<Action> getActions() {
        return actions;
    }

    /**
     * @param actions The actions
     */
    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    /**
     * @return The calendar
     */
    public Calendar getCalendar() {
        return calendar;
    }

    /**
     * @param calendar The calendar
     */
    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    /**
     * @return The cardIcon
     */
    public String getCardIcon() {
        return cardIcon;
    }

    /**
     * @param cardIcon The cardIcon
     */
    public void setCardIcon(String cardIcon) {
        this.cardIcon = cardIcon;
    }

    /**
     * @return The initiator
     */
    public Initiator getInitiator() {
        return initiator;
    }

    /**
     * @param initiator The initiator
     */
    public void setInitiator(Initiator initiator) {
        this.initiator = initiator;
    }

    /**
     * @return The responses
     */
    public List<FeedCardResponse> getResponses() {
        return responses;
    }

    /**
     * @param responses The responses
     */
    public void setResponses(List<FeedCardResponse> responses) {
        this.responses = responses;
    }

    /**
     * @return The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The cardTitle
     */
    public String getCardTitle() {
        return cardTitle;
    }

    /**
     * @param cardTitle The cardTitle
     */
    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * @return The applicationType
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * @param applicationType The applicationType
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * @return The cardId
     */
    public Integer getCardId() {
        return cardId;
    }

    /**
     * @param cardId The cardId
     */
    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

}
