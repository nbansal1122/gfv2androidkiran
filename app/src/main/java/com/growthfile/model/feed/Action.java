package com.growthfile.model.feed;

import android.net.Uri;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

public class Action implements Chip {

    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("location")
    @Expose
    private String location;

    public String getLocationUriToOpenMap() {
        if (TextUtils.isEmpty(location)) {
            return null;
        }
        try {
            String[] split = location.split("-");
            if (split != null && split.length == 2) {
                double latitude = Double.parseDouble(split[0]);
                double longitude = Double.parseDouble(split[1]);
                String uriBegin = "geo:" + latitude + "," + longitude;
                String query = latitude + "," + longitude;
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                return uriString;
            }
        } catch (Exception e) {

        }
        return null;
    }


    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getText() {
        return title;
    }
}