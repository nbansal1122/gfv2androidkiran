package com.growthfile.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nbansal2211 on 20/11/16.
 */
public class Calendar {

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("days")
    @Expose
    private Integer days;
    @SerializedName("from")
    @Expose
    private String from;

    /**
     * @return The to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to The to
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return The days
     */
    public Integer getDays() {
        return days;
    }

    /**
     * @param days The days
     */
    public void setDays(Integer days) {
        this.days = days;
    }

    /**
     * @return The from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from The from
     */
    public void setFrom(String from) {
        this.from = from;
    }

}
