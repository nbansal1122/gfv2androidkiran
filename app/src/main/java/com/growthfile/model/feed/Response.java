package com.growthfile.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 20/11/16.
 */
public class Response {

    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("limit")
    @Expose
    private Integer limit;
    @SerializedName("cards")
    @Expose
    private List<Card> cards = new ArrayList<Card>();

    /**
     * @return The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return The page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page The page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return The limit
     */
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit The limit
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return The cards
     */
    public List<Card> getCards() {
        return cards;
    }

    /**
     * @param cards The cards
     */
    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

}
