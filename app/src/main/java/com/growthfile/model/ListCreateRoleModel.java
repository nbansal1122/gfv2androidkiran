package com.growthfile.model;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.model.CreateRoleModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public class ListCreateRoleModel implements Serializable{

    List<CreateRoleModel> createRoleList = new ArrayList<>();

    private static Context mContext;

    public static void init(Context context)
    {
        mContext = context;
    }

    public static void saveList(ListCreateRoleModel listcreateRoleModel)
    {
        String json = new Gson().toJson(listcreateRoleModel);
        Preferences.saveData(AppConstants.PREF_KEYS.CREATE_ROLE_INSTANCE,json);
    }

    public static List<CreateRoleModel> loadList(){

        String data = Preferences.getData(AppConstants.PREF_KEYS.CREATE_ROLE_INSTANCE, "");
        if (!TextUtils.isEmpty(data)){
            ListCreateRoleModel listCreateRoleModel = new Gson().fromJson(data, ListCreateRoleModel.class);
            if (listCreateRoleModel != null){
                return listCreateRoleModel.getCreateRoleList();
            }else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }


    public List<CreateRoleModel> getCreateRoleList() {
        return createRoleList;
    }

    public void setCreateRoleList(List<CreateRoleModel> createRoleList) {
        this.createRoleList = createRoleList;
    }
}
