package com.growthfile.model.v2;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.responses.FeedCard;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 12/21/16.
 */

public class FeedCardModel extends BaseRecyclerModel {

    private FeedCard feedCard;

    public FeedCardModel(FeedCard feedCard) {
        this.feedCard = feedCard;
    }

    public FeedCard getFeedCard() {
        return feedCard;
    }

    public void setFeedCard(FeedCard feedCard) {
        this.feedCard = feedCard;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.USER_FEED;
    }
}
