package com.growthfile.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.growthfile.R;

import java.util.Locale;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import static android.content.Intent.ACTION_VIEW;

/**
 * Created by my on 12-11-2016.
 */

public class AppUtil {

    public static boolean checkPermission(Context context, String permission) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static void sendEmail(Context context, String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Ref: Help@Growthfile Android App");
        intent.setData(Uri.parse("mailto:" + email)); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        context.startActivity(intent);
    }

    public static void openUrl(Context context) {
        String url = "https://play.google.com/store/apps/details?id=com.growthfile";
        Intent i = new Intent(ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static int getColorForActionType(Context context, String actionText) {
        actionText = actionText.toLowerCase(Locale.getDefault());
        int colorId = R.color.color_blue_bg;
        switch (actionText) {
            case "respond":
            case "support":
            case "share":
            case "see location":
            case "withdraw":
            case "withdrow":

                break;
            case "accept":
            case "approve":
                colorId = R.color.color_green_bg;
                break;
            case "reject":
                colorId = R.color.color_red_bg;
                break;

        }
        return ContextCompat.getColor(context, colorId);
    }

    public static int getIconForActionType(String actionText) {
        actionText = actionText.toLowerCase(Locale.getDefault());
        int drawableId = R.mipmap.accept;
        switch (actionText) {
            case "support":
                drawableId = R.mipmap.support;
                break;
            case "withdraw":
            case "withdrow":
                drawableId = R.mipmap.withdraw;
                break;
            case "accept":
                drawableId = R.mipmap.accept;
                break;
            case "approve":
                drawableId = R.mipmap.accept;
                break;
            case "reject":
                drawableId = R.drawable.cross_1;
                break;
            case "edit":
                drawableId = R.drawable.pencil;
                break;
        }
        return drawableId;
    }

    public static void showCrouton(Activity context, String msg) {
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(4500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(150)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        Crouton.showText(context, msg, style);
    }
}
