package com.growthfile.util;

import com.growthfile.enums.LeaveType;
import com.growthfile.model.feed.Action;
import com.growthfile.model.feed.Card;
import com.growthfile.utility.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.enums.ActionTitleEnum;
import simplifii.framework.rest.enums.ActionTypeEnum;
import simplifii.framework.rest.enums.LeaveAction;
import simplifii.framework.rest.responses.BaseResponse;

/**
 * Created by nbansal2211 on 21/11/16.
 */

public class FeedAction {

    Action action;
    Card card;


    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public FeedAction(Action action, Card card) {
        this.action = action;
        this.card = card;
    }

    public HttpParamObject getHttpObjectBasedOnAction() {
        return getHttp(getActionId());
    }

    private HttpParamObject getDefaultHttpObject() {
        HttpParamObject obj = new HttpParamObject();
        obj.setPostMethod();
        obj.setJSONContentType();
        obj.setClassType(BaseResponse.class);
        obj.addAuthTokenHeader();
        return obj;
    }

    public int getActionId() {
        String title = action.getTitle();
        ActionTypeEnum actionTypeEnum = ActionTypeEnum.getByType(action.getType());
        if (null != actionTypeEnum) {
            switch (actionTypeEnum) {
                case RESIGNATION: {
                    if (ActionTitleEnum.WITHDRAW.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.WITHDRAW_SELF_RESIGNATION;
                    } else if (ActionTitleEnum.ACCEPT.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.ACCEPT_RESIGNATION;
                    } else if (ActionTitleEnum.REJECT.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.REJECT_RESIGNATION;
                    }
                }
                break;
                case LEAVE: {
                    if (ActionTitleEnum.WITHDRAW.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.WITHDRAW_SELF_LEAVE;
                    } else if (ActionTitleEnum.ACCEPT.getTitle().equalsIgnoreCase(title) || ActionTitleEnum.APPROVE.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.ACCEPT_LEAVE;
                    } else if (ActionTitleEnum.REJECT.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.REJECT_LEAVE;
                    }
                }
                break;
                case POSITION: {
                    if (ActionTitleEnum.WITHDRAW.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.WITHDRAW_POSITION;
                    } else if (ActionTitleEnum.ACCEPT.getTitle().equalsIgnoreCase(title) || ActionTitleEnum.APPROVE.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.ACCEPT_POSITION;
                    } else if (ActionTitleEnum.REJECT.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.REJECT_POSITION;
                    }
                }
                break;
                case SMILEY:
                    if (ActionTitleEnum.RESPOND.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.RESPOND_SMILEY;
                    }
                    break;
                case STAR:
                    if (ActionTitleEnum.SUPPORT.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.RESPOND_STAR;
                    }
                    break;
                case ATTENDANCE:
                    if (ActionTitleEnum.SEE_LOCATION.getTitle().equalsIgnoreCase(title)) {
                        return ACTION_IDS.SEE_LOCATION;
                    }
                    break;
            }
        }
        return -1;
    }

    public HttpParamObject getHttp(int actionId) {
        HttpParamObject httpParamObject = null;
        switch (actionId) {
            case ACTION_IDS.WITHDRAW_SELF_RESIGNATION: {
                httpParamObject = ApiRequestGenerator.withdrawSelfResignation(card.getApplicationId());
            }
            break;
            case ACTION_IDS.WITHDRAW_SELF_LEAVE: {
                LeaveType leaveType = LeaveType.findByType(card.getApplicationType());
                httpParamObject = ApiRequestGenerator.withdrawSelfLeave(null, leaveType, card.getApplicationId());
            }
            break;
            case ACTION_IDS.ACCEPT_RESIGNATION: {
                httpParamObject = ApiRequestGenerator.acceptRejectResignation(card.getApplicationId(), null, true);
            }
            break;
            case ACTION_IDS.REJECT_RESIGNATION: {
                httpParamObject = ApiRequestGenerator.acceptRejectResignation(card.getApplicationId(), null, false);
            }
            break;
            case ACTION_IDS.ACCEPT_LEAVE: {
                LeaveType leaveType = LeaveType.findByType(card.getApplicationType());
                httpParamObject = ApiRequestGenerator.acceptRejectLeave(null, leaveType, card.getApplicationId(), LeaveAction.ACCEPT);
            }
            break;
            case ACTION_IDS.REJECT_LEAVE: {
                LeaveType leaveType = LeaveType.findByType(card.getApplicationType());
                httpParamObject = ApiRequestGenerator.acceptRejectLeave(null, leaveType, card.getApplicationId(), LeaveAction.REJECT);
            }
            break;
            case ACTION_IDS.WITHDRAW_POSITION: {
                httpParamObject = ApiRequestGenerator.withdrawPositionRequest(card.getApplicationId());
            }
            break;
            case ACTION_IDS.ACCEPT_POSITION: {
                httpParamObject = ApiRequestGenerator.reviewPositionRequest(card.getApplicationId(), true, null);
            }
            break;
            case ACTION_IDS.REJECT_POSITION: {
                httpParamObject = ApiRequestGenerator.reviewPositionRequest(card.getApplicationId(), false, null);
            }
            break;
            case ACTION_IDS.RESPOND_SMILEY:

                break;
        }
        return httpParamObject;
    }

    private void moveToSmileyFragment() {

    }

    public interface ACTION_IDS {
        int WITHDRAW_SELF_RESIGNATION = 1;
        int WITHDRAW_SELF_LEAVE = 2;
        int ACCEPT_RESIGNATION = 3;
        int REJECT_RESIGNATION = 4;
        int ACCEPT_LEAVE = 5;
        int REJECT_LEAVE = 6;
        int WITHDRAW_POSITION = 7;
        int ACCEPT_POSITION = 8;
        int REJECT_POSITION = 9;
        int RESPOND_SMILEY = 10;
        int RESPOND_STAR = 11;
        int SEE_LOCATION = 12;
    }

}
