package com.growthfile.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by nbansal2211 on 11/07/17.
 */

public class GFLocationManager implements LocationListener {
    private WeakReference<LocationCallback> callback;
    LocationManager locationManager;
    private Location location;
    private static GFLocationManager instance = new GFLocationManager();

    public static GFLocationManager getInstance() {
        return instance;
    }

    private GFLocationManager() {

    }

    public void setCallback(LocationCallback callback) {
        this.callback = new WeakReference<LocationCallback>(callback);
    }

//    public GFLocationManager(LocationCallback callback) {
//        this.callback = new WeakReference<LocationCallback>(callback);
//    }


    public void startLocationTracking(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Log.d("locationtrack", locationManager + "");
        checkPermission(context);
    }

    private void checkPermission(Context context) {
        new TedPermission(context).setPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION).setPermissionListener(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                requestLocationUpdate();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        }).check();
    }

    private void requestLocationUpdate() {
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isNetworkEnabled) {
            if (location != null)
                onLocationChanged(location);
            Log.d("LocationManager", "ReqLocUpdate");
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0.0f, this);
        }
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("LocationAccuracy", "" + location.getAccuracy());
        this.location = location;
        if (callback != null && callback.get() != null) {

            callback.get().onLocationUpdate(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void stopLocationUpdate() {
        locationManager.removeUpdates(this);
    }


    public static interface LocationCallback {
        public void onLocationUpdate(Location location);
    }
}
