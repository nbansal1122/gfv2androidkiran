package com.growthfile.FCM;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.growthfile.v2.activities.SingleCardActivity;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/12/16.
 */

public class PushNavigationUtil {

    public static void navigate(Context context, Bundle bundle) {
        final String pushNotifType = bundle.getString(AppConstants.BUNDLE_KEYS.NOTIF_TYPE);
        switch (pushNotifType) {
            case "feed":
                break;
            case "card":
                navigateToCardDetailPage(context, bundle.getString(AppConstants.BUNDLE_KEYS.CARD_ID));
                break;
        }


    }

    public static void navigateToCardDetailPage(Context context, String cardId) {
        Bundle b = new Bundle();
        try{
            b.putInt(AppConstants.BUNDLE_KEYS.CARD_ID, Integer.parseInt(cardId));
        } catch (Exception e){
            Log.e("Invalid card id",cardId);
            e.printStackTrace();
        }
        Intent i = new Intent(context, SingleCardActivity.class);
        i.putExtras(b);
        context.startActivity(i);
    }
}
