package com.growthfile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.growthfile.R;
import com.growthfile.holders.BaseRecyclerHolder;
import com.growthfile.holders.CreateRoleHolder;
import com.growthfile.holders.GFileUserInfoHolder;
import com.growthfile.holders.GrowthFileHolderV2;
import com.growthfile.holders.MapHeaderHolder;
import com.growthfile.holders.OtherMemberHolder;
import com.growthfile.holders.TeamMemberHolder;
import com.growthfile.v2.holders.FeedHolderV2;
import com.growthfile.v2.holders.StarImageGrowthFileHolderV2;

import java.util.List;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public class BaseRecyclerAdapter extends RecyclerView.Adapter<BaseRecyclerHolder> {
    List<BaseRecyclerModel> baseRecyclerModels;
    private Context context;
    private RecyclerClickListener listener;
    private LayoutInflater inflater;

    public BaseRecyclerAdapter(List<BaseRecyclerModel> baseRecyclerModels, Context context) {
        this.baseRecyclerModels = baseRecyclerModels;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(RecyclerClickListener listener) {
        this.listener = listener;
    }

    @Override
    public BaseRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseRecyclerHolder holder = null;
        switch (viewType) {
            case AppConstants.VIEW_TYPE.TEAM_FRAGMENT_SELF:
                holder = new TeamMemberHolder(getInflatedView(R.layout.row_team_list, null));
                break;
            case AppConstants.VIEW_TYPE.CREATED_ROLE:
                holder = new CreateRoleHolder(getInflatedView(R.layout.row_created_role, null));
                break;
            case AppConstants.VIEW_TYPE.TEAM_FRAGMENT_OTHER:
                holder = new OtherMemberHolder(getInflatedView(R.layout.row_other_list, null));
                break;
            case AppConstants.VIEW_TYPE.USER_PROFILE_HEADER_INFO:
                holder = new GFileUserInfoHolder(getInflatedView(R.layout.row_gfile_data, null));
                break;
            case AppConstants.VIEW_TYPE.USER_FEED:
                holder = new FeedHolderV2(getInflatedView(R.layout.row_user_feed_v2, parent));
                break;
            case AppConstants.VIEW_TYPE.CARD_HEADER_MAP:
                holder = new MapHeaderHolder(getInflatedView(R.layout.feeds_cardheader, parent));
                break;
            case AppConstants.VIEW_TYPE.GROWTH_FILE:
                holder = new GrowthFileHolderV2(getInflatedView(R.layout.row_fragment_growthfile_v2, parent));
                break;
            case AppConstants.VIEW_TYPE.STAR_IMAGE_HEADER:
                holder = new StarImageGrowthFileHolderV2(getInflatedView(R.layout.layout_star_image_header_v2, parent));
                break;
        }
        if (listener != null)
            holder.setClickListener(listener);
        return holder;
    }

    private View getInflatedView(int layoutId, ViewGroup parent) {
        return inflater.inflate(layoutId, parent, false);
    }

    @Override
    public void onBindViewHolder(BaseRecyclerHolder holder, int position) {
        BaseRecyclerModel baseRecyclerModel = baseRecyclerModels.get(position);
        holder.onBindData(position, baseRecyclerModel);
    }

    @Override
    public int getItemCount() {
        return baseRecyclerModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        BaseRecyclerModel baseRecyclerModel = baseRecyclerModels.get(position);
        return baseRecyclerModel.getViewType();
    }

    public interface RecyclerClickListener {
        void onItemClicked(int position, View itemView, Object model, int actionType);
    }
}
