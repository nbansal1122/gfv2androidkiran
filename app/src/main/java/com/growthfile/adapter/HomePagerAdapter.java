package com.growthfile.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.fragment.TeamListFragment;
import com.growthfile.fragment.UserFeedFragment;
import com.growthfile.ui.MainActivity;
import com.growthfile.fragment.MoreOptionFragment;
import com.growthfile.ui.MyCalendar;
import com.growthfile.fragment.GrowthFileFragment;

import java.util.HashMap;

/**
 * Created by kundan on 8/29/2016.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private Context context_;
    HashMap<Integer, String> mFragmentTags = new HashMap<Integer, String>();
    private Bundle bundle;

    public HomePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context_ = context;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {


        Fragment fragment = null;
        AppBaseFragment appBaseFragment = null;
        switch (position) {
            case 0:
//                fragment= FeedListFragment.newInstance();
                appBaseFragment = new UserFeedFragment();
                appBaseFragment.setArguments(bundle);
                appBaseFragment.setOnUserLogoClickListener((MainActivity)context_);
                return appBaseFragment;
            case 1:
                fragment = MyCalendar.newInstance();
                break;
            case 2:
                appBaseFragment = GrowthFileFragment.getInstance(null);
                appBaseFragment.setArguments(bundle);
                appBaseFragment.setOnUserLogoClickListener((MainActivity)context_);
                return appBaseFragment;
            case 3:
                appBaseFragment = TeamListFragment.newInstance();
                appBaseFragment.setArguments(bundle);
                appBaseFragment.setOnUserLogoClickListener((MainActivity)context_);
                return appBaseFragment;
            case 4:
                fragment = MoreOptionFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
   /* @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
       // Log.i(TAG, "destroyItem() [position: " + position + "]" + " childCount:" + container.getChildCount());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }*/
   /* public Fragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null)
            return null;
        return mFragmentManager.findFragmentByTag(tag);
    }
*/

}
