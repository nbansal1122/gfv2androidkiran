package com.growthfile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.growthfile.R;

/**
 * Created by kundan on 9/22/2016.
 */

public class CalendarInfoListAdapter extends RecyclerView.Adapter<CalendarInfoListAdapter.CalendarInfoViewHolder> {
    private int[] colorForInfo = {R.drawable.present_dot,R.drawable.holiday_dot,R.drawable.leave_dot, R.drawable.weekoff_dot};
    private String[] leaveType = {"Present", "Holiday", "Leave", "Week-off"};
    LayoutInflater inflater;

    public CalendarInfoListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CalendarInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View convertview = inflater.inflate(R.layout.row_calendar_info, parent, false);
        CalendarInfoViewHolder holder = new CalendarInfoViewHolder(convertview);
        return holder;
    }

    @Override
    public void onBindViewHolder(CalendarInfoViewHolder holder, int position) {
        holder.dot.setBackgroundResource(colorForInfo[position]);
        holder.tv_calendar_info_leave_type.setText(leaveType[position]);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class CalendarInfoViewHolder extends RecyclerView.ViewHolder {
        private View dot;
        private TextView tv_calendar_info_leave_type;

        public CalendarInfoViewHolder(View itemView) {
            super(itemView);

            dot = itemView.findViewById(R.id.calendar_info_dot);
            tv_calendar_info_leave_type = (TextView) itemView.findViewById(R.id.tv_calendar_info_leave_type);
        }
    }
}
