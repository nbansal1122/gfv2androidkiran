package com.growthfile.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.growthfile.R;

import java.util.ArrayList;

import simplifii.framework.rest.responses.UserProfile;

/**
 * Created by my on 11-11-2016.
 */

public class ManagerFilterAdapter extends ArrayAdapter {
    ArrayList<UserProfile> listOriginal = null;
    ArrayList<UserProfile> listFilter = null;
    OnListClickListener listener;
    int resource;
    Context context;
    private ItemFilter mFilter = new ItemFilter();

    public ManagerFilterAdapter(Context context, int resource, ArrayList<UserProfile> products, ArrayList<UserProfile> productsFilter, OnListClickListener listener) {
        super(context, resource, productsFilter);
        this.listOriginal = products;
        this.listFilter = productsFilter;
        this.resource = resource;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public int getCount() {
        return listFilter.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final UserProfile managerList = listFilter.get(position);
        holder.tvTitle.setText(managerList.getName());
        holder.tvSubtitle.setText(managerList.getDesignation());
//        holder.ivImage.setImageResource(managerList.getPhoto());
//        holder.ivCheck.setImageResource(managerList.isChecked());
        return convertView;
    }

    protected int getResourceColor(int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    class Holder {
        TextView tvTitle, tvSubtitle;
        ImageView ivImage, ivCheck;

        public Holder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_team_titlename);
            tvSubtitle = (TextView) view.findViewById(R.id.tv_team_phone);
            ivImage = (ImageView) view.findViewById(R.id.img_team_picadd);
            ivCheck = (ImageView) view.findViewById(R.id.iv_tick);

        }
    }


    public interface OnListClickListener {
        void onEdit(UserProfile product);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();


            final ArrayList<UserProfile> filterList = new ArrayList<>();
            for (UserProfile product : listOriginal) {
                String filterableString = product.getName().toLowerCase();
                if (filterableString.toLowerCase().contains(filterString)) {
                    filterList.add(product);
                }

            }
            results.values = filterList;
            results.count = filterList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<UserProfile> values = (ArrayList<UserProfile>) results.values;
            listFilter.clear();
            listFilter.addAll(values);
            notifyDataSetChanged();


        }
    }

}
