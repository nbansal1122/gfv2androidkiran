package com.growthfile.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.growthfile.Pojo.FeedListAcceptPojo;
import com.growthfile.Pojo.FeedListCard;
import com.growthfile.R;
import com.growthfile.enums.LeaveType;
import com.growthfile.ui.FeedListFragment;
import com.growthfile.ui.MainActivity;
import com.growthfile.ui.OtherProfile;
import com.growthfile.ui.PunchInOutActivity;
import com.growthfile.utility.Constraint;

import simplifii.framework.utility.Prefs;

import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.google.gson.internal.LinkedTreeMap;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Administrator on 27-07-2016.
 */
public class FeedCardFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_ITEM_readSingle = 1;
    private double current_lat, current_lon;

    Context context;
    ArrayList<FeedListCard> feedListCards;
    LinkedTreeMap metadataValue;
    Prefs prefs;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private TextDrawable.IBuilder drawable1;
    boolean headerFlag;
    int counter = 0;
    String output;
    AdapterCallback mAdapterCallback;

    public FeedCardFragmentAdapter(ArrayList<FeedListCard> feedListCards, Context context, double lat, double lon, boolean headerFlag, FeedListFragment fragment) {
        this.feedListCards = feedListCards;
        this.context = context;
        this.current_lat = lat;
        this.current_lon = lon;
        this.headerFlag = headerFlag;
        this.mAdapterCallback = ((AdapterCallback) fragment);
        prefs = new Prefs();
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.map_placeholder)
                .showImageForEmptyUri(R.drawable.map_placeholder)
                .showImageOnFail(R.drawable.map_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feeds_cardheader, parent, false);
            return new MyViewHolderHearder(v);
        } else if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feeds_cardfooter, parent, false);
            return new MyViewHolderFooter(v);

        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.change_fragment_feed_newreadonlycard_second, parent, false);
            return new MyViewHolderactionfirst(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolderHearder) {
            MyViewHolderHearder VHheader = (MyViewHolderHearder) holder;

            if (headerFlag == true) {
                VHheader.lin1.setVisibility(View.GONE);
            } else {
                VHheader.lin1.setVisibility(View.VISIBLE);
                VHheader.liner_cardheader_timetab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, PunchInOutActivity.class);
                        intent.putExtra("ToPunch", "Adapter");
                        context.startActivity(intent);
                    }
                });

                VHheader.linear_cardhead_punchtab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, PunchInOutActivity.class);
                        intent.putExtra("ToPunch", "Adapter");
                        context.startActivity(intent);
                    }
                });

                VHheader.tv_cardheader_time.setText(getCurrentTimeForHeader());
                String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + current_lat + "," + current_lon + "&zoom=15&size=500x950&maptype=roadmap&format=png";

                imageLoader.displayImage(url, VHheader.iv_feed_list_header_map, options);
            }
        } else if (holder instanceof MyViewHolderFooter) {
            MyViewHolderFooter VHheader = (MyViewHolderFooter) holder;

        } else if (holder instanceof MyViewHolderactionfirst) {
            MyViewHolderactionfirst myViewHolderactionfirst = (MyViewHolderactionfirst) holder;

            final int index = position - 1;
            /////// CHECK WEATHER NAME CARD WAS SHOWN OR PROFILE PIC///////////

            final FeedListCard feedListCard = feedListCards.get(index);

            if (TextUtils.isEmpty(feedListCard.getAvatar())) {

                String test = feedListCard.getName();
                if (!TextUtils.isEmpty(test)) {
                    char first = test.charAt(0);
                    drawable1 = TextDrawable.builder().round();
                    TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));

                    myViewHolderactionfirst.img_feadcard_userdp.setImageDrawable(drawable);
                }

            } else {
                initiateImageLoader();
                imageLoader.displayImage(feedListCard.getAvatar(), myViewHolderactionfirst.img_feadcard_userdp, options);
            }

            String sourceString = "<b>" + feedListCard.getName() + "</b> " + feedListCard.getActionLabel();
            myViewHolderactionfirst.tv_feedcard_action.setText(Html.fromHtml(sourceString));

            String dt = feedListCard.getTimestamp();
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date theDate = simpleDateFormat.parse(dt);
                String localDateTime = getDateInTimeZone(theDate, TimeZone.getDefault().getID());


                if (localDateTime.endsWith("PM")) {
                    output = localDateTime.replaceAll("PM", "pm");
                    myViewHolderactionfirst.tv_feedcard_time.setText(output);
                } else if (localDateTime.endsWith("AM")) {
                    output = localDateTime.replaceAll("AM", "am");
                    myViewHolderactionfirst.tv_feedcard_time.setText(output);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(feedListCard.getComment())) {
                myViewHolderactionfirst.tv_feedcard_descrp.setVisibility(View.GONE);
            } else {
                myViewHolderactionfirst.tv_feedcard_descrp.setVisibility(View.VISIBLE);
                myViewHolderactionfirst.tv_feedcard_descrp.setText(feedListCards.get(index).getComment());
            }

            if ("MAP".equals(feedListCard.getCardType())) {

                myViewHolderactionfirst.linear_feedcard_actionread_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_acept_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_read_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_withdraw_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_map_section.setVisibility(View.VISIBLE);
                myViewHolderactionfirst.linear_feedacard_date_section.setVisibility(View.GONE);

                myViewHolderactionfirst.img_feedcard_logo.setImageResource(R.drawable.location_nav);

                metadataValue = (LinkedTreeMap) feedListCard.getMetaData();

                double lat = Double.valueOf(String.valueOf(metadataValue.get("lat")));
                double lon = Double.valueOf(String.valueOf(metadataValue.get("lng")));

                String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=15&size=450x160&maptype=roadmap&format=png&scale=2"
                        + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + current_lat + "," + current_lon;

                imageLoader.displayImage(url, myViewHolderactionfirst.iv_feed_list_map, options);

            } else if ("CAL".equals(feedListCard.getCardType())) {
                myViewHolderactionfirst.linear_feedacard_date_section.setVisibility(View.VISIBLE);
                myViewHolderactionfirst.img_feedcard_logo.setImageResource(R.drawable.calendar_icon);
                metadataValue = (LinkedTreeMap) feedListCard.getMetaData();
                String fromdate = String.valueOf(metadataValue.get("from"));
                String todate = String.valueOf(metadataValue.get("to"));
                String day = String.valueOf(metadataValue.get("days"));

                String newday = day.substring(0, day.length() - 2) + "";
                if (day.equals("1.0")) {

                    myViewHolderactionfirst.tv_feedcard_enddate.setVisibility(View.GONE);
                    myViewHolderactionfirst.tv_feedcard_endmonth.setVisibility(View.GONE);
                    myViewHolderactionfirst.img_feedcard_arrow.setVisibility(View.GONE);
                    String[] sepSingle = fromdate.split("-");
                    myViewHolderactionfirst.tv_feedcard_statdate.setText(sepSingle[2]);
                    myViewHolderactionfirst.tv_feedcard_daycount.setText(newday);

                    myViewHolderactionfirst.tv_feedcard_daytext.setText("Day");
                    String leaveTYpe = feedListCards.get(index).getApplicationType();

                    if (leaveTYpe.equals("EL")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("Earned Leave");
                    } else if (leaveTYpe.equals("CL")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("Casual Leave");
                    } else if (leaveTYpe.equals("SL")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("Medical Leave");
                    } else if (leaveTYpe.equals("PM")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("Present");
                    } else if (leaveTYpe.equals("AM")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("Attendance");
                    } else if (leaveTYpe.equals("LWP")) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText("LWP");
                    }
                    String yr = Character.toString(sepSingle[0].charAt(2)) + Character.toString(sepSingle[0].charAt(3));
                    myViewHolderactionfirst.tv_feedcard_startmonth.setText(getMonth(Integer.parseInt(sepSingle[1])) + "'" + yr);
                } else {
                    myViewHolderactionfirst.tv_feedcard_enddate.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.tv_feedcard_endmonth.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.img_feedcard_arrow.setVisibility(View.VISIBLE);
                    String[] sepStart = fromdate.split("-");
                    myViewHolderactionfirst.tv_feedcard_statdate.setText(sepStart[2]);

                    String[] sepEnd = todate.split("-");
                    myViewHolderactionfirst.tv_feedcard_enddate.setText(sepEnd[2]);
                    myViewHolderactionfirst.tv_feedcard_daycount.setText(newday);

                    myViewHolderactionfirst.tv_feedcard_daytext.setText("Days");

                    LeaveType leaveType = LeaveType.findByType(feedListCard.getApplicationType());

                    if (leaveType != null) {
                        myViewHolderactionfirst.tv_feedcard_leavecond.setText(leaveType.getValue());
                    }

                    String yrStr = Character.toString(sepStart[0].charAt(2)) + Character.toString(sepStart[0].charAt(3));
                    myViewHolderactionfirst.tv_feedcard_startmonth.setText(getMonth(Integer.parseInt(sepStart[1])) + "'" + yrStr);

                    String yrEnd = Character.toString(sepEnd[0].charAt(2)) + Character.toString(sepEnd[0].charAt(3));
                    myViewHolderactionfirst.tv_feedcard_endmonth.setText(getMonth(Integer.parseInt(sepEnd[1])) + "'" + yrEnd);
                }
            }
            if (feedListCard.getActionData() != null && feedListCard.getActionData().getIsReadonly() == true) {

                myViewHolderactionfirst.linear_feedcard_actionread_section.setVisibility(View.VISIBLE);
                myViewHolderactionfirst.linear_feedcard_acept_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_read_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_withdraw_section.setVisibility(View.GONE);
                myViewHolderactionfirst.linear_feedcard_map_section.setVisibility(View.GONE);

                myViewHolderactionfirst.tv_feedcard_actionread_time.setText(output);

                if (feedListCards.get(index).getActionData().getStatusUser().equals("") || feedListCard.getActionData().getStatusUser().getUserName() == null) {
                    myViewHolderactionfirst.tv_feedcard_actionread_name.setText(feedListCard.getActionData().getStatusLabel());
                    myViewHolderactionfirst.tv_feedcard_actionread_pic.setVisibility(View.INVISIBLE);
                } else {
                    myViewHolderactionfirst.tv_feedcard_actionread_pic.setVisibility(View.VISIBLE);

                    String nameString = "<b>" + feedListCards.get(index).getActionData().getStatusUser().getUserName() + "</b> " + feedListCard.getActionData().getStatusLabel();
                    myViewHolderactionfirst.tv_feedcard_actionread_name.setText(Html.fromHtml(nameString));

                    String test = feedListCard.getActionData().getStatusUser().getUserName();
                    char first = test.charAt(0);
                    drawable1 = TextDrawable.builder().round();
                    TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#FF006E"));
                    myViewHolderactionfirst.tv_feedcard_actionread_pic.setImageDrawable(drawable);
                }

                if (feedListCard.getActionData().getStatusType().equals(0)) {
                    myViewHolderactionfirst.img_feedcard_actionread_statustype.setImageResource(R.drawable.card_pending);
                } else if (feedListCard.getActionData().getStatusType().equals(1)) {
                    myViewHolderactionfirst.img_feedcard_actionread_statustype.setImageResource(R.drawable.card_check);
                } else {
                    myViewHolderactionfirst.img_feedcard_actionread_statustype.setImageResource(R.drawable.card_cross);
                }
            } else {
                int count = 0;
                if (feedListCard.getActionData() != null && feedListCard.getActionData().getActions() != null) {
                    count = feedListCard.getActionData().getActions().size();
                }
                if (count == 1) {
                    myViewHolderactionfirst.linear_feedcard_actionread_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_acept_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_read_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_withdraw_section.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.linear_feedcard_map_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.withdrawrtest.setText(feedListCard.getActionData().getStatusLabel());
                } else if (count == 2) {
                    myViewHolderactionfirst.linear_feedcard_actionread_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_acept_section.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.linear_feedcard_read_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_withdraw_section.setVisibility(View.GONE);
                    myViewHolderactionfirst.linear_feedcard_map_section.setVisibility(View.GONE);

                }
            }

            myViewHolderactionfirst.linear_feedcard_withdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int entityId = feedListCard.getApplicationId();
                    String entityType = feedListCard.getApplicationType();
                    // int actionId=feedListCards.get(index).getActionData().getActions().get(index).getActionType();
                    int actionId = 2;
                    String textWith = feedListCard.getActionData().getActions().get(0).getActionLabel();
                    if (counter == 0) {
                        counter = 1;
                        FeedListFragment.withdrawProcess(entityId, entityType, actionId, textWith);
                    } else {
                        counter = 0;
                        FeedListFragment.rl_withdraw_feed_list.setVisibility(View.GONE);
                    }
                }
            });

            myViewHolderactionfirst.linear_feedecard_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int entityId = feedListCard.getApplicationId();
                    String entityType = feedListCard.getApplicationType();
                    int actionId = 0;

                    FeedListFragment.rl_progress_feed_list.setVisibility(View.VISIBLE);
                    callOtherLeaveBalApi(entityId, entityType, actionId);

                }
            });

            myViewHolderactionfirst.linear_feedcard_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int entityId = feedListCard.getApplicationId();
                    String entityType = feedListCard.getApplicationType();
                    int actionId = 1;

                    FeedListFragment.rl_progress_feed_list.setVisibility(View.VISIBLE);
                    callOtherLeaveBalApi(entityId, entityType, actionId);
                }
            });

            myViewHolderactionfirst.img_feadcard_userdp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, OtherProfile.class);
                    intent.putExtra("USER_ID", feedListCard.getUserId());
                    context.startActivity(intent);
                }
            });
           /* myViewHolderactionfirst.linar_feed_switchprofile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(feedListCards.get(index).getActionData().getStatusUser().getUserName()!=null){
                        Intent intent = new Intent(context, OtherProfile.class);
                        intent.putExtra("USER_ID", feedListCards.get(index).getActionData().getStatusUser().getUserId());
                        context.startActivity(intent);
                    }else {

                    }
                }
            });*/
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        else if (isPositionFooter(position))
            return TYPE_FOOTER;
        else return TYPE_ITEM_readSingle;
        // return TYPE_ITEM_readSingle;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == feedListCards.size() + 1;
    }

    @Override
    public int getItemCount() {
        return feedListCards.size() + 2;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolderactionfirst extends RecyclerView.ViewHolder {

        TextView tv_feedcard_action, tv_feedcard_time, tv_feedcard_descrp,
                tv_feedcard_daycount, tv_feedcard_leavecond, tv_feedcard_statdate, tv_feedcard_enddate,
                tv_feedcard_startmonth, tv_feedcard_endmonth, withdrawrtest;

        LinearLayout linear_feedcard_acept_section, linear_feedcard_withdraw_section, linear_feedcard_read_section, linear_feedacard_date_section,
                linear_feedcard_actionread_section, linear_feedcard_map_section, linear_feedcard_withdraw, linear_feedcard_accept, linear_feedecard_reject;

        TextView tv_feedcard_actionread_name, tv_feedcard_daytext, tv_feedcard_actionread_time;

        ImageView img_feadcard_userdp, img_feedcard_arrow, img_feedcard_actionread_statustype, tv_feedcard_actionread_pic, iv_feed_list_map,
                img_feedcard_logo;

        public MyViewHolderactionfirst(View itemView) {
            super(itemView);

            img_feadcard_userdp = (ImageView) itemView.findViewById(R.id.img_feadcard_userdp);
            img_feedcard_arrow = (ImageView) itemView.findViewById(R.id.img_feedcard_arrow);
            iv_feed_list_map = (ImageView) itemView.findViewById(R.id.iv_feed_list_map);
            img_feedcard_logo = (ImageView) itemView.findViewById(R.id.img_feedcard_logo);

            tv_feedcard_action = (TextView) itemView.findViewById(R.id.tv_feedcard_action);
            tv_feedcard_time = (TextView) itemView.findViewById(R.id.tv_feedcard_time);
            tv_feedcard_descrp = (TextView) itemView.findViewById(R.id.tv_feedcard_descrp);

            tv_feedcard_daycount = (TextView) itemView.findViewById(R.id.tv_feedcard_daycount);
            tv_feedcard_leavecond = (TextView) itemView.findViewById(R.id.tv_feedcard_leavecond);
            tv_feedcard_daytext = (TextView) itemView.findViewById(R.id.tv_feedcard_daytext);

            tv_feedcard_statdate = (TextView) itemView.findViewById(R.id.tv_feedcard_statdate);
            tv_feedcard_enddate = (TextView) itemView.findViewById(R.id.tv_feedcard_enddate);
            tv_feedcard_startmonth = (TextView) itemView.findViewById(R.id.tv_feedcard_startmonth);
            tv_feedcard_endmonth = (TextView) itemView.findViewById(R.id.tv_feedcard_endmonth);
            tv_feedcard_actionread_time = (TextView) itemView.findViewById(R.id.tv_feedcard_actionread_time);

            tv_feedcard_actionread_name = (TextView) itemView.findViewById(R.id.tv_feedcard_actionread_name);
            img_feedcard_actionread_statustype = (ImageView) itemView.findViewById(R.id.img_feedcard_actionread_statustype);
            tv_feedcard_actionread_pic = (ImageView) itemView.findViewById(R.id.tv_feedcard_actionread_pic);

            withdrawrtest = (TextView) itemView.findViewById(R.id.withdrawrtest);

            linear_feedcard_map_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_map_section);
            linear_feedacard_date_section = (LinearLayout) itemView.findViewById(R.id.linear_feedacard_date_section);
            linear_feedecard_reject = (LinearLayout) itemView.findViewById(R.id.linear_feedecard_reject);
            linear_feedcard_accept = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_accept);
            linear_feedcard_withdraw = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_withdraw);
            linear_feedcard_acept_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_acept_section);
            linear_feedcard_withdraw_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_withdraw_section);
            linear_feedcard_read_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_read_section);
            linear_feedcard_actionread_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_actionread_section);
        }
    }

    public class MyViewHolderHearder extends RecyclerView.ViewHolder {

        TextView tv_cardheader_time;
        ImageView iv_feed_list_header_map;
        LinearLayout linear_cardhead_punchtab, liner_cardheader_timetab;
        LinearLayout lin1;

        public MyViewHolderHearder(View itemView) {
            super(itemView);
            iv_feed_list_header_map = (ImageView) itemView.findViewById(R.id.iv_feed_list_header_map);
            tv_cardheader_time = (TextView) itemView.findViewById(R.id.tv_cardheader_time);
            linear_cardhead_punchtab = (LinearLayout) itemView.findViewById(R.id.linear_cardhead_punchtab);
            liner_cardheader_timetab = (LinearLayout) itemView.findViewById(R.id.liner_cardheader_timetab);
            lin1 = (LinearLayout) itemView.findViewById(R.id.lin1);
        }
    }

    public class MyViewHolderFooter extends RecyclerView.ViewHolder {
        public MyViewHolderFooter(View itemView) {
            super(itemView);
        }
    }

    /**
     * method to get current time for punch in/out
     */
    String getCurrentTimeForHeader() {
        Date date = new Date();
        Calendar c = Calendar.getInstance();

        c.setTime(date);

        String current_time;

        if (String.valueOf(c.get(Calendar.MINUTE)).length() == 2) {
            if (String.valueOf(c.get(Calendar.HOUR)).equals("0")) {
                current_time = "12" + ":" + String.valueOf(c.get(Calendar.MINUTE));
            } else {
                current_time = String.valueOf(c.get(Calendar.HOUR)) + ":" + String.valueOf(c.get(Calendar.MINUTE));
            }
        } else {
            if (String.valueOf(c.get(Calendar.HOUR)).equals("0")) {
                current_time = "12:0" + String.valueOf(c.get(Calendar.MINUTE));
            } else {
                current_time = String.valueOf(c.get(Calendar.HOUR)) + ":0" + String.valueOf(c.get(Calendar.MINUTE));
            }
        }
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            return current_time + " am";
        } else {
            return current_time + " pm";
        }

    }


    public String getMonth(int month) {
        return new DateFormatSymbols().getShortMonths()[month - 1];
    }

    void callOtherLeaveBalApi(int entityId, String entityType, int actionId) {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(context, "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        // String endpoint_url = "/api/v1/userleaves/" + user_id;
        String endpoint = "/api/v1/action/" + entityType + "/" + entityId + "/" + actionId;
        RestApis restApis = retrofit.create(RestApis.class);
        Call<FeedListAcceptPojo> call = restApis.applyFeedAcept(endpoint, "bearer " + auth_token);
        call.enqueue(new Callback<FeedListAcceptPojo>() {
            @Override
            public void onResponse(Call<FeedListAcceptPojo> call, Response<FeedListAcceptPojo> response) {
                if (response.raw().code() == 200) {

                  /*  FeedList.rl_progress_feed_list.setVisibility(View.GONE);
                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120).setTileEnabled(true)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText((MainActivity)context, "Success\n"+response.body().getMsg(), style, FeedList.previous1);
             */
                    mAdapterCallback.onMethodCallback(response.body().getMsg());

                } else if (response.raw().code() == 401) {
                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                    //  FeedList.rl_withdraw_feed_list.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //    Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
                    FeedListFragment.newInstance().logout();
                } else {
                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText((MainActivity) context, name, style, FeedListFragment.previous1);


                    // Toast.makeText(context, name, Toast.LENGTH_LONG).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<FeedListAcceptPojo> call, Throwable t) {
                FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                String msg = t.getMessage();
                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    /*
     method to load profile pic
    */
    void initiateImageLoader() {
        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_pic)
                .showImageForEmptyUri(R.drawable.profile_pic)
                .showImageOnFail(R.drawable.profile_pic)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public String getDateInTimeZone(Date currentDate, String timeZoneId) {

        //String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";
        DateFormat targetFormat = new SimpleDateFormat("MMM dd, hh:mm a");
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);

        Date localDateTime = new Date(currentDate.getTime() + timeZone.getOffset(currentDate.getTime()));
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(localDateTime.getTime());
        if (timeZone.useDaylightTime()) {
            // time zone uses Daylight Saving
            cal.add(Calendar.MILLISECOND, timeZone.getDSTSavings() * -1);// in milliseconds
        }
        return targetFormat.format(cal.getTime());
    }

    public interface AdapterCallback {
        void onMethodCallback(String str);
    }
}
