package com.growthfile.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import simplifii.framework.rest.responses.UserProfile;
import com.growthfile.R;
import com.growthfile.ui.OthersCalendar;
import simplifii.framework.utility.Prefs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Administrator on 27-07-2016.
 */
public class OtherProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM_firt=1;
    private static final int TYPE_ITEM_second=2;
    Context context;
    UserProfile userProfilePojo;
    SelfProfileResponsePojo profilePojo;
    Prefs prefs;

    public OtherProfileAdapter(Context context, UserProfile userProfilePojo, SelfProfileResponsePojo profilePojo) {
         this.context=context;
        this.userProfilePojo=userProfilePojo;
        this.profilePojo=profilePojo;
        prefs= new Prefs();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_myprofile_topinflaterow, parent, false);
            return  new MyViewHolderHearder(v);
        }
        else if(viewType == TYPE_ITEM_firt)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_myprofile_secondinflaterow, parent, false);
            return new MyViewHolder(itemView);
        }
        else if(viewType == TYPE_ITEM_second)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_myprofile_inflaterow, parent, false);
            return new MyViewHolderlast(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof MyViewHolderHearder)
        {
            MyViewHolderHearder myViewHolderHearder = (MyViewHolderHearder)holder;

            myViewHolderHearder.tv_my_profile_name.setText(userProfilePojo.getName());
            myViewHolderHearder.tv_my_profile_contactno.setText(userProfilePojo.getMobile());
            myViewHolderHearder.tv_my_profile_mailid.setText(userProfilePojo.getEmail());
            myViewHolderHearder.tv_my_profile_deg.setText(userProfilePojo.getDesignation());
            myViewHolderHearder.tv_my_profile_reporting.setText("Reporting to " + profilePojo.getReportsTo().getUserName());
            myViewHolderHearder.tv_my_profile_location.setText(profilePojo.getCenter().getCode() + ", " + profilePojo.getCenter().getState());
            myViewHolderHearder.tv_my_profile_field.setText(profilePojo.getDepartment().getDepartmentName());
            myViewHolderHearder.tv_my_profile_company.setText(profilePojo.getOrg().getOrgName());



            myViewHolderHearder.tv_my_profile_el.setText(""+profilePojo.getLeaves().getEarnedLeaves());
            myViewHolderHearder.tv_my_profile_cl.setText(""+profilePojo.getLeaves().getCasualLeaves());
            myViewHolderHearder.tv_my_profile_sl.setText(""+profilePojo.getLeaves().getMedicalLeaves());




            String dt = userProfilePojo.getDateOfJoining();
            String[] seperate = dt.split("T");

            // the string representation of date (month/day/year)
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("dd MMM, yyyy");
            Date date = null;
            try {
                date = originalFormat.parse(seperate[0]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String formattedDate = targetFormat.format(date);
            myViewHolderHearder.tv_my_profile_joined.setText("Joined on " + formattedDate);

            myViewHolderHearder.linear_profile_cal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (profilePojo.getPermissions().getCanViewCalender()){
                        Intent intent = new Intent(context, OthersCalendar.class);
                        prefs.setPreferencesString(context,"Othr_USER_NAME",userProfilePojo.getName());
                        prefs.setPreferencesInt(context,"Othr_USER_ID",userProfilePojo.getId());
                        context.startActivity(intent);
                    }else {
                        Toast.makeText(context,"You don't have permission to view "+profilePojo.getProfile().getName()+"'s calendar",Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else if(holder instanceof MyViewHolder)
        {
            MyViewHolder myViewHolder = (MyViewHolder)holder;

            myViewHolder.tv_myprofile_ontime.setText(""+profilePojo.getGrowthfile().getOnTime());
            myViewHolder.tv_myprofile_attended.setText(""+profilePojo.getGrowthfile().getAttended());
            myViewHolder.tv_myprofile_total.setText(""+profilePojo.getGrowthfile().getTotal());
            myViewHolder.tv_myprofile_leaves.setText(""+profilePojo.getGrowthfile().getLeaves());
            myViewHolder.tv_myprofile_dayavg.setText(""+profilePojo.getGrowthfile().getAvgDaysTaken());

            myViewHolder.tv_myprofile_receivedby.setText(""+profilePojo.getSmiley().getReceived());
            myViewHolder.tv_myprofile_givenby.setText(""+profilePojo.getSmiley().getGiven());

        }else if(holder instanceof MyViewHolderlast)
        {


        }
    }

    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader(position))
            return TYPE_HEADER;
        if(isPositionFirst(position))
            return TYPE_ITEM_firt;
       return TYPE_ITEM_second;
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }

    private boolean isPositionFirst(int position)
    {
        return position == 1;
    }

    @Override
    public int getItemCount() {
        return 2+5;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_myprofile_ontime,tv_myprofile_attended,tv_myprofile_total,tv_myprofile_leaves,tv_myprofile_dayavg,
                tv_myprofile_givenby,tv_myprofile_receivedby;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_myprofile_ontime=(TextView)itemView.findViewById(R.id.tv_myprofile_ontime);
            tv_myprofile_attended=(TextView)itemView.findViewById(R.id.tv_myprofile_attended);
            tv_myprofile_total=(TextView)itemView.findViewById(R.id.tv_myprofile_total);
            tv_myprofile_leaves=(TextView)itemView.findViewById(R.id.tv_myprofile_leaves);
            tv_myprofile_dayavg=(TextView)itemView.findViewById(R.id.tv_myprofile_dayavg);
            tv_myprofile_givenby=(TextView)itemView.findViewById(R.id.tv_myprofile_givenby);
            tv_myprofile_receivedby=(TextView)itemView.findViewById(R.id.tv_myprofile_receivedby);
        }
    }


    public class MyViewHolderlast extends RecyclerView.ViewHolder {

        TextView tv_myprofile_row_comment,tv_myprofile_row_headname,tv_myprofile_row_headdeg,tv_myprofile_row_time,
                tv_myprofile_row_givername,tv_myprofile_row_giverdeg,tv_myprofile_row_othergiver;
        public MyViewHolderlast(View itemView) {
            super(itemView);

            tv_myprofile_row_comment=(TextView)itemView.findViewById(R.id.tv_myprofile_row_comment);
            tv_myprofile_row_headname=(TextView)itemView.findViewById(R.id.tv_myprofile_row_headname);
            tv_myprofile_row_headdeg=(TextView)itemView.findViewById(R.id.tv_myprofile_row_headdeg);
            tv_myprofile_row_time=(TextView)itemView.findViewById(R.id.tv_myprofile_row_time);
            tv_myprofile_row_givername=(TextView)itemView.findViewById(R.id.tv_myprofile_row_givername);
            tv_myprofile_row_giverdeg=(TextView)itemView.findViewById(R.id.tv_myprofile_row_giverdeg);
            tv_myprofile_row_othergiver=(TextView)itemView.findViewById(R.id.tv_myprofile_row_othergiver);
        }
    }

    public class MyViewHolderHearder extends RecyclerView.ViewHolder {

        LinearLayout linear_profile_cal;
        ImageView iv_my_profile_pic;
        TextView tv_my_profile_name,tv_my_profile_contactno,tv_my_profile_mailid,tv_my_profile_company,tv_my_profile_joined,
                tv_my_profile_deg,tv_my_profile_field,tv_my_profile_reporting,tv_my_profile_location,tv_my_profile_sl,tv_my_profile_cl,
                tv_my_profile_el;
      public MyViewHolderHearder(View itemView) {
            super(itemView);

          iv_my_profile_pic=(ImageView)itemView.findViewById(R.id.iv_my_profile_pic);

          linear_profile_cal=(LinearLayout) itemView.findViewById(R.id.linear_profile_cal);
          tv_my_profile_name=(TextView)itemView.findViewById(R.id.tv_my_profile_name);
          tv_my_profile_contactno=(TextView)itemView.findViewById(R.id.tv_my_profile_contactno);
          tv_my_profile_mailid=(TextView)itemView.findViewById(R.id.tv_my_profile_mailid);
          tv_my_profile_company=(TextView)itemView.findViewById(R.id.tv_my_profile_company);
          tv_my_profile_joined=(TextView)itemView.findViewById(R.id.tv_my_profile_joined);
          tv_my_profile_deg=(TextView)itemView.findViewById(R.id.tv_my_profile_deg);
          tv_my_profile_field=(TextView)itemView.findViewById(R.id.tv_my_profile_field);
          tv_my_profile_reporting=(TextView)itemView.findViewById(R.id.tv_my_profile_reporting);
          tv_my_profile_location=(TextView)itemView.findViewById(R.id.tv_my_profile_location);
          tv_my_profile_cl=(TextView)itemView.findViewById(R.id.tv_my_profile_cl);
          tv_my_profile_sl=(TextView)itemView.findViewById(R.id.tv_my_profile_sl);
          tv_my_profile_el=(TextView)itemView.findViewById(R.id.tv_my_profile_el);

        }
    }
}
