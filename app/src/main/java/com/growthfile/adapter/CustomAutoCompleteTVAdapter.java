package com.growthfile.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.CollectionsUtils;

/**
 * Created by aman on 23/12/16.
 */

public class CustomAutoCompleteTVAdapter<T> extends ArrayAdapter implements AdapterView.OnItemClickListener {

    private List<T> filteredList;
    private List<T> mainList;
    private CustomAutoCompleteAdapterInterface ref;

    public List<T> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<T> filteredList) {
        this.filteredList.clear();
        this.filteredList.addAll(filteredList);
    }

    public List<T> getMainList() {
        return mainList;
    }

    public CustomAutoCompleteTVAdapter(Context context, int mainResourceId, List<T> objectList, CustomAutoCompleteAdapterInterface ref) {
        super(context, mainResourceId);
        this.filteredList = new ArrayList<>(objectList);
        this.ref = ref;
        this.mainList = objectList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return ref.getView(position, convertView, parent,  filteredList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    @Nullable
    @Override
    public T getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }


    Filter filter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ref.convertResultToString(resultValue);
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            filteredList.clear();
            FilterResults filterResults = new FilterResults();
            if(charSequence==null||charSequence.length()==0){
                filteredList.clear();
                filteredList.addAll(mainList);
            } else {
                List<T> newFilteredList = ref.performFiltering(mainList, charSequence);
                if(CollectionsUtils.isNotEmpty(newFilteredList)){
                    filteredList.addAll(newFilteredList);
                }
            }
            filterResults.values = filteredList;
            filterResults.count = filteredList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            List<T> filteredPeople = (List<T>) filterResults.values;

            if(filterResults != null && filterResults.count > 0){
                for(T object : filteredPeople){
                    add(object);
                }
                notifyDataSetChanged();
            }else {
                notifyDataSetInvalidated();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ref.onItemClick(parent, view, position, id, filteredList);
    }

    public interface CustomAutoCompleteAdapterInterface{
        View getView(int position, View convertView, ViewGroup parent,  List filteredList);
        List performFiltering(List objectList, CharSequence charSequence);
        CharSequence convertResultToString(Object resultValue);
        void onItemClick(AdapterView<?> parent, View view, int position, long id, List filteredList);
    }
}
