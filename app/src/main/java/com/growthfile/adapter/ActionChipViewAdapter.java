package com.growthfile.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.util.AppUtil;
import com.plumillonforge.android.chipview.ChipViewAdapter;

/**
 * Created by robin on 11/28/16.
 */

public class ActionChipViewAdapter extends ChipViewAdapter {

    public ActionChipViewAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutRes(int position) {
        return R.layout.clickable_action_chip_v2;
    }

    @Override
    public int getBackgroundRes(int position) {
        return R.drawable.shape_send_button;
    }

    @Override
    public int getBackgroundColor(int position) {
        return R.color.white;
    }

    @Override
    public int getBackgroundColorSelected(int position) {
        return R.color.light_gray;
    }

    @Override
    public void onLayout(View view, int position) {

        String action = getChip(position).getText();
        TextView tvActionName = (TextView) view.findViewById(R.id.tv_feed_card_action);
        ImageView ivActionIcon = (ImageView) view.findViewById(R.id.iv_feed_card_action);
        ivActionIcon.setImageResource(AppUtil.getIconForActionType(action));
        tvActionName.setTextColor(AppUtil.getColorForActionType(getContext(), action));
        tvActionName.setText(action);
        LinearLayout layout = (LinearLayout) view;
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
//        if (!TextUtils.isEmpty(action.getImg())) {
//            ivActionIcon.setVisibility(View.VISIBLE);
//            Picasso.with(getContext()).load(action.getImg()).into(ivActionIcon);
//        } else {
//            ivActionIcon.setVisibility(View.GONE);
//        }
    }
}
