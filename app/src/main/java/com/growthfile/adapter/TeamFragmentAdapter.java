package com.growthfile.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import simplifii.framework.rest.responses.TeamResponseMangerPojo;
import simplifii.framework.rest.responses.TeamResponseTeamPojo;
import com.growthfile.R;
import com.growthfile.ui.OtherProfile;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Administrator on 27-07-2016.
 */
public class TeamFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM=1;
    Context context;
    TeamResponseMangerPojo mng;
    ArrayList<TeamResponseTeamPojo> arrayListteam;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private TextDrawable.IBuilder drawable1;

    public TeamFragmentAdapter(Context context,ArrayList<TeamResponseTeamPojo> arrayListteam,TeamResponseMangerPojo mng) {
         this.context=context;
        this.mng=mng;
        this.arrayListteam=arrayListteam;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_team_header, parent, false);
            return  new MyViewHolderHearder(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.frag_team_row, parent, false);
            return new MyViewHolder(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof MyViewHolderHearder)
        {
            MyViewHolderHearder holder1 = (MyViewHolderHearder)holder;
            /*if(arrayListteam.size()==0){
                holder1.tv_myteamtopheader.setText("Others");
            }else
            {
                holder1.tv_myteamtopheader.setText("Team Members");
            }*/
        }
        else if(holder instanceof MyViewHolder)
        {
            MyViewHolder myViewHolderactionfirst = (MyViewHolder)holder;

            if(arrayListteam.size()==0) {
               // int i=0;
                         if(position==1) {
                             myViewHolderactionfirst.liner_teammain.setVisibility(View.GONE);
                        myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.VISIBLE);
                        myViewHolderactionfirst.team_row_view.setVisibility(View.INVISIBLE);
                        myViewHolderactionfirst.liner_teamnext.setVisibility(View.VISIBLE);
                             //i=1;
                    }
                    if(position==2){
                        myViewHolderactionfirst.liner_teammain.setVisibility(View.VISIBLE);
                        myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.GONE);
                        myViewHolderactionfirst.team_row_view.setVisibility(View.INVISIBLE);
                        myViewHolderactionfirst.liner_teamnext.setVisibility(View.GONE);

                        myViewHolderactionfirst.tv_team_titlename.setText(mng.getName());
                        myViewHolderactionfirst.tv_team_phone.setText(mng.getMobile());
                        myViewHolderactionfirst.tv_team_postion.setText(mng.getDesignation());

                        if (mng.getAvatar().equals("")) {
                            String test = mng.getName();
                            char first = test.charAt(0);
                            drawable1 = TextDrawable.builder().round();
                            TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));
                            myViewHolderactionfirst.img_team_pic.setImageDrawable(drawable);

                        } else {
                            initiateImageLoader();
                            imageLoader.displayImage(mng.getAvatar(), myViewHolderactionfirst.img_team_pic, options);
                        }
                }
            }
            else if(position<=arrayListteam.size()){
                if(position==arrayListteam.size()){
                    myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.team_row_view.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.liner_teamnext.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.tv_team_titlename.setText(arrayListteam.get(position-1).getName());
                    myViewHolderactionfirst.tv_team_phone.setText(arrayListteam.get(position-1).getMobile());
                    myViewHolderactionfirst.tv_team_postion.setText(arrayListteam.get(position-1).getDesignation());
                    /*
                     * CHECK FOR NAME CARD AND PROFILE PIC
                     */
                    if(arrayListteam.get(position-1).getAvatar().equals("")){
                        String test =arrayListteam.get(position-1).getName();
                        char first = test.charAt(0);
                        drawable1 = TextDrawable.builder().round();
                        TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));
                        myViewHolderactionfirst.img_team_pic.setImageDrawable(drawable);

                    }else {
                        initiateImageLoader();
                        imageLoader.displayImage(arrayListteam.get(position - 1).getAvatar(), myViewHolderactionfirst.img_team_pic, options);
                    }
                }else {
                    myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.GONE);
                    myViewHolderactionfirst.team_row_view.setVisibility(View.VISIBLE);
                    myViewHolderactionfirst.liner_teamnext.setVisibility(View.GONE);
                    myViewHolderactionfirst.tv_team_titlename.setText(arrayListteam.get(position-1).getName());
                    myViewHolderactionfirst.tv_team_phone.setText(arrayListteam.get(position-1).getMobile());
                    myViewHolderactionfirst.tv_team_postion.setText(arrayListteam.get(position-1).getDesignation());

                     /*
                     * CHECK FOR NAME CARD AND PROFILE PIC
                     */
                    if(arrayListteam.get(position-1).getAvatar().equals("")){
                        String test =arrayListteam.get(position-1).getName();
                        char first = test.charAt(0);
                        drawable1 = TextDrawable.builder().round();
                        TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));
                        myViewHolderactionfirst.img_team_pic.setImageDrawable(drawable);

                    }else {
                        initiateImageLoader();
                        imageLoader.displayImage(arrayListteam.get(position - 1).getAvatar(), myViewHolderactionfirst.img_team_pic, options);
                    }
                }
                }else{

                    /*
                     * SET MANAGER DATA BELLOW TEAM LIST
                     */

                System.out.println("postion inside mng--"+position);
                if(mng.equals(null) || mng==  null || mng.equals("")){

                    myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.GONE);
                    myViewHolderactionfirst.team_row_view.setVisibility(View.INVISIBLE);
                    myViewHolderactionfirst.liner_teamnext.setVisibility(View.INVISIBLE);
                    myViewHolderactionfirst.tv_team_titlename.setText("");
                    myViewHolderactionfirst.tv_team_phone.setText("");
                    myViewHolderactionfirst.tv_team_postion.setText("");

                }else
                {

                    if(position==arrayListteam.size()+1) {
                        myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.GONE);
                        myViewHolderactionfirst.team_row_view.setVisibility(View.INVISIBLE);
                        myViewHolderactionfirst.liner_teamnext.setVisibility(View.INVISIBLE);
                        myViewHolderactionfirst.tv_team_titlename.setText(mng.getName());
                        myViewHolderactionfirst.tv_team_phone.setText(mng.getMobile());
                        myViewHolderactionfirst.tv_team_postion.setText(mng.getDesignation());

 /*
                     * CHECK FOR NAME CARD AND PROFILE PIC
                     */
                        if (mng.getAvatar().equals("")) {
                            String test = mng.getName();
                            char first = test.charAt(0);
                            drawable1 = TextDrawable.builder().round();
                            TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));
                            myViewHolderactionfirst.img_team_pic.setImageDrawable(drawable);

                        } else {
                            initiateImageLoader();
                            imageLoader.displayImage(mng.getAvatar(), myViewHolderactionfirst.img_team_pic, options);

                        }
                    }else{
                        myViewHolderactionfirst.liner_teammain.setVisibility(View.GONE);
                        myViewHolderactionfirst.linear_belowteamrow.setVisibility(View.GONE);
                        myViewHolderactionfirst.team_row_view.setVisibility(View.GONE);
                        myViewHolderactionfirst.liner_teamnext.setVisibility(View.GONE);
                    }
                }
            }
            myViewHolderactionfirst.img_team_star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(position<=arrayListteam.size()){
                        Intent intent=new Intent(context,OtherProfile.class);
                        intent.putExtra("USER_ID",arrayListteam.get(position-1).getId());
                        context.startActivity(intent);
                    }else {
                        Intent intent=new Intent(context,OtherProfile.class);
                        intent.putExtra("USER_ID",mng.getId());
                        context.startActivity(intent);
                    }
                }
            });

           /* myViewHolderactionfirst.img_team_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mob= "tel:91"+arrayListteam.get(position-1).getMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setDepartmentList(Uri.parse(mob));
                    context.startActivity(intent);
                }
            });*/
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader(position))
            return TYPE_HEADER;
       return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        return arrayListteam.size()+3;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linear_belowteamrow,liner_teamnext,liner_teammain;
        View team_row_view;
        RelativeLayout rel_teamdetailrow;
        ImageView img_team_pic,img_team_add,img_team_delete,img_team_call,img_team_star;
        TextView tv_team_titlename,tv_team_phone,tv_team_postion;
        public MyViewHolder(View itemView) {
            super(itemView);

            linear_belowteamrow=(LinearLayout)itemView.findViewById(R.id.linear_belowteamrow);
            liner_teammain=(LinearLayout)  itemView.findViewById(R.id.liner_teammain);
            liner_teamnext=(LinearLayout) itemView.findViewById(R.id.liner_teamnext);
            team_row_view=(View) itemView.findViewById(R.id.team_row_view);
            rel_teamdetailrow=(RelativeLayout)itemView.findViewById(R.id.rel_teamdetailrow);
            tv_team_titlename=(TextView)itemView.findViewById(R.id.tv_team_titlename);
            img_team_pic=(ImageView)itemView.findViewById(R.id.img_team_pic);
            tv_team_phone=(TextView)itemView.findViewById(R.id.tv_team_phone);
            tv_team_postion=(TextView)itemView.findViewById(R.id.tv_team_postion);
            img_team_add=(ImageView) itemView.findViewById(R.id.img_team_add);
            img_team_delete=(ImageView)itemView.findViewById(R.id.img_team_delete);
            img_team_call=(ImageView) itemView.findViewById(R.id.img_team_call);
            img_team_star=(ImageView) itemView.findViewById(R.id.img_team_star);
        }
    }

    public class MyViewHolderHearder extends RecyclerView.ViewHolder {
        TextView tv_myteamtopheader;
      public MyViewHolderHearder(View itemView) {
            super(itemView);
          tv_myteamtopheader=(TextView) itemView.findViewById(R.id.tv_myteamtopheader);
        }
    }


    /*
      method to load profile pic
     */
    void initiateImageLoader(){
        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_pic)
                .showImageForEmptyUri(R.drawable.profile_pic)
                .showImageOnFail(R.drawable.profile_pic)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
}
