package com.growthfile.data;

import java.io.Serializable;

/**
 * Created by kundan on 9/5/2016.
 */
public class PunchLogData implements Serializable {

    public double current_lat,current_lon;
    public String timestamp;
    public int sync_status,is_attendence;

}
