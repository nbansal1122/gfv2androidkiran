package com.growthfile.enums;

/**
 * Created by robin on 11/15/16.
 */

public enum LeaveType {
    EARNED_LEAVE("EL","Earned Leave"),
    CASUAL_LEAVE("CL","Casual Leave"),
    SICK_LEAVE("SL","Medical Leave"),
    PRESENT("PM","Mark Present"),
    ATTENDANCE("AM","Attendance"),
    LWP("LWP","LWP"),
    LEAVE("LV","Apply Leave");

    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    private LeaveType(String type, String value){
        this.type = type;
        this.value = value;
    }

    public static LeaveType findByType(String type) {
        for(LeaveType leaveType : LeaveType.values()){
            if(leaveType.type.equals(type)){
                return leaveType;
            }
        }
        return null;
    }
}
