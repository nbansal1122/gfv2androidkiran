package com.growthfile.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.model.CreateRoleModel;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public class CreateRoleHolder extends BaseRecyclerHolder implements View.OnClickListener {

    private TextView tvTitle;
    private ImageView iv_add, iv_delete;
    private LinearLayout llHeader;
    private CreateRoleModel createRoleModel;
    private int viewPosition;

    public CreateRoleHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) findView(R.id.tv_role);
        iv_add = (ImageView) findView(R.id.iv_map_role);
        iv_delete = (ImageView) findView(R.id.iv_delete_role);

        llHeader = (LinearLayout) findView(R.id.ll_team_members_header);
    }


    @Override
    public void onBindData(final int position, BaseRecyclerModel baseRecyclerModel) {
        this.model = baseRecyclerModel;
        createRoleModel = (CreateRoleModel) baseRecyclerModel;

        if(createRoleModel.isShowListHeader()){
            llHeader.setVisibility(View.VISIBLE);
        } else {
            llHeader.setVisibility(View.GONE);
        }

        setTextOrGone(tvTitle, createRoleModel.getDesignation());
        viewPosition = position;
        iv_add.setOnClickListener(this);
        iv_delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_team_add:
                sendClickEvent(AppConstants.ACTION_TYPE.ADD_TEAM_MEMBER);
                break;
            case R.id.img_team_delete:

                break;
        }
    }



}
