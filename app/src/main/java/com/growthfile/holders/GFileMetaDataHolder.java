package com.growthfile.holders;

import android.view.View;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;

import simplifii.framework.model.BaseRecyclerModel;

/**
 * Created by nbansal2211 on 20/11/16.
 */

public class GFileMetaDataHolder extends BaseRecyclerHolder {
    TextView tv_myprofile_ontime, tv_myprofile_attended, tv_myprofile_total, tv_myprofile_leaves, tv_myprofile_dayavg,
            tv_myprofile_givenby, tv_myprofile_receivedby;

    public GFileMetaDataHolder(View itemView) {
        super(itemView);
        tv_myprofile_ontime = (TextView) itemView.findViewById(R.id.tv_myprofile_ontime);
        tv_myprofile_attended = (TextView) itemView.findViewById(R.id.tv_myprofile_attended);
        tv_myprofile_total = (TextView) itemView.findViewById(R.id.tv_myprofile_total);
        tv_myprofile_leaves = (TextView) itemView.findViewById(R.id.tv_myprofile_leaves);
        tv_myprofile_dayavg = (TextView) itemView.findViewById(R.id.tv_myprofile_dayavg);
        tv_myprofile_givenby = (TextView) itemView.findViewById(R.id.tv_myprofile_givenby);
        tv_myprofile_receivedby = (TextView) itemView.findViewById(R.id.tv_myprofile_receivedby);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        GFileMetaDataHolder myViewHolder = this;
        SelfProfileResponsePojo profilePojo = (SelfProfileResponsePojo) baseRecyclerModel;

        myViewHolder.tv_myprofile_ontime.setText("" + profilePojo.getGrowthfile().getOnTime());
        myViewHolder.tv_myprofile_attended.setText("" + profilePojo.getGrowthfile().getAttended());
        myViewHolder.tv_myprofile_total.setText("" + profilePojo.getGrowthfile().getTotal());
        myViewHolder.tv_myprofile_leaves.setText("" + profilePojo.getGrowthfile().getLeaves());
        myViewHolder.tv_myprofile_dayavg.setText("" + profilePojo.getGrowthfile().getAvgDaysTaken());

        myViewHolder.tv_myprofile_receivedby.setText("" + profilePojo.getSmiley().getReceived());
        myViewHolder.tv_myprofile_givenby.setText("" + profilePojo.getSmiley().getGiven());

    }
}
