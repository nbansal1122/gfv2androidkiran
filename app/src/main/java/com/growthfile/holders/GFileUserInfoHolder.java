package com.growthfile.holders;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;

import simplifii.framework.rest.responses.UserProfile;

import com.growthfile.R;

import simplifii.framework.model.BaseRecyclerModel;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 20/11/16.
 */

public class GFileUserInfoHolder extends BaseRecyclerHolder {

    LinearLayout linear_profile_cal, llGiveSmiley, llGiveStar;
    ImageView iv_my_profile_pic;
    TextView tv_my_profile_name, tv_my_profile_contactno, tv_my_profile_mailid, tv_my_profile_company, tv_my_profile_joined,
            tv_my_profile_deg, tv_my_profile_field, tv_my_profile_reporting, tv_my_profile_location, tv_my_profile_cl, tv_my_profile_sl,
            tv_my_profile_el;


    TextView tv_myprofile_ontime, tv_myprofile_attended, tv_myprofile_total, tv_myprofile_leaves, tv_myprofile_dayavg,
            tv_myprofile_givenby, tv_myprofile_receivedby;

    TextView tvInitials;


    public GFileUserInfoHolder(View itemView) {
        super(itemView);
        iv_my_profile_pic = (ImageView) itemView.findViewById(R.id.iv_my_profile_pic);
        tvInitials = (TextView) itemView.findViewById(R.id.tv_initials);

        linear_profile_cal = (LinearLayout) itemView.findViewById(R.id.linear_profile_cal);

        llGiveSmiley = (LinearLayout) itemView.findViewById(R.id.ll_give_smiley);
        llGiveStar = (LinearLayout) itemView.findViewById(R.id.ll_give_star);


        tv_my_profile_name = (TextView) itemView.findViewById(R.id.tv_my_profile_name);
        tv_my_profile_contactno = (TextView) itemView.findViewById(R.id.tv_my_profile_contactno);
        tv_my_profile_mailid = (TextView) itemView.findViewById(R.id.tv_my_profile_mailid);
        tv_my_profile_company = (TextView) itemView.findViewById(R.id.tv_my_profile_company);
        tv_my_profile_joined = (TextView) itemView.findViewById(R.id.tv_my_profile_joined);
        tv_my_profile_deg = (TextView) itemView.findViewById(R.id.tv_my_profile_deg);
        tv_my_profile_field = (TextView) itemView.findViewById(R.id.tv_my_profile_field);
        tv_my_profile_reporting = (TextView) itemView.findViewById(R.id.tv_my_profile_reporting);
        tv_my_profile_location = (TextView) itemView.findViewById(R.id.tv_my_profile_location);
        tv_my_profile_cl = (TextView) itemView.findViewById(R.id.tv_my_profile_cl);
        tv_my_profile_sl = (TextView) itemView.findViewById(R.id.tv_my_profile_sl);
        tv_my_profile_el = (TextView) itemView.findViewById(R.id.tv_my_profile_el);


        // Meta Data
        tv_myprofile_ontime = (TextView) itemView.findViewById(R.id.tv_myprofile_ontime);
        tv_myprofile_attended = (TextView) itemView.findViewById(R.id.tv_myprofile_attended);
        tv_myprofile_total = (TextView) itemView.findViewById(R.id.tv_myprofile_total);
        tv_myprofile_leaves = (TextView) itemView.findViewById(R.id.tv_myprofile_leaves);
        tv_myprofile_dayavg = (TextView) itemView.findViewById(R.id.tv_myprofile_dayavg);
        tv_myprofile_givenby = (TextView) itemView.findViewById(R.id.tv_myprofile_givenby);
        tv_myprofile_receivedby = (TextView) itemView.findViewById(R.id.tv_myprofile_receivedby);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        final GFileUserInfoHolder myViewHolderHearder = this;
        this.model = baseRecyclerModel;
        final SelfProfileResponsePojo profilePojo = (SelfProfileResponsePojo) baseRecyclerModel;
        final UserProfile userProfilePojo = profilePojo.getProfile();

        fillMetaInfoOfUser(profilePojo, this);
        boolean canGiveSmiley = false;
        boolean canGiveStar = false;
        boolean canViewCalendar = false;

        if (profilePojo != null && profilePojo.getPermissions() != null) {
            canGiveSmiley = profilePojo.getPermissions().getCanGiveSmiley() == null ? canGiveSmiley : profilePojo.getPermissions().getCanGiveSmiley();
            canGiveStar = profilePojo.getPermissions().getCanGiveStar() == null ? canGiveStar : profilePojo.getPermissions().getCanGiveStar();
            canViewCalendar = profilePojo.getPermissions().getCanViewCalender() == null ? canViewCalendar : profilePojo.getPermissions().getCanViewCalender();
        }


        if (!canGiveSmiley) {
            ImageView ivSmiley = (ImageView) itemView.findViewById(R.id.iv_give_smiley);
            ivSmiley.setImageResource(R.drawable.disable_smiley);
        }
        if (!canGiveStar) {
            ImageView ivStar = (ImageView) itemView.findViewById(R.id.iv_give_star);
            ivStar.setImageResource(R.drawable.disable_star);
        }
        if (!canViewCalendar) {
            ImageView ivCalendar = (ImageView) itemView.findViewById(R.id.iv_view_calendar);
            ivCalendar.setImageResource(R.drawable.disable_calendar);
        }


        myViewHolderHearder.tv_my_profile_name.setText(userProfilePojo.getName());
        myViewHolderHearder.tv_my_profile_contactno.setText(userProfilePojo.getMobile());
        myViewHolderHearder.tv_my_profile_mailid.setText(userProfilePojo.getEmail());
        myViewHolderHearder.tv_my_profile_deg.setText(userProfilePojo.getDesignation());
        myViewHolderHearder.tv_my_profile_reporting.setText("" + profilePojo.getReportsTo().getUserName());
        myViewHolderHearder.tv_my_profile_location.setText(profilePojo.getCenter().getCode() + ", " + profilePojo.getCenter().getState());
        myViewHolderHearder.tv_my_profile_field.setText(profilePojo.getDepartment().getDepartmentName());
        myViewHolderHearder.tv_my_profile_company.setText(profilePojo.getOrg().getOrgName());

        myViewHolderHearder.tv_my_profile_el.setText("" + profilePojo.getLeaves().getEarnedLeaves());
        myViewHolderHearder.tv_my_profile_cl.setText("" + profilePojo.getLeaves().getCasualLeaves());
        myViewHolderHearder.tv_my_profile_sl.setText("" + profilePojo.getLeaves().getMedicalLeaves());

        if (!TextUtils.isEmpty(userProfilePojo.getAvatar())) {
            Picasso.with(context).load(userProfilePojo.getAvatar()).into(myViewHolderHearder.iv_my_profile_pic, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    String initials = Util.getInitialsFromName(userProfilePojo.getName());
                    tvInitials.setText(initials);
                    tvInitials.setVisibility(View.VISIBLE);
                    myViewHolderHearder.iv_my_profile_pic.setVisibility(View.GONE);
                }
            });
            myViewHolderHearder.iv_my_profile_pic.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        } else {
            String initials = Util.getInitialsFromName(userProfilePojo.getName());
            tvInitials.setText(initials);
            tvInitials.setVisibility(View.VISIBLE);
            myViewHolderHearder.iv_my_profile_pic.setVisibility(View.GONE);
        }

        String dt = userProfilePojo.getDateOfJoining();
        String[] seperate = dt.split("T");

        // the string representation of date (month/day/year)
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd MMM, yyyy");
        Date date = null;
        try {
            date = originalFormat.parse(seperate[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        myViewHolderHearder.tv_my_profile_joined.setText("Joined on " + formattedDate);

        if (canViewCalendar) {
            myViewHolderHearder.linear_profile_cal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendClickEvent(AppConstants.ACTION_TYPE.CAL_CLICKED);
                }
            });
        }

        if (canGiveSmiley) {
            myViewHolderHearder.llGiveSmiley.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, userProfilePojo.getId());
                    bundle.putString(AppConstants.BUNDLE_KEYS.USER_NAME, userProfilePojo.getName());
                    sendClickEvent(AppConstants.ACTION_TYPE.GIVE_SMILEY, bundle);
                }
            });
        }

        if (canGiveStar) {
            myViewHolderHearder.llGiveStar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, userProfilePojo.getId());
                    bundle.putString(AppConstants.BUNDLE_KEYS.USER_NAME, userProfilePojo.getName());
                    sendClickEvent(AppConstants.ACTION_TYPE.GIVE_STAR, bundle);
                }
            });
        }

    }

    private void fillMetaInfoOfUser(SelfProfileResponsePojo profilePojo, GFileUserInfoHolder myViewHolder) {
        myViewHolder.tv_myprofile_ontime.setText("" + profilePojo.getGrowthfile().getOnTime());
        myViewHolder.tv_myprofile_attended.setText("" + profilePojo.getGrowthfile().getAttended());
        myViewHolder.tv_myprofile_total.setText("" + profilePojo.getGrowthfile().getTotal());
        myViewHolder.tv_myprofile_leaves.setText("" + profilePojo.getGrowthfile().getLeaves());
        myViewHolder.tv_myprofile_dayavg.setText("" + profilePojo.getGrowthfile().getAvgDaysTaken());

        myViewHolder.tv_myprofile_receivedby.setText("" + profilePojo.getSmiley().getReceived());
        myViewHolder.tv_myprofile_givenby.setText("" + profilePojo.getSmiley().getGiven());
    }

}
