package com.growthfile.holders;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;

import simplifii.framework.model.BaseRecyclerModel;

import com.growthfile.adapter.ActionChipViewAdapter;
import com.growthfile.model.feed.Action;
import com.growthfile.model.feed.Calendar;
import com.growthfile.model.feed.Card;

import simplifii.framework.rest.responses.FeedCardResponse;
import simplifii.framework.rest.responses.Initiator;

import com.growthfile.util.FeedAction;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 20/11/16.
 */
public class FeedHolder extends BaseRecyclerHolder {
    TextView tvFeedcardPrimaryTitle, tvFeedCardTimestamp, tvFeedCardDescription,
            tvFeedCardDayCount, tvFeedCardSecondaryTitle, tvFeedCardStatDate, tvFeedCardEndDate,
            tvFeedCardStartMonth, tvFeedCardEndMonth, withdrawrtest;

    LinearLayout calendarLayout, linear_feedcard_acept_section, linear_feedcard_withdraw_section, linear_feedcard_read_section, linear_feedacard_date_section,
            linear_feedcard_actionread_section, linear_feedcard_map_section, linear_feedcard_withdraw, linear_feedcard_accept, linear_feedecard_reject;

    TextView tv_feedcard_actionread_name, tvFeedCardDaysLiteral, tv_feedcard_actionread_time;

    ImageView imgFeadcardUserdp, fromDateToDateArrow, img_feedcard_actionread_statustype, tv_feedcard_actionread_pic, ivFeedListMap,
            imgFeedCardLogo;

    TextView tvUserInitials;

    View lineAvatarToCardIcon;//Make it gone if card icon is empty

    //Clickable Actions
    LinearLayout llViewClickableActions;
    ChipView actionChipView;

    //Readonly actions
    LinearLayout llViewReadOnlyActions;

    public FeedHolder(final View itemView) {
        super(itemView);

        imgFeadcardUserdp = (ImageView) itemView.findViewById(R.id.img_feadcard_userdp);
        tvUserInitials = (TextView) itemView.findViewById(R.id.tv_initials);
        tvUserInitials.setVisibility(View.GONE);

        ivFeedListMap = (ImageView) itemView.findViewById(R.id.iv_feed_list_map);

        imgFeedCardLogo = (ImageView) itemView.findViewById(R.id.img_feedcard_logo);
        lineAvatarToCardIcon = itemView.findViewById(R.id.line_avatar_to_card_icon);

        tvFeedcardPrimaryTitle = (TextView) itemView.findViewById(R.id.tv_card_primary_title);
        tvFeedCardTimestamp = (TextView) itemView.findViewById(R.id.tv_card_timestamp);

        tvFeedCardDescription = (TextView) itemView.findViewById(R.id.tv_card_description);
        tvFeedCardSecondaryTitle = (TextView) itemView.findViewById(R.id.tv_card_secondary_title);

        calendarLayout = (LinearLayout) itemView.findViewById(R.id.layout_calendar);
        tvFeedCardDayCount = (TextView) itemView.findViewById(R.id.tv_feedcard_daycount);//1
        tvFeedCardDaysLiteral = (TextView) itemView.findViewById(R.id.tv_feedcard_daytext);//Day
        fromDateToDateArrow = (ImageView) itemView.findViewById(R.id.img_from_to_arrow);//-->

        tvFeedCardStatDate = (TextView) itemView.findViewById(R.id.tv_feedcard_statdate);//11
        tvFeedCardEndDate = (TextView) itemView.findViewById(R.id.tv_feedcard_enddate);//12
        tvFeedCardStartMonth = (TextView) itemView.findViewById(R.id.tv_feedcard_startmonth);//Aug
        tvFeedCardEndMonth = (TextView) itemView.findViewById(R.id.tv_feedcard_endmonth);//Sep


        llViewClickableActions = (LinearLayout) findView(R.id.view_clickable_actions);
        actionChipView = (ChipView) findView(R.id.chip_actions);

        llViewReadOnlyActions = (LinearLayout) findView(R.id.view_read_only_actions);
//        tv_feedcard_actionread_time = (TextView) itemView.findViewById(R.id.tv_feedcard_actionread_time);
//
//        tv_feedcard_actionread_name = (TextView) itemView.findViewById(R.id.tv_feedcard_actionread_name);
//        img_feedcard_actionread_statustype = (ImageView) itemView.findViewById(R.id.img_feedcard_actionread_statustype);
//        tv_feedcard_actionread_pic = (ImageView) itemView.findViewById(R.id.tv_feedcard_actionread_pic);
//
//        withdrawrtest = (TextView) itemView.findViewById(R.id.withdrawrtest);
//
//        linear_feedcard_map_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_map_section);
//        linear_feedacard_date_section = (LinearLayout) itemView.findViewById(R.id.linear_feedacard_date_section);
//        linear_feedecard_reject = (LinearLayout) itemView.findViewById(R.id.linear_feedecard_reject);
//        linear_feedcard_accept = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_accept);
//        linear_feedcard_withdraw = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_withdraw);
//        linear_feedcard_acept_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_acept_section);
//        linear_feedcard_withdraw_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_withdraw_section);
//        linear_feedcard_read_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_read_section);
//        linear_feedcard_actionread_section = (LinearLayout) itemView.findViewById(R.id.linear_feedcard_actionread_section);

    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        this.model = baseRecyclerModel;
        Card card = (Card) baseRecyclerModel;
        setCardData(card);
        setInitiator(card.getInitiator());
        setCalendar(card.getCalendar());
        setClickableActions(card.getActions());
        setReadOnlyActions(card.getResponses());
    }

    private void setCardData(Card card) {
        setTextOrGone(tvFeedcardPrimaryTitle, card.getCardTitle());
        setTextOrGone(tvFeedCardDescription, card.getDescription());
        setTextOrGone(tvFeedCardSecondaryTitle, card.getTitle());
        if (TextUtils.isEmpty(card.getTimestamp())) {
            tvFeedCardTimestamp.setVisibility(View.GONE);
        } else {
            String time = Util.getGFileDate(card.getTimestamp());
            setTextOrGone(tvFeedCardTimestamp, time);
        }

        if (!TextUtils.isEmpty(card.getCardIcon())) {
            lineAvatarToCardIcon.setVisibility(View.VISIBLE);
            Picasso.with(context).load(card.getCardIcon()).into(imgFeedCardLogo);
        } else {
//            imgFeedCardLogo.setVisibility(View.INVISIBLE);
            imgFeedCardLogo.setVisibility(View.GONE);
            lineAvatarToCardIcon.setVisibility(View.GONE);
        }


    }

    private void setCalendar(Calendar calendar) {
        if (calendar != null && calendar.getDays() != null) {

            setTextOrGone(tvFeedCardDayCount, calendar.getDays() + "");
            String startDay = Util.convertDateFormat(calendar.getFrom(), Util.GFILE_DATE_PATTERN, Util.GFILE_DAY_PATTERN);
            String startMonthYr = Util.convertDateFormat(calendar.getFrom(), Util.GFILE_DATE_PATTERN, Util.GFILE_MONTH_YEAR_PATTERN);
            String endDay = Util.convertDateFormat(calendar.getTo(), Util.GFILE_DATE_PATTERN, Util.GFILE_DAY_PATTERN);
            String endMonthYr = Util.convertDateFormat(calendar.getTo(), Util.GFILE_DATE_PATTERN, Util.GFILE_MONTH_YEAR_PATTERN);

            setTextOrGone(tvFeedCardStatDate, startDay);
            setTextOrGone(tvFeedCardStartMonth, startMonthYr);

            if (1 == calendar.getDays()) {
                tvFeedCardDaysLiteral.setText("Day");//Change it from Days to Day
                fromDateToDateArrow.setVisibility(View.GONE);
                tvFeedCardEndDate.setVisibility(View.GONE);
                tvFeedCardEndMonth.setVisibility(View.GONE);
            } else {
                tvFeedCardDaysLiteral.setText("Days");//Change it back to days
                fromDateToDateArrow.setVisibility(View.VISIBLE);
                setTextOrGone(tvFeedCardEndDate, endDay);
                setTextOrGone(tvFeedCardEndMonth, endMonthYr);
            }

        } else {
            calendarLayout.setVisibility(View.GONE);
        }
    }

    private void setClickableActions(List<Action> actions) {
        if (CollectionsUtils.isNotEmpty(actions)) {
            llViewClickableActions.setVisibility(View.VISIBLE);
            List<Chip> chips = new ArrayList<>();
            for (Action action : actions) {
                chips.add(action);
            }

            actionChipView.removeAllViewsInLayout();

            ActionChipViewAdapter actionChipViewAdapter = new ActionChipViewAdapter(context);
            actionChipViewAdapter.setChipList(chips);
            actionChipView.setAdapter(actionChipViewAdapter);

            actionChipView.setOnChipClickListener(new OnChipClickListener() {
                @Override
                public void onChipClick(Chip chip) {
                    Action action = (Action) chip;
                    onActionClicked(action);
                }
            });
        } else {
            llViewClickableActions.setVisibility(View.GONE);
        }
    }

    private void setReadOnlyActions(List<FeedCardResponse> feedCardResponses) {
        if (CollectionsUtils.isNotEmpty(feedCardResponses)) {
            llViewReadOnlyActions.setVisibility(View.VISIBLE);
            llViewReadOnlyActions.removeAllViews();
            for (final FeedCardResponse response : feedCardResponses) {
                View view = layoutInflater.inflate(R.layout.view_card_header, null, false);

                ImageView ivInitiatorLogo = (ImageView) view.findViewById(R.id.img_feadcard_userdp);
                TextView tvInitiatorInitials = (TextView) view.findViewById(R.id.tv_initials);

                String imageUrl = null;
                String initiatorName = null;
                if (response.getInitiator() != null) {
                    imageUrl = response.getInitiator().getAvatar();
                    initiatorName = response.getInitiator().getName();
                    View.OnClickListener onClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (clickListener != null) {
                                clickListener.onItemClicked(getAdapterPosition(), itemView, response.getInitiator().getId(), AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE);
                            }
                        }
                    };
                    ivInitiatorLogo.setOnClickListener(onClickListener);
                    tvInitiatorInitials.setOnClickListener(onClickListener);
                }
                setImageOrSetInitials(context, imageUrl, initiatorName, ivInitiatorLogo, tvInitiatorInitials);

                TextView tvActionTitle = (TextView) view.findViewById(R.id.tv_card_primary_title);
                TextView tvActionTimestamp = (TextView) view.findViewById(R.id.tv_card_timestamp);

                setTextOrGone(tvActionTitle, response.getCardTitle());

                String time = Util.convertDateFormat(response.getTimestamp(), Util.DISCVER_SERVER_DATE_PATTERN, Util.GFILE_UI_PATTERN);
                time = Util.getGFileDate(response.getTimestamp());
                setTextOrGone(tvActionTimestamp, time);

                ImageView ivActionLogo = (ImageView) view.findViewById(R.id.img_feedcard_logo);
                if (TextUtils.isEmpty(response.getCardIcon())) {
                    ivActionLogo.setVisibility(View.GONE);
                } else {
                    Picasso.with(context).load(response.getCardIcon()).into(ivActionLogo);
                    ivActionLogo.setVisibility(View.VISIBLE);
                }
                llViewReadOnlyActions.addView(view);
            }
        } else {
            llViewReadOnlyActions.setVisibility(View.GONE);
        }
    }

    private void onActionClicked(Action action) {
        Log.d("FeedHolder", "action :" + action.getType() + "," + action.getText());
        FeedAction feedAction = new FeedAction(action, (com.growthfile.model.feed.Card) model);

        if (clickListener != null) {
            clickListener.onItemClicked(getAdapterPosition(), itemView, feedAction, AppConstants.ACTION_TYPE.ACTION_FEED);
        }
    }

    private void setInitiator(final Initiator initiator) {
        if (initiator != null) {
            setImageOrSetInitials(context, initiator.getAvatar(), initiator.getName(), imgFeadcardUserdp, tvUserInitials);
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onItemClicked(getAdapterPosition(), itemView, initiator.getId(), AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE);
                    }
                }
            };
            imgFeadcardUserdp.setOnClickListener(onClickListener);
            tvUserInitials.setOnClickListener(onClickListener);
        }
    }

    private void setImageOrSetInitials(final Context context, String imageUrl, final String name, final ImageView imageView, final TextView tvInitials) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    setImageOrSetInitials(context, null, name, imageView, tvInitials);
                }
            });
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(name)) {
            tvInitials.setText(Util.getInitialsFromName(name));
            imageView.setVisibility(View.GONE);
            tvInitials.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.drawable.unknown);
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        }
    }

}
