package com.growthfile.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.util.FusedLocationService;
import com.squareup.picasso.Picasso;

import java.util.Date;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;


/**
 * Created by nbansal2211 on 02/12/16.
 */

public class MapHeaderHolder extends BaseRecyclerHolder {
    TextView tv_cardheader_time;
    ImageView iv_feed_list_header_map;
    LinearLayout linear_cardhead_punchtab;
    LinearLayout liner_cardheader_timetab; RelativeLayout punchLayout;
    LinearLayout lin1;

    public MapHeaderHolder(View itemView) {
        super(itemView);
        iv_feed_list_header_map = (ImageView) itemView.findViewById(R.id.iv_feed_list_header_map);
        tv_cardheader_time = (TextView) itemView.findViewById(R.id.tv_cardheader_time);
        linear_cardhead_punchtab = (LinearLayout) itemView.findViewById(R.id.linear_cardhead_punchtab);
        liner_cardheader_timetab = (LinearLayout) itemView.findViewById(R.id.liner_cardheader_timetab);
        punchLayout = (RelativeLayout) findView(R.id.ll_map_punch);
        lin1 = (LinearLayout) itemView.findViewById(R.id.lin1);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        final FusedLocationService.MyLocation location = (FusedLocationService.MyLocation) baseRecyclerModel;
        if (location != null) {
            double lat = location.getLatitude();
            double lon = location.getLongitude();
            if (lat == 0.0 || lon == 0.0) {
            } else {
                String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=15&size=450x160&maptype=roadmap&format=png&scale=2"
                        + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + lat + "," + lon;
                Picasso.with(context).load(url).placeholder(R.drawable.map_placeholder).into(iv_feed_list_header_map);
            }
            String time = Util.convertDateFormat(new Date(), Util.HH_MM_AM);
            tv_cardheader_time.setText(time);
            punchLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        sendClickEvent(AppConstants.ACTION_TYPE.MAP_HEADER_CLICK, location);
                    }
                }
            });
        }
    }
}
