package com.growthfile.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.adapter.BaseRecyclerAdapter;

import simplifii.framework.model.BaseRecyclerModel;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public abstract class BaseRecyclerHolder extends RecyclerView.ViewHolder {
    protected Context context;
    protected BaseRecyclerAdapter.RecyclerClickListener clickListener;
    protected BaseRecyclerModel model;
    protected LayoutInflater layoutInflater;


    public void setClickListener(BaseRecyclerAdapter.RecyclerClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public BaseRecyclerHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// LayoutInflater.from(context);
    }

    protected void setText(int id, String text) {
        TextView tv = (TextView) findView(id);
        tv.setText(text);
    }

    protected void sendClickEvent(int actionType) {
        sendClickEvent(actionType, model);
    }

    protected void sendClickEvent(int actionType, Object model) {
        if (clickListener != null) {
            clickListener.onItemClicked(getAdapterPosition(), itemView, model, actionType);
        }
    }

    protected void showVisibility(int... ids) {
        for (int id : ids) {
            findView(id).setVisibility(View.VISIBLE);
        }
    }

    protected void hideVisibility(int... ids) {
        for (int id : ids) {
            findView(id).setVisibility(View.GONE);
        }
    }

    protected void setTextView(int id, String s) {
        ((TextView) findView(id)).setText(s);
    }

    public abstract void onBindData(int position, BaseRecyclerModel baseRecyclerModel);

    protected View findView(int id) {
        return itemView.findViewById(id);
    }

    public void showToast(String message, int duration){
        Toast.makeText(context,message,duration).show();
    }

    protected void setTextOrGone(TextView tv, String text) {
        if (!TextUtils.isEmpty(text)) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(text);
        } else {
            tv.setVisibility(View.GONE);
        }
    }

    protected void setTextOrSetNA(TextView tv, String text) {
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        } else {
            tv.setText("NA");
        }
    }

    protected void setTextOrSetEmpty(TextView tv, String text) {
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        } else {
            tv.setText("");
        }
    }

}
