package com.growthfile.holders;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.R;
import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.model.TeamMemberModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import com.squareup.picasso.Picasso;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public class TeamMemberHolder extends BaseRecyclerHolder {
    private TextView tvTitle, tvDesignation, tvInitials;
    private ImageView ivUserLogo, ivCall, ivOpenGrowthProfile;

    private LinearLayout llHeader;

    public TeamMemberHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) findView(R.id.tv_name);
        tvDesignation = (TextView) findView(R.id.tv_designation);
        ivUserLogo = (ImageView) findView(R.id.iv_user_logo);
        tvInitials = (TextView) findView(R.id.tv_initials);
        ivCall = (ImageView) findView(R.id.iv_call_user);
        ivOpenGrowthProfile = (ImageView) findView(R.id.iv_give_star);

        llHeader = (LinearLayout) findView(R.id.ll_team_members_header);
    }


    protected void setTextView(int id, String s) {
        ((TextView) findView(id)).setText(s);
    }

    @Override
    public void onBindData(final int position, BaseRecyclerModel baseRecyclerModel) {
        final TeamMemberModel teamMemberProfile = (TeamMemberModel) baseRecyclerModel;

        if(teamMemberProfile.isShowListHeader()){
            llHeader.setVisibility(View.VISIBLE);
        } else {
            llHeader.setVisibility(View.GONE);
        }

        setTextOrGone(tvTitle, teamMemberProfile.getName());
        setTextOrGone(tvDesignation, teamMemberProfile.getDesignation());

        setImageOrSetInitials(context, teamMemberProfile.getAvatar(), teamMemberProfile.getName(), ivUserLogo, tvInitials);
        View.OnClickListener userLogoClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickListener!=null){
                    clickListener.onItemClicked(position, itemView, teamMemberProfile.getId(), AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE);
                }
            }
        };

//        tvInitials.setOnClickListener(userLogoClickListener);
//        ivUserLogo.setOnClickListener(userLogoClickListener);

        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(teamMemberProfile.getMobile())){
                    Util.makePhoneCall(teamMemberProfile.getMobile(), context);
                } else {
                    showToast("Number not found",Toast.LENGTH_SHORT);
                }

            }
        });

        ivOpenGrowthProfile.setOnClickListener(userLogoClickListener);
        itemView.setOnClickListener(userLogoClickListener);
    }

    private void setImageOrSetInitials(Context context, String imageUrl, String name, ImageView imageView, TextView tvInitials) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(imageView);
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        } else if(!TextUtils.isEmpty(name)){
            tvInitials.setText(Util.getInitialsFromName(name));
            imageView.setVisibility(View.GONE);
            tvInitials.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.drawable.unknown);
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        }
    }

}
