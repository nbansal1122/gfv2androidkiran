package com.growthfile.holders;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.v2.models.GFileModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.enums.TransactionType;
import simplifii.framework.rest.v2.responses.AttendanceSummary;
import simplifii.framework.rest.v2.responses.BasicProfile;
import simplifii.framework.rest.v2.responses.GFileContainer;
import simplifii.framework.rest.v2.responses.ReportsTo;
import simplifii.framework.rest.v2.responses.SmileysLedger;
import simplifii.framework.rest.v2.responses.Transaction;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by ajay on 18/12/16.
 */

public class GrowthFileHolderV2 extends BaseRecyclerHolder {


    private ImageView ivHeaderProfile, ivEditHederProfile;

    private TextView tvName, tvDesignation, tvDateOfJoining, tvDepartment, tvReportingUserName,
            tvAddress, tvOnTime, tvMarked, tvTotal, tvSmileysReceived, tvSmileysSent, tvCollectionAmount, tvCollectionCount,
            tvPaymentAmount, tvPaymentCount, tvInitials, tvPaymentMonth;

    private RelativeLayout rlViewCollection;

    public GrowthFileHolderV2(View itemView) {
        super(itemView);

        ivHeaderProfile = (ImageView) findView(R.id.iv_user_feed_logo);
        tvInitials = (TextView) findView(R.id.tv_initials);
        ivEditHederProfile = (ImageView) findView(R.id.iv_edit_header_image);

        tvName = (TextView) findView(R.id.tv_profile_title);
        tvDesignation = (TextView) findView(R.id.tv_profile_subtitle);
        tvDateOfJoining = (TextView) findView(R.id.tv_date_header_profile);
        tvDepartment = (TextView) findView(R.id.tv_department);
        tvAddress = (TextView) findView(R.id.tv_address);
        tvReportingUserName = (TextView) findView(R.id.tv_reporting_user);
        tvOnTime = (TextView) findView(R.id.tv_myprofile_ontime);
        tvMarked = (TextView) findView(R.id.tv_myprofile_markded);
        tvTotal = (TextView) findView(R.id.tv_myprofile_total);
        tvSmileysReceived = (TextView) findView(R.id.tv_myprofile_received);
        tvSmileysSent = (TextView) findView(R.id.tv_myprofile_sent);
        tvCollectionAmount = (TextView) findView(R.id.tv_collection_amount);
        tvCollectionCount = (TextView) findView(R.id.tv_collection_count);
        tvPaymentAmount = (TextView) findView(R.id.tv_payment_amount);
        tvPaymentCount = (TextView) findView(R.id.tv_payment_count);
        tvPaymentMonth = (TextView) findView(R.id.tv_collection_payment_month);

        rlViewCollection = (RelativeLayout) findView(R.id.lay_gfile_transaction);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        GFileModel gFileModel = (GFileModel) baseRecyclerModel;
        if (gFileModel.isImageEditable()) {
            ivEditHederProfile.setVisibility(View.VISIBLE);
            ivEditHederProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.onItemClicked(getAdapterPosition(), itemView, null, AppConstants.ACTION_TYPE.EDIT_IMAGE);
                    }
                }
            });
        } else {
            ivEditHederProfile.setVisibility(View.GONE);
        }
        if (gFileModel.getGrowthFile() != null) {
            setHeader(gFileModel.getGrowthFile());
            setAttendanceInfo(gFileModel.getGrowthFile().getAttendanceSummary());
            setSmileysInfo(gFileModel.getGrowthFile().getSmileysLedger());
            setCollectionsInfo(gFileModel.getGrowthFile().getTransactions(), gFileModel.getGrowthFile());
        }
    }

    private void setCollectionsInfo(List<Transaction> transactions, GFileContainer gFile) {
        if (CollectionsUtils.isNotEmpty(transactions)) {
            double collectionAmount = 0;
            double paymentAmount = 0;
            int collectionCount = 0;
            int paymentCount = 0;
            for (Transaction transaction : transactions) {
                if (transaction.getAmount() != null) {
                    if (TransactionType.COLLECTION.getApiText().equalsIgnoreCase(transaction.getStatus())) {
                        collectionAmount += transaction.getAmount();
                        collectionCount++;
                    } else if (TransactionType.PAYMENT.getApiText().equalsIgnoreCase(transaction.getStatus())) {
                        paymentAmount += transaction.getAmount();
                        paymentCount++;
                    }
                }
            }
            if (collectionAmount > 0)
                tvCollectionAmount.setText(Util.formatDouble(collectionAmount));
            else
                tvCollectionAmount.setText("0.0");
            if (paymentAmount > 0)
                tvPaymentAmount.setText(Util.formatDouble(paymentAmount));
            else
                tvPaymentAmount.setText("0.0");
            if (!TextUtils.isEmpty(gFile.getTransaction_month())) {
                tvPaymentMonth.setText(gFile.getTransaction_month());
            }
            tvCollectionCount.setText(String.valueOf(collectionCount));
            tvPaymentCount.setText(String.valueOf(paymentCount));
            rlViewCollection.setVisibility(View.VISIBLE);

        } else {
            rlViewCollection.setVisibility(View.GONE);
        }
    }

    private void setSmileysInfo(SmileysLedger smileysLedger) {
        if (smileysLedger != null) {
            tvSmileysReceived.setText(String.valueOf(smileysLedger.getRecd()));
            tvSmileysSent.setText(String.valueOf(smileysLedger.getGiven()));
        }
    }

    private void setAttendanceInfo(AttendanceSummary attendanceSummary) {
        if (attendanceSummary != null) {
            tvOnTime.setText(String.valueOf(attendanceSummary.getOnTime()));
            tvMarked.setText(String.valueOf(attendanceSummary.getAttended()));
            tvTotal.setText(String.valueOf(attendanceSummary.getTotal()));
        }
    }

    private void setHeader(GFileContainer growthFile) {
        BasicProfile basicProfile = growthFile.getBasicProfile();
        if (basicProfile != null) {
            setTextOrSetNA(tvName, basicProfile.getName());
            setTextOrSetEmpty(tvDesignation, basicProfile.getDesignation());
            try {
                String dateOfJoining = basicProfile.getDateOfJoining();
                String formattedDate = Util.convertDateFormat(dateOfJoining, "yyyy-MM-dd", "dd MMMM yyyy");
                tvDateOfJoining.setText(formattedDate);
            } catch (Exception e) {
                setTextOrSetNA(tvDateOfJoining, null);
            }

            setImageOrSetInitials(basicProfile.getPic(), basicProfile.getName(), ivHeaderProfile, tvInitials);

            setTextOrSetNA(tvDepartment, basicProfile.getDepartment());
            setTextOrSetNA(tvReportingUserName, null);
            setTextOrSetNA(tvAddress, basicProfile.getAddress());
        }
        ReportsTo reportsTo = growthFile.getReportsTo();
        if (reportsTo != null) {
            setTextOrSetNA(tvReportingUserName, reportsTo.getName());
        }
    }

    private void setImageOrSetInitials(String imageUrl, final String name, final ImageView imageView, final TextView tvInitials) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    setImageOrSetInitials(null, name, imageView, tvInitials);
                }
            });
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(name)) {
            tvInitials.setText(Util.getInitialsFromName(name));
            imageView.setVisibility(View.GONE);
            tvInitials.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.drawable.unknown);
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        }
    }
}
