package com.growthfile.utility;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.v2.requests.EditTransactionRequest;
import simplifii.framework.rest.v2.requests.GenerateOtpRequest;
import simplifii.framework.rest.v2.requests.SendSmileyRequest;
import simplifii.framework.rest.v2.requests.SendStarRequest;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.GFileApiResponse;
import simplifii.framework.rest.v2.responses.GenerateOtpApiResponse;
import simplifii.framework.rest.v2.responses.GetAccountApiResponse;
import simplifii.framework.rest.v2.responses.GetEmployeesApiResponse;
import simplifii.framework.rest.v2.responses.GetFeedCardApiResponse;
import simplifii.framework.rest.v2.responses.ValidateOtpApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by robin on 12/20/16.
 */

public class ApiRequestGeneratorV2 {

    interface API_PARAMS {
        String MOBILE = "mobile";
        String OTP = "otp";
        String APP_ID = "app_id";
        String DEVICE_ID = "device_id";
        String DEVICE_MODEL = "device_model";
        String OS_TYPE = "os_type";
        String OS_VERSION = "os_version";
        String DEVCIE_TYPE = "device_type";
        String DEVICE_TOKEN = "device_token";
        String FROM_DATE = "from_date";
        String TO_DATE = "to_date";
        String REMARK = "remark";
        String LEAVE_TYPE = "leave_type";
        String USER_ID = "user_id";
        String LAT = "lat";
        String LNG = "lng";
        String CARD_ID = "card_id";
        String ACTION = "action";
        String DEVICE_TYPE = "device_type";
        String ACCOUNT_ID = "account_id";
        String COMMENT = "comment";
        String AMOUNT = "amount";
        String STATUS = "status";
        String DUE_DATE = "due_date";
        String DATE = "date";
    }

    public static HttpParamObject generateOtp(String mobile) {
        GenerateOtpRequest request = new GenerateOtpRequest(mobile);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.GENERATE_OTP);
        httpParamObject.setPatchMethod();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(GenerateOtpApiResponse.class);
//        httpParamObject.setApiBasedClassType(OtpContainer.class);
        return httpParamObject;
    }

    public static HttpParamObject validateOtp(String mobile, String otp) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.VALIDATE_OTP);
        httpParamObject.setPostMethod();
        httpParamObject.addParameter(API_PARAMS.MOBILE, mobile);
        httpParamObject.addParameter(API_PARAMS.OTP, otp);
        httpParamObject.addParameter(API_PARAMS.APP_ID, "gfv2");
        httpParamObject.addParameter(API_PARAMS.DEVICE_ID, "device id");
        httpParamObject.addParameter(API_PARAMS.DEVICE_MODEL, "iphone 6s");
        httpParamObject.addParameter(API_PARAMS.OS_TYPE, "ANDROID");
        httpParamObject.addParameter(API_PARAMS.DEVICE_TYPE, "ANDROID");
        httpParamObject.addParameter(API_PARAMS.OS_VERSION, "23");
        httpParamObject.addParameter(API_PARAMS.DEVICE_TOKEN, Preferences.getData(AppConstants.PREF_KEYS.FCM_TOKEN, ""));
        httpParamObject.setClassType(ValidateOtpApiResponse.class);
//        httpParamObject.setApiBasedClassType(UserProfile.class);
        return httpParamObject;
    }

    public static HttpParamObject getUserFeedCards() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.GET_CARDS);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(GetFeedCardApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject applyLeaveRequest(String fromDate, String toDate, String remark, String leaveType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.APPLY_LEAVE);
        httpParamObject.setPostMethod();
        httpParamObject.addParameter(API_PARAMS.FROM_DATE, fromDate);
        httpParamObject.addParameter(API_PARAMS.TO_DATE, toDate);
        httpParamObject.addParameter(API_PARAMS.REMARK, remark);
        httpParamObject.addParameter(API_PARAMS.LEAVE_TYPE, leaveType);
        httpParamObject.addAuthTokenHeader();
        return httpParamObject;
    }

    public static HttpParamObject getSelfGrowthFile() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.GET_GROWTH_FILE);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(GFileApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getGrowthFileForOther(Integer userId) {
        HttpParamObject httpParamObject = getSelfGrowthFile();
        httpParamObject.addParameter(API_PARAMS.USER_ID, String.valueOf(userId));
        return httpParamObject;
    }

    public static HttpParamObject getEmployees() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.LIST_EMPLOYEES);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(GetEmployeesApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject logout() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.LOGOUT);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setPatchMethod();
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject markLocation(LatLng latLng, String comment, Integer accountId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.MARK_LOCATION);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addParameter(API_PARAMS.LAT, String.valueOf(latLng.latitude));
        httpParamObject.addParameter(API_PARAMS.LNG, String.valueOf(latLng.longitude));
        httpParamObject.addParameter(API_PARAMS.DEVICE_ID, "device id");
        if (accountId != null)
            httpParamObject.addParameter(API_PARAMS.ACCOUNT_ID, String.valueOf(accountId));
        if (!TextUtils.isEmpty(comment))
            httpParamObject.addParameter(API_PARAMS.COMMENT, comment);
        return httpParamObject;
    }

    public static HttpParamObject sendSmileyRequest(Integer receiverId) {
        SendSmileyRequest request = new SendSmileyRequest();
        request.setReceiverId(receiverId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.SEND_SMILEY);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject sendStarRequest(List<Integer> receiverIds, String comment) {
        SendStarRequest request = new SendStarRequest();
        request.setReceiverIds(receiverIds);
        request.setComment(comment);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.SEND_STAR);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        return httpParamObject;
    }

    public static HttpParamObject singleCardDetailsRequest(int cardId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.SINGLE_CARD_DETAILS);
        httpParamObject.addParameter(API_PARAMS.CARD_ID, String.valueOf(cardId));
        httpParamObject.setClassType(GetFeedCardApiResponse.class);
        httpParamObject.addAuthTokenHeader();
        return httpParamObject;
    }

    public static HttpParamObject followUpAction(String action, Integer cardId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.FOLLOW_UP_ACTION);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addParameter(API_PARAMS.CARD_ID, String.valueOf(cardId));
        httpParamObject.addParameter(API_PARAMS.ACTION, action);
        return httpParamObject;
    }

    public static HttpParamObject getAccountListRequest() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.GET_ACCOUNTS);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(GetAccountApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject bookTransactionRequest(String amount, String status, String dueDate, Integer accountId, String comment) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.BOOK_TRANSACTION);
        httpParamObject.setPostMethod();
        httpParamObject.addParameter(API_PARAMS.AMOUNT, amount);
        httpParamObject.addParameter(API_PARAMS.STATUS, status);
        httpParamObject.addParameter(API_PARAMS.DUE_DATE, dueDate);
        if (accountId != null)
            httpParamObject.addParameter(API_PARAMS.ACCOUNT_ID, String.valueOf(accountId));
        if (!TextUtils.isEmpty(comment))
            httpParamObject.addParameter(API_PARAMS.COMMENT, comment);
        httpParamObject.addAuthTokenHeader();
        return httpParamObject;
    }


    public static HttpParamObject resignRequest(String date, String remark) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.RESIGN_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addParameter(API_PARAMS.DATE, date);
        httpParamObject.addParameter(API_PARAMS.REMARK, remark);
        return httpParamObject;
    }

    public static HttpParamObject editTransactionRequest(int cardId, String dueDate, Integer accountId, String amount, String status, String comment) {
        EditTransactionRequest request = new EditTransactionRequest(cardId, dueDate, accountId, Double.parseDouble(amount), status, comment);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setPatchMethod();
        httpParamObject.setUrl(AppConstants.EMPLOYEES_PAGE_URL.EDIT_TRANSACTION);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }

}
