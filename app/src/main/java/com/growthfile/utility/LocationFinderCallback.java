package com.growthfile.utility;

/**
 * Created by kundan on 9/6/2016.
 */
public interface LocationFinderCallback {

    void locationFound();
}
