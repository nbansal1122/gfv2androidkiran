package com.growthfile.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

public class LocationData implements LocationListener {

	boolean isGPSEnabled = false;
	boolean isNetworkEnabled = false;
	boolean isPassiveProviderEnabled = false;

	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

	Context context;

	protected LocationManager locationManager;

	public LocationData(Context context) {
		this.context = context;
	}

	/*
	 * FETCHING LOCATION FROM ALL RESOURCES..........
	 */

	public Location getLocation() {
		try {
			locationManager = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);

			try {
				if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
						ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(context, "Bolt Riders App does not have permission to access your GPS. Please verify the settings of your device.", Toast.LENGTH_LONG).show();
					return null;
				}
			}catch (Exception e1){}

			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			isPassiveProviderEnabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled && !isPassiveProviderEnabled) {
				return null;
			} else {
				this.canGetLocation = true;
				if (isGPSEnabled) {
					if (location == null) {
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
								return location;
							}
						}
					}
				}

				if (isNetworkEnabled) {
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							return location;
						}
					}
				}

				if(isPassiveProviderEnabled){
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							return location;
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	@Override
	public void onLocationChanged(Location location) {
	//	Log.e("LOCATION CHANGED", " " + location.getLatitude());
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onProviderDisabled(String provider) {
	}
}
