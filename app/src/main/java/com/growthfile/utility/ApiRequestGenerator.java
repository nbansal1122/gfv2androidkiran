package com.growthfile.utility;

import com.growthfile.Pojo.SelfProfilePojo;

import simplifii.framework.rest.requests.ChangeImageUrlRequest;
import simplifii.framework.rest.responses.GetEmployeeResponse;
import simplifii.framework.rest.responses.GetTeamResponse;

import com.growthfile.enums.LeaveType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.enums.LeaveAction;
import simplifii.framework.rest.requests.AcceptRejectChangeReportingManagerRequest;
import simplifii.framework.rest.requests.AcceptRejectEmployeePositionMappingRequest;
import simplifii.framework.rest.requests.AcceptRejectResignRequest;
import simplifii.framework.rest.requests.AcceptRejectTerminationRequest;
import simplifii.framework.rest.requests.AddPositionRequest;
import simplifii.framework.rest.requests.ChangeMobileNumber;
import simplifii.framework.rest.requests.ChangeReportingManagerRequest;
import simplifii.framework.rest.requests.EditEmployeeDetailsRequest;
import simplifii.framework.rest.requests.EditPositionRequest;
import simplifii.framework.rest.requests.GiveSmileyRequest;
import simplifii.framework.rest.requests.GiveStarRequest;
import simplifii.framework.rest.requests.MapEmployeeRequest;
import simplifii.framework.rest.requests.ResignRequest;
import simplifii.framework.rest.requests.ReviewPositionRequest;
import simplifii.framework.rest.requests.TerminateRequest;
import simplifii.framework.rest.requests.UpvoteStarRequest;
import simplifii.framework.rest.requests.WithdrawPositionRequest;
import simplifii.framework.rest.requests.WithdrawSelfLeaveRequest;
import simplifii.framework.rest.requests.WithdrawSelfResignationRequest;
import simplifii.framework.rest.responses.AddPositionResponse;
import simplifii.framework.rest.responses.GetCentersResponse;
import simplifii.framework.rest.responses.GetCostCentersResponse;
import simplifii.framework.rest.responses.GetDepartmentsResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;

/**
 * Created by robin on 11/14/16.
 */

public class ApiRequestGenerator {

    public static HttpParamObject changeMobileNumberRequest(String newNumber, Integer userId) {
        ChangeMobileNumber request = new ChangeMobileNumber(newNumber, userId);


        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setPatchMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.CHANGE_MOBILE);
        return httpParamObject;
    }


    public static HttpParamObject getUserProfileSelf() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER_PROFILE_SELF_V1);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(SelfProfilePojo.class);
        return httpParamObject;
    }

    public static HttpParamObject changeReportingManager(Integer employeeId, Integer reportsToId) {
        ChangeReportingManagerRequest request = new ChangeReportingManagerRequest();
        request.setEmployeeId(employeeId);
        request.setReportsTo(reportsToId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.CHANGE_REPORTING_MANAGER);
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject resignRequest(String lastWorkingDay) {
        ResignRequest request = new ResignRequest();
        request.setLastWorkingDay(lastWorkingDay);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.RESIGN_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject terminateRequest(Integer employeeId, String lastWorkingDay) {
        TerminateRequest request = new TerminateRequest();
        request.setEmployeeId(employeeId);
        request.setLastWorkingDay(lastWorkingDay);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.TERMINATE_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject getDefaultHttpPost(String url) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(url);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        return httpParamObject;
    }

    public static HttpParamObject giveStarRequest(List<Integer> receiverIds, String comment) {
        GiveStarRequest request = new GiveStarRequest();
        request.setReceiverIds(receiverIds);
        request.setComment(comment);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.GIVE_STAR_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject giveStarRequest(Integer receiverId, String comment) {
        GiveSmileyRequest request = new GiveSmileyRequest();
        request.setReceiverId(receiverId);
        request.setComment(comment);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.GIVE_STAR_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject giveSmileyRequest(Integer receiverId, String comment) {
        GiveSmileyRequest request = new GiveSmileyRequest();
        request.setReceiverId(receiverId);
        request.setComment(comment);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.GIVE_SMILEY_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    private static String getSmileyJson(int applicationId, int receiverId, String comment) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("receiver_id", receiverId);
            if (applicationId != -1) {
                obj.put("in_response_to", applicationId);
            }
            obj.put("COMMENT", comment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    public static HttpParamObject giveSmileyRequest(int applicationID, Integer receiverId, String comment) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(getSmileyJson(applicationID, receiverId, comment));
        httpParamObject.setUrl(AppConstants.PAGE_URL.GIVE_SMILEY_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject upvoteStarRequest(Integer starId, String comment) {
        UpvoteStarRequest request = new UpvoteStarRequest();
        request.setStarId(starId);
        request.setComment(comment);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPVOTE_STAR_REQUEST);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject addPositionRequest(String designation, String roleType, String roleDescription,
                                                     Integer costCenterId, Integer centerId, Integer departmentId) {
        AddPositionRequest request = new AddPositionRequest();
        request.setDesignation(designation);

        if (roleType.contains("-")) {
            roleType = roleType.replace("-", "_");
        }
        request.setRoleType(roleType);
        request.setRoleDescription(roleDescription);
        request.setCcId(costCenterId);
        request.setCenterId(centerId);
        request.setDeptId(departmentId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_POSITION_REQUEST);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(AddPositionResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject editPositionRequest(Integer positionId, String designation, String roleType, String roleDescription,
                                                      Integer costCenterId, Integer centerId, Integer departmentId) {
        EditPositionRequest request = new EditPositionRequest();
        request.setPositionId(positionId);
        request.setDesignation(designation);
        request.setRoleType(roleType);
        request.setRoleDescription(roleDescription);
        request.setCcId(costCenterId);
        request.setCenterId(centerId);
        request.setDeptId(departmentId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.EDIT_POSITION_REQUEST);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject reviewPositionRequest(Integer positionId, boolean isApproved, String reason) {
        ReviewPositionRequest request = new ReviewPositionRequest();
        request.setPositionId(positionId);
        request.setIsApproved(isApproved);
        request.setReason(reason);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.REVIEW_POSITION_REQUEST);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        if (isApproved) {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.ACCEPT_POSITION);
        } else {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.REJECT_POSITION);
        }
        return httpParamObject;
    }

    public static HttpParamObject withdrawPositionRequest(Integer positionId) {
        WithdrawPositionRequest request = new WithdrawPositionRequest();
        request.setPositionId(positionId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.WITHDRAW_POSITION_REQUEST);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setTaskCode(AppConstants.TASK_CODES.WITHDRAW_POSITION);
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject editEmployeeDetailsRequest(Integer employeeId, Integer departmentId, Integer costCenterId,
                                                             String roleDescription, Integer centerId, String roleType) {
        EditEmployeeDetailsRequest request = new EditEmployeeDetailsRequest();
        request.setEmployeeId(employeeId);
        request.setDeptId(departmentId);
        request.setCcId(costCenterId);
        request.setRoleDescription(roleDescription);
        request.setCenterId(centerId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.EDIT_EMPLOYEE_DETAILS);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject acceptRejectChangeReportingManagerRequest(Integer proposedChangeId, boolean isApproved) {
        AcceptRejectChangeReportingManagerRequest request = new AcceptRejectChangeReportingManagerRequest();
        request.setProposedChangesId(proposedChangeId);
        request.setIsApproved(isApproved);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACCEPT_REJECT_CHANGE_REPORTING_MANAGER);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject acceptRejectEmployeePositionRequest(Integer positionId, boolean isApproved) {
        AcceptRejectEmployeePositionMappingRequest request = new AcceptRejectEmployeePositionMappingRequest();
        request.setPositionId(positionId);
        request.setIsApproved(isApproved);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACCEPT_REJECT_EMPLOYEE_POSITION_MAPPING);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject getMyTeam() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.MY_TEAM);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(GetTeamResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getEmployees() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_EMPLOYEES);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(GetEmployeeResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getDepartmentsRequest() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_DEPARTMENTS);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(GetDepartmentsResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getCentersRequest() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.CENTER_LIST);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(GetCentersResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getCostCentersRequest() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_COST_CENTERS);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setClassType(GetCostCentersResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject mapEmployeeToPosition(Integer positionId, String dateOfJoining, String email,
                                                        Long mobile, String name) {
        MapEmployeeRequest request = new MapEmployeeRequest();
        request.setPositionId(positionId);
        request.setDateOfJoining(dateOfJoining);
        request.setEmail(email);
        request.setMobile(mobile);
        request.setName(name);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.MAP_EMPLOYEE);
        httpParamObject.setPostMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
//        httpParamObject.setClassType();
        return httpParamObject;
    }

    public static HttpParamObject withdrawSelfResignation(Integer resignationId) {
        WithdrawSelfResignationRequest request = new WithdrawSelfResignationRequest();
        request.setResignationId(resignationId);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.WITHDRAW_RESIGNATION);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setTaskCode(AppConstants.TASK_CODES.WITHDRAW_RESIGNATION);
        return httpParamObject;
    }

    public static HttpParamObject withdrawSelfLeave(String remark, LeaveType leaveType, Integer applicationId) {
        WithdrawSelfLeaveRequest request = new WithdrawSelfLeaveRequest();
        request.setRemark(remark);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACTION_LEAVE + "/" + leaveType.getType() + "/" + applicationId + "/" + LeaveAction.WITHDRAW.getType());
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        httpParamObject.setTaskCode(AppConstants.TASK_CODES.WITHDRAW_LEAVE);
        return httpParamObject;
    }

    public static HttpParamObject acceptRejectResignation(Integer resignationId, String lastWorkingDay, Boolean isApproved) {
        AcceptRejectResignRequest request = new AcceptRejectResignRequest();
        request.setResignationId(resignationId);
        request.setLastWorkingDay(lastWorkingDay);
        request.setIsApproved(isApproved);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACCEPT_REJECT_RESIGN);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        if (isApproved) {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.ACCEPT_RESIGNATION);
        } else {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.REJECT_RESIGNATION);
        }
        return httpParamObject;
    }

    public static HttpParamObject acceptRejectTerminationRequest(Integer resignationId, String lastWorkingDay, Boolean isApproved) {
        AcceptRejectTerminationRequest request = new AcceptRejectTerminationRequest();
        request.setResignationId(resignationId);
        request.setLastWorkingDay(lastWorkingDay);
        request.setIsApproved(isApproved);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACCEPT_REJECT_TERMINATION);
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        if (isApproved) {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.ACCEPT_TERMINATION);
        } else {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.REJECT_TERMINATION);
        }
        return httpParamObject;
    }

    public static HttpParamObject acceptRejectLeave(String remark, LeaveType leaveType, Integer applicationId, LeaveAction leaveAction) {
        WithdrawSelfLeaveRequest request = new WithdrawSelfLeaveRequest();
        request.setRemark(remark);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setUrl(AppConstants.PAGE_URL.ACTION_LEAVE + "/" + leaveType.getType() + "/" + applicationId + "/" + leaveAction.getType());
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        if (LeaveAction.ACCEPT.equals(leaveAction)) {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.ACCEPT_LEAVE);
        } else if (LeaveAction.REJECT.equals(leaveAction)) {
            httpParamObject.setTaskCode(AppConstants.TASK_CODES.REJECT_LEAVE);
        }
        return httpParamObject;
    }

    public static HttpParamObject getOtherUserProfileRequest(Integer employeeId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER_PROFILE_OTHER_V1 + "/" + employeeId);
        httpParamObject.addAuthTokenHeader();
        httpParamObject.setClassType(SelfProfilePojo.class);
        return httpParamObject;
    }

    public static HttpParamObject changeImageUrlRequest(String imageUrl) {
        ChangeImageUrlRequest request = new ChangeImageUrlRequest();
        request.setProfileImage(imageUrl);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EDIT_SELF_PROFILE);
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setPatchMethod();
        httpParamObject.addAuthTokenHeader();
        httpParamObject.addJsonContentTypeHeader();
        return httpParamObject;
    }
}
