package com.growthfile.utility;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;

import java.util.TimeZone;

/**
 * Created by kundan on 9/8/2016.
 */
public class Helper {




    /**
     * Method to check Internet connection
     *
     * @return
     */
    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);

        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            //no conection
            return false;
        }
    }

    /**
     * method to convert time to local format
     * @param time
     * @param to
     * @return
     */
    public static long toLocalTime(long time, TimeZone to) {
        return time + to.getOffset(time);
    }


    /**
     * method to convert time to utc format
     * @param time
     * @param from
     * @return
     */
    public static long toUTC(long time, TimeZone from) {
        return time - from.getOffset(time);
    }


    public static boolean checkGps(Context context){
        LocationManager manager = (LocationManager)context.getSystemService(context.LOCATION_SERVICE);
        boolean isGpsOn = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return isGpsOn;
    }
}
