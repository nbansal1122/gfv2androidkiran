package com.growthfile.fragment;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.growthfile.R;

import simplifii.framework.rest.responses.UserProfile;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Prefs;
import simplifii.framework.utility.Util;

import com.growthfile.database.DbHelper;
import com.growthfile.ui.FirstTimePinSetup;
import com.growthfile.ui.Splash;
import com.growthfile.ui.WebViewActivity;
import com.growthfile.util.AppUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreOptionFragment extends AppBaseFragment {

    private RelativeLayout rel_logout;
    TextView tv_more_titlename, tv_more_phone, tv_team_email;
    ImageView img_more_pic;
    private Prefs prefs;
    private DbHelper dbHelper;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private TextDrawable.IBuilder drawable1;
    private TextView tvInitials;

    public MoreOptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void initViews() {
        prefs = new Prefs();
        dbHelper = DbHelper.getInstance(getActivity());
        tv_more_titlename = (TextView) findView(R.id.tv_more_titlename);
        rel_logout = (RelativeLayout) findView(R.id.rel_logout);
        tv_more_phone = (TextView) findView(R.id.tv_more_phone);
        tv_team_email = (TextView) findView(R.id.tv_team_email);
        img_more_pic = (ImageView) findView(R.id.iv_user_logo);
        tvInitials = (TextView) findView(R.id.tv_initials);

        String userJson = Preferences.getData(Preferences.USER_PROFILE, "");
        if (!TextUtils.isEmpty(userJson)) {
            UserProfile userProfile = (UserProfile) JsonUtil.parseJson(userJson, UserProfile.class);
            if (userProfile != null) {
                tv_more_titlename.setText(userProfile.getName());
                tv_more_phone.setText(userProfile.getMobile());
                tv_team_email.setText(userProfile.getEmail());

                String selfImage = userProfile.getAvatar();
                try {
                    Picasso.with(getActivity()).load(selfImage).into(img_more_pic, new Callback() {
                        @Override
                        public void onSuccess() {
                            img_more_pic.setVisibility(View.VISIBLE);
                            tvInitials.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            String initials = Util.getInitialsFromName(Preferences.getData("pinnane", ""));
                            tvInitials.setText(initials);
                            img_more_pic.setVisibility(View.VISIBLE);
                            tvInitials.setVisibility(View.GONE);
                        }
                    });
                } catch (Exception e) {
                    String initials = Util.getInitialsFromName(Preferences.getData("pinnane", ""));
                    tvInitials.setText(initials);
                    img_more_pic.setVisibility(View.VISIBLE);
                    tvInitials.setVisibility(View.GONE);
                }
            }
        }

//        tv_more_titlename.setText(prefs.getPreferencesString(getActivity(),"pinnane"));
//        tv_more_phone.setText(prefs.getPreferencesString(getActivity(),"moreMobile"));
//        tv_team_email.setText(prefs.getPreferencesString(getActivity(),"moreemail"));

//        if(prefs.getPreferencesString(getActivity(),"moreimage").equals("")){
//
//            String test =prefs.getPreferencesString(getActivity(),"pinnane");
//            char first = test.charAt(0);
//            drawable1 = TextDrawable.builder().round();
//            TextDrawable drawable = drawable1.build(String.valueOf(first), Color.parseColor("#5ab2e0"));
//            img_more_pic.setImageDrawable(drawable);
//
//        }else {
//            initiateImageLoader();
//            imageLoader.displayImage(prefs.getPreferencesString(getActivity(),"moreimage"), img_more_pic, options);
//        }


        rel_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.clearAppData(getActivity());
                dbHelper.deleteDb();
                Intent splashIntent = new Intent(getActivity(), Splash.class);
                splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(splashIntent);
                getActivity().finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        findView(R.id.rl_change_pin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent splashIntent = new Intent(getActivity(), FirstTimePinSetup.class);
                splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(splashIntent);
                getActivity().finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        setOnClickListener(R.id.ll_help, R.id.ll_tnc, R.id.ll_privacy_policy, R.id.ll_show_love, R.id.ll_tour);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_help:
                AppUtil.sendEmail(getActivity(), "help@growthfile.com");
                break;
            case R.id.ll_tnc:
                WebViewActivity.startActivity(getActivity(), AppConstants.PAGE_URL.TnC, getString(R.string.terms_of_use));
                break;
            case R.id.ll_show_love:
                AppUtil.openUrl(getActivity());
                break;
            case R.id.ll_privacy_policy:
                WebViewActivity.startActivity(getActivity(), AppConstants.PAGE_URL.PRIVACY_POLICY, getString(R.string.privacy_policy));
                break;
            case R.id.ll_tour:
                showToast("Screens pending from your end");
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_more_option;
    }

    public static MoreOptionFragment newInstance() {
        MoreOptionFragment f = new MoreOptionFragment();
        return f;
    }

}
