package com.growthfile.fragment;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;

import java.util.Arrays;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by aman on 17/12/16.
 */

public class EmptyViewFragment extends BaseFragment {

    List<String> reportTexts;
    private LinearLayout llStaticPoints;

    @Override
    public void initViews() {
        llStaticPoints = (LinearLayout) findView(R.id.ll_growth_reports);

        reportTexts = Arrays.asList(getResources().getStringArray(R.array.static_points));
        setStaticPoints();
    }






    private void setStaticPoints() {
       for (String point : reportTexts){
           View row = activity.getLayoutInflater().inflate(R.layout.row_empty_start_screen_v2, null);
           TextView tvStaticPoint = (TextView) row.findViewById(R.id.tv_reports_text);
           tvStaticPoint.setText(Html.fromHtml(point));
           llStaticPoints.addView(row);
       }


    }

    private class ImageGetter implements Html.ImageGetter{

        @Override
        public Drawable getDrawable(String s) {
            int id;
            if(s.equals("p_smiley.png")){
                id = R.drawable.p_smiley;
            }else {
                return null;
            }

            LevelListDrawable d1 = new LevelListDrawable();
            Drawable d = getResources().getDrawable(id);
            d1.addLevel(0,0,d);
            d.setBounds(0, 0, d.getIntrinsicWidth() , d.getIntrinsicHeight());
            return d;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_empty_start_screen_v2;
    }

}

