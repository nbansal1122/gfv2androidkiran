package com.growthfile.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;

import simplifii.framework.model.BaseRecyclerModel;

import com.growthfile.database.DbHelper;
import com.growthfile.model.feed.Card;
import com.growthfile.model.feed.FeedResponse;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.ui.PunchInOutActivity;
import com.growthfile.util.AppUtil;
import com.growthfile.util.EndlessRecyclerViewScrollListener;
import com.growthfile.util.FeedAction;
import com.growthfile.util.FusedLocationService;
import com.google.android.gms.common.ConnectionResult;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Prefs;

/**
 * Created by nbansal2211 on 20/11/16.
 */

public class UserFeedFragment extends AppBaseFragment implements SwipeRefreshLayout.OnRefreshListener, BaseRecyclerAdapter.RecyclerClickListener {
    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<BaseRecyclerModel> list = new ArrayList<>();
    private BaseRecyclerAdapter adapter;
    private FusedLocationListener fusedLocationService;
    private FusedLocationService.MyLocation location;
    private boolean headerFlag;
    private Prefs prefs;
    private int page = 1, total;
    private FeedResponse feedResponse;
    private boolean isLoading;

    @Override
    public void initViews() {
        initProgressBar();
        recyclerView = (RecyclerView) findView(R.id.rw_feeds);
        swipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipeContainer1);
        swipeRefreshLayout.setOnRefreshListener(this);
        prefs = new Prefs();
        initRecyclerView();
        checkPermissionForFileStorage();
        checkLocationPermission();
    }


    private void checkLocationPermission() {
        if (AppUtil.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLocation();
        } else {
            new TedPermission(getActivity()).setPermissionListener(new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    getLocation();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                    fetchUserFeeds(page);
                }
            }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION).check();
        }
    }

    private void checkPermissionForFileStorage() {
        if (AppUtil.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            getPunchData();
        }
    }


    private void getLocation() {
        fusedLocationService = new FusedLocationListener(getActivity());
    }

    void getPunchData() {

        ArrayList<String> currentAttendance = new ArrayList<>();
        currentAttendance = DbHelper.getInstance(getActivity()).getAttendance();
        //Current date//
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time_stamp = dateFormat.format(cal.getTime());
        //Last index//
        if (currentAttendance.size() > 0) {
            String lastIndx = currentAttendance.get(currentAttendance.size() - 1);
            String[] sep = lastIndx.split("T");

            String lastDate = sep[0];
            if (time_stamp.equals(lastDate)) {
                headerFlag = true;
            } else {
                if (prefs.getPreferencesString(getActivity(), "MapFlag").equals("punched") && prefs.getPreferencesString(getActivity(), "MapFlagTime").equals(time_stamp)) {
                    headerFlag = true;
                } else {
                    headerFlag = false;
                }
            }
        } else {
            if (prefs.getPreferencesString(getActivity(), "MapFlag").equals("punched")) {
                headerFlag = true;
            } else {
                headerFlag = false;
            }
        }
    }

    class FusedLocationListener extends FusedLocationService {

        public FusedLocationListener(Activity mContext) {
            super(mContext);
        }

        @Override
        protected void onSuccess(MyLocation mLastKnownLocation) {
            UserFeedFragment.this.location = mLastKnownLocation;
            addPunchInHeaderView();
            fetchUserFeeds(page);
        }

        @Override
        protected void onFailed(ConnectionResult connectionResult) {
            fetchUserFeeds(page);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        switch (taskCode) {
            case AppConstants.TASK_CODES.USER_FEED_MORE:
                return;
            case AppConstants.TASK_CODES.USER_FEED:
                break;
        }
        super.onPreExecute(taskCode);
    }

    private void initRecyclerView() {
        adapter = new BaseRecyclerAdapter(list, getActivity());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
        listener.setmLayoutManager(manager);
        recyclerView.addOnScrollListener(listener);
    }

    EndlessRecyclerViewScrollListener listener = new EndlessRecyclerViewScrollListener() {
        @Override
        public void onLoadMore(int pageNumber, int totalItemsCount) {
            if (feedResponse != null && feedResponse.getResponse() != null) {
                if (totalItemsCount >= total || isLoading) {
                    return;
                }
            }
            page = page + 1;
            fetchUserFeeds(page);
        }
    };


    private void fetchUserFeeds(int page) {
        if (!isLoading) {

            isLoading = true;
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.USER_FEED);
            httpParamObject.addParameter("offset", String.valueOf(page));
            httpParamObject.addAuthTokenHeader();
            httpParamObject.setClassType(FeedResponse.class);
            if (page == 1) {
                executeTask(AppConstants.TASK_CODES.USER_FEED, httpParamObject);
            } else {
                executeTask(AppConstants.TASK_CODES.USER_FEED_MORE, httpParamObject);
            }

        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        swipeRefreshLayout.setRefreshing(false);
        isLoading = false;
        if (re != null) {
            showToast(re.getMessage());
        } else if (e != null) {
            showToast(e.getMessage());
        }
    }

    private void addPunchInHeaderView() {
        if (!headerFlag) {
            if (location == null) {
                location = new FusedLocationService.MyLocation(0.0, 0.0);
            }
            list.add(location);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        swipeRefreshLayout.setRefreshing(false);
        switch (taskCode) {
            case AppConstants.TASK_CODES.USER_FEED:
                feedResponse = (FeedResponse) response;
                isLoading = false;
                if (feedResponse != null && feedResponse.getResponse() != null && feedResponse.getResponse().getCards() != null) {
                    total = feedResponse.getResponse().getTotal();
                    setDataToAdapter(feedResponse.getResponse().getCards(), false);
                }
                break;
            case AppConstants.TASK_CODES.USER_FEED_MORE:
                feedResponse = (FeedResponse) response;
                isLoading = false;
                if (feedResponse != null && feedResponse.getResponse() != null && feedResponse.getResponse().getCards() != null) {
                    total = feedResponse.getResponse().getTotal();
                    setDataToAdapter(feedResponse.getResponse().getCards(), true);
                }
                break;
            case AppConstants.TASK_CODES.WITHDRAW_RESIGNATION:
            case AppConstants.TASK_CODES.WITHDRAW_LEAVE:
            case AppConstants.TASK_CODES.WITHDRAW_POSITION:
            case AppConstants.TASK_CODES.ACCEPT_LEAVE:
            case AppConstants.TASK_CODES.ACCEPT_POSITION:
            case AppConstants.TASK_CODES.ACCEPT_RESIGNATION:
            case AppConstants.TASK_CODES.ACCEPT_TERMINATION:
            case AppConstants.TASK_CODES.REJECT_LEAVE:
            case AppConstants.TASK_CODES.REJECT_POSITION:
            case AppConstants.TASK_CODES.REJECT_RESIGNATION:
            case AppConstants.TASK_CODES.REJECT_TERMINATION:
            default: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (!TextUtils.isEmpty(baseResponse.getMsg())) {
                    showToast(baseResponse.getMsg());
                }
                refreshData();
            }
        }
    }

    @Override
    public void refreshData() {
        onRefresh();
    }

    private void setDataToAdapter(List<Card> cards, boolean isAppend) {
        if (!isAppend) {
            list.clear();
            addPunchInHeaderView();
        }
        list.addAll(cards);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_user_feed;
    }

    @Override
    public void onRefresh() {
        page = 1;
        swipeRefreshLayout.setRefreshing(false);
        fetchUserFeeds(page);
    }

    private void goToLocation(String uriString) {
        if (!TextUtils.isEmpty(uriString)) {
            Uri urii = Uri.parse(uriString);
            Intent intent = new Intent(Intent.ACTION_VIEW, urii);
            startActivity(intent);
        }
    }

    public void takeActionOnFeed(FeedAction feedAction) {
        int actionId = feedAction.getActionId();
        switch (actionId) {
            case FeedAction.ACTION_IDS.RESPOND_SMILEY:
                respondToSmiley(feedAction);
                break;
            case FeedAction.ACTION_IDS.RESPOND_STAR:
                respondToStar(feedAction);
                break;
            case FeedAction.ACTION_IDS.SEE_LOCATION:
                goToLocation(feedAction.getAction().getLocationUriToOpenMap());
                break;
            default: {
                HttpParamObject obj = feedAction.getHttpObjectBasedOnAction();
                if (obj == null) {
                    showToast("Action not integrated yet...");
                    return;
                }
                executeTask(obj.getTaskCode(), obj);
            }
            break;
        }

    }

    private void respondToStar(FeedAction feedAction) {
        FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.GIVE_STAR, getBundleToRepond(feedAction), AppConstants.REQUEST_CODES.GIVE_STAR, this);
    }

    private void respondToSmiley(FeedAction feedAction) {
        FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.GIVE_SMILEY, getBundleToRepond(feedAction), AppConstants.REQUEST_CODES.GIVE_SMILEY, this);
    }

    private Bundle getBundleToRepond(FeedAction feedAction) {
        Card card = feedAction.getCard();
        String name = card.getInitiator().getName();
        int id = card.getInitiator().getId();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, id);
        bundle.putString(AppConstants.BUNDLE_KEYS.USER_NAME, name);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.IS_UPVOTE_STAR, true);
        bundle.putInt(AppConstants.BUNDLE_KEYS.APPLICATION_ID, card.getApplicationId());
        return bundle;
    }

    private void onMapClicked(FusedLocationService.MyLocation location) {
        Bundle bundle = new Bundle();
        bundle.putString("ToPunch", "Adapter");
        Intent i = new Intent(getActivity(), PunchInOutActivity.class);
        i.putExtras(bundle);
        startActivityForResult(i, AppConstants.REQUEST_CODES.MARK_PUNCH);
    }


    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.ACTION_FEED:
                takeActionOnFeed((FeedAction) model);
                break;
            case AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE:
                if (onUserLogoClickListener != null) {
                    onUserLogoClickListener.openEmployeeProfile((Integer) model);
                }
                break;
            case AppConstants.ACTION_TYPE.MAP_HEADER_CLICK:
                onMapClicked((FusedLocationService.MyLocation) model);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.MARK_PUNCH:
                checkPermissionForFileStorage();
                onRefresh();
                break;
            default:
                onRefresh();
        }
    }
}
