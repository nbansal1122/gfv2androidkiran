package com.growthfile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;
import com.growthfile.ui.FragmentContainerActivity;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.DateUtils;

/**
 * Created by my on 19-11-2016.
 */

public class ResignCalenderFragment extends AppBaseFragment {

    private CaldroidFragment caldroidFragment;

    private RelativeLayout resignButton;
    private RelativeLayout calenderContainer;
    private TextView dateResign;
    private String apiDate;
    private Date prevDate;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        initProgressBar();

        resignButton = (RelativeLayout) findView(R.id.rl_apply_for_resign);
        dateResign = (TextView) findView(R.id.tv_selected_Date_for_resign);
        calenderContainer = (RelativeLayout) findView(R.id.calendar_container_resign);

        resignButton.setOnClickListener(this);

        setCalendar();
    }

    private void setCalendar() {
        Date submittedResignDate = null;
        if (getArguments() != null) {
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if (selfProfileResponsePojo != null && selfProfileResponsePojo.getProfile() != null) {
                String terminationDate = selfProfileResponsePojo.getProfile().getDateOfTermination();
                try {
                    submittedResignDate = DateUtils.parseDate(terminationDate, "yyyy-MM-dd");
                } catch (ParseException pe) {

                }
            }
        }

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        final Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidCustomDefault);
        caldroidFragment.setArguments(args);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.calendar_container_resign, caldroidFragment);
        fragmentTransaction.commit();

        caldroidFragment.refreshView();
        caldroidFragment.setTextColorForDate(R.color.colorAccent, cal.getTime());
        if (submittedResignDate != null) {
            caldroidFragment.setSelectedDate(submittedResignDate);
        }
        caldroidFragment.setCaldroidListener(new CaldroidListener() {

            private SimpleDateFormat uiDateFormat = new SimpleDateFormat("dd MMM,yyyy");
            private SimpleDateFormat apiDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            @Override
            public void onSelectDate(Date date, View view) {
                caldroidFragment.refreshView();
                if(prevDate != null){
                    caldroidFragment.clearBackgroundDrawableForDate(prevDate);
                    caldroidFragment.clearTextColorForDate(prevDate);
                }
                prevDate = date;
                caldroidFragment.clearSelectedDates();
//                caldroidFragment.setTextColorForDate(R.color.colorAccent, cal.getTime());
                apiDate = apiDateFormat.format(date);
                String uiDate = uiDateFormat.format(date);
                caldroidFragment.setTextColorForDate(R.color.cyan_text, date);
                caldroidFragment.setBackgroundDrawableForDate(ContextCompat.getDrawable(getActivity(), R.drawable.date_selected_bg), date);
                dateResign.setText(uiDate);
            }
        });
        caldroidFragment.setMinDate(cal.getTime());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_apply_for_resign: {
                if (apiDate == null) {
                    showToast("Please select a date.");
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.DATE_API_FORMAT, apiDate);
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.SUBMIT_RESIGN_FRAGMENT, bundle, AppConstants.REQUEST_CODES.SUBMIT_RESIGN, this);
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.view_resign_calender;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.SUBMIT_RESIGN: {
                getActivity().finish();
            }
        }
    }
}
