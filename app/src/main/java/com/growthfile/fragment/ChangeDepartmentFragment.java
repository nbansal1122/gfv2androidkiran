package com.growthfile.fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.rest.responses.Department;
import simplifii.framework.rest.responses.GetDepartmentsResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;

/**
 * Created by robin on 11/14/16.
 */
public class ChangeDepartmentFragment extends AppBaseFragment implements TextWatcher, CustomListAdapterInterface {
    private EditText searchView;
    private LinearLayout llClearSearch;
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private List<Department> filteredList;
    private List<Department> completeList;
    private RelativeLayout rlBtnSave;
    private int selectedPosition = -1;
    private TextView tvToolbarTitle;
    private Integer departmentId;
    private Integer userId;
    private RelativeLayout rlEmptyView;
    private RelativeLayout rlListViewContainer;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        tvToolbarTitle = (TextView) findView(R.id.tv_toolbar_title);
        tvToolbarTitle.setText(getString(R.string.change_department));
        initProgressBar();

        searchView = (EditText) findView(R.id.et_local_search);
        llClearSearch = (LinearLayout) findView(R.id.ll_clear_search);
        rlBtnSave = (RelativeLayout) findView(R.id.rl_save_btn);
        searchView.setTextColor(getResourceColor(R.color.black));
        listView = (ListView) findView(R.id.list);
        filteredList = new ArrayList<>();
        completeList = new ArrayList<>();
//        listAdapter = new ManagerFilterAdapter(getActivity(),R.layout.search_row,contactList, filteredList,this);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.search_row,filteredList,this);
        listView.setAdapter(customListAdapter);
        rlEmptyView = (RelativeLayout) findView(R.id.empty);
        rlListViewContainer = (RelativeLayout) findView(R.id.rl_list_view);
        hideListView();

        llClearSearch.setOnClickListener(this);
        searchView.addTextChangedListener(this);
        rlBtnSave.setOnClickListener(this);

        if(getArguments()!=null){
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if(selfProfileResponsePojo!=null){
                if(selfProfileResponsePojo.getDepartment()!=null){
                    departmentId = selfProfileResponsePojo.getDepartment().getDepartmentId();
                }
                if(selfProfileResponsePojo.getProfile()!=null){
                    userId = selfProfileResponsePojo.getProfile().getId();
                }
            }
        }
        fetchDepartments();
    }

    private void hideListView() {
        rlListViewContainer.setVisibility(View.GONE);
        rlEmptyView.setVisibility(View.VISIBLE);
    }

    private void showListView() {
        rlListViewContainer.setVisibility(View.VISIBLE);
        rlEmptyView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_clear_search:
                searchView.setText("");
                break;
            case R.id.rl_save_btn:
                if(selectedPosition==-1){
                    showToast("Please select a department...");
                    break;
                }
                saveDepartment();
                break;
        }
    }

    private void saveDepartment() {
        Department selectedDepartment = filteredList.get(selectedPosition);
        if(selectedDepartment.getId()==departmentId){
            showToast("Please select a different department");
            return;
        }
        HttpParamObject httpParamObject = ApiRequestGenerator.editEmployeeDetailsRequest(userId,selectedDepartment.getId(),null,null,null,null);
        executeTask(AppConstants.TASK_CODES.EDIT_PROFILE, httpParamObject);
    }


    private void fetchDepartments() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getDepartmentsRequest();
        executeTask(AppConstants.TASK_CODES.GET_DEPARTMENTS, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if(taskCode==AppConstants.TASK_CODES.GET_DEPARTMENTS){
            hideListView();
        }
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_DEPARTMENTS:{
                GetDepartmentsResponse getDepartmentsResponse = (GetDepartmentsResponse) response;
                if(getDepartmentsResponse!=null){
                    if(getDepartmentsResponse.getResponse()!=null&& CollectionsUtils.isNotEmpty(getDepartmentsResponse.getResponse().getDepartmentList())){
                        List<Department> departmentList = getDepartmentsResponse.getResponse().getDepartmentList();
                        completeList.clear();
                        for(Department department : departmentList){
                            if(department.getIsActive()==1){
                                department.setChecked(false);
                                completeList.add(department);
                            }
                        }
                        filteredList.clear();
                        filteredList.addAll(completeList);
                        customListAdapter.notifyDataSetChanged();
                        showListView();
                    } else {

                    }
                }
            }
            break;
            case AppConstants.TASK_CODES.EDIT_PROFILE:{
                BaseResponse baseResponse = (BaseResponse) response;
                if(baseResponse!=null){
                    if(!TextUtils.isEmpty(baseResponse.getMsg()))
                        showToast(baseResponse.getMsg());
                    getActivity().setResult(getActivity().RESULT_OK);
                    getActivity().finish();
                }
            }
            break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_from;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if(convertView==null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Department department = filteredList.get(position);

        if(!TextUtils.isEmpty(department.getName())){
            holder.tvTitle.setText(department.getName());
            holder.ivImage.setVisibility(View.GONE);
            holder.tvInitials.setVisibility(View.VISIBLE);
            holder.tvInitials.setText(String.valueOf(department.getName().charAt(0)).toString().toUpperCase());
        } else {
            holder.tvTitle.setText("");
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.tvInitials.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(Department dept : completeList){
                    dept.setChecked(false);
                }
                for(Department dept : filteredList){
                    dept.setChecked(false);
                }
                department.setChecked(true);
                selectedPosition = position;
                customListAdapter.notifyDataSetChanged();
            }
        });

        if(department.isChecked()){
            holder.ivCheck.setVisibility(View.VISIBLE);
        } else {
            holder.ivCheck.setVisibility(View.GONE);
        }

        return convertView;
    }

    class Holder {
        TextView tvTitle,tvSubtitle,tvInitials;
        ImageView ivImage, ivCheck;

        public Holder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_team_titlename);
            tvSubtitle = (TextView) view.findViewById(R.id.tv_team_phone);
            ivImage = (ImageView) view.findViewById(R.id.img_team_picadd);
            ivCheck = (ImageView) view.findViewById(R.id.iv_tick);
            tvInitials = (TextView) view.findViewById(R.id.tv_initials);

            tvSubtitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        if(TextUtils.isEmpty(s)){
            filteredList.clear();
            filteredList.addAll(completeList);
        } else {
            Iterator<Department> iterator = filteredList.iterator();
            while (iterator.hasNext()){
                Department department = iterator.next();
                if(!department.getName().toLowerCase().contains(s.toString().toLowerCase())){
                    iterator.remove();
                }
            }
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
