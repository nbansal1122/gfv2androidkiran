package com.growthfile.fragment;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.rest.responses.Center;
import simplifii.framework.rest.responses.CostCenter;
import simplifii.framework.rest.responses.Department;
import simplifii.framework.utility.AppConstants;

/**
 * Created by my on 12-11-2016.
 */

public class EditRoleFragment extends AppBaseFragment{

    private TextInputLayout til_designation, til_role_type, til_cost_center, til_office, til_department, til_description;
    private RelativeLayout rl_cross, rl_save_btn;
    private FrameLayout frag_container, fl_costCenter;
    private EditText et_cost;
    private Department selectedDepartment;
    private Center selectedCenter;
    private CostCenter selectedCostCenter;
    private Integer userId;
    private Integer departmentId;
    private Integer centerId;
    private Integer costCenterId;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        TextView tvToolbarTitle = (TextView) findView(R.id.tv_toolbar_title);
        tvToolbarTitle.setText(getString(R.string.change_edit_a_role));

        initProgressBar();

        findViews();

        if(getArguments()!=null){
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if(selfProfileResponsePojo!=null){
                if(selfProfileResponsePojo.getProfile()!=null){
                    userId = selfProfileResponsePojo.getProfile().getId();
                    String designation = selfProfileResponsePojo.getProfile().getDesignation();
                    if(!TextUtils.isEmpty(designation)){
                        til_designation.getEditText().setText(designation);
                    }
                    String userRole = selfProfileResponsePojo.getProfile().getUserRole();
                    if(!TextUtils.isEmpty(userRole)){
                        til_role_type.getEditText().setText(userRole);
                    }
                }
                if(selfProfileResponsePojo.getDepartment()!=null){
                    departmentId = selfProfileResponsePojo.getDepartment().getDepartmentId();
                    String deptName = selfProfileResponsePojo.getDepartment().getDepartmentName();
                    if(!TextUtils.isEmpty(deptName)){
                        til_department.getEditText().setText(deptName);
                    }
                }
                if(selfProfileResponsePojo.getCenter()!=null){
                    centerId = selfProfileResponsePojo.getCenter().getCenterId();
                    String centerName = selfProfileResponsePojo.getCenter().getCode();
                    if(!TextUtils.isEmpty(centerName)){
                        til_office.getEditText().setText(centerName);
                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
//                removeFragment();
        }
        return true;
    }

    private void findViews() {
        rl_cross = (RelativeLayout) findView(R.id.rel_applyleavecard_cross);
        rl_save_btn = (RelativeLayout) findView(R.id.rl_save_btn);
        til_designation = (TextInputLayout) findView(R.id.til_designation);
        til_role_type = (TextInputLayout) findView(R.id.til_role_type);
        til_cost_center = (TextInputLayout) findView(R.id.til_cost_center);
        til_office = (TextInputLayout) findView(R.id.til_office);
        til_department = (TextInputLayout) findView(R.id.til_department);
        til_description = (TextInputLayout) findView(R.id.til_description);
        setOnClickListener(R.id.rl_save_btn, R.id.et_cost_center, R.id.et_office, R.id.et_department);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_save_btn:
                validateData();
                break;
//            case R.id.fl_cost_center: {
//                ChangeReportingManagerFragment changeReportingManagerFragment = new ChangeReportingManagerFragment();
//                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
//                fragmentTransaction.addToBackStack("");
//                fragmentTransaction.add(R.id.location_frag_container, changeReportingManagerFragment, null);
//                fragmentTransaction.commit();
//            }
//            break;
            case R.id.et_department:{
                FragmentContainerActivity.startActivityForResult(getActivity(),AppConstants.FRAGMENT_TYPE.SELECT_DEPARTMENT,null, AppConstants.REQUEST_CODES.SELECT_DEPARTMENT,this);
            }
            break;
            case R.id.et_office:{
                FragmentContainerActivity.startActivityForResult(getActivity(),AppConstants.FRAGMENT_TYPE.SELECT_CENTER,null, AppConstants.REQUEST_CODES.SELECT_CENTER,this);
            }
            break;
            case R.id.et_cost_center:{
                FragmentContainerActivity.startActivityForResult(getActivity(),AppConstants.FRAGMENT_TYPE.SELECT_COST_CENTER,null, AppConstants.REQUEST_CODES.SELECT_COST_CENTER,this);
            }
            break;
        }
    }

    private void validateData() {
        String designation = til_designation.getEditText().getText().toString().trim();
        String roleType = til_role_type.getEditText().getText().toString().trim();
        String costCenter = til_cost_center.getEditText().getText().toString().trim();
        String office = til_office.getEditText().getText().toString().trim();
        String department = til_department.getEditText().getText().toString().trim();
        String description = til_description.getEditText().getText().toString().trim();

//        if (TextUtils.isEmpty(designation)) {
//            showToast(getString(R.string.designation_not_empty));
//            return;
//        }
//        if (TextUtils.isEmpty(roleType)) {
//            showToast(getString(R.string.role_not_empty));
//            return;
//        }
//        if (TextUtils.isEmpty(costCenter)) {
//            showToast(getString(R.string.cost_not_empty));
//            return;
//        }
//        if (TextUtils.isEmpty(office)) {
//            showToast(getString(R.string.office_not_empty));
//            return;
//        }
//        if (TextUtils.isEmpty(department)) {
//            showToast(getString(R.string.department_not_empty));
//            return;
//        }
//        if (TextUtils.isEmpty(description)) {
//            showToast(getString(R.string.desc_not_empty));
//            return;
//        }
        HttpParamObject httpParamObject = ApiRequestGenerator.editEmployeeDetailsRequest(userId,departmentId,costCenterId,description,centerId,roleType);
        executeTask(AppConstants.TASK_CODES.EDIT_PROFILE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.EDIT_PROFILE:{
                BaseResponse baseResponse = (BaseResponse) response;
                if(baseResponse!=null){
                    if(!TextUtils.isEmpty(baseResponse.getMsg()))
                        showToast(baseResponse.getMsg());
                    getActivity().setResult(getActivity().RESULT_OK);
                    getActivity().finish();
                }
            }
            break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_create_role;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=getActivity().RESULT_OK){
            return;
        }
        switch (requestCode){
            case AppConstants.REQUEST_CODES.SELECT_DEPARTMENT:{
                selectedDepartment = (Department) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_DEPARTMENT);
                if(selectedDepartment!=null){
                    til_department.getEditText().setText(selectedDepartment.getName());
                    departmentId = selectedDepartment.getId();
                }
                break;
            }
            case AppConstants.REQUEST_CODES.SELECT_CENTER:{
                selectedCenter = (Center) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_CENTER);
                if(selectedCenter!=null){
                    til_office.getEditText().setText(selectedCenter.getCode());
                    centerId = selectedCenter.getId();
                }
                break;
            }
            case AppConstants.REQUEST_CODES.SELECT_COST_CENTER:{
                selectedCostCenter = (CostCenter) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_COST_CENTER);
                if(selectedCostCenter!=null){
                    til_cost_center.getEditText().setText(selectedCostCenter.getCcName());
                    costCenterId = selectedCostCenter.getCcId();
                }
                break;
            }
        }
    }
}
