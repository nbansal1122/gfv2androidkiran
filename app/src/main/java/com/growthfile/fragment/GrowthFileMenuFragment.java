package com.growthfile.fragment;

import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.Pojo.SelfProfileUserPermissionPojo;
import com.growthfile.Pojo.SelfProfileUserReportPojo;
import com.growthfile.R;
import com.growthfile.ui.FragmentContainerActivity;

import simplifii.framework.fragments.BaseDialogFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by robin on 11/14/16.
 */

public class GrowthFileMenuFragment extends BaseDialogFragment {

    private static GrowthFileMenuFragment runningInstance;

    private OnProfileChangeListener onProfileChangeListener;

    private SelfProfileResponsePojo selfProfileResponsePojo;

    public GrowthFileMenuFragment() {
        runningInstance = this;
    }

    public static GrowthFileMenuFragment getRunningInstance() {
        return runningInstance;
    }

    public void setOnProfileChangeListener(OnProfileChangeListener onProfileChangeListener) {
        this.onProfileChangeListener = onProfileChangeListener;
    }

    public void setSelfProfileResponsePojo(SelfProfileResponsePojo selfProfileResponsePojo) {
        this.selfProfileResponsePojo = selfProfileResponsePojo;
    }

    @Override
    public void initViews() {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.TOP | Gravity.RIGHT);

        LinearLayout llEditProfile = (LinearLayout) findView(R.id.ll_edit_profile);
        if (selfProfileResponsePojo != null && selfProfileResponsePojo.getPermissions() != null) {
            SelfProfileUserPermissionPojo permission = selfProfileResponsePojo.getPermissions();
            hideVisibilityOrSetListener(R.id.ll_edit_profile, permission.getCanEditProfile());
            hideVisibilityOrSetListener(R.id.ll_change_number, permission.getCanEditMobile());
            boolean isAllowedToChange = isAllowedToChanged();
            hideVisibilityOrSetListener(R.id.ll_change_reporting_manager, isAllowedToChange);
            hideVisibilityOrSetListener(R.id.ll_change_designation, isAllowedToChange);
            hideVisibilityOrSetListener(R.id.ll_change_department, isAllowedToChange);
            hideVisibilityOrSetListener(R.id.ll_change_cost_center, isAllowedToChange);
            //ToDo need to implement transfer
            hideVisibilityOrSetListener(R.id.ll_transfer, false);
            //
            hideVisibilityOrSetListener(R.id.ll_change_edit_role, permission.getCanEditRole());
            hideVisibilityOrSetListener(R.id.ll_resign, permission.getCanResign());
        }


//        if(selfProfileResponsePojo!=null&&selfProfileResponsePojo.getPermissions()!=null){
//        }

//        setOnClickListener(R.id.ll_edit_profile, R.id.ll_change_number, R.id.ll_change_reporting_manager, R.id.ll_transfer,
//                R.id.ll_change_designation, R.id.ll_change_department, R.id.ll_change_cost_center, R.id.ll_resign, R.id.ll_change_edit_role);

    }

    private boolean isAllowedToChanged() {
        boolean flag = false;
        if (selfProfileResponsePojo != null && selfProfileResponsePojo.getReportsTo() != null) {
            SelfProfileUserReportPojo pojo = selfProfileResponsePojo.getReportsTo();
            if (pojo.getUserId() != null) {
                int currentUserId = Preferences.getData(Preferences.USER_ID, -1);
                int reportingUserId = pojo.getUserId();
                int responseId = selfProfileResponsePojo.getProfile().getId();
                if (currentUserId == reportingUserId) {
                    return true;
                }
            }
        }
        return flag;
    }

    private void hideVisibilityOrSetListener(int id, boolean isAllowed) {
        if (isAllowed) {
            findView(id).setVisibility(View.VISIBLE);
            setOnClickListener(id);
        } else {
            findView(id).setVisibility(View.GONE);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.custom_view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_edit_profile:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.EDIT_PROFILE, getArguments(), AppConstants.REQUEST_CODES.EDIT_PROFILE, this);
                break;
            case R.id.ll_change_number:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_MOBILE_NUMBER, getArguments(), AppConstants.REQUEST_CODES.CHANGE_NUMBER, this);
                break;
            case R.id.ll_change_reporting_manager:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_REPORTING_MANAGER, getArguments(), AppConstants.REQUEST_CODES.CHANGE_REPORTING_MANAGER, this);
                break;
            case R.id.ll_transfer:
//                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.TRANSFER, getArguments());
                break;
            case R.id.ll_change_designation:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_DESIGNATION, getArguments(), AppConstants.REQUEST_CODES.CHANGE_DESIGNATION, this);
                break;
            case R.id.ll_change_department:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_DEPARTMENT, getArguments(), AppConstants.REQUEST_CODES.CHANGE_DEPARTMENT, this);
                break;
            case R.id.ll_change_cost_center:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_COST_CENTER, getArguments(), AppConstants.REQUEST_CODES.CHANGE_COST_CENTER, this);
                break;
            case R.id.ll_resign:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.RESIGN, getArguments(), AppConstants.REQUEST_CODES.PUT_RESIGN, this);
                break;
            case R.id.ll_change_edit_role:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CHANGE_EDIT_ROLE, getArguments(), AppConstants.REQUEST_CODES.CHANGE_EDIT_ROLE, this);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.EDIT_PROFILE:
            case AppConstants.REQUEST_CODES.CHANGE_NUMBER:
            case AppConstants.REQUEST_CODES.CHANGE_REPORTING_MANAGER:
            case AppConstants.REQUEST_CODES.CHANGE_DESIGNATION:
            case AppConstants.REQUEST_CODES.CHANGE_DEPARTMENT:
            case AppConstants.REQUEST_CODES.CHANGE_COST_CENTER:
            case AppConstants.REQUEST_CODES.PUT_RESIGN:
            case AppConstants.REQUEST_CODES.CHANGE_EDIT_ROLE:
            default: {
                if (onProfileChangeListener != null) {
                    onProfileChangeListener.refreshGrowthFile();
                    this.dismiss();
                }
            }
        }
    }

    public interface OnProfileChangeListener {
        void refreshGrowthFile();
    }
}
