package com.growthfile.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import simplifii.framework.model.CreateRoleModel;
import com.growthfile.utility.ApiRequestGenerator;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by Neeraj Yadav on 11/11/2016.
 */

public class AssignRoleFragment extends BaseFragment {

    private CreateRoleModel roleToAssign;

    private TextView tvRoleDesignation;
    private EditText etRoleType, etCostCenter, etOffice, etDepartment;

    private TextInputLayout tilAssigneeName, tilAssigneeEmail, tilAssigneeNumber;
    private EditText etJoiningDate;
    private String joiningDate;
    private ImageView ivChooseDate;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        tvRoleDesignation = (TextView) findView(R.id.tv_designation);
        etRoleType = (EditText) findView(R.id.et_created_role);
        etCostCenter = (EditText) findView(R.id.et_created_cost_center);
        etOffice = (EditText) findView(R.id.et_created_office);
        etDepartment = (EditText) findView(R.id.et_created_department);

        tilAssigneeName = (TextInputLayout) findView(R.id.til_assign_to);
        tilAssigneeEmail =(TextInputLayout) findView(R.id.til_assignee_email);
        tilAssigneeNumber = (TextInputLayout) findView(R.id.til_assignee_number);
        etJoiningDate = (EditText) findView(R.id.et_date_of_joining);
        ivChooseDate = (ImageView) findView(R.id.iv_choose_date);

        RelativeLayout rlBtnAssignRole = (RelativeLayout) findView(R.id.rl_btn_assign_role);

        if(getArguments()!=null){
            roleToAssign = (CreateRoleModel) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.CREATE_ROLE_DATA);
            if(roleToAssign!=null){
                tvRoleDesignation.setText(roleToAssign.getDesignation());
                etRoleType.setText("");
                etCostCenter.setText("");
                etOffice.setText("");
                etDepartment.setText("");
            }
        }

        setOnClickListener(R.id.rl_btn_assign_role, R.id.iv_choose_date);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return true;
    }

    private void removeFragment() {
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.remove(this);
//        getChildFragmentManager().popBackStack();
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_btn_assign_role:{
                validateAndAssign();
            }
            break;
            case R.id.iv_choose_date:{
                showDatePicker();
            }
        }
    }

    private void showDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            private final SimpleDateFormat uiDateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
            private final SimpleDateFormat apiDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                calendar.set(year, month, date);
                etJoiningDate.setText(uiDateFormatter.format(calendar.getTime()));
                joiningDate = apiDateFormatter.format(calendar.getTime());
            }
        }, year, month, day);

//        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    private void validateAndAssign() {
        String name = tilAssigneeName.getEditText().getText().toString().trim();
        String number = tilAssigneeNumber.getEditText().getText().toString().trim();
        String email = tilAssigneeEmail.getEditText().getText().toString().trim();

        if(TextUtils.isEmpty(name)){
            tilAssigneeName.setError(getString(R.string.cannot_empty));
            return;
        } else {
            tilAssigneeName.setError(null);
        }
        if(TextUtils.isEmpty(number)){
            tilAssigneeNumber.setError(getString(R.string.cannot_empty));
            return;
        } else if(number.length()!=10){
            tilAssigneeNumber.setError(getString(R.string.error_invalid_mobile));
            return;
        } else {
            tilAssigneeNumber.setError(null);
        }
        if(TextUtils.isEmpty(email)){
            tilAssigneeEmail.setError(getString(R.string.cannot_empty));
            return;
        } else if(!Util.isValidEmail(email)){
            tilAssigneeEmail.setError(getString(R.string.error_invalid_email));
        } else {
            tilAssigneeEmail.setError(null);
        }
        if(TextUtils.isEmpty(joiningDate)){
            showToast(getString(R.string.error_invalid_date));
            showDatePicker();
            return;
        }
//        removeFragment();
        HttpParamObject httpParamObject = ApiRequestGenerator.mapEmployeeToPosition(roleToAssign.getRoleId(),joiningDate,email,Long.parseLong(number),name);
        executeTask(AppConstants.TASK_CODES.MAP_EMPLOYEE,httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            switch (taskCode){
                case AppConstants.TASK_CODES.MAP_EMPLOYEE:{
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_assign_role;
    }

    public interface AssignRoleListener{
        void onAssignRole();
    }
}
