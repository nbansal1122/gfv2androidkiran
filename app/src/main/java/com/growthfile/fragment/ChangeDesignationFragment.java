package com.growthfile.fragment;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.growthfile.R;

/**
 * Created by robin on 11/14/16.
 */
public class ChangeDesignationFragment extends AppBaseFragment {

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        initProgressBar();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_create_role;
    }
}
