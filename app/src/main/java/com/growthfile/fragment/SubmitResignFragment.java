package com.growthfile.fragment;

import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;

import java.text.ParseException;
import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.DateUtils;
import simplifii.framework.utility.Logger;

/**
 * Created by robin on 11/14/16.
 */
public class SubmitResignFragment extends AppBaseFragment{

    private TextView tvResignDay, tvResignMonthYear;
    private EditText etComment;
    private RelativeLayout rlSubmitResign;
    private String apiDate;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        initProgressBar();

        tvResignDay = (TextView) findView(R.id.tv_resign_date);
        tvResignMonthYear = (TextView) findView(R.id.tv_resign_month_year);
        etComment = (EditText) findView(R.id.et_comment);
        rlSubmitResign = (RelativeLayout) findView(R.id.rl_submit_resign);

        if(getArguments()!=null){
            apiDate = getArguments().getString(AppConstants.BUNDLE_KEYS.DATE_API_FORMAT);
            if(!TextUtils.isEmpty(apiDate)){
                try{
                    Date resignDate = DateUtils.parseDate(apiDate,"yyyy-MM-dd");
                    String day = DateUtils.formatDate(resignDate,"dd");
                    tvResignDay.setText(day);
                    String monthYear = DateUtils.formatDate(resignDate,"MMM,yyyy");
                    tvResignMonthYear.setText(monthYear);
                } catch (ParseException pe){
                    Logger.error("dateParsing Error","apiDate = "+apiDate);
                }
            }
        }

        rlSubmitResign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = etComment.getText().toString();
                HttpParamObject httpParamObject = ApiRequestGenerator.resignRequest(apiDate);
                executeTask(AppConstants.TASK_CODES.SUBMIT_RESIGN, httpParamObject);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }

        switch (taskCode){
            case AppConstants.TASK_CODES.SUBMIT_RESIGN:{
                BaseResponse baseResponse = (BaseResponse) response;
                if(baseResponse!=null){
                    if(!TextUtils.isEmpty(baseResponse.getMsg())){
                        showToast(baseResponse.getMsg());
                    }
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_resign;
    }
}
