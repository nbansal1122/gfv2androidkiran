package com.growthfile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.CostCenter;
import simplifii.framework.rest.responses.GetCostCentersResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 11/18/16.
 */

public class SelectCostCenterFragment extends AppBaseFragment implements CustomListAdapterInterface, TextWatcher {

    private TextView tvToolbarTitle;
    private EditText searchView;
    private LinearLayout llClearSearch;
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private List<CostCenter> filteredList;
    private List<CostCenter> completeList;
    private RelativeLayout rlBtnSave;
    private int selectedPosition=-1;
    private RelativeLayout rlEmptyView;
    private RelativeLayout rlListViewContainer;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        tvToolbarTitle = (TextView) findView(R.id.tv_toolbar_title);
        tvToolbarTitle.setText(getString(R.string.select_cost_center));
        initProgressBar();

        searchView = (EditText) findView(R.id.et_local_search);
        llClearSearch = (LinearLayout) findView(R.id.ll_clear_search);
        searchView.setTextColor(getResourceColor(R.color.black));
        rlBtnSave = (RelativeLayout) findView(R.id.rl_save_btn);
        listView = (ListView) findView(R.id.list);
        rlEmptyView = (RelativeLayout) findView(R.id.empty);
        rlListViewContainer = (RelativeLayout) findView(R.id.rl_list_view);
        hideListView();

        filteredList = new ArrayList<>();
        completeList = new ArrayList<>();
//        listAdapter = new ManagerFilterAdapter(getActivity(),R.layout.search_row,contactList, filteredList,this);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.search_row,filteredList,this);
        listView.setAdapter(customListAdapter);

        llClearSearch.setOnClickListener(this);
        searchView.addTextChangedListener(this);
        rlBtnSave.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchDepartments();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_clear_search:
                searchView.setText("");
                break;
            case R.id.rl_save_btn:
                if(selectedPosition==-1){
                    showToast("Please select a center...");
                    break;
                }
                CostCenter selectedCostCenter = filteredList.get(selectedPosition);
                Intent intent = new Intent();
                intent.putExtra(AppConstants.BUNDLE_KEYS.SELECTED_COST_CENTER, selectedCostCenter);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
        }
    }

    private void hideListView() {
        rlListViewContainer.setVisibility(View.GONE);
        rlEmptyView.setVisibility(View.VISIBLE);
    }

    private void showListView() {
        rlListViewContainer.setVisibility(View.VISIBLE);
        rlEmptyView.setVisibility(View.GONE);
    }

    private void fetchDepartments() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getCostCentersRequest();
        executeTask(AppConstants.TASK_CODES.GET_COST_CENTERS, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        hideListView();
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_COST_CENTERS:{
                GetCostCentersResponse getCostCentersResponse = (GetCostCentersResponse) response;
                if(getCostCentersResponse!=null){
                    if(getCostCentersResponse.getResponse()!=null&& CollectionsUtils.isNotEmpty(getCostCentersResponse.getResponse().getCostCenterList())){
                        List<CostCenter> costCenterList = getCostCentersResponse.getResponse().getCostCenterList();
                        completeList.clear();
                        for(CostCenter costCenter : costCenterList){
                            if(costCenter.getIsActive()==1){
                                costCenter.setChecked(false);
                                completeList.add(costCenter);
                            }
                        }
                        filteredList.clear();
                        filteredList.addAll(completeList);
                        customListAdapter.notifyDataSetChanged();
                        showListView();
                    } else {

                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_from;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if(convertView==null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final CostCenter costCenter = filteredList.get(position);

        if(!TextUtils.isEmpty(costCenter.getCcName())){
            holder.tvTitle.setText(costCenter.getCcName());
            String initials = Util.getInitialsFromName(costCenter.getCcName());
            holder.tvInitials.setVisibility(View.VISIBLE);
            holder.ivImage.setVisibility(View.GONE);
            holder.tvInitials.setText(initials);
        } else {
            holder.tvTitle.setText("");
            holder.tvInitials.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.VISIBLE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(CostCenter cc : completeList){
                    cc.setChecked(false);
                }
                for(CostCenter cc : filteredList){
                    cc.setChecked(false);
                }
                costCenter.setChecked(true);
                selectedPosition = position;
                customListAdapter.notifyDataSetChanged();
            }
        });

        if(costCenter.isChecked()){
            holder.ivCheck.setVisibility(View.VISIBLE);
        } else {
            holder.ivCheck.setVisibility(View.GONE);
        }

        return convertView;
    }

    class Holder {
        TextView tvTitle,tvSubtitle,tvInitials;
        ImageView ivImage, ivCheck;

        public Holder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_team_titlename);
            tvSubtitle = (TextView) view.findViewById(R.id.tv_team_phone);
            ivImage = (ImageView) view.findViewById(R.id.img_team_picadd);
            ivCheck = (ImageView) view.findViewById(R.id.iv_tick);
            tvInitials = (TextView) view.findViewById(R.id.tv_initials);
            tvInitials.setVisibility(View.VISIBLE);
            ivImage.setVisibility(View.GONE);
            tvSubtitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        if(TextUtils.isEmpty(s)){
            filteredList.clear();
            filteredList.addAll(completeList);
        } else {
            Iterator<CostCenter> iterator = filteredList.iterator();
            while (iterator.hasNext()){
                CostCenter center = iterator.next();
                if(!center.getCcName().contains(s)){
                    iterator.remove();
                }
            }
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
