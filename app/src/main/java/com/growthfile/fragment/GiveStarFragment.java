package com.growthfile.fragment;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class GiveStarFragment extends AppBaseFragment {

    private TextView tvRecipient, tvRecipientInitials;
    private EditText etComment;
    private int recipientId;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        initProgressBar();

        tvRecipient = (TextView) findView(R.id.tv_recipient);
        tvRecipientInitials = (TextView) findView(R.id.tv_recipient_initials);
        etComment = (EditText) findView(R.id.et_comment);
        RelativeLayout rlSendSmiley = (RelativeLayout) findView(R.id.rl_send_smiley);
        setOnClickListener(R.id.rl_send_star);
        if (getArguments() != null) {
            recipientId = getArguments().getInt(AppConstants.BUNDLE_KEYS.USER_ID);
            String recipientName = getArguments().getString(AppConstants.BUNDLE_KEYS.USER_NAME);
            if (!TextUtils.isEmpty(recipientName)) {
                tvRecipient.setText(recipientName);
                String initials = Util.getInitialsFromName(recipientName);
                tvRecipientInitials.setText(initials);
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_send_star: {
                validateAndSendStar();
            }
            break;
        }
    }

    private void validateAndSendStar() {
        String comment = etComment.getText().toString();
        if (TextUtils.isEmpty(comment)) {
            showToast("Please write a COMMENT");
            etComment.requestFocus();
            return;
        }
        boolean isUpvoteStar = getArguments().getBoolean(AppConstants.BUNDLE_KEYS.IS_UPVOTE_STAR, false);
        if (isUpvoteStar) {
            int applicationId = getArguments().getInt(AppConstants.BUNDLE_KEYS.APPLICATION_ID, -1);
            upvoteStar(applicationId, comment);
            return;
        }
        List<Integer> recipientIds = new ArrayList<>();
        recipientIds.add(recipientId);
        HttpParamObject httpParamObject = ApiRequestGenerator.giveStarRequest(recipientIds, comment);
        executeTask(AppConstants.TASK_CODES.GIVE_STAR, httpParamObject);
    }

    private void upvoteStar(int starId, String comment) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("star_id", starId);
            jsonObject.put("COMMENT", comment);

            HttpParamObject obj = ApiRequestGenerator.getDefaultHttpPost(AppConstants.PAGE_URL.UPVOTE_STAR_REQUEST);
            obj.setJson(jsonObject.toString());
            obj.setClassType(BaseResponse.class);
            obj.setPatchMethod();
            executeTask(AppConstants.TASK_CODES.UPVOTE_STAR, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null) {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GIVE_STAR: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse != null) {
                    if (!TextUtils.isEmpty(baseResponse.getMsg())) {
                        showToast(baseResponse.getMsg());
                    }
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
            case AppConstants.TASK_CODES.UPVOTE_STAR: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse != null) {
                    if (!TextUtils.isEmpty(baseResponse.getMsg())) {
                        showToast(baseResponse.getMsg());
                    }
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_give_star;
    }
}
