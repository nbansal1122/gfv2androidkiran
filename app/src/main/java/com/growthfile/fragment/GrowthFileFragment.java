package com.growthfile.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfilePojo;
import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.Pojo.SelfProfileUserPermissionPojo;
import com.growthfile.Pojo.SelfProfileUserReportPojo;
import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;

import simplifii.framework.model.BaseRecyclerModel;

import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.Helper;

import simplifii.framework.asyncmanager.HttpParamObject;

import com.growthfile.utility.ApiRequestGenerator;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Prefs;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GrowthFileFragment extends AppBaseFragment implements BaseRecyclerAdapter.RecyclerClickListener, GrowthFileMenuFragment.OnProfileChangeListener {

    RecyclerView rw_myprofile;
    MyCalendarCallback mCallback;
    Prefs prefs;
    RelativeLayout rlProgressBar, rel_newmyprofile_offline;
    TextView tv_newprofile_head;
    ImageView ivMore;
    private SelfProfileResponsePojo selfProfileResponsePojo;
    private boolean canSeeMenu = false;
    private boolean selfProfile = true;

    private MyCalendarCallback myCalendarCallback;

    private BaseRecyclerAdapter adapter;
    private List<BaseRecyclerModel> list = new ArrayList<>();
    private Integer userId;

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public GrowthFileFragment() {
        // Required empty public constructor
    }

    public static GrowthFileFragment getInstance(Integer userId) {
        GrowthFileFragment newMyProfile = new GrowthFileFragment();
        newMyProfile.setUserId(userId);
        return newMyProfile;
    }

    public void setMyCalendarCallback(MyCalendarCallback myCalendarCallback) {
        this.myCalendarCallback = myCalendarCallback;
    }

    @Override
    public void initViews() {
        prefs = new Prefs();

        rw_myprofile = (RecyclerView) findView(R.id.rw_myprofile);
        rlProgressBar = (RelativeLayout) findView(R.id.progress_bar);
        rel_newmyprofile_offline = (RelativeLayout) findView(R.id.rel_newmyprofile_offline);
        tv_newprofile_head = (TextView) findView(R.id.tv_newprofile_head);

         /*
        * API FOR SELF PROFILE DATA
         */

        if (Helper.checkInternetConnection(getActivity())) {
            rel_newmyprofile_offline.setVisibility(View.GONE);
            rlProgressBar.setVisibility(View.VISIBLE);
            tv_newprofile_head.setText("Growth File");
//            callSelfProfileApi();
            Integer currentUserId = Preferences.getData(Preferences.USER_ID, -1);
            if (userId != null && userId != currentUserId) {
                selfProfile = false;
            } else {
                selfProfile = true;
            }

            loadGrowthFile();

        } else {
            rlProgressBar.setVisibility(View.GONE);
            rel_newmyprofile_offline.setVisibility(View.VISIBLE);
            tv_newprofile_head.setText("Offline");

        }

        ivMore = (ImageView) findView(R.id.ivMore);

        initRecyclerView();
    }

    private void checkIfAllFalse() {
        boolean flag = false;
        if (selfProfileResponsePojo != null && selfProfileResponsePojo.getPermissions() != null) {
            SelfProfileUserPermissionPojo permissionPojo = selfProfileResponsePojo.getPermissions();
            flag = permissionPojo.getCanEditMobile() || permissionPojo.getCanEditProfile() || permissionPojo.getCanEditRole()
                    || permissionPojo.getCanResign() || isAllowedToChanged();
        }
        if (flag) {
            ivMore.setVisibility(View.VISIBLE);
            ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GrowthFileMenuFragment.getRunningInstance() != null && GrowthFileMenuFragment.getRunningInstance().isVisible()) {
                        GrowthFileMenuFragment.getRunningInstance().dismiss();
                    } else {
                        if (canSeeMenu)
                            showDialog();
                    }
                }
            });
        } else {
            ivMore.setVisibility(View.GONE);
        }
    }

    private boolean isAllowedToChanged() {
        boolean flag = false;
        if (selfProfileResponsePojo != null && selfProfileResponsePojo.getReportsTo() != null) {
            SelfProfileUserReportPojo pojo = selfProfileResponsePojo.getReportsTo();
            if (pojo.getUserId() != null) {
                int currentUserId = Preferences.getData(Preferences.USER_ID, -1);
                int pojoId = pojo.getUserId();
                if (currentUserId == pojo.getUserId()) {
                    return true;
                }
            }
        }
        return flag;
    }

    private void loadGrowthFile() {
        if (selfProfile) {
            callSelfProfileApiWithAsyncTask();
        } else {
            callOtherUserProfileApi(userId);
        }
    }


    private void callOtherUserProfileApi(Integer employeeId) {
        HttpParamObject httpParamObject = ApiRequestGenerator.getOtherUserProfileRequest(employeeId);
        executeTask(AppConstants.TASK_CODES.GET_USER_PROFILE_OTHER, httpParamObject);
    }

    private void initRecyclerView() {
        rw_myprofile.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter(list, getActivity());
        adapter.setListener(this);
        rw_myprofile.setAdapter(adapter);
        addDummyRowHeader();
    }

    private void addDummyRowHeader(){
        BaseRecyclerModel brm = new BaseRecyclerModel();
        brm.setViewType(AppConstants.VIEW_TYPE.USER_PROFILE_HEADER_INFO);
        list.add(brm);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_profile_newui;
    }

    private void callSelfProfileApiWithAsyncTask() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getUserProfileSelf();
        executeTask(AppConstants.TASK_CODES.GET_USER_PROFILE_SELF, httpParamObject);
    }

    public void showDialog() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE, selfProfileResponsePojo);
        GrowthFileMenuFragment growthFileMenuFragment = new GrowthFileMenuFragment();
        growthFileMenuFragment.setArguments(bundle);
        growthFileMenuFragment.setSelfProfileResponsePojo(selfProfileResponsePojo);
        growthFileMenuFragment.setOnProfileChangeListener(this);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        growthFileMenuFragment.show(fragmentTransaction, "");
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPreExecute(int taskCode) {
        rlProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        rlProgressBar.setVisibility(View.GONE);
        if (re != null) {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        rlProgressBar.setVisibility(View.GONE);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        canSeeMenu = true;
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_USER_PROFILE_SELF: {
                SelfProfilePojo result = (SelfProfilePojo) response;
                if (result != null) {
                    selfProfileResponsePojo = result.getResponse();
                    if (selfProfileResponsePojo != null) {
                        checkIfAllFalse();
                        setAdapter(selfProfileResponsePojo);
                        if (selfProfile && selfProfileResponsePojo.getProfile() != null) {
                            Preferences.saveData(Preferences.USER_PROFILE, JsonUtil.toJson(selfProfileResponsePojo.getProfile()));
                        }
                    } else {
                        showToast(result.getMsg());
                    }
                }
            }
            break;
            case AppConstants.TASK_CODES.GET_USER_PROFILE_OTHER: {
                SelfProfilePojo result = (SelfProfilePojo) response;
                if (result != null) {
                    selfProfileResponsePojo = result.getResponse();
                    if (selfProfileResponsePojo != null) {
                        checkIfAllFalse();
                        setAdapter(selfProfileResponsePojo);
                    } else {
                        showToast(result.getMsg());
                    }
                }
            }
            break;
        }
    }

    private void setAdapter(SelfProfileResponsePojo
                                    selfProfileResponsePojo) {
        list.clear();
        list.add(selfProfileResponsePojo);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.CAL_CLICKED: {
                if (myCalendarCallback != null) {
                    myCalendarCallback.changeTabToCalendar();
                }
            }
            break;
            case AppConstants.ACTION_TYPE.GIVE_SMILEY:
                Bundle bundle = (Bundle) model;
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.GIVE_SMILEY, bundle, AppConstants.REQUEST_CODES.GIVE_SMILEY, this);
                break;
            case AppConstants.ACTION_TYPE.GIVE_STAR:
                Bundle bundle1 = (Bundle) model;
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.GIVE_STAR, bundle1, AppConstants.REQUEST_CODES.GIVE_SMILEY, this);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            refreshGrowthFile();
        }
    }

    @Override
    public void refreshGrowthFile() {
        loadGrowthFile();
    }


    public interface MyCalendarCallback {
        void changeTabToCalendar();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (MyCalendarCallback) activity;
    }
}