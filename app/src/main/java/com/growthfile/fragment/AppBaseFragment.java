package com.growthfile.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.growthfile.R;
import com.growthfile.v2.activities.SplashActivityV2;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 11/19/16.
 */

public abstract class AppBaseFragment extends BaseFragment {
    protected RelativeLayout rlProgressBar;
    protected OnUserLogoClickListener onUserLogoClickListener;
    private ViewGroup croutonView;
    private FrameLayout flNoInternet;
    private ImageView ivRetry;

    public void initProgressBar() {
        this.rlProgressBar = (RelativeLayout) findView(R.id.progress_bar);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.profile_back;
    }

    @Override
    public void onPreExecute(int taskCode) {
        showProgressBar();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        hideProgressBar();
        hideNoInternetFrame();
    }

    public void hideNoInternetFrame() {
        if (flNoInternet != null) {
            flNoInternet.setVisibility(View.GONE);
//            if(ivRetry!=null&&ivRetry.getAnimation()!=null){
//                ivRetry.getAnimation().cancel();
//            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        hideProgressBar();
        hideNoInternetFrame();
        if (re != null) {
            switch (re.getHttpStatusCode()) {
                case 401:
                    showCrouton(re.getMessage());
                    logoutUser();
                case 500:
                    showToast("Session expired! Please login again to continue.");
                    logoutUser();
                    break;
                case 403:
                case 400:
                    showCrouton(re.getMessage());
                    break;
                default:
                    handleDefaultError(re, e, taskCode, params);
                    break;
            }
        }
    }

    protected void handleDefaultError(RestException re, Exception e, int taskCode, Object... params) {
        if (TextUtils.isEmpty(re.getMessage())) {
            showCrouton("Internal Server Error!");
        } else {
            showCrouton(re.getMessage());
        }
    }

    private void logoutUser() {
        UserSession.removeCurrentSession();
        startNextActivity(null, SplashActivityV2.class);
        getActivity().finish();
    }

    @Override
    public void showProgressBar() {
        if (rlProgressBar != null) {
            rlProgressBar.setVisibility(View.VISIBLE);
        } else {
            super.showProgressBar();
        }

    }

    @Override
    protected void onInternetException(final int taskCode) {
        showCrouton("Internet not connected...");
        flNoInternet = (FrameLayout) findView(R.id.no_internet_frame);
        if (flNoInternet != null) {
            flNoInternet.setVisibility(View.VISIBLE);
            ivRetry = (ImageView) findView(R.id.iv_retry);
            ivRetry.clearAnimation();
            flNoInternet.findViewById(R.id.ll_reload).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Animation a = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_in);
                    a.setDuration(800);
                    ivRetry.startAnimation(a);
                    if (Util.isConnectingToInternet(getActivity())) {
                        onRetryClicked(view, taskCode);
                    }
                }
            });
        }
    }

    @Override
    public void onRetryClicked(View view, int taskCode) {
        super.onRetryClicked(view, taskCode);
    }

    @Override
    public void hideProgressBar() {
        if (rlProgressBar != null) {
            rlProgressBar.setVisibility(View.GONE);
        } else {
            super.hideProgressBar();
        }
    }

    public void setOnUserLogoClickListener(OnUserLogoClickListener onUserLogoClickListener) {
        this.onUserLogoClickListener = onUserLogoClickListener;
    }

    public interface OnUserLogoClickListener {
        void openEmployeeProfile(Integer userId);
    }

    public void showCrouton(String msg) {
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(4500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(150)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        if (croutonView != null) {
            Crouton crouton = Crouton.makeText(activity, msg, style, croutonView);
            crouton.show();
        } else {
            Crouton.showText(activity, msg, style);
        }
    }

    protected void initCroutonView() {
        setCroutonView(R.id.frame_crouton);
    }

    public void setCroutonView(int viewId) {
        ViewGroup view = (ViewGroup) findView(viewId);
        this.croutonView = view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
