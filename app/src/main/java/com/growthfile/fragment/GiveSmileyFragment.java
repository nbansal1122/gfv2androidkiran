package com.growthfile.fragment;

import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class GiveSmileyFragment extends AppBaseFragment {

    private TextView tvRecipient, tvRecipientInitials;
    private EditText etComment;
    private int recipientId;
    private int applicationID = -1;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);

        tvRecipient = (TextView) findView(R.id.tv_recipient);
        tvRecipientInitials = (TextView) findView(R.id.tv_recipient_initials);
        etComment = (EditText) findView(R.id.et_comment);

        initProgressBar();

        RelativeLayout rlSendSmiley = (RelativeLayout) findView(R.id.rl_send_smiley);
        setOnClickListener(R.id.rl_send_smiley);
        if (getArguments() != null) {
            recipientId = getArguments().getInt(AppConstants.BUNDLE_KEYS.USER_ID);
            String recipientName = getArguments().getString(AppConstants.BUNDLE_KEYS.USER_NAME);
            applicationID = getArguments().getInt(AppConstants.BUNDLE_KEYS.APPLICATION_ID, -1);
            if (!TextUtils.isEmpty(recipientName)) {
                tvRecipient.setText(recipientName);
                String initials = Util.getInitialsFromName(recipientName);
                tvRecipientInitials.setText(initials);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_send_smiley: {
                validateAndSendSmiley();
            }
            break;
        }
    }

    private void validateAndSendSmiley() {
        String comment = etComment.getText().toString();
        HttpParamObject httpParamObject = ApiRequestGenerator.giveSmileyRequest(applicationID, recipientId, comment);
        executeTask(AppConstants.TASK_CODES.GIVE_SMILEY, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GIVE_SMILEY: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (!TextUtils.isEmpty(baseResponse.getMsg())) {
                    showToast(baseResponse.getMsg());
                }
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_give_smiley;
    }
}
