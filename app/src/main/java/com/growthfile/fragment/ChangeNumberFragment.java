package com.growthfile.fragment;

import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;

import com.growthfile.utility.ApiRequestGenerator;

import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 11/14/16.
 */

public class ChangeNumberFragment extends AppBaseFragment {

//    private Prefs prefs;

    private TextView tvCurrentNumber, tvNewNumber;
    private TextView tvError;
    private Integer userId;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        tvCurrentNumber = (TextView) findView(R.id.tv_current_number);
        tvNewNumber = (TextView) findView(R.id.tv_new_number);
        tvError = (TextView) findView(R.id.tv_errorMsg);

        initProgressBar();

        tvError.setVisibility(View.GONE);
        RelativeLayout rlSaveNewNumber = (RelativeLayout) findView(R.id.rl_save_changed_number);

        setOnClickListener(R.id.rl_save_changed_number);
//        prefs = new Prefs();
//        String userJson = prefs.getPreferencesString(getActivity(), Preferences.USER_PROFILE);
        if(getArguments()!=null){
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if(selfProfileResponsePojo!=null&&selfProfileResponsePojo.getProfile()!=null){
                String mobile = selfProfileResponsePojo.getProfile().getMobile();
                if(!TextUtils.isEmpty(mobile)){
                    tvCurrentNumber.setText(mobile);
                } else {
                    tvCurrentNumber.setText("");
                }
                userId = selfProfileResponsePojo.getProfile().getId();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_save_changed_number:
                verifyInputsAndSave();
                break;
        }
    }

    private void verifyInputsAndSave() {
        String newNumber = tvNewNumber.getText().toString().trim();

        if(TextUtils.isEmpty(newNumber)||newNumber.length()!=10){
            tvError.setText(R.string.invalid_number_error);
            tvError.setVisibility(View.VISIBLE);
            return;
        } else {
            tvError.setText("");
            tvError.setVisibility(View.GONE);
        }

        HttpParamObject httpParamObject = ApiRequestGenerator.changeMobileNumberRequest(newNumber, userId);
        executeTask(AppConstants.TASK_CODES.CHANGE_MOBILE_NUMBER, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        rlProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        rlProgressBar.setVisibility(View.GONE);
        if(re!=null){
            showToast(re.getMessage());
        } else {
            showToast(getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        rlProgressBar.setVisibility(View.GONE);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.CHANGE_MOBILE_NUMBER:{
                BaseResponse baseResponse = (BaseResponse) response;
                if(baseResponse!=null){
                    showToast(baseResponse.getMsg());
                    getActivity().setResult(getActivity().RESULT_OK);
                    getActivity().finish();
                }
            }
            break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_change_number;
    }

}
