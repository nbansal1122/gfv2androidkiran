package com.growthfile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.growthfile.R;
import com.growthfile.model.ListCreateRoleModel;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.enums.CreateRoleType;
import simplifii.framework.rest.responses.AddPositionResponse;
import simplifii.framework.rest.responses.CostCenter;
import simplifii.framework.rest.responses.Department;
import simplifii.framework.rest.responses.Office;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 11/10/2016.
 */

public class CreateRoleFragment extends AppBaseFragment {
    private TextInputLayout til_designation, til_role_type, til_cost_center, til_office, til_department, til_description;
    private RelativeLayout rl_cross, rl_save_btn;
    private String designation, role_type, cost_center, office, department, description;
    private FrameLayout frag_container, fl_costCenter;
    private EditText et_cost;
    private Department selectedDepartment;
    private Office selectedCenter;
    private CostCenter selectedCostCenter;

    public static CreateRoleFragment getInstance() {
        return new CreateRoleFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);

        initProgressBar();

        ListCreateRoleModel.init(getActivity());
        findViews();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cross:
                getActivity().finish();
//                removeFragment();
        }
        return true;
    }

    private void findViews() {
        rl_cross = (RelativeLayout) findView(R.id.rel_applyleavecard_cross);
        rl_save_btn = (RelativeLayout) findView(R.id.rl_save_btn);
        til_designation = (TextInputLayout) findView(R.id.til_designation);
        til_role_type = (TextInputLayout) findView(R.id.til_role_type);
        til_cost_center = (TextInputLayout) findView(R.id.til_cost_center);
        til_office = (TextInputLayout) findView(R.id.til_office);
        til_department = (TextInputLayout) findView(R.id.til_department);
        til_description = (TextInputLayout) findView(R.id.til_description);

        setOnClickListener(R.id.rl_save_btn, R.id.et_role_type, R.id.et_cost_center, R.id.et_office, R.id.et_department);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_save_btn:
                validateData();
                break;
//            case R.id.fl_cost_center: {
//                ChangeReportingManagerFragment changeReportingManagerFragment = new ChangeReportingManagerFragment();
//                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
//                fragmentTransaction.addToBackStack("");
//                fragmentTransaction.add(R.id.location_frag_container, changeReportingManagerFragment, null);
//                fragmentTransaction.commit();
//            }
//            break;
            case R.id.et_department: {
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.SELECT_DEPARTMENT, null, AppConstants.REQUEST_CODES.SELECT_DEPARTMENT, this);
            }
            break;
            case R.id.et_office: {
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.SELECT_CENTER, null, AppConstants.REQUEST_CODES.SELECT_CENTER, this);
            }
            break;
            case R.id.et_cost_center: {
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.SELECT_COST_CENTER, null, AppConstants.REQUEST_CODES.SELECT_COST_CENTER, this);
            }
            break;
            case R.id.et_role_type: {
                EditText etRoleType = (EditText) findView(R.id.et_role_type);
                showRoleTypePopup(etRoleType);
            }
            break;
        }
    }

    private void showRoleTypePopup(final EditText etRoleType) {
        final PopupWindow popupWindow = new PopupWindow(getActivity());

        final List<String> allowedRoles = new ArrayList<>();
        for (CreateRoleType createRoleType : CreateRoleType.values()) {
            allowedRoles.add(createRoleType.getValue());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.row_popup, allowedRoles);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View popupView = layoutInflater.inflate(R.layout.custom_role_popup_list, null, false);
        ListView roleTypeListView = (ListView) popupView.findViewById(R.id.lv_role_list);
        roleTypeListView.setAdapter(adapter);

        popupWindow.setContentView(popupView);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);

        popupWindow.showAsDropDown(etRoleType, 0, 0);

        roleTypeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedRole = allowedRoles.get(position);
                popupWindow.dismiss();
                etRoleType.setText(selectedRole);
            }
        });
    }

    private void removeFragment() {
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.remove(this);
//        getChildFragmentManager().popBackStack();
        fragmentTransaction.commit();
    }

    private void clearErrors(TextInputLayout... layouts) {
        for (TextInputLayout layout : layouts) {
            layout.setError("");
        }
    }

    private void validateData() {
        clearErrors(til_cost_center, til_department, til_description, til_office, til_role_type, til_designation);
        designation = til_designation.getEditText().getText().toString().trim();
        role_type = til_role_type.getEditText().getText().toString().trim();
        cost_center = til_cost_center.getEditText().getText().toString().trim();
        office = til_office.getEditText().getText().toString().trim();
        department = til_department.getEditText().getText().toString().trim();
        description = til_description.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(designation)) {
            showToast(getString(R.string.designation_not_empty));
            return;
        }
        if (TextUtils.isEmpty(role_type)) {
            showToast(getString(R.string.role_not_empty));
            return;
        }
        if (TextUtils.isEmpty(cost_center)) {
            showToast(getString(R.string.cost_not_empty));
            return;
        }
        if (TextUtils.isEmpty(office)) {
            showToast(getString(R.string.office_not_empty));
            return;
        }
        if (TextUtils.isEmpty(department)) {
            showToast(getString(R.string.department_not_empty));
            return;
        }
//        if (TextUtils.isEmpty(description)) {
//            showToast(getString(R.string.desc_not_empty));
//            return;
//        }
        saveData();
    }

    private void saveData() {
        Integer costCenterId = null;
        if (selectedCostCenter != null) {
            costCenterId = selectedCostCenter.getCcId();
        }
        Integer centerId = null;
        if (selectedCenter != null) {
            centerId = selectedCenter.getId();
        }
        Integer departmentId = null;
        if (selectedDepartment != null) {
            departmentId = selectedDepartment.getId();
        }

        HttpParamObject httpParamObject = ApiRequestGenerator.addPositionRequest(designation, role_type, description,
                costCenterId, centerId, departmentId);
        executeTask(AppConstants.TASK_CODES.ADD_POSITION, httpParamObject);

    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null) {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.ADD_POSITION: {
                AddPositionResponse addPositionResponse = (AddPositionResponse) response;
                if (addPositionResponse != null) {
                    if (!TextUtils.isEmpty(addPositionResponse.getMsg()))
                        showToast(addPositionResponse.getMsg(), Toast.LENGTH_SHORT);
                    if (addPositionResponse.getResponse() != null && addPositionResponse.getResponse().getPosition() != null) {
                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();
                    }
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_create_role;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.SELECT_DEPARTMENT: {
                selectedDepartment = (Department) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_DEPARTMENT);
                if (selectedDepartment != null) {
                    til_department.getEditText().setText(selectedDepartment.getName());
                }
                break;
            }
            case AppConstants.REQUEST_CODES.SELECT_CENTER: {
                selectedCenter = (Office) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_CENTER);
                if (selectedCenter != null) {
                    til_office.getEditText().setText(selectedCenter.getName());
                }
                break;
            }
            case AppConstants.REQUEST_CODES.SELECT_COST_CENTER: {
                selectedCostCenter = (CostCenter) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.SELECTED_COST_CENTER);
                if (selectedCostCenter != null) {
                    til_cost_center.getEditText().setText(selectedCostCenter.getCcName());
                }
                break;
            }
        }
    }
}
