package com.growthfile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.ImageUploadTask;
import simplifii.framework.utility.Logger;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 11/14/16.
 */

public class EditProfileFragment extends AppBaseFragment {

    private ImageView ivEditProfile, ivUserLogo;
    private RelativeLayout rlSaveBtn;
    private EditText etUserName, etUserEmail;
    private Bitmap userImageBitmap;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);

        initProgressBar();

        ivEditProfile = (ImageView) findView(R.id.iv_edit_image);
        ivUserLogo = (ImageView) findView(R.id.iv_user_logo);
        rlSaveBtn = (RelativeLayout) findView(R.id.rl_save_btn);
        etUserName = (EditText) findView(R.id.et_name);
        etUserEmail = (EditText) findView(R.id.et_email);

        if(getArguments()!=null){
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if(selfProfileResponsePojo!=null&&selfProfileResponsePojo.getProfile()!=null) {
                String name = selfProfileResponsePojo.getProfile().getName();
                if(!TextUtils.isEmpty(name)){
                    etUserName.setText(name);
                }
                String emailId = selfProfileResponsePojo.getProfile().getEmail();
                if(!TextUtils.isEmpty(emailId)){
                    etUserEmail.setText(emailId);
                }
                String avatar = selfProfileResponsePojo.getProfile().getAvatar();
                try{
                    Picasso.with(getActivity()).load(avatar).into(ivUserLogo, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            ivUserLogo.setImageResource(R.drawable.unknown);
                        }
                    });
                } catch (Exception e){
                    ivUserLogo.setImageResource(R.drawable.unknown);
                }
            }
        }

        setOnClickListener(R.id.iv_edit_image, R.id.rl_save_btn);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_edit_image:{
                openImagePicker();
            }
            break;
            case R.id.rl_save_btn:{
                uploadImageAndSave();
            }
            break;
        }
    }

    private void uploadImageAndSave() {
        if(userImageBitmap==null){
            showToast("Please select a new image.");
            return;
        }
        showProgressBar();
        ImageUploadTask imageUploadTask = ImageUploadTask.uploadImage(new ImageUploadTask.UploadListener() {
            @Override
            public void onUpload(String imageUrls) {
                String imageUrl = imageUrls;
                Logger.info("Uploaded ImageUrl",imageUrl);
                hideProgressBar();
                saveImageUrl(imageUrl);
            }

            @Override
            public void onUploadFailed() {
                showToast("Image upload failed");
                hideProgressBar();

            }
        }, userImageBitmap);
        imageUploadTask.execute();
    }

    private void saveImageUrl(String imageUrl) {
        HttpParamObject httpParamObject = ApiRequestGenerator.changeImageUrlRequest(imageUrl);
        executeTask(AppConstants.TASK_CODES.EDIT_PROFILE_IMAGE,httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if(re!=null){
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.EDIT_PROFILE_IMAGE:{
                BaseResponse baseResponse = (BaseResponse) response;
                showToast("Image Uploaded successfully");
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void openImagePicker() {
        ImagePicker.create(this)
                .returnAfterCapture(true) // set whether camera action should return immediate result or not
                .folderMode(true) // folder mode (false by default)
                .imageTitle("Select image") // image selection title
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Talluk")
                // directory name for captured image  ("Camera" folder by default)
                .start(AppConstants.REQUEST_CODES.REQ_PICK_IMAGE); // start image picker activity with request code
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_edit_personal_info;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode){
            case AppConstants.REQUEST_CODES.REQ_PICK_IMAGE:{
                ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
                if (images != null && images.size() == 1) {
                    Log.d(TAG, "Path :" + images.get(0).getPath());
                    Bitmap bMap = getBitmapFromFilePath(images.get(0).getPath());
                    if (bMap != null){
                        userImageBitmap = bMap;
                        ivUserLogo.setImageBitmap(bMap);
                    }
                }
            }
            break;
        }
    }

    private Bitmap getBitmapFromFilePath(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            bitmap = Util.getResizeBitmap(bitmap, 1024);
            return bitmap;
        } catch (Exception e) {
            showToast(getString(R.string.insufficient_memory));
        } catch (OutOfMemoryError e) {
            showToast(R.string.insufficient_memory);
        }
        return null;
    }
}
