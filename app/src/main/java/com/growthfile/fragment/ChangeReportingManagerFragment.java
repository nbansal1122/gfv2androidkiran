package com.growthfile.fragment;

import android.app.Activity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfileResponsePojo;
import com.growthfile.R;
import com.growthfile.utility.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.rest.responses.GetEmployeeResponse;
import simplifii.framework.rest.responses.UserProfile;
import simplifii.framework.utility.AppConstants;

/**
 * Created by my on 11-11-2016.
 */

public class ChangeReportingManagerFragment extends AppBaseFragment implements CustomListAdapterInterface {
    private static final int REQ_ADD_CUSTOM = 2;
    private ListView lv_manager;
    private CustomListAdapter customListAdapter;
    private List<UserProfile> filteredList = new ArrayList<>();
    private EditText searchView;
    private LinearLayout llClearSearch;
    private List<UserProfile> completeList = new ArrayList<>();
    private Integer userId;
    private UserProfile selectedUser;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);

        initProgressBar();

        TextView tvToolbarTitle = (TextView) findView(R.id.tv_toolbar_title);
        tvToolbarTitle.setText(getString(R.string.change_reporting_manager));

        searchView = (EditText) findView(R.id.et_local_search);
        llClearSearch = (LinearLayout) findView(R.id.ll_clear_search);

        searchView.setTextColor(getResourceColor(R.color.black));
        setData();
        lv_manager = (ListView) findView(R.id.list);
        filteredList = new ArrayList<>();
//        listAdapter = new ManagerFilterAdapter(getActivity(),R.layout.search_row,contactList, filteredList,this);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.search_row, filteredList, this);
        lv_manager.setAdapter(customListAdapter);
//        lv_manager.setAdapter(listAdapter);
        llClearSearch.setOnClickListener(this);
        if (getArguments() != null) {
            SelfProfileResponsePojo selfProfileResponsePojo = (SelfProfileResponsePojo) getArguments().getSerializable(AppConstants.BUNDLE_KEYS.USER_PROFILE_RESPONSE);
            if (selfProfileResponsePojo != null) {
                if (selfProfileResponsePojo.getProfile() != null) {
                    userId = selfProfileResponsePojo.getProfile().getId();
                }
            }
        }
        fetchTeamList();
        setOnClickListener(R.id.rl_save_btn);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cross_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cross:
                getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_clear_search:
                searchView.setText("");
                break;
            case R.id.rl_save_btn:
                if (selectedUser == null) {
                    showToast("Please select an employee...");
                    return;
                }
                changeManager(selectedUser.getId());
                break;
        }
    }

    private void setData() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                listAdapter.getFilter().filter(s);
                if (TextUtils.isEmpty(s)) {
                    filteredList.clear();
                    filteredList.addAll(completeList);
                } else {
                    filteredList.clear();
                    filteredList.addAll(completeList);
                    Iterator<UserProfile> iterator = filteredList.iterator();
                    while (iterator.hasNext()) {
                        UserProfile reportingManager = iterator.next();
                        if (!reportingManager.getName().contains(s)) {
                            iterator.remove();
                        }
                    }
                }
                customListAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void fetchTeamList() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getEmployees();
        executeTask(AppConstants.TASK_CODES.GET_EMPLOYEES, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null && "401".equals(re.getHttpStatusCode())) {
            showToast(getString(R.string.something_went_wrong));
        } else {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_EMPLOYEES: {
                GetEmployeeResponse employeeResponse = (GetEmployeeResponse) response;
                if (employeeResponse != null) {
                    if (employeeResponse.getData() != null) {
                        List<UserProfile> list = employeeResponse.getData();
                        completeList.clear();
                        filteredList.clear();
                        completeList.addAll(list);
                        filteredList.addAll(list);
                        customListAdapter.notifyDataSetChanged();
                    } else {

                    }
                }
            }
            break;
            case AppConstants.TASK_CODES.CHANGE_REPORTING_MANAGER:
                BaseResponse res = (BaseResponse) response;
                showToast(res.getMsg());
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
                break;

        }
    }

    private void changeManager(int reportingManagerId) {
        HttpParamObject obj = ApiRequestGenerator.getDefaultHttpPost(AppConstants.PAGE_URL.CHANGE_REPORTING_MANAGER);
        obj.setJson(getJsonToChangeManager(userId, reportingManagerId));
        executeTask(AppConstants.TASK_CODES.CHANGE_REPORTING_MANAGER, obj);
    }

    //    {
//        "employee_id": 5, === Current Employee ID
//            "reports_to": 6 == Selected ID
//    }
    private String getJsonToChangeManager(int empId, int reportingManagerId) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("employee_id", empId);
            obj.put("reports_to", reportingManagerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_from;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final UserProfile userProfile = filteredList.get(position);
        if (!TextUtils.isEmpty(userProfile.getAvatar())) {
            Picasso.with(getActivity()).load(userProfile.getAvatar()).into(holder.ivImage);
        } else {
            holder.ivImage.setImageResource(R.drawable.user);
        }

        if (!TextUtils.isEmpty(userProfile.getName())) {
            holder.tvTitle.setText(userProfile.getName());
        } else {
            holder.tvTitle.setText("");
        }

        if (!TextUtils.isEmpty(userProfile.getDesignation())) {
            holder.tvSubtitle.setText(userProfile.getDesignation());
        } else {
            holder.tvSubtitle.setText("");
        }

        if (userProfile.isChecked()) {
            holder.ivCheck.setVisibility(View.VISIBLE);
        } else {
            holder.ivCheck.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (UserProfile reportingManagerRow : completeList) {
                    reportingManagerRow.setChecked(false);
                }
                for (UserProfile reportingManagerRow : filteredList) {
                    reportingManagerRow.setChecked(false);
                }
                userProfile.setChecked(true);
                customListAdapter.notifyDataSetChanged();
                selectedUser = userProfile;
            }
        });

        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubtitle;
        ImageView ivImage, ivCheck;

        public Holder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_team_titlename);
            tvSubtitle = (TextView) view.findViewById(R.id.tv_team_phone);
            ivImage = (ImageView) view.findViewById(R.id.img_team_picadd);
            ivCheck = (ImageView) view.findViewById(R.id.iv_tick);
        }
    }

}
