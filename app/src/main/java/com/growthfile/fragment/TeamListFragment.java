package com.growthfile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import simplifii.framework.model.OtherMemberModel;
import simplifii.framework.model.TeamMemberModel;
import simplifii.framework.rest.responses.GetTeamResponse;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.model.CreateRoleModel;

import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.responses.Role;
import simplifii.framework.rest.responses.UserProfile;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;

/**
 * Created by robin on 11/17/16.
 */

public class TeamListFragment extends AppBaseFragment implements BaseRecyclerAdapter.RecyclerClickListener, TextWatcher {

    private RelativeLayout rlMyteamOffline, rlNoTeamMemberFound, progressTeamList, rlAddRole;
    private EditText etLocalTeamSearch;
    private FrameLayout fragContainer;
    private LinearLayout llTeamOfflineSetting, llCancelTeamSearch, llTeamsearch;
    private TextView tvMyteamHeading;
    private ImageView teamRetry;
    private RecyclerView recyclerView;
    private BaseRecyclerAdapter baseRecyclerAdapter;
    private List<BaseRecyclerModel> recyclerModelList = new ArrayList<>();
    private List<BaseRecyclerModel> filteredModelList = new ArrayList<>();

    public static TeamListFragment newInstance() {
        return new TeamListFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");
//        setHasOptionsMenu(true);
        initProgressBar();

        teamRetry = (ImageView) findView(R.id.team_retry);
        recyclerView = (RecyclerView) findView(R.id.rw_teams);
        fragContainer = (FrameLayout) findView(R.id.frag_container);

        rlMyteamOffline = (RelativeLayout) findView(R.id.relative_myteam_offline);
        rlNoTeamMemberFound = (RelativeLayout) findView(R.id.rl_no_team_member_found);
        llTeamOfflineSetting = (LinearLayout) findView(R.id.liner_teamoffline_setting);
        llCancelTeamSearch = (LinearLayout) findView(R.id.ll_cancel_team_search);
        llTeamsearch = (LinearLayout) findView(R.id.ll_search);
        llCancelTeamSearch.setOnClickListener(this);
        etLocalTeamSearch = (EditText) findView(R.id.et_local_team_search);
        etLocalTeamSearch.addTextChangedListener(this);
        tvMyteamHeading = (TextView) findView(R.id.tv_myteam_heading);

        setRecyclerAdapter();

        setOnClickListener(llTeamOfflineSetting, llCancelTeamSearch);
        fetchTeamList();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.liner_teamoffline_setting: {

            }
            break;
            case R.id.ll_cancel_team_search: {
                etLocalTeamSearch.setText("");
            }
            break;
        }
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        menu.clear();
//        inflater.inflate(R.menu.add_member_menu, menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_add_member:
//                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.CREATE_ROLE, null, AppConstants.REQUEST_CODES.CREATE_ROLE, this);
////                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CREATE_ROLE, null);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void setRecyclerAdapter() {
        baseRecyclerAdapter = new BaseRecyclerAdapter(filteredModelList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        baseRecyclerAdapter.setListener(this);
        recyclerView.setAdapter(baseRecyclerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
//        fetchTeamList();
    }

    private void fetchTeamList() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getMyTeam();
        executeTask(AppConstants.TASK_CODES.MY_TEAM, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null && "401".equals(re.getHttpStatusCode())) {
            showToast(getString(R.string.something_went_wrong));
        } else {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.MY_TEAM: {
                GetTeamResponse teamPojo = (GetTeamResponse) response;
                if (teamPojo != null) {
                    if (teamPojo.getResponse() != null) {
                        boolean makeSearchViewVisible = false;
                        recyclerModelList.clear();
                        filteredModelList.clear();

                        if (CollectionsUtils.isNotEmpty(teamPojo.getResponse().getTeam())) {
                            makeSearchViewVisible = true;
                            List<TeamMemberModel> teamMemberModelList = new ArrayList<>();
                            for (UserProfile userProfile : teamPojo.getResponse().getTeam()) {
                                TeamMemberModel teamMemberModel = new TeamMemberModel(userProfile);
                                teamMemberModelList.add(teamMemberModel);
                            }
                            teamMemberModelList.get(0).setShowListHeader(true);
                            recyclerModelList.addAll(teamMemberModelList);
                            filteredModelList.addAll(teamMemberModelList);
                            baseRecyclerAdapter.notifyDataSetChanged();
                        }

                        if (CollectionsUtils.isNotEmpty(teamPojo.getResponse().getRoles())) {
                            makeSearchViewVisible = true;
                            List<CreateRoleModel> createRoleModelList = new ArrayList<>();
                            for (Role role : teamPojo.getResponse().getRoles()) {
                                CreateRoleModel createRoleModel = new CreateRoleModel(role);
                                createRoleModelList.add(createRoleModel);
                            }
                            if (CollectionsUtils.isEmpty(teamPojo.getResponse().getTeam())) {
                                createRoleModelList.get(0).setShowListHeader(true);
                            }
                            recyclerModelList.addAll(createRoleModelList);
                            filteredModelList.addAll(createRoleModelList);
                            baseRecyclerAdapter.notifyDataSetChanged();
                        }

                        if (CollectionsUtils.isNotEmpty(teamPojo.getResponse().getOthers())) {
                            makeSearchViewVisible = true;
                            List<OtherMemberModel> otherMemberModelList = new ArrayList<>();
                            for (UserProfile userProfile : teamPojo.getResponse().getOthers()) {
                                OtherMemberModel otherMemberModel = new OtherMemberModel(userProfile);
                                otherMemberModelList.add(otherMemberModel);
                            }

                            otherMemberModelList.get(0).setShowListHeader(true);
                            recyclerModelList.addAll(otherMemberModelList);
                            filteredModelList.addAll(otherMemberModelList);
                            baseRecyclerAdapter.notifyDataSetChanged();
                        }

                        if (makeSearchViewVisible) {
                            llTeamsearch.setVisibility(View.VISIBLE);
                        } else {
                            llTeamsearch.setVisibility(View.GONE);
                        }
                    }

                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_team_list;
    }

    @Override
    protected void onInternetException(int taskCode) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.CREATE_ROLE: {
                fetchTeamList();
            }
            break;
            case AppConstants.REQUEST_CODES.ASSIGN_ROLE: {
                fetchTeamList();
            }
            break;
        }
    }

    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.ADD_TEAM_MEMBER: {
                Bundle bundle = new Bundle();
                CreateRoleModel createRoleModel = (CreateRoleModel) model;
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.CREATE_ROLE_DATA, createRoleModel);
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.ASSIGN_ROLE, bundle, AppConstants.REQUEST_CODES.ASSIGN_ROLE, this);
            }
            break;
            case AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE: {
                if (onUserLogoClickListener != null) {
                    onUserLogoClickListener.openEmployeeProfile((Integer) model);
                }
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String searchText = charSequence.toString().trim().toLowerCase();
        filteredModelList.clear();
        filteredModelList.addAll(recyclerModelList);
        if (!TextUtils.isEmpty(searchText)) {
            List<TeamMemberModel> filteredTeamMembers = null;
            List<CreateRoleModel> filteredRoles = null;
            List<OtherMemberModel> filteredOtherMembers = null;
            for (BaseRecyclerModel baseRecyclerModel : filteredModelList) {
                switch (baseRecyclerModel.getViewType()) {
                    case AppConstants.VIEW_TYPE.TEAM_FRAGMENT_SELF: {
                        TeamMemberModel teamMemberModel = (TeamMemberModel) baseRecyclerModel;
                        String name = "";
                        if (!TextUtils.isEmpty(teamMemberModel.getName())) {
                            name = teamMemberModel.getName().toLowerCase();
                        }
                        String designation = "";
                        if (!TextUtils.isEmpty(teamMemberModel.getDesignation())) {
                            designation = teamMemberModel.getDesignation().toLowerCase();
                        }
                        if (name.contains(searchText) || designation.contains(searchText)) {
                            if (filteredTeamMembers == null) {
                                filteredTeamMembers = new ArrayList<>();
                            }
                            filteredTeamMembers.add(teamMemberModel);
                        }
                    }
                    break;
                    case AppConstants.VIEW_TYPE.CREATED_ROLE: {
                        CreateRoleModel createRoleModel = (CreateRoleModel) baseRecyclerModel;
                        String designation = "";
                        if (!TextUtils.isEmpty(createRoleModel.getDesignation())) {
                            designation = createRoleModel.getDesignation().toLowerCase();
                        }
                        if (designation.contains(searchText)) {
                            if (filteredRoles == null) {
                                filteredRoles = new ArrayList<>();
                            }
                            filteredRoles.add(createRoleModel);
                        }
                    }
                    break;
                    case AppConstants.VIEW_TYPE.TEAM_FRAGMENT_OTHER: {
                        OtherMemberModel otherMemberModel = (OtherMemberModel) baseRecyclerModel;
                        String name = "";
                        if (!TextUtils.isEmpty(otherMemberModel.getName())) {
                            name = otherMemberModel.getName().toLowerCase();
                        }
                        String designation = "";
                        if (!TextUtils.isEmpty(otherMemberModel.getDesignation())) {
                            designation = otherMemberModel.getDesignation().toLowerCase();
                        }
                        if (name.contains(searchText) || designation.contains(searchText)) {
                            if (filteredOtherMembers == null) {
                                filteredOtherMembers = new ArrayList<>();
                            }
                            filteredOtherMembers.add(otherMemberModel);
                        }
                    }
                    break;
                }
            }
            filteredModelList.clear();
            if (CollectionsUtils.isNotEmpty(filteredTeamMembers)) {
                filteredTeamMembers.get(0).setShowListHeader(true);
                filteredModelList.addAll(filteredTeamMembers);
            }
            if (CollectionsUtils.isNotEmpty(filteredRoles)) {
                if (CollectionsUtils.isEmpty(filteredTeamMembers)) {
                    filteredRoles.get(0).setShowListHeader(true);
                }
                filteredModelList.addAll(filteredRoles);
            }
            if (CollectionsUtils.isNotEmpty(filteredOtherMembers)) {
                filteredOtherMembers.get(0).setShowListHeader(true);
                filteredModelList.addAll(filteredOtherMembers);
            }
        }
        baseRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
