package com.growthfile.v2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.v2.responses.Employee;
import simplifii.framework.rest.v2.responses.GetEmployeesApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 1/2/17.
 */

public class SearchEmployeeFragment extends AppBaseFragment implements CustomListAdapterInterface, TextWatcher {

    private EditText etSearchText;
    private ListView lvEmployees;
    private CustomListAdapter<Employee> adapter;
    private List<Employee> employeeList, filteredList;

    @Override
    public void initViews() {
        initToolBar("");
        initCroutonView();
        initProgressBar();
        getHomeIcon();
        etSearchText = (EditText) findView(R.id.et_local_search);
        etSearchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    hideKeyBoard();
                }
            }
        });
        etSearchText.addTextChangedListener(this);
        lvEmployees = (ListView) findView(R.id.lv_search);
        employeeList = new ArrayList<>();
        filteredList = new ArrayList<>();
        adapter = new CustomListAdapter<>(getActivity(), R.layout.row_send_smiley,filteredList, this);
        lvEmployees.setAdapter(adapter);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchEmployeeList();
    }

    private void fetchEmployeeList() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.getEmployees();
        executeTask(AppConstants.TASK_CODES.GET_EMPLOYEES, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_EMPLOYEES: {
                GetEmployeesApiResponse getEmployeesApiResponse = (GetEmployeesApiResponse) response;
                if (getEmployeesApiResponse != null && getEmployeesApiResponse.getEmployeeListContainer() != null) {
                    List<Employee> employeeList = getEmployeesApiResponse.getEmployeeListContainer().getEmployeeList();
                    if (CollectionsUtils.isNotEmpty(employeeList)) {
                        this.employeeList.clear();
                        this.employeeList.addAll(employeeList);
                        this.filteredList.clear();
                        this.filteredList.addAll(employeeList);
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_view_v2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.row_send_smiley, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Employee employee = filteredList.get(position);
        setUserFeedDataOnCurrentView(viewHolder, employee);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, employee.getId());
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.OPEN_EMPLOYEE_PROFILE, bundle);
                getActivity().finish();
            }
        });
        return convertView;
    }

    private void setUserFeedDataOnCurrentView(ViewHolder holder, Employee employee){
        holder.tvEmployeeName.setText(employee.getName());
        String designation = employee.getDesignation();
        if(!TextUtils.isEmpty(designation)){
            holder.tvEmployeeDesignation.setText(designation);
            holder.tvEmployeeDesignation.setVisibility(View.VISIBLE);
        } else {
            holder.tvEmployeeDesignation.setVisibility(View.GONE);
        }
        String imageUrl = employee.getProfileImage();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).into(holder.ivProfilePic);
            holder.tvInitials.setVisibility(View.GONE);
            holder.ivProfilePic.setVisibility(View.VISIBLE);
        } else {
            holder.tvInitials.setText(Util.getInitialsFromName(employee.getName()));
            holder.tvInitials.setVisibility(View.VISIBLE);
            holder.ivProfilePic.setVisibility(View.GONE);
        }
    }

    public class ViewHolder{

        ImageView ivProfilePic;
        TextView tvEmployeeName;
        TextView tvEmployeeDesignation;
        LinearLayout rlCross;
        TextView tvInitials;

        public ViewHolder(View view){
            ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_feed_logo);
            tvEmployeeName = (TextView) view.findViewById(R.id.tv_user_feed_user_name);
            tvEmployeeDesignation = (TextView) view.findViewById(R.id.tv_user_feed_designation);
            tvInitials = (TextView) view.findViewById(R.id.tv_user_feed_initial);
            rlCross = (LinearLayout) view.findViewById(R.id.lay_right);
            rlCross.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String searchedText = s.toString().trim().toLowerCase();
        filteredList.clear();
        filteredList.addAll(employeeList);
        if(TextUtils.isEmpty(searchedText)){

        } else {
            Iterator<Employee> employeeIterator = filteredList.iterator();
            while (employeeIterator.hasNext()){
                Employee employee = employeeIterator.next();
                if(!employee.getName().toLowerCase().contains(searchedText)){
                    employeeIterator.remove();
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRetryClicked(View view, int taskCode) {
        super.onRetryClicked(view, taskCode);
        fetchEmployeeList();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
