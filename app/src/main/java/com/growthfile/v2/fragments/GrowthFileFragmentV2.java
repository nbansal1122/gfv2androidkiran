package com.growthfile.v2.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.model.v2.FeedCardModel;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.growthfile.v2.models.GFileModel;
import com.growthfile.v2.models.StarImageHeaderModel;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.responses.FeedCard;
import simplifii.framework.rest.v2.responses.GFileApiResponse;
import simplifii.framework.rest.v2.responses.GFileContainer;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by ajay on 17/12/16.
 */

public class GrowthFileFragmentV2 extends AppBaseFragment implements BaseRecyclerAdapter.RecyclerClickListener {

    private RecyclerView rwGrowthFile;
    private BaseRecyclerAdapter adapter;
    private List<BaseRecyclerModel> list = new ArrayList<>();
    private boolean selfProfile;

    @Override
    public void initViews() {

        initToolBar("");
        getHomeIcon();
        initProgressBar();
        setHasOptionsMenu(true);
        rwGrowthFile = (RecyclerView) findView(R.id.recyclerView_fragment_growthfile);
        initRecyclerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchGrowthFile();
    }

    private void fetchGrowthFile() {
        if (getArguments() != null && getArguments().getInt(AppConstants.BUNDLE_KEYS.USER_ID, -1) != -1) {
            selfProfile = false;
            Integer userId = getArguments().getInt(AppConstants.BUNDLE_KEYS.USER_ID);
            UserSession userSession = UserSession.getSessionInstance();
            if (userSession != null) {
                Integer sessionUserId = userSession.getId();
                if (userId.equals(sessionUserId)) {
                    selfProfile = true;
                }
            }
            HttpParamObject httpParamObject = ApiRequestGeneratorV2.getGrowthFileForOther(userId);
            executeTask(AppConstants.TASK_CODES.GET_USER_PROFILE_OTHER, httpParamObject);
        } else {
            selfProfile = true;
            HttpParamObject httpParamObject = ApiRequestGeneratorV2.getSelfGrowthFile();
            executeTask(AppConstants.TASK_CODES.GET_USER_PROFILE_SELF, httpParamObject);
        }

    }
    //9958140560

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_USER_PROFILE_OTHER:
            case AppConstants.TASK_CODES.GET_USER_PROFILE_SELF: {
                GFileApiResponse gFileApiResponse = (GFileApiResponse) response;
                if (gFileApiResponse != null && CollectionsUtils.isNotEmpty(gFileApiResponse.getgFileContainerList())) {
                    list.clear();
                    GFileContainer gFileContainer = gFileApiResponse.getgFileContainerList().get(0);
                    GFileModel gFileModel = new GFileModel();
                    gFileModel.setImageEditable(selfProfile);
                    gFileModel.setGrowthFile(gFileContainer);
                    list.add(gFileModel);
                    if (CollectionsUtils.isNotEmpty(gFileContainer.getReceivedStarCards())) {
                        List<FeedCard> feedCardList = gFileContainer.getReceivedStarCards();
                        for (FeedCard feedCard : feedCardList) {
                            StarImageHeaderModel starImageHeaderModel = new StarImageHeaderModel();
                            list.add(starImageHeaderModel);
                            FeedCardModel feedCardModel = new FeedCardModel(feedCard);
                            list.add(feedCardModel);
                        }
                        ((StarImageHeaderModel) list.get(1)).setStarImageEnabled(true);
                    }
                    adapter.notifyDataSetChanged();
                }
                break;

            }
            case AppConstants.TASK_CODES.EDIT_PHOTO:
                String url = (String) response;
                if (!TextUtils.isEmpty(url)) {
                    updatePhoto(url);
                }
                break;
            case AppConstants.TASK_CODES.EDIT_PROFILE:
                fetchGrowthFile();
                break;
        }
    }

    private void updatePhoto(String url) {
        HttpParamObject obj = new HttpParamObject();
        obj.addAuthTokenHeader();
        obj.setJSONContentType();
        obj.setUrl(AppConstants.PAGE_URL.UPDATE_PROFILE_PIC_V2);
        obj.setPatchMethod();
        obj.setJson(getJsonToUpdatePhoto(url));
        executeTask(AppConstants.TASK_CODES.EDIT_PROFILE, obj);
    }

    private String getJsonToUpdatePhoto(String url) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("profile_image_url", url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    private void initRecyclerView() {

        rwGrowthFile.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter(list, getActivity());
        adapter.setListener(this);
        rwGrowthFile.setAdapter(adapter);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.profile_back;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.app_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_search: {
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.SEARCH_EMPLOYEE, null);
                break;
            }
            case android.R.id.home: {
                getActivity().onBackPressed();
                break;
            }
        }
        return true;
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_growthfile_v2;
    }

    @Override
    public void onRetryClicked(View view, int taskCode) {
        super.onRetryClicked(view, taskCode);
        fetchGrowthFile();
    }

    private void openImagePicker() {
        ImagePicker.create(this)
                .returnAfterCapture(true) // set whether camera action should return immediate result or not
                .folderMode(true) // folder mode (false by default)
                .imageTitle("Select image") // image selection title
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("GrowthFile")
                // directory name for captured image  ("Camera" folder by default)
                .start(AppConstants.REQUEST_CODES.REQ_PICK_IMAGE); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == AppConstants.REQUEST_CODES.REQ_PICK_IMAGE) {
            ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
            if (images != null && images.size() == 1) {
                Log.d(TAG, "Path :" + images.get(0).getPath());
                String filePath = images.get(0).getPath();
                executePhotoUpload(filePath);
            }
        }
    }

    private void executePhotoUpload(String filePath) {
        executeTask(AppConstants.TASK_CODES.EDIT_PHOTO, filePath);
    }

    private Bitmap getBitmapFromFilePath(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            bitmap = Util.getResizeBitmap(bitmap, 512);
            return bitmap;
        } catch (Exception e) {
            showToast(getString(R.string.insufficient_memory));
        } catch (OutOfMemoryError e) {
            showToast(R.string.insufficient_memory);
        }
        return null;
    }

    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.EDIT_IMAGE:
                openImagePicker();
                break;
            case AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE:
                openEmployeeProfile((Integer) model);
                break;
        }
    }

    private void openEmployeeProfile(Integer employeeId) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, employeeId);
        FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.OPEN_EMPLOYEE_PROFILE, bundle);
    }
}

