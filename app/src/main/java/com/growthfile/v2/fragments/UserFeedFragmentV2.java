package com.growthfile.v2.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.model.feed.FeedResponse;
import com.growthfile.model.v2.FeedCardModel;
import com.growthfile.ui.FragmentContainerActivity;
import com.growthfile.util.EndlessRecyclerViewScrollListener;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.growthfile.v2.activities.TransactionActivity;
import com.growthfile.v2.models.EmptyScreenRow;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.enums.ActionTitleEnum;
import simplifii.framework.rest.v2.enums.ActionTableType;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.FeedCard;
import simplifii.framework.rest.v2.responses.GetFeedCardApiResponse;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.FeedActionV2;

/**
 * Created by nbansal2211 on 20/11/16.
 */

public class UserFeedFragmentV2 extends AppBaseFragment implements SwipeRefreshLayout.OnRefreshListener, BaseRecyclerAdapter.RecyclerClickListener {

    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<BaseRecyclerModel> list = new ArrayList<>();
    private BaseRecyclerAdapter adapter;
    private int page = 1, total;
    private FeedResponse feedResponse;
    private boolean isLoading;
    private EmptyRowItemClickListener emptyRowItemClickListener;

    public EmptyRowItemClickListener getEmptyRowItemClickListener() {
        return emptyRowItemClickListener;
    }

    public void setEmptyRowItemClickListener(EmptyRowItemClickListener emptyRowItemClickListener) {
        this.emptyRowItemClickListener = emptyRowItemClickListener;
    }

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        getHomeIcon();
        initProgressBar();
        initCroutonView();
        recyclerView = (RecyclerView) findView(R.id.rv_feeds);
        swipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipeContainer1);
        swipeRefreshLayout.setOnRefreshListener(this);
        initRecyclerView();
        registerReceiver();
    }

    @Override
    public void onReceive(Intent intent) {
        super.onReceive(intent);
        fetchUserFeeds(0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchUserFeeds(0);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.profile_back;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.app_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search: {
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.SEARCH_EMPLOYEE, null);
                break;
            }
            case android.R.id.home: {
                getActivity().onBackPressed();
                break;
            }
        }
        return true;
    }

    private void initRecyclerView() {
        adapter = new BaseRecyclerAdapter(list, getActivity());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
        listener.setmLayoutManager(manager);
        recyclerView.addOnScrollListener(listener);
    }

    EndlessRecyclerViewScrollListener listener = new EndlessRecyclerViewScrollListener() {
        @Override
        public void onLoadMore(int pageNumber, int totalItemsCount) {
            if (feedResponse != null && feedResponse.getResponse() != null) {
                if (totalItemsCount >= total || isLoading) {
                    return;
                }
            }
            page = page + 1;
//            fetchUserFeeds(page);
        }
    };

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        isLoading = false;

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.USER_FEED: {
                isLoading = false;
                GetFeedCardApiResponse getFeedCardApiResponse = (GetFeedCardApiResponse) response;
                if (getFeedCardApiResponse != null && getFeedCardApiResponse.getFeedCardContainer() != null) {
                    if (CollectionsUtils.isNotEmpty(getFeedCardApiResponse.getFeedCardContainer().getFeedCardList())) {
                        List<FeedCard> feedCardList = getFeedCardApiResponse.getFeedCardContainer().getFeedCardList();
                        list.clear();
                        for (FeedCard feedCard : feedCardList) {
                            FeedCardModel feedCardModel = new FeedCardModel(feedCard);
                            list.add(feedCardModel);
                        }
                        adapter.notifyDataSetChanged();
                        return;
                    }
                }
                showEmptyScreen();
                break;
            }
            case AppConstants.TASK_CODES.FOLLOW_UP_ACTION: {
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                showCrouton(baseApiResponse.getMsg());
                fetchUserFeeds(page);
                break;
            }
        }
    }


    private void fetchUserFeeds(int page) {
        hideVisibility(R.id.no_feed_layout);
        if (!isLoading) {
            if (CollectionsUtils.isNotEmpty(list))
                recyclerView.scrollToPosition(0);
            isLoading = true;
            UserSession userSession = UserSession.getSessionInstance();
            if (userSession != null) {
                HttpParamObject httpParamObject = ApiRequestGeneratorV2.getUserFeedCards();
                executeTask(AppConstants.TASK_CODES.USER_FEED, httpParamObject);
            }
        }
    }

    @Override
    public void refreshData() {
        onRefresh();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_user_feed_v2;
    }

    @Override
    public void onRefresh() {
        page = 1;
        swipeRefreshLayout.setRefreshing(false);
        fetchUserFeeds(page);
    }

    public void takeActionOnFeed(FeedActionV2 feedAction) {
        ActionTableType actionTableType = feedAction.getActionTableType();
        switch (actionTableType) {
            case TRANSACTION: {
                if (ActionTitleEnum.EDIT.getTitle().equalsIgnoreCase(feedAction.getAction())) {
                    Intent intent = new Intent(getActivity(), TransactionActivity.class);
                    intent.putExtra(AppConstants.BUNDLE_KEYS.EDIT_TRANSACTION, true);
                    intent.putExtra(AppConstants.BUNDLE_KEYS.CARD_ID, feedAction.getCardId());
                    if (feedAction.getFeedCard().getMetaData() != null) {
                        intent.putExtra(AppConstants.BUNDLE_KEYS.DUE_DATE, feedAction.getFeedCard().getMetaData().getDueDate());
                        intent.putExtra(AppConstants.BUNDLE_KEYS.AMOUNT, feedAction.getFeedCard().getMetaData().getAmount());
                        if (!TextUtils.isEmpty(feedAction.getFeedCard().getUpdateComment())) {
                            intent.putExtra(AppConstants.BUNDLE_KEYS.COMMENT, feedAction.getFeedCard().getUpdateComment());
                        } else {
                            intent.putExtra(AppConstants.BUNDLE_KEYS.COMMENT, feedAction.getFeedCard().getMetaData().getComment());
                        }
                        intent.putExtra(AppConstants.BUNDLE_KEYS.STATUS, feedAction.getFeedCard().getMetaData().getStatus());
                        intent.putExtra(AppConstants.BUNDLE_KEYS.ACCOUNT_NAME, feedAction.getFeedCard().getMetaData().getAccountName());
                    }
                    startActivityForResult(intent, AppConstants.REQUEST_CODES.EDIT_TRANSACTION);
                    break;
                }
            }
            default:
                HttpParamObject httpParamObject = ApiRequestGeneratorV2.followUpAction(feedAction.getAction(), feedAction.getCardId());
                executeTask(AppConstants.TASK_CODES.FOLLOW_UP_ACTION, httpParamObject);
        }
    }

    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.ACTION_FEED:
                takeActionOnFeed((FeedActionV2) model);
                break;
            case AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE:
                openEmployeeProfile((Integer) model);
                break;
            case AppConstants.ACTION_TYPE.MAP_HEADER_CLICK:
//                onMapClicked((MyLocation) model);
                break;
        }
    }

    private void openEmployeeProfile(Integer employeeId) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.USER_ID, employeeId);
        FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.OPEN_EMPLOYEE_PROFILE, bundle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            default:
                onRefresh();
        }
    }

    @Override
    protected void handleDefaultError(RestException re, Exception e, int taskCode, Object... params) {
        showEmptyScreen();
    }

    private void showEmptyScreen() {
        if (list == null || list.size() == 0) {
            showVisibility(R.id.no_feed_layout);
            LinearLayout layout = (LinearLayout) findView(R.id.ll_growth_reports);
            layout.removeAllViewsInLayout();
            List<EmptyScreenRow> list = EmptyScreenRow.getList();
            for (EmptyScreenRow row : list) {
                addRow(row, layout);
            }
        } else {
            hideVisibility(R.id.no_feed_layout);
        }
    }

    private void addRow(EmptyScreenRow row, LinearLayout layout) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.row_empty_start_screen_v2, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_reports_text);
        ImageView iv = (ImageView) view.findViewById(R.id.iv_reports_bullet);
        tv.setText(row.getText());
        iv.setImageResource(row.getDrawableId());
        view.setTag(row);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmptyScreenRow r = (EmptyScreenRow) v.getTag();
                if (emptyRowItemClickListener != null) {
                    emptyRowItemClickListener.onEmptyRowClicked(r.getClickType());
                }
            }
        });
        layout.addView(view);
    }

    @Override
    protected void onInternetException(int taskCode) {
        super.onInternetException(taskCode);
        isLoading = false;
    }

    @Override
    public void onRetryClicked(View view, int taskCode) {
        super.onRetryClicked(view, taskCode);
        fetchUserFeeds(page);
    }

    public static interface EmptyRowItemClickListener {
        public void onEmptyRowClicked(int id);
    }

}
