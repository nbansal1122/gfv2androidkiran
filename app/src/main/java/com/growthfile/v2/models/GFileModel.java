package com.growthfile.v2.models;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.responses.GFileContainer;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 12/26/16.
 */

public class GFileModel extends BaseRecyclerModel {

    private GFileContainer growthFile;
    private boolean imageEditable;

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.GROWTH_FILE;
    }

    public GFileContainer getGrowthFile() {
        return growthFile;
    }

    public void setGrowthFile(GFileContainer growthFile) {
        this.growthFile = growthFile;
    }

    public boolean isImageEditable() {
        return imageEditable;
    }

    public void setImageEditable(boolean imageEditable) {
        this.imageEditable = imageEditable;
    }
}
