package com.growthfile.v2.models;

import com.growthfile.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 24/01/17.
 */

public class EmptyScreenRow {
    private int drawableId;
    private String text;
    private int clickType;

    public int getClickType() {
        return clickType;
    }

    public void setClickType(int clickType) {
        this.clickType = clickType;
    }

    public EmptyScreenRow(int drawableId, String text, int clickType) {
        this.drawableId = drawableId;
        this.text = text;
        this.clickType = clickType;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static List<EmptyScreenRow> getList() {
        List<EmptyScreenRow> list = new ArrayList<>();
        list.add(new EmptyScreenRow(R.drawable.location_nav, AppConstants.LOCATION_TEXT, R.id.iv_location_tab));
        list.add(new EmptyScreenRow(R.drawable.rupee_bottom, AppConstants.TRANSACTION_TEXT, R.id.iv_collection_tab));
        list.add(new EmptyScreenRow(R.drawable.smiley_nav, AppConstants.SMILEY_TEXT, R.id.iv_growth_file_tab));
        list.add(new EmptyScreenRow(R.drawable.growthfile_nav, AppConstants.STAR_TEXT, R.id.iv_smiley_tab));
        return list;
    }
}
