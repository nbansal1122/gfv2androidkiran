package com.growthfile.v2.models;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 1/9/17.
 */

public class StarImageHeaderModel extends BaseRecyclerModel{

    private boolean starImageEnabled = false;
    private boolean verticalLineEnabled = true;

    public boolean isStarImageEnabled() {
        return starImageEnabled;
    }

    public void setStarImageEnabled(boolean starImageEnabled) {
        this.starImageEnabled = starImageEnabled;
    }

    public boolean isVerticalLineEnabled() {
        return verticalLineEnabled;
    }

    public void setVerticalLineEnabled(boolean verticalLineEnabled) {
        this.verticalLineEnabled = verticalLineEnabled;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.STAR_IMAGE_HEADER;
    }
}
