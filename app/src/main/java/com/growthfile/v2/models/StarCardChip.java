package com.growthfile.v2.models;

import com.plumillonforge.android.chipview.Chip;

import simplifii.framework.rest.v2.responses.Receiver;

/**
 * Created by robin on 12/27/16.
 */

public class StarCardChip implements Chip {

    private Receiver receiver;

    public StarCardChip(Receiver receiver) {
        this.receiver = receiver;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public String getText() {
        return receiver.getName();
    }

}
