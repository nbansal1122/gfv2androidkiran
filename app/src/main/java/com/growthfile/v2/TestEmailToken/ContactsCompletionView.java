package com.growthfile.v2.TestEmailToken;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.growthfile.R;
import com.squareup.picasso.Picasso;
import com.tokenautocomplete.TokenCompleteTextView;

import simplifii.framework.rest.v2.responses.Employee;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 23/12/16.
 */

public class ContactsCompletionView extends TokenCompleteTextView<Employee> {

    Context context;


    public ContactsCompletionView(Context context, AttributeSet attr) {
        super(context, attr);
    }

//    @Override
//    public boolean enoughToFilter() {
//        return true;
//    }
//    @Override
//    public void onFocusChanged(boolean focused, int direction,
//                                  Rect previouslyFocusedRect) {
//        previouslyFocusedRect = null;
//        super.onFocusChanged(false, direction, previouslyFocusedRect);
//        if(!isPopupShowing())
//            focused = false;
//        if (focused) {
//            performFiltering(getText(), 0);
//        }
//    }

//    @Override
//    public void allowDuplicates(boolean allow) {
//        super.allowDuplicates(false);
//    }



    @Override
    protected View getViewForObject(Employee emp) {
        View view = null;
        if(view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.chip_star_recipient_v2, (ViewGroup) getParent(), false);
        }
        TokenTextView tokenName = (TokenTextView) view.findViewById(R.id.tv_send_star_card_name);
        TextView tvInitials = (TextView) view.findViewById(R.id.tv_send_star_initial);
        ImageView ivUserLogo = (ImageView) view.findViewById(R.id.iv_user_feed_logo);
        tokenName.setText(emp.getName());
        String imageUrl = emp.getProfileImage();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(ivUserLogo);
            tvInitials.setVisibility(GONE);
            ivUserLogo.setVisibility(VISIBLE);
        } else{
            String initials = Util.getInitialsFromName(emp.getName());
            tvInitials.setText(initials);
            tvInitials.setVisibility(VISIBLE);
            ivUserLogo.setVisibility(GONE);
        }

        return view;
    }



    @Override
    protected Employee defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
//        int index = completionText.indexOf('@');
//        if (index == -1) {
//            return new TestEmployee(completionText, completionText.replace(" ", "") + "@example.com");
//        } else {
//            return new TestEmployee(completionText.substring(0, index), completionText);
//        }
//    }
        return null;
    }
}
