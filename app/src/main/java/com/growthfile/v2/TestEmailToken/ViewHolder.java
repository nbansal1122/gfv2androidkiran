package com.growthfile.v2.TestEmailToken;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;

/**
 * Created by aman on 26/12/16.
 */

public class ViewHolder {

    public ImageView ivProfilePic;
    public TextView tvEmployeeName;
    public TextView tvEmployeeDesignation;
    public LinearLayout rlCross;

    public ViewHolder(View view){
        ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_feed_logo);
        tvEmployeeName = (TextView) view.findViewById(R.id.tv_user_feed_user_name);
        tvEmployeeDesignation = (TextView) view.findViewById(R.id.tv_user_feed_designation);
        rlCross = (LinearLayout) view.findViewById(R.id.lay_right);
        rlCross.setVisibility(View.GONE);
    }
}
