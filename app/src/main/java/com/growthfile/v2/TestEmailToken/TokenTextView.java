package com.growthfile.v2.TestEmailToken;

import android.content.Context;
import android.util.AttributeSet;

import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by aman on 23/12/16.
 */

public class TokenTextView extends CustomFontTextView {

    public TokenTextView(Context context) {
        super(context);
    }

    public TokenTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TokenTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(false);
//        setCompoundDrawablesWithIntrinsicBounds(0,0, selected ? R.drawable.cross_small : 0,0);
//        setCompoundDrawablePadding(10);
//        setCursorVisible(false);
    }
}
