package com.growthfile.v2.TestEmailToken;

//import com.growthfiles.model.v2.TestEmployee;


/**
 * Created by aman on 26/12/16.
 */

//public class ActivitySendStart extends AppCompatActivity implements TokenCompleteTextView.TokenListener<TestEmployee>{

//    private List<TestEmployee> employeeList;
//    private ContactsCompletionView completionView;
//    private ArrayAdapter<TestEmployee> adapter;
//    private ViewHolder viewHolder;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_send_start_v2);
//
//        addEmployeesToList();
//
//        adapter = new FilteredArrayAdapter<TestEmployee>(this, R.layout.row_send_smiley, employeeList) {
//
//            @NonNull
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                viewHolder = null;
//                if(convertView == null){
//                    convertView = LayoutInflater.from(ActivitySendStart.this).inflate(R.layout.row_send_smiley, parent, false);
//                    viewHolder = new ViewHolder(convertView);
//                    convertView.setTag(viewHolder);
//                }else {
//                    viewHolder = (ViewHolder) convertView.getTag();
//                }
//
//                TestEmployee employee = getItem(position);
//                viewHolder.ivProfilePic.setImageResource(employee.getImgID());
//                viewHolder.tvEmployeeName.setText(employee.getName());
//                viewHolder.tvEmployeeDesignation.setText(employee.getDesignation());
//
//                return convertView;
//            }
//
//            @Override
//            protected boolean keepObject(TestEmployee emp, String mask) {
//                mask = mask.toLowerCase();
//                return emp.getName().toLowerCase().startsWith(mask) || emp.getDesignation().toLowerCase().startsWith(mask);
//            }
//        };
//
//        completionView = (ContactsCompletionView) findViewById(R.id.searchView);
//        completionView.setAdapter(adapter);
//        completionView.setTokenListener(this);
//        completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
//        completionView.setThreshold(1);
//
//        if(savedInstanceState == null){
//            completionView.setPrefix("To: ");
//
//            // default added objects here
//        }
//    }
//
//
//
//    @Override
//    public void onTokenAdded(TestEmployee token) {
//        Log.d("added", "added");
//        // show animation or voice on token added
//    }
//
//    @Override
//    public void onTokenRemoved(TestEmployee token) {
//        Log.d("removed", "removed");
//        // show animation or voice on token removed
//    }
//
//
//    private void addEmployeesToList() {
//        employeeList = new ArrayList<TestEmployee>();
//        employeeList.add(new TestEmployee("Prashant Kumar Prajapati", "Ui and Ux designer", R.drawable.user));
//        employeeList.add(new TestEmployee("Prashant Kumar Gupta", "Ui and Ux designer", R.drawable.user));
//        employeeList.add(new TestEmployee("Prateek Bansal", "Android developer", R.drawable.user));
//        employeeList.add(new TestEmployee("Prateek Bansal", "Project manager", R.drawable.user));
//    }
//}
