package com.growthfile.v2.holders;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.enums.LeaveType;
import com.growthfile.holders.BaseRecyclerHolder;
import com.growthfile.model.v2.FeedCardModel;
import com.growthfile.v2.viewhelper.CustomCenteredChips;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.enums.ActionTableType;
import simplifii.framework.rest.v2.enums.TransactionType;
import simplifii.framework.rest.v2.responses.FeedCard;
import simplifii.framework.rest.v2.responses.MetaData;
import simplifii.framework.rest.v2.responses.Receiver;
import simplifii.framework.rest.v2.responses.Updater;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.FeedActionV2;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 20/11/16.
 */
public class FeedHolderV2 extends BaseRecyclerHolder {

    private ImageView ivUserFeedLogo;
    private TextView tvUserfeedinitial, tvUserFeedName, tvUserFeedDesignation, tvUserFeedActioin, tvUserFeedTime,
            tvUserFeedDescription, tvUserFeedDescriptionTitle;

    private LinearLayout llUserFeedCardContainer;
    private LinearLayout llDescriptionContainer, llNameDesignation;

    private LinearLayout llViewClickableActions;
    private ChipView actionChipView;

    private TextView tvSupportedCount;

    private View separatorLine;
    private LinearLayout llChipLayout;

    public FeedHolderV2(final View itemView) {
        super(itemView);

        ivUserFeedLogo = (ImageView) findView(R.id.iv_user_feed_logo);
        tvUserfeedinitial = (TextView) findView(R.id.tv_user_feed_initial);
        tvUserFeedName = (TextView) findView(R.id.tv_user_feed_user_name);
        tvUserFeedDesignation = (TextView) findView(R.id.tv_user_feed_designation);
        tvUserFeedActioin = (TextView) findView(R.id.tv_user_feed_aciton);
        tvUserFeedTime = (TextView) findView(R.id.tv_user_feed_aciton_time);

        tvUserFeedDescription = (TextView) findView(R.id.tv_user_feed_description);
        tvUserFeedDescriptionTitle = (TextView) findView(R.id.tv_user_feed_description_title);
        llDescriptionContainer = (LinearLayout) findView(R.id.lay_description_container);

        llUserFeedCardContainer = (LinearLayout) findView(R.id.ll_user_feed_card_details);
        separatorLine = findView(R.id.view_sep_chip);
        llViewClickableActions = (LinearLayout) findView(R.id.view_clickable_actions);
        actionChipView = (ChipView) findView(R.id.chip_actions);
        llChipLayout = (LinearLayout) findView(R.id.ll_chip_layout);
        tvSupportedCount = (TextView) findView(R.id.tv_supported_count);
        llNameDesignation = (LinearLayout) findView(R.id.lay_name_designation);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        this.model = baseRecyclerModel;
        FeedCardModel feedCardModel = (FeedCardModel) baseRecyclerModel;
        FeedCard feedCard = feedCardModel.getFeedCard();
        if (feedCard != null) {
            setUserDetailsRow(feedCard);
            setCardDetails(feedCard);
            setCardDescription(feedCard);
            setCreatorActions(feedCard);
        }

    }

    private void setCreatorActions(final FeedCard feedCard) {
        List<String> actions = feedCard.getUserActions();
        if (CollectionsUtils.isNotEmpty(actions)) {
            llViewClickableActions.setVisibility(View.VISIBLE);
            separatorLine.setVisibility(View.VISIBLE);
            llChipLayout.setVisibility(View.VISIBLE);
            addCustomCenterdChips(feedCard, llViewClickableActions, actions);
        } else {
            separatorLine.setVisibility(View.GONE);
            llChipLayout.setVisibility(View.GONE);
            llViewClickableActions.setVisibility(View.GONE);
        }
    }

    private void addCustomCenterdChips(final FeedCard feedCard, LinearLayout layout, List<String> actions) {
        CustomCenteredChips customCenteredChips = new CustomCenteredChips(context, layout, actions, new CustomCenteredChips.IActionClickListener() {
            @Override
            public void onActionClick(String actionText) {
                FeedActionV2 action = new FeedActionV2(feedCard.getId(), actionText, ActionTableType.findByName(feedCard.getTable()), feedCard);
                onActionClicked(action);
            }
        });
    }

    private void setCardDescription(FeedCard feedCard) {
        boolean showDescription = false;
        String descriptionTitle = null;
        if (feedCard.getMetaData() != null) {
            descriptionTitle = feedCard.getMetaData().getAccountName();
        }
        if (!TextUtils.isEmpty(descriptionTitle)) {
            showDescription = true;
        }
        setTextOrGone(tvUserFeedDescriptionTitle, descriptionTitle);
        if (!TextUtils.isEmpty(feedCard.getUpdateComment())) {
            showDescription = true;
            setTextOrGone(tvUserFeedDescription, feedCard.getUpdateComment());
        } else if (feedCard.getMetaData() != null) {
            String description = feedCard.getMetaData().getComment();
            setTextOrGone(tvUserFeedDescription, description);
            if (!TextUtils.isEmpty(description)) {
                showDescription = true;
            }
        }

        if (!showDescription) {
            llDescriptionContainer.setVisibility(View.GONE);
        } else {
            llDescriptionContainer.setVisibility(View.VISIBLE);
        }

    }


    private void setCardDetails(final FeedCard feedCard) {
        String tableName = feedCard.getTable();
        ActionTableType actionTableType = ActionTableType.findByName(tableName);
        llUserFeedCardContainer.removeAllViews();
        if (actionTableType != null) {
            switch (actionTableType) {
                case LEAVE: {
                    View viewLeave = layoutInflater.inflate(R.layout.layout_feed_card_type_leave_v2, null, false);
                    TextView tvLeaveEmployee = (TextView) viewLeave.findViewById(R.id.tv_leave_employee);
                    TextView tvFeedCardType = (TextView) viewLeave.findViewById(R.id.tv_feed_card_type);
                    TextView tvUserFeedLeaveStartDay = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_start_Day);
                    TextView tvUserFeedLeaveStartMonth = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_srart_month);
                    TextView tvUserFeedLeaveStartYear = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_srart_year);
                    TextView tvUserFeedLeaveEndDay = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_end_Day);
                    TextView tvUserFeedLeaveEndMonth = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_end_month);
                    TextView tvUserFeedLeaveEndYear = (TextView) viewLeave.findViewById(R.id.tv_user_feed_leave_end_year);
                    if (LeaveType.PRESENT.getType().equals(feedCard.getMetaData().getLeaveType())) {
                        tvFeedCardType.setText(LeaveType.PRESENT.name());
                    } else {
                        tvFeedCardType.setText(LeaveType.LEAVE.name());
                    }
                    llUserFeedCardContainer.addView(viewLeave);

                    if (feedCard.getMetaData() != null && !TextUtils.isEmpty(feedCard.getMetaData().getEmpName())) {
                        tvLeaveEmployee.setText(feedCard.getMetaData().getEmpName());
                    } else {
                        setTextOrSetNA(tvLeaveEmployee, "");
                    }

                    String fromDateString = feedCard.getRelevantDateFrom();
                    try {
                        Date fromDate = Util.convertStringToDate(fromDateString, "yyyy-MM-dd");
                        String fromDayString = Util.convertDateFormat(fromDate, "dd");
                        String fromMonthString = Util.convertDateFormat(fromDate, "MMMM");
                        String fromYearString = Util.convertDateFormat(fromDate, "yyyy");

                        tvUserFeedLeaveStartDay.setText(fromDayString);
                        tvUserFeedLeaveStartMonth.setText(fromMonthString);
                        tvUserFeedLeaveEndYear.setText(fromYearString);
                    } catch (Exception e) {
                        setTextOrSetNA(tvUserFeedLeaveStartDay, "");
                        setTextOrGone(tvUserFeedLeaveStartMonth, "");
                        setTextOrGone(tvUserFeedLeaveStartYear, "");
                    }
                    String endDateString = feedCard.getRelevantDateTo();
                    try {
                        Date toDate = Util.convertStringToDate(endDateString, "yyyy-MM-dd");
                        String toDayString = Util.convertDateFormat(toDate, "dd");
                        String toMonthString = Util.convertDateFormat(toDate, "MMMM");
                        String toYearString = Util.convertDateFormat(toDate, "yyyy");

                        tvUserFeedLeaveEndDay.setText(toDayString);
                        tvUserFeedLeaveEndMonth.setText(toMonthString);
                        tvUserFeedLeaveEndYear.setText(toYearString);
                    } catch (Exception e) {
                        setTextOrSetNA(tvUserFeedLeaveEndDay, "");
                        setTextOrGone(tvUserFeedLeaveEndMonth, "");
                        setTextOrGone(tvUserFeedLeaveEndYear, "");
                    }
                    break;
                }
                case LOCATION: {
                    View viewLocation = layoutInflater.inflate(R.layout.layout_feed_card_type_location_v2, null, false);
                    viewLocation.setOnClickListener(null);
                    ImageView ivMapView = (ImageView) viewLocation.findViewById(R.id.iv_location_image);
                    if (feedCard.getMetaData() != null) {
                        String lat = feedCard.getMetaData().getLat();
                        String lon = feedCard.getMetaData().getLng();
                        if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                            String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=15&size=450x180&maptype=roadmap&format=png&scale=2"
                                    + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + lat + "," + lon;
                            Picasso.with(context).load(url).into(ivMapView);
                            viewLocation.setTag(feedCard.getMetaData());
                            viewLocation.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MetaData data = (MetaData) v.getTag();
                                    Uri gmmIntentUri = Uri.parse(String.format("geo:%s,%s?q=%s,%s", data.getLat(), data.getLng(), data.getLat(), data.getLng()));
                                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                    mapIntent.setPackage("com.google.android.apps.maps");

                                    if (mapIntent.resolveActivity(itemView.getContext().getPackageManager()) != null) {
                                        itemView.getContext().startActivity(mapIntent);
                                    }
                                }
                            });
                        }
                        llUserFeedCardContainer.addView(viewLocation);
                    }
                    break;
                }
                case STAR: {
                    if (CollectionsUtils.isNotEmpty(feedCard.getReceivers())) {
                        View viewStar = layoutInflater.inflate(R.layout.layout_feed_card_type_star_new_v2, null, false);
                        TextView tvSender = (TextView) viewStar.findViewById(R.id.tv_star_sender);
                        ImageView ivAppliedCardLogo = (ImageView) viewStar.findViewById(R.id.iv_applied_card_logo);
                        LinearLayout llStarRecipients = (LinearLayout) viewStar.findViewById(R.id.lay_star_recipient_container);

                        llUserFeedCardContainer.addView(viewStar);

                        ivAppliedCardLogo.setImageResource(R.drawable.growthfile_nav);
                        if (feedCard.getMetaData() != null && !TextUtils.isEmpty(feedCard.getMetaData().getCreatorName())) {
                            tvSender.setText(feedCard.getMetaData().getCreatorName());
                        } else {
                            setTextOrSetNA(tvSender, "");
                        }

                        llStarRecipients.removeAllViews();
                        if (CollectionsUtils.isNotEmpty(feedCard.getReceivers())) {
                            for (Receiver receiver : feedCard.getReceivers()) {
                                TextView tvRecipient = (TextView) layoutInflater.inflate(R.layout.layout_star_recipient_feed_card_v2, null, false);
                                tvRecipient.setText(receiver.getName());
                                llStarRecipients.addView(tvRecipient);
                            }
                            if ("SUPPORTED".equals(feedCard.getTopRightText())) {
                                if (feedCard.getSupportedStarCardInfo() != null && CollectionsUtils.isNotEmpty(feedCard.getSupportedStarCardInfo().getSupporters()))
                                    tvSupportedCount.setText("+" + feedCard.getSupportedStarCardInfo().getSupporters().get(0));
                                tvSupportedCount.setVisibility(View.VISIBLE);
                            }
                            //Not to display as of now
                            tvSupportedCount.setVisibility(View.GONE);
                        }

                    }
                    break;
                }
                case SMILEY: {
                    View viewSmiley = layoutInflater.inflate(R.layout.layout_feed_card_type_smiley_v2, null, false);
                    ImageView ivAppliedCardLogo = (ImageView) viewSmiley.findViewById(R.id.iv_applied_card_logo);
                    TextView tvSender = (TextView) viewSmiley.findViewById(R.id.tv_smiley_sender);
                    TextView tvReceiver = (TextView) viewSmiley.findViewById(R.id.tv_smiley_receiver);

                    ivAppliedCardLogo.setImageResource(R.drawable.smiley);
                    String sender = null;
                    if (feedCard.getUpdater() != null) {
                        sender = feedCard.getUpdater().getName();
                    }
                    setTextOrSetNA(tvSender, sender);

                    String receiver = null;
                    if (CollectionsUtils.isNotEmpty(feedCard.getReceivers())) {
                        receiver = feedCard.getReceivers().get(0).getName();
                    }
                    setTextOrSetNA(tvReceiver, receiver);

                    llUserFeedCardContainer.addView(viewSmiley);
                    break;
                }
                case TRANSACTION: {
                    View viewCollection = layoutInflater.inflate(R.layout.layout_feed_card_type_collection_v2, null, false);
                    ImageView ivAppliedCardLogo = (ImageView) viewCollection.findViewById(R.id.iv_applied_card_logo);
//                    TextView tvTranscardCardTitle = (TextView) viewCollection.findViewById(R.id.tv_transaction_card_title);
                    TextView tvTransactionType = (TextView) viewCollection.findViewById(R.id.tv_transaction_type);
                    TextView tvCollectionCardAmount = (TextView) viewCollection.findViewById(R.id.tv_card_collection_amount);
                    TextView tvCollectionDueDateDay = (TextView) viewCollection.findViewById(R.id.tv_collection_due_date_day);
                    TextView tvCollectionDueDateMonth = (TextView) viewCollection.findViewById(R.id.tv_collection_due_date_month);
                    TextView tvCollectionDueDateYear = (TextView) viewCollection.findViewById(R.id.tv_collection_due_date_year);

                    ivAppliedCardLogo.setImageResource(R.drawable.rupee_blue_bg);
                    if (feedCard.getMetaData() != null) {
//                        setTextOrSetNA(tvTranscardCardTitle, feedCard.getMetaData().getAccountName());
                        TransactionType transactionType = TransactionType.findByApiText(feedCard.getMetaData().getStatus());
                        String displayValue = null;
                        if (transactionType != null) {
                            displayValue = transactionType.getDisplayText();
                        }
                        setTextOrSetNA(tvTransactionType, displayValue);
                        String amount = null;
                        if (!TextUtils.isEmpty(feedCard.getMetaData().getAmount())) {
                            amount = "\u20B9" + feedCard.getMetaData().getAmount();
                        }
                        setTextOrSetNA(tvCollectionCardAmount, amount);
                        String dueDateString = feedCard.getMetaData().getDueDate();
                        try {
                            Date dueDate = Util.convertStringToDate(dueDateString, "yyyy-MM-dd");
                            String dueDayString = Util.convertDateFormat(dueDate, "dd");
                            String dueMonthString = Util.convertDateFormat(dueDate, "MMMM");
                            String dueYearString = Util.convertDateFormat(dueDate, "yyyy");

                            tvCollectionDueDateDay.setText(dueDayString);
                            tvCollectionDueDateMonth.setText(dueMonthString);
                            tvCollectionDueDateYear.setText(dueYearString);
                        } catch (Exception e) {
                            setTextOrSetNA(tvCollectionDueDateDay, "");
                            setTextOrGone(tvCollectionDueDateMonth, "");
                            setTextOrGone(tvCollectionDueDateYear, "");
                        }
                    }


                    llUserFeedCardContainer.addView(viewCollection);
                    break;
                }
                case RESIGN: {
                    View resignView = layoutInflater.inflate(R.layout.layout_feed_card_type_resign_v2, null, false);
                    TextView tvFeedCardType = (TextView) resignView.findViewById(R.id.tv_feed_card_type);
                    TextView tvResignEmployee = (TextView) resignView.findViewById(R.id.tv_resign_employee);
                    TextView tvResignDay = (TextView) resignView.findViewById(R.id.tv_resign_day);
                    TextView tvResignMonth = (TextView) resignView.findViewById(R.id.tv_resign_month);
                    TextView tvResignYear = (TextView) resignView.findViewById(R.id.tv_resign_year);
                    llUserFeedCardContainer.addView(resignView);
                    if (feedCard.getMetaData() != null && !TextUtils.isEmpty(feedCard.getMetaData().getEmpName())) {
                        tvResignEmployee.setText(feedCard.getMetaData().getEmpName());
                    } else {
                        setTextOrSetNA(tvResignEmployee, "");
                    }
                    tvFeedCardType.setText(actionTableType.name());
                    String resignDateString = feedCard.getMetaData().getLastWorkingDay();
                    try {
                        Date resignDate = Util.convertStringToDate(resignDateString, "yyyy-MM-dd");
                        String resignDayString = Util.convertDateFormat(resignDate, "dd");
                        String resignMonthString = Util.convertDateFormat(resignDate, "MMMM");
                        String resignYearString = Util.convertDateFormat(resignDate, "yyyy");

                        tvResignDay.setText(resignDayString);
                        tvResignMonth.setText(resignMonthString);
                        tvResignYear.setText(resignYearString);
                    } catch (Exception e) {
                        setTextOrSetNA(tvResignDay, "");
                        setTextOrGone(tvResignMonth, "");
                        setTextOrGone(tvResignYear, "");
                    }
                }
                default: {

                }
            }
        }
    }

    private void setUserDetailsRow(FeedCard feedCard) {
        final Updater updater = feedCard.getUpdater();
        String topRightText = feedCard.getTopRightText();
        if (updater != null) {
            tvSupportedCount.setVisibility(View.GONE);
            setTextOrSetNA(tvUserFeedName, updater.getName());
            setImageOrSetInitials(updater.getProfileImage(), updater.getName(), ivUserFeedLogo, tvUserfeedinitial);
            setTextOrGone(tvUserFeedDesignation, updater.getDesignation());
            setTextOrGone(tvUserFeedActioin, topRightText);
            try {
                String updatedAt = feedCard.getUpdatedAt();
//                String uiDate = Util.convertDateFormat(updatedAt,"yyyy-MM-dd HH:mm:ss","dd MMM hh:MM a");
//                String uiDate = Util.convertDateFormat(updatedAt, "yyyy-MM-dd HH:mm:ss", "hh:mm a");
                String uiDate = Util.getDateTimeForGfileCards(updatedAt, "yyyy-MM-dd HH:mm:ss");
                tvUserFeedTime.setText(uiDate);
                tvUserFeedTime.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                setTextOrGone(tvUserFeedTime, feedCard.getUpdatedAt());
            }
            View.OnClickListener onLogoClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null)
                        clickListener.onItemClicked(getAdapterPosition(), itemView, updater.getId(), AppConstants.ACTION_TYPE.OPEN_EMPLOYEE_PROFILE);
                }
            };
            ivUserFeedLogo.setOnClickListener(onLogoClickListener);
            tvUserfeedinitial.setOnClickListener(onLogoClickListener);
            llNameDesignation.setOnClickListener(onLogoClickListener);
        }
    }


    private void onActionClicked(FeedActionV2 action) {
        Log.d("FeedHolder", "action :" + action.getAction() + "," + action.getCardId());

        if (clickListener != null) {
            clickListener.onItemClicked(getAdapterPosition(), itemView, action, AppConstants.ACTION_TYPE.ACTION_FEED);
        }
    }

    private void setImageOrSetInitials(String imageUrl, final String name, final ImageView imageView, final TextView tvInitials) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    setImageOrSetInitials(null, name, imageView, tvInitials);
                }
            });
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(name)) {
            tvInitials.setText(Util.getInitialsFromName(name));
            imageView.setVisibility(View.GONE);
            tvInitials.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.drawable.unknown);
            imageView.setVisibility(View.VISIBLE);
            tvInitials.setVisibility(View.GONE);
        }
    }

}

