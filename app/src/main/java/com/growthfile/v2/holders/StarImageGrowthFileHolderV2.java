package com.growthfile.v2.holders;

import android.view.View;
import android.widget.ImageView;

import com.growthfile.R;
import com.growthfile.holders.BaseRecyclerHolder;
import com.growthfile.v2.models.StarImageHeaderModel;

import simplifii.framework.model.BaseRecyclerModel;

/**
 * Created by robin on 1/9/17.
 */

public class StarImageGrowthFileHolderV2 extends BaseRecyclerHolder {

    private ImageView ivStarImage;
    private View verticalLine;

    public StarImageGrowthFileHolderV2(View itemView) {
        super(itemView);
        ivStarImage = (ImageView) findView(R.id.iv_star_image);
        verticalLine = findView(R.id.view_vertical_line);
    }

    @Override
    public void onBindData(int position, BaseRecyclerModel baseRecyclerModel) {
        StarImageHeaderModel starImageHeaderModel = (StarImageHeaderModel) baseRecyclerModel;
        if(starImageHeaderModel.isStarImageEnabled()){
            ivStarImage.setVisibility(View.VISIBLE);
        } else {
            ivStarImage.setVisibility(View.GONE);
        }
        if(starImageHeaderModel.isVerticalLineEnabled()){
            verticalLine.setVisibility(View.VISIBLE);
        } else {
            verticalLine.setVisibility(View.GONE);
        }
    }
}
