package com.growthfile.v2.viewhelper;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.util.AppUtil;

import java.util.List;

/**
 * Created by nbansal2211 on 05/01/17.
 */

public class CustomCenteredChips {
    private IActionClickListener listener;
    private Context context;
    private List<String> actions;
    private LinearLayout layout;

    public CustomCenteredChips(Context context, LinearLayout layout, List<String> actions, IActionClickListener listener) {
        layout.removeAllViewsInLayout();
        this.context = context;
        this.listener = listener;
        this.actions = actions;
        this.layout = layout;
        initViews();
    }

    private void initViews() {
        boolean isReminder = actions.size() % 2 == 1;
        int midValue = actions.size() / 2;
        for (int i = 0; i < midValue; i++) {
            LinearLayout horizontal = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.layout_chip_holder, null);
            horizontal.addView(getButtonRow(actions.get(i)));
            View sep = new View(context);
            int width = context.getResources().getDimensionPixelOffset(R.dimen.margin_16dp);
            sep.setLayoutParams(new LinearLayout.LayoutParams(width, width));
            horizontal.addView(sep);
            horizontal.addView(getButtonRow(actions.get(i + 1)));
            horizontal.setGravity(Gravity.CENTER_HORIZONTAL);
            layout.addView(horizontal);
        }
        if (isReminder) {
            LinearLayout horizontal = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.layout_chip_holder, null);
            horizontal.addView(getButtonRow(actions.get(actions.size() - 1)));
            horizontal.setGravity(Gravity.CENTER_HORIZONTAL);
            layout.addView(horizontal);
        }
    }

    private View getButtonRow(final String action) {
        View view = LayoutInflater.from(context).inflate(R.layout.clickable_action_chip_v2, null);
        TextView tvActionName = (TextView) view.findViewById(R.id.tv_feed_card_action);
        ImageView ivActionIcon = (ImageView) view.findViewById(R.id.iv_feed_card_action);
        ivActionIcon.setImageResource(AppUtil.getIconForActionType(action));
        tvActionName.setTextColor(AppUtil.getColorForActionType(context, action));
        tvActionName.setText(action);
        view.setTag(action);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String action = (String) v.getTag();
                listener.onActionClick(action);
            }
        });
        return view;
    }

    public static interface IActionClickListener {
        public void onActionClick(String action);
    }
}
