package com.growthfile.v2.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;
import com.growthfile.model.v2.FeedCardModel;
import com.growthfile.utility.ApiRequestGeneratorV2;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.rest.v2.responses.FeedCard;
import simplifii.framework.rest.v2.responses.GetFeedCardApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.FeedActionV2;

/**
 * Created by robin on 12/27/16.
 */

public class SingleCardActivity extends AppBaseActivity implements BaseRecyclerAdapter.RecyclerClickListener {

    private RecyclerView recyclerView;
    private List<BaseRecyclerModel> list = new ArrayList<>();
    private BaseRecyclerAdapter adapter;
    private int cardId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_card_v2);
        initToolBar("");
        initCroutonView();
        initProgressBar();
        initRecyclerView();
        if(getIntent().getExtras()!=null){
            cardId = getIntent().getIntExtra(AppConstants.BUNDLE_KEYS.CARD_ID,-1);
            if(cardId==-1){
                showToast("Couldn't get the card information!");
                finish();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchCardDetails();
    }

    private void fetchCardDetails() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.singleCardDetailsRequest(cardId);
        executeTask(AppConstants.TASK_CODES.SINGLE_CARD_DETAILS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.SINGLE_CARD_DETAILS:{
                GetFeedCardApiResponse getFeedCardApiResponse = (GetFeedCardApiResponse) response;
                if(getFeedCardApiResponse!=null&&getFeedCardApiResponse.getFeedCardContainer()!=null){
                    if(CollectionsUtils.isNotEmpty(getFeedCardApiResponse.getFeedCardContainer().getFeedCardList())){
                        List<FeedCard> feedCardList = getFeedCardApiResponse.getFeedCardContainer().getFeedCardList();
                        list.clear();
                        for(FeedCard feedCard : feedCardList){
                            FeedCardModel feedCardModel = new FeedCardModel(feedCard);
                            list.add(feedCardModel);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
            }
            case AppConstants.TASK_CODES.FOLLOW_UP_ACTION:{
                fetchCardDetails();
                break;
            }
        }
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.rv_feeds);
        adapter = new BaseRecyclerAdapter(list, this);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(int position, View itemView, Object model, int actionType) {
        FeedActionV2 feedActionV2 = (FeedActionV2) model;
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.followUpAction(feedActionV2.getAction(), feedActionV2.getCardId());
        executeTask(AppConstants.TASK_CODES.SINGLE_CARD_DETAILS, httpParamObject);
    }
}
