package com.growthfile.v2.activities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.util.SMSListener;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import me.philio.pinentry.PinEntryView;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.v2.responses.ValidateOtpApiResponse;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by ajay on 20/12/16.
 */

public class OtpVerificationActivityV2 extends AppBaseActivity {

    private TextView tvResendOtp;
    private PinEntryView pinEntryView;
    private RelativeLayout rlConfirmOtp;
    private String mobile;
    private SMSListener smsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification_v2);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_up);
        initProgressBar();

        rlConfirmOtp = (RelativeLayout) findViewById(R.id.rl_confirm_otp);
        tvResendOtp = (TextView) findViewById(R.id.tv_resend_otp);
        pinEntryView = (PinEntryView) findViewById(R.id.pin_entry_view);
        rlConfirmOtp.setOnClickListener(this);
        tvResendOtp.setOnClickListener(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            mobile = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.NUMBER);
        }
        getSmsPermission();
    }

    private void getSmsPermission() {
        new TedPermission(this).setPermissions(Manifest.permission.RECEIVE_SMS)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        registerOTPReceiver();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).check();
    }

    private void registerOTPReceiver() {
        if (isPermissionGranted()) {
            if (smsListener == null) {
                smsListener = new SMSListener(new SMSListener.OTPCallback() {
                    @Override
                    public void onOTPReceived(String otp) {
                        pinEntryView.setText((otp));
                        Util.hideKeyboard(OtpVerificationActivityV2.this);
                        validateAndConfirm();
                    }
                });
            }
            registerReceiver(smsListener, getTelephonyFilter());
        }
    }

    private IntentFilter getTelephonyFilter() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        return filter;
    }


    private boolean isPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_confirm_otp: {
                Util.hideKeyboard(this);
                validateAndConfirm();
                break;
            }
            case R.id.tv_resend_otp: {
                resendOtp();
                break;
            }
        }
    }

    private void resendOtp() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.generateOtp(mobile);
        executeTask(AppConstants.TASK_CODES.RESEND_OTP, httpParamObject);
    }

    private void validateAndConfirm() {
        String pin = pinEntryView.getText().toString().trim();
        if (TextUtils.isEmpty(pin) || pin.length() != 6) {
            return;
        }
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.validateOtp(mobile, pin);
        executeTask(AppConstants.TASK_CODES.VALIDATE_OTP, httpParamObject);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (smsListener != null) {
            unregisterReceiver(smsListener);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.VALIDATE_OTP: {
                ValidateOtpApiResponse userProfileBaseApiResponse = (ValidateOtpApiResponse) response;
                UserSession.saveUserSessionAndLogIn(userProfileBaseApiResponse.getToken(), userProfileBaseApiResponse.getResponse());
                Intent intent = new Intent(this, HomeTabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.TASK_CODES.RESEND_OTP: {
                showCrouton("Otp resent successfully!");
                break;
            }
        }
    }
}
