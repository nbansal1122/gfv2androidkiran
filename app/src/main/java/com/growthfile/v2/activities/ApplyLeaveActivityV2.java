package com.growthfile.v2.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.enums.LeaveType;
import com.growthfile.utility.ApiRequestGeneratorV2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 12/17/16.
 */

public class ApplyLeaveActivityV2 extends AppBaseActivity {

    private AutoCompleteTextView autotvSelectLeavetype;
    private TextView tvStartDate, tvEndDate;
    private EditText etWriteComment;
    private Date dateStart;
    private Date dateEnd;
    private TextView tvToolbarTitle;

    private RelativeLayout rlSubmit, rlCancel;
    private String leaveType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave_v2);
        initCroutonView();
        initProgressBar();
        initToolBar("");
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        if(getIntent()!=null){
            String toolbarTitle = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.TOOLBAR_TITLE);
            tvToolbarTitle.setText(toolbarTitle);
            leaveType = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.LEAVE_TYPE);
        }
        getHomeIcon();
        autotvSelectLeavetype = (AutoCompleteTextView) findViewById(R.id.tv_leave_type);
        tvStartDate = (TextView) findViewById(R.id.tv_start_date);
        tvEndDate = (TextView) findViewById(R.id.tv_end_date);
        rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);
        etWriteComment = (EditText) findViewById(R.id.et_write_comment_leave);
        setOnClickListener(R.id.tv_start_date, R.id.tv_end_date, R.id.rl_cancel, R.id.rl_submit);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_start_date:
                setDate(new Date(),tvStartDate);
                break;
            case R.id.tv_end_date:
                if (dateStart == null) {
                    setDate(new Date(),tvEndDate);
                }else {
                    setDate(dateStart,tvEndDate);
                }
                break;
            case R.id.rl_cancel:
                finish();
                break;
            case R.id.rl_submit:
                validateAndSubmit();
                break;
        }
    }

    private void validateAndSubmit() {
        if(dateStart==null){
            showCrouton(getString(R.string.select_start_date));
//            setDate(new Date(),tvStartDate);
            return;
        }
        if(dateEnd==null){
            showCrouton(getString(R.string.select_end_date));
//            setDate(dateStart,tvEndDate);
            return;
        }
        if(dateStart.after(dateEnd)){
            showCrouton(getString(R.string.choose_start_date_before_end_date));
            return;
        }
        SimpleDateFormat apiFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String startDateString = apiFormatter.format(dateStart);
        String endDateString = apiFormatter.format(dateEnd);
        String comment = etWriteComment.getText().toString().trim();

        HttpParamObject httpParamObject = ApiRequestGeneratorV2.applyLeaveRequest(startDateString, endDateString, comment, leaveType);
        executeTask(AppConstants.TASK_CODES.APPLY_LEAVE, httpParamObject);
        enableOrDisableButton(R.id.rl_submit, false);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.APPLY_LEAVE:{
                enableOrDisableButton(R.id.rl_submit, true);
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse!=null){
                    if(!TextUtils.isEmpty(baseApiResponse.getMsg())){
                        showToast(baseApiResponse.getMsg());
                    } else {
                        showToast("Leave applied successfully!");
                    }
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    private void setDate(Date date, final TextView textView) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int mYear = calendar.get(Calendar.YEAR);
        final int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                if (textView.getId() == R.id.tv_start_date) {
                    dateStart = calendar.getTime();
                } else {
                    dateEnd = calendar.getTime();
                }
                SimpleDateFormat formatedDate = new SimpleDateFormat("dd MMMM yyyy");
                String formatDate = formatedDate.format(calendar.getTime());
                textView.setText(formatDate);
            }
        }, mYear, mMonth, mDay);
        if(LeaveType.PRESENT.getType().equals(leaveType)){
            dpd.getDatePicker().setMaxDate(new Date().getTime());
            calendar.add(Calendar.MONTH, -6);
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
        } else {
//            dpd.getDatePicker().setMinDate(date.getTime());
            calendar.add(Calendar.MONTH, -6);
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
            calendar.add(Calendar.MONTH, 12);
            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        }
        if (textView.getId() == R.id.tv_start_date) {
            dpd.setTitle("Select start date");
        } else {
            dpd.setTitle("Select end date");
        }
        dpd.show();
    }

}
