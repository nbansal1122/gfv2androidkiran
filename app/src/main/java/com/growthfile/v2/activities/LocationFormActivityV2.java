package com.growthfile.v2.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLngBounds;
import com.growthfile.R;
import com.growthfile.util.FusedLocationService;
import com.growthfile.util.GFLocationManager;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.Account;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.GetAccountApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;

/**
 * Created by robin on 12/17/16.
 */

public class LocationFormActivityV2 extends AppBaseActivity implements OnMapReadyCallback, FusedLocationService.MyLocationCallback, GFLocationManager.LocationCallback {
    private static final int REQ_CODE_GET_LOCATION = 101;
    private LatLng latLng;

    private TextView tvSelectAccount;
    private EditText etWriteComment;
    private RelativeLayout rlSubmit, rlCancel;
    private GoogleMap googleMap;
    private List<Account> accountList;
    private CustomListAdapter<Account> accountAdapter;
    private Account selectedAccount;
    private Dialog dialog;
    private Double latitude = 0.0, longitude = 0.0;

    private MapView mapView;

    private ImageView ivAccountDropdown;
    private boolean locationFetched = false;
    private boolean locationRefreshed = false;
    private boolean isAccountDropDownClicked;
    private FusedLocationService fusedLocationService;
    private MarkerOptions marker;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_form_v2);
        initCroutonView();
        initProgressBar();
        initToolBar("");
        getHomeIcon();
        handler = new Handler();
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map_view);
//        mapFragment.getMapAsync(this);

        mapView = (MapView) findViewById(R.id.map_location);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        setReceivedLocation();

        ivAccountDropdown = (ImageView) findViewById(R.id.iv_account_dropdown);

        rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);

        tvSelectAccount = (TextView) findViewById(R.id.tv_auto_select_account);

        etWriteComment = (EditText) findViewById(R.id.et_write_comment_location);
        setOnClickListener(R.id.rl_submit, R.id.rl_cancel, R.id.tv_auto_select_account, R.id.iv_account_dropdown);
        checkGPSenable();
    }

    private void setReceivedLocation() {
//        Bundle b = getIntent().getExtras();
//        if (b != null) {
//            latLng = new LatLng(b.getDouble(AppConstants.PREF_KEYS.KEY_LAT), b.getDouble(AppConstants.PREF_KEYS.KEY_LNG));
//            latitude = latLng.latitude;
//            longitude = latLng.longitude;
//            setStaticPositionOnImageView(latLng);
//            setGoogleMap();
//        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        accountList = new ArrayList<>();
        accountAdapter = new CustomListAdapter<>(this, R.layout.row_popup, accountList, new CustomListAdapterInterface() {
            @Override
            public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
                if (convertView == null) {
                    convertView = inflater.inflate(resourceID, parent, false);
                }
                final Account account = accountList.get(position);
                TextView tvAccountName = (TextView) convertView.findViewById(R.id.text1);
                tvAccountName.setText(account.getAccountName());
                tvAccountName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        tvSelectAccount.setText(account.getAccountName());
                        selectedAccount = account;
                        ivAccountDropdown.setImageResource(R.drawable.cross);
                    }
                });
                return convertView;
            }
        });
        GetAccountApiResponse getAccountApiResponse = GetAccountApiResponse.getInstance();
        initiateAccounts(getAccountApiResponse);
        fetchAccountList();
    }

    private void fetchAccountList() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.getAccountListRequest();
        executeTask(AppConstants.TASK_CODES.GET_ACCOUNTS, httpParamObject);
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setLocation();
            return;
        }
        new TedPermission(this).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        setLocation();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).check();
    }


    private GFLocationManager gfLocationManager;

    private void fetchLocation() {
//        showProgressDialog();
        GFLocationManager instance = GFLocationManager.getInstance();
        instance.setCallback(this);
        instance.startLocationTracking(this);
    }

//    private GFLocationManager.LocationCallback locationCallback = new GFLocationManager.LocationCallback() {
//
//    };


    public void onLocationUpdate(final Location location) {
//            hideProgressBar();
//            if (gfLocationManager != null) {
//                gfLocationManager.stopLocationUpdate();
//            }


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (location != null) {
                    latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    Log.d("latlong", latLng.latitude + "");
                    setGoogleMapCamera();
                }
            }
        },3000);
    }

    private void setLocation() {
        fetchLocation();
    }


    private void setStaticPositionOnImageView(LatLng latLng) {
//        if(!TextUtils.isEmpty(latLng.latitude)&&!TextUtils.isEmpty(lon)){
        String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latLng.latitude + "," + latLng.longitude + "&zoom=15&size=450x180&maptype=roadmap&format=png&scale=2"
                + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + latLng.latitude + "," + latLng.longitude;
//        Picasso.with(this).load(url).into(ivMapLocation, new Callback() {
//            @Override
//            public void onSuccess() {
//                hideProgressBar();
//                locationFetched = true;
//                showCrouton("Location Refreshed!");
//            }
//
//            @Override
//            public void onError() {
//                if (!Util.isConnectingToInternet(LocationFormActivityV2.this)) {
//                    showCrouton(getString(R.string.internet_error));
//                    locationFetched = false;
//                    return;
//                }
//                setLocation();
//            }
//        });
//        }
    }

    private void checkGPSenable() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocationPermission();
        } else {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_error)
                .setCancelable(false)
                .setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        setLocation();
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQ_CODE_GET_LOCATION);
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_submit:
                submitData();
                break;
            case R.id.rl_cancel:
                finish();
                break;
//            case R.id.iv_current_location:
//                checkGPSenable();
//                break;
            case R.id.tv_auto_select_account:
                if (CollectionsUtils.isEmpty(accountList)) {
                    isAccountDropDownClicked = true;
                    fetchAccountList();
                } else {
                    showAccountListDialog();
                }
                break;
            case R.id.iv_account_dropdown:
                if (selectedAccount == null) {
                    showAccountListDialog();
                } else {
                    tvSelectAccount.setText(getString(R.string.select_account));
                    selectedAccount = null;
                    ivAccountDropdown.setImageResource(R.drawable.pointer_down);
                }
        }
    }

    private void submitData() {
//        if(selectedAccount==null){
//            showCrouton("Please select an account!");
//            return;
//        }
//        if (!Util.isConnectingToInternet(this) && !locationFetched) {
//            showCrouton("Couldn't get your latest location. Please check your internet connection...");
//            return;
//        } else if (Util.isConnectingToInternet(this) && !locationFetched) {
//            setLocation();
//            showCrouton("Refreshing location...");
//            return;
//        }

        Integer accountId = null;
        if (selectedAccount != null) {
            accountId = selectedAccount.getId();
        }
        String comment = etWriteComment.getText().toString().trim();
        if (latLng != null) {
            HttpParamObject httpParamObject = ApiRequestGeneratorV2.markLocation(latLng, comment, accountId);
            executeTask(AppConstants.TASK_CODES.MARK_LOCATION, httpParamObject);
            enableOrDisableButton(R.id.rl_submit, false);
        } else {
            showToast("Please wait while we are finding your current location");
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.MARK_LOCATION:
                enableOrDisableButton(R.id.rl_submit, true);
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null) {
                    if (!TextUtils.isEmpty(baseApiResponse.getMsg())) {
                        showToast(baseApiResponse.getMsg());
                    }
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case AppConstants.TASK_CODES.GET_ACCOUNTS: {
                GetAccountApiResponse getAccountApiResponse = (GetAccountApiResponse) response;
                initiateAccounts(getAccountApiResponse);
            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    @Override
    protected void handleDefaultError(RestException re, Exception e, int taskCode, Object... params) {
        if (taskCode == AppConstants.TASK_CODES.GET_ACCOUNTS) {
            if (isAccountDropDownClicked) {
                showCrouton(getString(R.string.no_account_exist));
                return;
            } else {
                return;
            }
        }
        super.handleDefaultError(re, e, taskCode, params);
    }

    private void initiateAccounts(GetAccountApiResponse getAccountApiResponse) {
        if (getAccountApiResponse != null) {
            if (!getAccountApiResponse.isError() && getAccountApiResponse.getAccountListContainer() != null) {
                List<Account> accountList = getAccountApiResponse.getAccountListContainer().getAccountList();
                if (CollectionsUtils.isNotEmpty(accountList)) {
                    this.accountList.clear();
                    this.accountList.addAll(accountList);
                    accountAdapter.notifyDataSetChanged();
                } else {
                    if (isAccountDropDownClicked) {
                        showCrouton(getString(R.string.no_account_exist));
                    }
                }
            } else {
                showCrouton(getAccountApiResponse.getMsg());
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);


//        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

        }
        googleMap.setMyLocationEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        MapsInitializer.initialize(this);
//        setGoogleMapCamera();
    }

    private void setGoogleMapCamera() {
        if (googleMap == null) return;
        if (latLng == null) return;
        if (marker != null) {
            marker.position(latLng);
        } else {
            marker = new MarkerOptions()
                    .anchor(0.0f, 1.0f)
                    .position(latLng);
            googleMap.addMarker(marker);
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(latLng.latitude, latLng.longitude));
        LatLngBounds bounds = builder.build();
//        int padding = 0;
//        // Updates the location and zoom of the MapView
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//        googleMap.moveCamera(cameraUpdate);
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = 200;
        int padding = (int) (0); // offset from edges of the map 12% of screen

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_GET_LOCATION) {
            checkGPSenable();
        }
    }

    public void setPossition(LatLng location) {
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, 15);
//        googleMap.animateCamera(cameraUpdate);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(location);
//        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//                latLng = mMap.getCameraPosition().target;
//            }
//        });
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void showAccountListDialog() {
        dialog = new Dialog(this);
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
//        window.setGravity(Gravity.NO_GRAVITY);
//        int[] location = new int[2];
//        tvSelectAccount.getLocationOnScreen(location);
//        window.getAttributes().x = location[0];
//        window.getAttributes().y = location[1];
        dialog.setContentView(R.layout.layout_listview);
        ListView lvAccounts = (ListView) dialog.findViewById(R.id.list_view);
        lvAccounts.setAdapter(accountAdapter);

        dialog.show();
    }

    @Override
    public void onLocationChanged(FusedLocationService.MyLocation myLocation) {
        //fusedLocationService.stopLocationUpdates();
        if (myLocation != null) {
            showProgressDialog();
            setStaticPositionOnImageView(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
        }
    }
}