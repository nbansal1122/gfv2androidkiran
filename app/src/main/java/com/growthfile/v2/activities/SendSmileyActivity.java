package com.growthfile.v2.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.Employee;
import simplifii.framework.rest.v2.responses.GetEmployeesApiResponse;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 24/12/16.
 */

public class SendSmileyActivity extends AppBaseActivity implements View.OnClickListener, CustomListAdapterInterface, TextWatcher, AdapterView.OnItemClickListener {

    private EditText etSearchEmployee;
    private RelativeLayout rlSearchView;
    private RelativeLayout rlSelectedView;
    private TextView tvName, tvDesignation;
    private ImageView ivLogo;
    private LinearLayout rlCross;
    private TextView tvInitials;

    private Employee selectedEmployee;

    private List<Employee> employeeList, filteredList;

    private ListView lvEmployeesView;
    private CustomListAdapter<Employee> adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_smiley);

        initCroutonView();
        initProgressBar();

        etSearchEmployee = (EditText) findViewById(R.id.actv_send_smiley);
        rlSearchView = (RelativeLayout) findViewById(R.id.lay_chip_container);
        rlSelectedView = (RelativeLayout) findViewById(R.id.lay_selected_chip_container);
        rlCross = (LinearLayout) findViewById(R.id.lay_right);
        tvName = (TextView) findViewById(R.id.tv_user_feed_user_name);
        tvInitials = (TextView) findViewById(R.id.tv_user_feed_initial);
        tvDesignation = (TextView) findViewById(R.id.tv_user_feed_designation);
        ivLogo = (ImageView) findViewById(R.id.iv_user_feed_logo);

        RelativeLayout rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        RelativeLayout rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);

        employeeList = new ArrayList<>();
        filteredList = new ArrayList<>();
        lvEmployeesView = (ListView) findViewById(R.id.lv_employees_list);
        adapter = new CustomListAdapter<>(this, R.layout.row_send_smiley, filteredList, this);
        lvEmployeesView.setAdapter(adapter);
        lvEmployeesView.setOnItemClickListener(this);

        rlCross.setOnClickListener(this);

        setOnClickListener(R.id.rl_cancel, R.id.rl_submit);

        etSearchEmployee.addTextChangedListener(this);

        etSearchEmployee.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){

                } else {

                }
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchEmployeeList();
    }

    private void fetchEmployeeList() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.getEmployees();
        executeTask(AppConstants.TASK_CODES.GET_EMPLOYEES, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_EMPLOYEES:{
                GetEmployeesApiResponse getEmployeesApiResponse = (GetEmployeesApiResponse) response;
                initiateList(getEmployeesApiResponse);
                break;
            }
            case AppConstants.TASK_CODES.SEND_SMILEY:{
                enableOrDisableButton(R.id.rl_submit, true);
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse!=null){
                    if(!TextUtils.isEmpty(baseApiResponse.getMsg()))
                        showToast(baseApiResponse.getMsg());
                    else
                        showToast("You have successfully given the smiley");
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    private void initiateList(GetEmployeesApiResponse getEmployeesApiResponse) {
        if (getEmployeesApiResponse != null && getEmployeesApiResponse.getEmployeeListContainer() != null) {
            List<Employee> employeeList = getEmployeesApiResponse.getEmployeeListContainer().getEmployeeList();
            if (CollectionsUtils.isNotEmpty(employeeList)) {
                this.employeeList.clear();
                UserSession session = UserSession.getSessionInstance();
                for (Employee e : employeeList) {
                    if (e.getId().compareTo(session.getId()) == 0) {
                    } else {
                        this.employeeList.add(e);
                    }
                }
                this.filteredList.clear();
                this.filteredList.addAll(this.employeeList);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_right:
                rlSelectedView.setVisibility(View.GONE);
                rlSearchView.setVisibility(View.VISIBLE);
                onTextChanged(etSearchEmployee.getText().toString(),0,0,0);
                selectedEmployee = null;
              break;
            case R.id.rl_cancel:
                finish();
                break;
            case R.id.rl_submit:
                validateAndSubmit();
                break;
        }
    }

    private void validateAndSubmit() {
        if(selectedEmployee==null){
            showCrouton("Please select an employee first!");
            return;
        }
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.sendSmileyRequest(selectedEmployee.getId());
        executeTask(AppConstants.TASK_CODES.SEND_SMILEY, httpParamObject);
        enableOrDisableButton(R.id.rl_submit, false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(this).inflate(R.layout.row_send_smiley, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Employee employees = filteredList.get(position);
        setUserFeedDataOnCurrentView(viewHolder, employees);
        return convertView;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String searchedText = s.toString().trim().toLowerCase();
        filteredList.clear();
        filteredList.addAll(employeeList);
        if(TextUtils.isEmpty(searchedText)){

        } else {
            Iterator<Employee> employeeIterator = filteredList.iterator();
            while (employeeIterator.hasNext()){
                Employee employee = employeeIterator.next();
                if(!employee.getName().toLowerCase().contains(searchedText)){
                    employeeIterator.remove();
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        hideKeyBoard();
        rlSearchView.setVisibility(View.GONE);
        rlSelectedView.setVisibility(View.VISIBLE);
        selectedEmployee = filteredList.get(position);
        tvName.setText(selectedEmployee.getName());
        String designation = selectedEmployee.getDesignation();
        if(!TextUtils.isEmpty(designation)){
            tvDesignation.setText(designation);
            tvDesignation.setVisibility(View.VISIBLE);
        } else {
            tvDesignation.setVisibility(View.GONE);
        }
        String imageUrl = selectedEmployee.getProfileImage();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(SendSmileyActivity.this).load(imageUrl).into(ivLogo);
            tvInitials.setVisibility(View.GONE);
            ivLogo.setVisibility(View.VISIBLE);
        } else {
            tvInitials.setText(Util.getInitialsFromName(selectedEmployee.getName()));
            tvInitials.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.GONE);
        }
    }

    public class ViewHolder{

        ImageView ivProfilePic;
        TextView tvEmployeeName;
        TextView tvEmployeeDesignation;
        LinearLayout rlCross;
        TextView tvInitials;

        public ViewHolder(View view){
            ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_feed_logo);
            tvEmployeeName = (TextView) view.findViewById(R.id.tv_user_feed_user_name);
            tvEmployeeDesignation = (TextView) view.findViewById(R.id.tv_user_feed_designation);
            tvInitials = (TextView) view.findViewById(R.id.tv_user_feed_initial);
            rlCross = (LinearLayout) view.findViewById(R.id.lay_right);
            rlCross.setVisibility(View.GONE);
        }
    }

    private void setUserFeedDataOnCurrentView(ViewHolder holder, Employee employee){
        holder.tvEmployeeName.setText(employee.getName());
        String designation = employee.getDesignation();
        if(!TextUtils.isEmpty(designation)){
            holder.tvEmployeeDesignation.setText(designation);
            holder.tvEmployeeDesignation.setVisibility(View.VISIBLE);
        } else {
            holder.tvEmployeeDesignation.setVisibility(View.GONE);
        }
        String imageUrl = employee.getProfileImage();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(SendSmileyActivity.this).load(imageUrl).into(holder.ivProfilePic);
            holder.tvInitials.setVisibility(View.GONE);
            holder.ivProfilePic.setVisibility(View.VISIBLE);
        } else {
            holder.tvInitials.setText(Util.getInitialsFromName(employee.getName()));
            holder.tvInitials.setVisibility(View.VISIBLE);
            holder.ivProfilePic.setVisibility(View.GONE);
        }
    }


}
