package com.growthfile.v2.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.growthfile.R;
import com.growthfile.enums.LeaveType;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.util.AppUtil;
import com.growthfile.util.FusedLocationService;
import com.growthfile.util.GFLocationManager;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.growthfile.v2.fragments.UserFeedFragmentV2;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.model.LatLng;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 12/17/16.
 */

public class HomeTabActivity extends AppBaseActivity implements UserFeedFragmentV2.EmptyRowItemClickListener {


    private static final int REQ_CODE_APPLY_LEAVE = 100;
    private static final int REQ_CODE_GET_LOCATION = 101;
    private ImageView ivHome, ivLocation, ivCollection, ivGrowthFile, ivSmiley, ivMore;
    private View viewHomeIndicator, viewLocationIndicator, viewCollectionIndicator, viewGrowthFileIndicator, viewSmileyIndicator, viewMoreIndicator;
    private Dialog menuDialog;
    private AppBaseFragment currentFragment;
    private boolean locationRefreshed;
    private LatLng latLng;
    private boolean locationFetched;
    private GFLocationManager instanceLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v2);
        initProgressBar();
        initTabs();
        setUpDefaultTab();
        handlePushData(getIntent().getExtras());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        handlePushData(bundle);
    }

    protected void logoutUser() {
        HttpParamObject paramObject = ApiRequestGeneratorV2.logout();
        executeTask(AppConstants.TASK_CODES.LOGOUT, paramObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.LOGOUT:
                doLogout();
                break;

        }
    }

    private void handlePushData(Bundle bundle) {
        if (bundle != null) {
            boolean isFormPush = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IS_FROM_PUSH, false);
            if (isFormPush) {
//                PushNavigationUtil.navigate(this, bundle);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        closeDialog();
        locationFetched = false;
    }

    private void setUpDefaultTab() {
        setSelectedIndicatorVisible(viewHomeIndicator);
        UserFeedFragmentV2 userFeedFragmentV2 = new UserFeedFragmentV2();
        userFeedFragmentV2.setEmptyRowItemClickListener(this);
        addFragment(userFeedFragmentV2, false);
    }

    private void initTabs() {

        ivHome = (ImageView) findViewById(R.id.iv_home_tab);
        ivLocation = (ImageView) findViewById(R.id.iv_location_tab);
        ivCollection = (ImageView) findViewById(R.id.iv_collection_tab);
        ivGrowthFile = (ImageView) findViewById(R.id.iv_growth_file_tab);
        ivSmiley = (ImageView) findViewById(R.id.iv_smiley_tab);
        ivMore = (ImageView) findViewById(R.id.iv_more_tab);

        viewHomeIndicator = findViewById(R.id.view_home);
        viewLocationIndicator = findViewById(R.id.view_location);
        viewCollectionIndicator = findViewById(R.id.view_collection);
        viewGrowthFileIndicator = findViewById(R.id.view_growth_file);
        viewSmileyIndicator = findViewById(R.id.view_smiley);
        viewMoreIndicator = findViewById(R.id.view_more);

        setOnClickListener(R.id.iv_home_tab, R.id.iv_location_tab, R.id.iv_collection_tab, R.id.iv_growth_file_tab, R.id.iv_smiley_tab, R.id.iv_more_tab);

        getLocationPermission();

    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        onViewIdClicked(viewId);
    }

    private void onViewIdClicked(int viewId) {
        switch (viewId) {
            case R.id.iv_home_tab:
                setSelectedIndicatorVisible(viewHomeIndicator);
                UserFeedFragmentV2 userFeedFragmentV2 = new UserFeedFragmentV2();
                userFeedFragmentV2.setEmptyRowItemClickListener(this);
                addFragment(userFeedFragmentV2, false);
                break;
            case R.id.iv_location_tab:
//                checkGPSenable();
                moveToLocationActivity();

                break;
            case R.id.iv_collection_tab:
                startActivityForResult(TransactionActivity.class, AppConstants.REQUEST_CODES.MARK_LOCATION, null);
                break;
            case R.id.iv_growth_file_tab:
                startActivityForResult(SendStarActivity.class, AppConstants.REQUEST_CODES.GIVE_STAR, null);
                break;
            case R.id.iv_smiley_tab:
                startActivityForResult(SendSmileyActivity.class, AppConstants.REQUEST_CODES.GIVE_SMILEY, null);
                break;
            case R.id.iv_more_tab:
                openDialog();
                break;
        }
    }

    private void setSelectedIndicatorVisible(View view) {
        setAllTabsIndicatorInvisible();
        view.setVisibility(View.VISIBLE);
    }

    private void setAllTabsIndicatorInvisible() {

        viewHomeIndicator.setVisibility(View.INVISIBLE);
        viewLocationIndicator.setVisibility(View.INVISIBLE);
        viewCollectionIndicator.setVisibility(View.INVISIBLE);
        viewGrowthFileIndicator.setVisibility(View.INVISIBLE);
        viewSmileyIndicator.setVisibility(View.INVISIBLE);
        viewMoreIndicator.setVisibility(View.INVISIBLE);
    }

    private void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack("");
        }
        currentFragment = (AppBaseFragment) fragment;
        fragmentTransaction.replace(R.id.fl_home_tab, fragment).commit();
    }

    private void openDialog() {

        LinearLayout llResign, llLeave, llMarkPresent, llLogOut;

        menuDialog = new Dialog(this);
        menuDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        menuDialog.getWindow().setGravity(Gravity.BOTTOM | Gravity.RIGHT);
        menuDialog.setContentView(R.layout.dialog_layout_more);


        llResign = (LinearLayout) menuDialog.findViewById(R.id.ll_lay_resign);
        llLeave = (LinearLayout) menuDialog.findViewById(R.id.ll_lay_leave);
        llMarkPresent = (LinearLayout) menuDialog.findViewById(R.id.ll_lay_present);
        llLogOut = (LinearLayout) menuDialog.findViewById(R.id.ll_lay_logout);

        llMarkPresent.setVisibility(View.GONE);
        llResign.setVisibility(View.GONE);
        //setClickListener(R.id.ll_lay_leave, R.id.ll_lay_resign);
        llLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeTabActivity.this, ApplyLeaveActivityV2.class);
                intent.putExtra(AppConstants.BUNDLE_KEYS.TOOLBAR_TITLE, LeaveType.LEAVE.getValue());
                intent.putExtra(AppConstants.BUNDLE_KEYS.LEAVE_TYPE, LeaveType.LEAVE.getType());
                startActivityForResult(intent, AppConstants.REQUEST_CODES.APPLY_LEAVE);
            }
        });
        llLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
        menuDialog.show();
    }

    private void closeDialog() {
        if (menuDialog != null && menuDialog.isShowing()) {
            menuDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_GET_LOCATION) {
            checkGPSenable();
            return;
        }
        if (currentFragment != null) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    // Location Marking Issue
    private void checkGPSenable() {
        if (checkInternetConnection()) {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                getLocationPermission();
            } else {
                buildAlertMessageNoGps();
            }
        }
    }

    private boolean checkInternetConnection() {
        if (Util.isConnectingToInternet(this)) {
            return true;
        } else {
            showCrouton(getString(R.string.internet_error));
        }
        return false;
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_error)
                .setCancelable(false)
                .setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQ_CODE_GET_LOCATION);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocationPermission() {
        if (AppUtil.checkPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
//            setLocation();
            startLocationFetch();
            return;
        }
        new TedPermission(this).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
//                        setLocation();
                        startLocationFetch();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).check();
    }

    private void startLocationFetch() {
        instanceLocation = GFLocationManager.getInstance();
        instanceLocation.startLocationTracking(this);
    }

    private void setLocation() {
        fetchLocation();
    }

    private GFLocationManager gfLocationManager;

    private void fetchLocation() {
        moveToLocationActivity();
//        showLoctionDialog();
//        gfLocationManager = new GFLocationManager(locationCallback);
//        gfLocationManager.startLocationTracking(this);
    }

    private ProgressDialog locationDialog;

    private void showLoctionDialog() {
        if (locationDialog != null && locationDialog.isShowing()) {
            return;
        } else {
            if (locationDialog == null) {
                locationDialog = new ProgressDialog(this);
            }
            locationDialog.setMessage("Please wait while we are fetching location");
            locationDialog.show();
        }
    }

    private void moveToLocationActivity() {
        Bundle b = new Bundle();
//        b.putDouble(AppConstants.PREF_KEYS.KEY_LAT, 28.7041);
//        b.putDouble(AppConstants.PREF_KEYS.KEY_LNG, location.getLongitude());
        startActivityForResult(LocationFormActivityV2.class, AppConstants.REQUEST_CODES.MARK_LOCATION, null);
    }

    private void hideLocationDialog() {
        if (locationDialog != null && locationDialog.isShowing()) {
            locationDialog.dismiss();
        }
    }

    private GFLocationManager.LocationCallback locationCallback = new GFLocationManager.LocationCallback() {
        @Override
        public void onLocationUpdate(Location location) {
            hideLocationDialog();
            if (gfLocationManager != null) {
                gfLocationManager.stopLocationUpdate();
            }
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            Bundle b = new Bundle();
            b.putDouble(AppConstants.PREF_KEYS.KEY_LAT, location.getLatitude());
            b.putDouble(AppConstants.PREF_KEYS.KEY_LNG, location.getLongitude());
            startActivityForResult(LocationFormActivityV2.class, AppConstants.REQUEST_CODES.MARK_LOCATION, b);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instanceLocation.stopLocationUpdate();
    }

    @Override
    public void onEmptyRowClicked(int id) {
        onViewIdClicked(id);
    }
}
