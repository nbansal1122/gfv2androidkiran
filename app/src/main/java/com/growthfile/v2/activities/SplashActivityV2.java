package com.growthfile.v2.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.ui.WebViewActivity;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Preferences;

/**
 * Created by ajay on 20/12/16.
 */

public class SplashActivityV2 extends BaseActivity {

    private final int SPLASH_DISPLAY_TIME = 3000;

    private TextView tvTermAndCondition;
    private ImageView ivLogo;
    private LinearLayout layTerms;
    private boolean isLogedIn;

    private static final String TNC_URL = "http://www.growthfile.com/terms.php";
    private static final String PP_URL = "http://www.growthfile.com/privacy.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_v2);
        ivLogo = (ImageView) findViewById(R.id.img_logo);
        layTerms = (LinearLayout) findViewById(R.id.liner_splash_terms);
        setAnimation();
        isLogedIn = Preferences.isUserLoggerIn();
        tvTermAndCondition = (TextView) findViewById(R.id.tv_term_condition);
        tvTermAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(LoginActivityV2.class);
                finish();
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_up);
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLogedIn) {
                    startNextActivity(getIntent().getExtras(), HomeTabActivity.class);
                    finish();
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_up);
                }
            }
        }, SPLASH_DISPLAY_TIME);
        setClickableSpan();
    }

    private void setClickableSpan() {
        String fullText = getString(R.string.growthfiles_tc);
        SpannableString ss = new SpannableString(fullText);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                WebViewActivity.startActivity(SplashActivityV2.this, TNC_URL, getString(R.string.tnc));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan privacyPolicy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                WebViewActivity.startActivity(SplashActivityV2.this, PP_URL, getString(R.string.privacy_policy));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        String tnc = getString(R.string.tnc);
        int i = fullText.indexOf(tnc);
        if (i < 0) {
            return;
        }
        ss.setSpan(clickableSpan, i, i + tnc.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        String pp = getString(R.string.privacy_policy);
        int j = fullText.indexOf(pp);
        if (j < 0) {
            return;
        }
        ss.setSpan(privacyPolicy, j, j + pp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView textView = (TextView) findViewById(R.id.tv_growthfile_tc);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    private void setAnimation() {

        Animation animation = AnimationUtils.loadAnimation(this,
                R.anim.bottom_up);
        ivLogo.setAnimation(animation);
        animation.start();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation animation1 = AnimationUtils.loadAnimation(SplashActivityV2.this,
                        R.anim.bottom_up);
                layTerms.setAnimation(animation1);
                if (!isLogedIn) {
                    layTerms.setVisibility(View.VISIBLE);
                }
                animation1.start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
