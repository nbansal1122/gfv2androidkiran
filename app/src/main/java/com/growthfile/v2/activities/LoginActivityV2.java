package com.growthfile.v2.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGeneratorV2;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.GenerateOtpApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by ajay on 20/12/16.
 */

public class LoginActivityV2 extends AppBaseActivity {

    private EditText etPhoneNo;
    private TextView tvUnableLogin;
    private RelativeLayout rlGetOtp;
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_v2);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_up);
        initProgressBar();
        etPhoneNo = (EditText) findViewById(R.id.et_login_phone_no);
        tvUnableLogin = (TextView) findViewById(R.id.tv_login_unable);
        rlGetOtp = (RelativeLayout) findViewById(R.id.rl_get_otp);
        rlGetOtp.setOnClickListener(this);
        setOnClickListener(R.id.rl_know_why);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_get_otp: {
                Util.hideKeyboard(this, etPhoneNo);
                validateAndGenerateOtp();
                break;
            }
            case R.id.rl_know_why:
                showDialog();
                break;
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.reason_not_login));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void validateAndGenerateOtp() {
        mobile = etPhoneNo.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            etPhoneNo.setError(getString(R.string.cannot_empty));
            return;
        } else if (mobile.length() != 10) {
            etPhoneNo.setError(getString(R.string.error_invalid_mobile));
            return;
        } else {
            etPhoneNo.setError(null);
        }
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.generateOtp(mobile);
        executeTask(AppConstants.TASK_CODES.GENERATE_OTP, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        hideProgressBar();
        hideNoInternetFrame();
        if (re != null) {
            switch (re.getHttpStatusCode()) {
                case 401:
                    if (!"Token invalid".equalsIgnoreCase(re.getMessage())) {
                        showCrouton(re.getMessage());
                        break;
                    }//Else logout as token is invalid
                    break;
                case 403:
                case 400:
                case 404:
                    showCrouton(re.getMessage());
                    break;
                default:
                    showCrouton("Internal Server Error!");
                    break;
            }
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GENERATE_OTP: {
                GenerateOtpApiResponse otpResponse = (GenerateOtpApiResponse) response;
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.OTP, otpResponse.getResponse().getOtp());
                bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, mobile);
                startNextActivity(bundle, OtpVerificationActivityV2.class);
                finish();
                break;
            }
        }
    }
}
