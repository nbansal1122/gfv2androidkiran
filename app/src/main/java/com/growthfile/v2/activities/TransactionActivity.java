package com.growthfile.v2.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGeneratorV2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.enums.TransactionType;
import simplifii.framework.rest.v2.responses.Account;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.GetAccountApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

/**
 * Created by ajay on 18/12/16.
 */

public class TransactionActivity extends AppBaseActivity {

    private EditText etTransactionAmount, etDueDate, etWriteComment;
    private ImageView ivAccountDropdown;
    private RadioGroup radioGroup;
    private RadioButton btnCollections, btnPayments;
    private final String UI_FORMAT = "dd-MM-yyyy";
    private final String API_FORMAT = "yyyy-MM-dd";


    private List<Account> accountList;
    private CustomListAdapter<Account> accountAdapter;
    private Account selectedAccount;
    private TextView tvSelectAccount;

    private RelativeLayout rlSubmit, rlCancel;
    private Dialog dialog;

    String dueDate;
    private boolean editTransaction;
    private int cardId;

    private boolean isAccountDropDownClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_transaction_form_v2);

        initToolBar("");
        getHomeIcon();
        initCroutonView();
        initProgressBar();

        tvSelectAccount = (TextView) findViewById(R.id.tv_auto_select_account);
        ivAccountDropdown = (ImageView) findViewById(R.id.iv_account_dropdown);
        etTransactionAmount = (EditText) findViewById(R.id.et_trans_amount);
        etDueDate = (EditText) findViewById(R.id.et_due_date);
        etWriteComment = (EditText) findViewById(R.id.et_write_comment);
        rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);

        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        btnCollections = (RadioButton) findViewById(R.id.btn_radio_collections);
        btnPayments = (RadioButton) findViewById(R.id.btn_radio_payment);

        setOnClickListener(R.id.et_due_date, R.id.rl_cancel, R.id.rl_submit, R.id.tv_auto_select_account, R.id.iv_account_dropdown);
        GetAccountApiResponse getAccountApiResponse = GetAccountApiResponse.getInstance();
        initiateAccounts(getAccountApiResponse);
    }

    private void initiateAccounts(GetAccountApiResponse getAccountApiResponse) {
        if (getAccountApiResponse != null) {
            if (!getAccountApiResponse.isError() && getAccountApiResponse.getAccountListContainer() != null) {
                List<Account> accountList = getAccountApiResponse.getAccountListContainer().getAccountList();
                if (CollectionsUtils.isNotEmpty(accountList)) {
                    if (editTransaction) {
                        for (Account account : accountList) {
                            if (tvSelectAccount.getText().toString().equals(account.getAccountName())) {
                                selectedAccount = account;
                                break;
                            }
                        }
                    }
                    this.accountList.clear();
                    this.accountList.addAll(accountList);
                    accountAdapter.notifyDataSetChanged();
                } else {
                    if (isAccountDropDownClicked) {
                        showCrouton(getString(R.string.no_account_exist));
                    }
                }
            } else {
                showCrouton(getAccountApiResponse.getMsg());
            }
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (getIntent() != null) {
            editTransaction = getIntent().getBooleanExtra(AppConstants.BUNDLE_KEYS.EDIT_TRANSACTION, false);
            if (editTransaction) {
                cardId = getIntent().getIntExtra(AppConstants.BUNDLE_KEYS.CARD_ID, -1);
                String status = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.STATUS);
                radioGroup.setEnabled(false);
                btnCollections.setEnabled(false);
                btnPayments.setEnabled(false);
                if (TransactionType.COLLECTION.getApiText().equalsIgnoreCase(status)) {
                    btnCollections.setChecked(true);
                    btnPayments.setChecked(false);
                } else if (TransactionType.PAYMENT.getApiText().equalsIgnoreCase(status)) {
                    btnCollections.setChecked(false);
                    btnPayments.setChecked(true);
                }
                String accountName = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.ACCOUNT_NAME);
                if (!TextUtils.isEmpty(accountName)) {
                    tvSelectAccount.setText(accountName);
                    ivAccountDropdown.setImageResource(R.drawable.cross);
                } else {
                    ivAccountDropdown.setImageResource(R.drawable.pointer_down);
                }
                String amount = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.AMOUNT);
                if (!TextUtils.isEmpty(amount)) {
                    etTransactionAmount.setText(amount);
                }
                String comment = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.COMMENT);
                if (!TextUtils.isEmpty(comment)) {
                    etWriteComment.setText(comment);
                }
                String savedDueDate = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.DUE_DATE);
                try {
                    String requiredDateString = Util.convertDateFormat(savedDueDate, API_FORMAT, UI_FORMAT);
                    etDueDate.setText(requiredDateString);
                    dueDate = savedDueDate;
                } catch (Exception e) {
                }
            }
        }
        accountList = new ArrayList<>();
        accountAdapter = new CustomListAdapter<>(this, R.layout.row_popup, accountList, new CustomListAdapterInterface() {
            @Override
            public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
                if (convertView == null) {
                    convertView = inflater.inflate(resourceID, parent, false);
                }
                final Account account = accountList.get(position);
                TextView tvAccountName = (TextView) convertView.findViewById(R.id.text1);
                tvAccountName.setText(account.getAccountName());
                tvAccountName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        tvSelectAccount.setText(account.getAccountName());
                        selectedAccount = account;
                        ivAccountDropdown.setImageResource(R.drawable.cross);
                    }
                });
                return convertView;
            }
        });
        fetchAccountList();
    }

    @Override
    protected void handleDefaultError(RestException re, Exception e, int taskCode, Object... params) {
        if (taskCode == AppConstants.TASK_CODES.GET_ACCOUNTS) {
            if (isAccountDropDownClicked) {
                showCrouton(getString(R.string.no_account_exist));
                return;
            } else {
                return;
            }
        }
        super.handleDefaultError(re, e, taskCode, params);

    }

    private void fetchAccountList() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.getAccountListRequest();
        executeTask(AppConstants.TASK_CODES.GET_ACCOUNTS, httpParamObject);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.et_due_date:
                setDate();
                break;
            case R.id.rl_cancel:
                finish();
                break;
            case R.id.rl_submit:
                validateAndBookTransaction();
                break;
            case R.id.tv_auto_select_account:
                if (editTransaction) {
                    showCrouton("You cannot change the account while editing a transaction!");
                    return;
                }
                if (CollectionsUtils.isEmpty(accountList)) {
                    isAccountDropDownClicked = true;
                    fetchAccountList();
                } else {
                    showAccountListDialog();
                }
                break;
            case R.id.iv_account_dropdown:
                if (editTransaction) {
                    showCrouton("You cannot change the account while editing a transaction!");
                    return;
                }
                if (CollectionsUtils.isEmpty(accountList))
                    fetchAccountList();
                if (selectedAccount == null) {
                    showAccountListDialog();
                } else {
                    tvSelectAccount.setText(getString(R.string.select_account));
                    selectedAccount = null;
                    ivAccountDropdown.setImageResource(R.drawable.pointer_down);
                }
                break;
        }
    }

    private void validateAndBookTransaction() {
        String status = null;
        if (!btnCollections.isChecked() && !btnPayments.isChecked()) {
            showCrouton("Please select transaction type!");
            radioGroup.requestFocus();
            return;
        }
        if (btnCollections.isChecked()) {
            status = TransactionType.COLLECTION.getApiText();
        } else {
            status = TransactionType.PAYMENT.getApiText();
        }
//        if (selectedAccount == null) {
//            showCrouton("Please select an account!");
//            return;
//        }
        String amount = etTransactionAmount.getText().toString().trim();
        if (TextUtils.isEmpty(amount)) {
            showCrouton("Please enter amount to book transaction!");
            return;
        } else if (Double.parseDouble(amount) < 0) {
            showCrouton("Amount must be greater than or equal to 0!");
            return;
        }
        if (dueDate == null) {
            showCrouton("Please select due date for transaction!");
            return;
        }
        String comment = etWriteComment.getText().toString().trim();
        Integer accountId = null;
        if (selectedAccount != null) {
            accountId = selectedAccount.getId();
        }
        if (editTransaction) {
            HttpParamObject httpParamObject = ApiRequestGeneratorV2.editTransactionRequest(cardId, dueDate, accountId, amount, status, comment);
            executeTask(AppConstants.TASK_CODES.EDIT_TRANSACTION, httpParamObject);
        } else {
            HttpParamObject httpParamObject = ApiRequestGeneratorV2.bookTransactionRequest(amount, status, dueDate, accountId, comment);
            executeTask(AppConstants.TASK_CODES.BOOK_TRANSACTION, httpParamObject);
        }
        enableOrDisableButton(R.id.rl_submit, false);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
        switch (taskCode) {
            case AppConstants.TASK_CODES.BOOK_TRANSACTION: {
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null) {
                    if (!TextUtils.isEmpty(baseApiResponse.getMsg())) {
                        showToast(baseApiResponse.getMsg());
                    }
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            }
            case AppConstants.TASK_CODES.GET_ACCOUNTS: {
                GetAccountApiResponse getAccountApiResponse = (GetAccountApiResponse) response;
                initiateAccounts(getAccountApiResponse);
                break;
            }
            case AppConstants.TASK_CODES.EDIT_TRANSACTION: {
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (!TextUtils.isEmpty(baseApiResponse.getMsg())) {
                    showToast(baseApiResponse.getMsg());
                } else {
                    showToast("Transaction edited successfully!");
                }
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    public void setDate() {

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        final int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        final int mMonth = calendar.get(Calendar.MONTH);
        final int mYear = calendar.get(Calendar.YEAR);

        final DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            private SimpleDateFormat uiDateFormatter = new SimpleDateFormat(UI_FORMAT);
            private SimpleDateFormat apiDateFormatter = new SimpleDateFormat(API_FORMAT);

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                calendar.set(year, month, dayOfMonth);

                String date = uiDateFormatter.format(calendar.getTime());
                etDueDate.setText(date);

                dueDate = apiDateFormatter.format(calendar.getTime());
            }
        }, mYear, mMonth, mDay);

        calendar.add(Calendar.MONTH, -6);
        dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, 12);
        dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dpd.show();
    }

    private void showAccountListDialog() {
        dialog = new Dialog(this);
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
//        window.setGravity(Gravity.NO_GRAVITY);
//        int[] location = new int[2];
//        tvSelectAccount.getLocationOnScreen(location);
//        window.getAttributes().x = location[0];
//        window.getAttributes().y = location[1];
        dialog.setContentView(R.layout.layout_listview);
        ListView lvAccounts = (ListView) dialog.findViewById(R.id.list_view);
        lvAccounts.setAdapter(accountAdapter);
        dialog.show();
    }
}


