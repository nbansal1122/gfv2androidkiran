package com.growthfile.v2.activities;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.growthfile.R;

//Domain: https://console.developers.google.com/apis/library
//        Login ID:Pujacapital@gmail.com
//Password: Saket.D90

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.Util;

/**
 * Created by robin on 12/21/16.
 */

public class AppBaseActivity extends BaseActivity {

    protected RelativeLayout rlProgressBar;
    private ViewGroup croutonView;
    private FrameLayout flNoInternet;
    private ImageView ivRetry;

    public void initProgressBar() {
        this.rlProgressBar = (RelativeLayout) findViewById(R.id.progress_bar);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.profile_back;
    }

    @Override
    public void onPreExecute(int taskCode) {
        showProgressDialog();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        hideProgressBar();
        hideNoInternetFrame();
    }

    public void hideNoInternetFrame() {
        if (flNoInternet != null) {
            flNoInternet.setVisibility(View.GONE);
//            if(ivRetry!=null&&ivRetry.getAnimation()!=null){
//                ivRetry.getAnimation().cancel();
//            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        hideProgressBar();
        hideNoInternetFrame();
        if (re != null) {
            switch (re.getHttpStatusCode()) {
                case 401:
                    showCrouton(re.getMessage());
                    doLogout();
                case 500:
                    showCrouton(re.getMessage());
//                    doLogout();
                    break;
                case 403:
                case 400:
                    showCrouton(re.getMessage());
                    break;
                default:
                    handleDefaultError(re, e, taskCode, params);
                    break;
            }
        }
    }

    protected void handleDefaultError(RestException re, Exception e, int taskCode, Object... params) {
        if (TextUtils.isEmpty(re.getMessage())) {
            showCrouton("Internal Server Error!");
        } else {
            showCrouton(re.getMessage());
        }
    }

    protected void doLogout() {
        UserSession.removeCurrentSession();
        startNextActivity(null, SplashActivityV2.class);
        finish();
    }

    @Override
    public void showProgressDialog() {
        if (rlProgressBar != null) {
            rlProgressBar.setVisibility(View.VISIBLE);
        } else {
            super.showProgressDialog();
        }
    }

    @Override
    protected void onInternetException(final int taskCode) {
        showCrouton(getString(R.string.internet_error));
        flNoInternet = (FrameLayout) findViewById(R.id.no_internet_frame);
        if (flNoInternet != null) {
            flNoInternet.setVisibility(View.VISIBLE);
            ivRetry = (ImageView) findViewById(R.id.iv_retry);
            ivRetry.clearAnimation();
            flNoInternet.findViewById(R.id.ll_reload).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Animation a = AnimationUtils.loadAnimation(AppBaseActivity.this,
                            R.anim.slide_in);
                    a.setDuration(800);
                    ivRetry.startAnimation(a);
                    if (Util.isConnectingToInternet(AppBaseActivity.this)) {
                        onRetryClicked(view, taskCode);
                    }
                }
            });
        }
    }

    @Override
    public void onRetryClicked(View view, int taskCode) {
        super.onRetryClicked(view, taskCode);
    }


    @Override
    public void hideProgressBar() {
        if (rlProgressBar != null) {
            rlProgressBar.setVisibility(View.GONE);
        } else {
            super.hideProgressBar();
        }
    }

    public void showCrouton(String msg) {
        if (true) {
            showToast(msg);
            return;
        }
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(4500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(150)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        if (croutonView != null) {
            Crouton crouton = Crouton.makeText(AppBaseActivity.this, msg, style, croutonView);
            crouton.show();
        } else {
            Crouton.showText(AppBaseActivity.this, msg, style);
        }
    }

    protected void enableOrDisableButton(int id, boolean enable) {
        findViewById(id).setEnabled(enable);
    }

    protected void initCroutonView() {
        setCroutonView(R.id.frame_crouton);
    }

    public void setCroutonView(int viewId) {
        this.croutonView = (ViewGroup) findViewById(viewId);
    }

//    @Override
//    public void showToast(String message) {
//        showCrouton(message);
//    }

}
