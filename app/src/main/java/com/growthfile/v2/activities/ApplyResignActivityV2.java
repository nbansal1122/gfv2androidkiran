package com.growthfile.v2.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGeneratorV2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;

/**
 * Created by ajay on 19/12/16.
 */

public class ApplyResignActivityV2 extends AppBaseActivity {

    private RelativeLayout rlSubmit, rlCancel;
    private EditText etLastDate;
    private String lastDate;
    private TextView tvEmpName;
    private EditText etWriteCmt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resign_v2);
        initCroutonView();
        initProgressBar();
        initToolBar("");
        getHomeIcon();
        rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);
        etLastDate = (EditText) findViewById(R.id.et_last_date);
        tvEmpName = (TextView) findViewById(R.id.tv_emp_name);
        etWriteCmt = (EditText) findViewById(R.id.et_write_comment);

        String empName = UserSession.getSessionInstance().getName();
        tvEmpName.setText(empName);

        setOnClickListener(R.id.rl_cancel, R.id.rl_submit, R.id.et_last_date);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_cancel:
                finish();
                break;
            case R.id.rl_submit:
                validateAndSubmit();
                // showCrouton("Click not implemented yet!");
                break;
            case R.id.et_last_date:
                setDate();
        }
    }

    private void setDate() {
        final Calendar calendar = Calendar.getInstance();
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String date = dateFormat.format(calendar.getTime());
                etLastDate.setText(date);

                SimpleDateFormat apiDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                lastDate = apiDateFormat.format(calendar.getTime());
            }
        }, day, month, year);
        Calendar maxDateCal = Calendar.getInstance();
        maxDateCal.setTime(new Date());
        dpd.getDatePicker().setMinDate(maxDateCal.getTimeInMillis());
        maxDateCal.add(Calendar.MONTH, 3);
        dpd.getDatePicker().setMaxDate(maxDateCal.getTimeInMillis());
//        dpd.getDatePicker().setMaxDate(new Date().getTime() + 43200000);
        dpd.show();
    }

    private void validateAndSubmit() {
        String cmt = etWriteCmt.getText().toString().trim();
        if (lastDate == null) {
            showCrouton("Please select last working date");
            return;
        }
        if (TextUtils.isEmpty(cmt)) {
            showCrouton(getString(R.string.comment_required));
            return;
        }
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.resignRequest(lastDate, cmt);
        executeTask(AppConstants.TASK_CODES.RESIGN_REQUEST, httpParamObject);
        enableOrDisableButton(R.id.rl_submit, false);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
        switch (taskCode) {
            case AppConstants.TASK_CODES.RESIGN_REQUEST:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null) {
                    if (!TextUtils.isEmpty(baseApiResponse.getMsg())) {
                        showToast(baseApiResponse.getMsg());
                    }
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }
}
