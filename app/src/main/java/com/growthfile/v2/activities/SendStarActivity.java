package com.growthfile.v2.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.R;
import com.growthfile.utility.ApiRequestGeneratorV2;
import com.growthfile.v2.TestEmailToken.ContactsCompletionView;
import com.squareup.picasso.Picasso;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.v2.responses.BaseApiResponse;
import simplifii.framework.rest.v2.responses.Employee;
import simplifii.framework.rest.v2.responses.GetEmployeesApiResponse;
import simplifii.framework.rest.v2.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.Util;

public class SendStarActivity extends AppBaseActivity implements TokenCompleteTextView.TokenListener<Employee> {

    private List<Employee> employeeList;
    private ContactsCompletionView completionView;
    private ArrayAdapter<Employee> adapter;
    private ViewHolder viewHolder;
    private Map<Integer, Employee> idVsEmployeeMap = new HashMap<>();
    private RelativeLayout rlSubmit, rlCancel;
    private EditText etComment;
    private int tokenCounter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_star_v2);
        initProgressBar();
        initCroutonView();
        employeeList = new ArrayList<>();

        rlSubmit = (RelativeLayout) findViewById(R.id.rl_submit);
        rlCancel = (RelativeLayout) findViewById(R.id.rl_cancel);
        etComment = (EditText) findViewById(R.id.et_comment);
        adapter = new FilteredArrayAdapter<Employee>(this, R.layout.row_send_smiley, employeeList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder viewHolder = null;
                if (convertView == null) {
                    convertView = LayoutInflater.from(SendStarActivity.this).inflate(R.layout.row_send_smiley, parent, false);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }

                Employee employee = getItem(position);
                viewHolder.tvEmployeeName.setText(employee.getName());
                String designation = employee.getDesignation();
                if (!TextUtils.isEmpty(designation)) {
                    viewHolder.tvEmployeeDesignation.setText(designation);
                    viewHolder.tvEmployeeDesignation.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvEmployeeDesignation.setVisibility(View.GONE);
                }
                String imageUrl = employee.getProfileImage();
                if (!TextUtils.isEmpty(imageUrl)) {
                    Picasso.with(SendStarActivity.this).load(imageUrl).into(viewHolder.ivProfilePic);
                    viewHolder.tvInitials.setVisibility(View.GONE);
                    viewHolder.ivProfilePic.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvInitials.setText(Util.getInitialsFromName(employee.getName()));
                    viewHolder.tvInitials.setVisibility(View.VISIBLE);
                    viewHolder.ivProfilePic.setVisibility(View.GONE);
                }

                return convertView;
            }

            @Override
            protected boolean keepObject(Employee emp, String mask) {
                if (idVsEmployeeMap.containsKey(emp.getId()))
                    return false;
                if ("".equals(mask))
                    return true;
                return emp.getName().toLowerCase().contains(mask.toLowerCase());
            }
        };
        GetEmployeesApiResponse getEmployeesApiResponse = GetEmployeesApiResponse.getInstance();
        initiateList(getEmployeesApiResponse);
        completionView = (ContactsCompletionView) findViewById(R.id.searchView);
        completionView.setAdapter(adapter);
        completionView.setTokenListener(this);
//        completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.None);
        completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.None);
        completionView.setThreshold(1);
        completionView.setTokenLimit(6);
        completionView.allowCollapse(false);
        completionView.setTextIsSelectable(false);
        completionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idVsEmployeeMap != null && idVsEmployeeMap.keySet().size() < 6 && tokenCounter < 6) {
                    if (CollectionsUtils.isNotEmpty(employeeList))
                        completionView.showDropDown();
                }
            }
        });

//        completionView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if(hasFocus){
//                    if(CollectionsUtils.isNotEmpty(employeeList))
//                        completionView.showDropDown();
//                } else {
//                    if(completionView.isPopupShowing())
//                        completionView.dismissDropDown();
//                }
//            }
//        });
        completionView.requestFocus();
        completionView.setPrefix("To: ");
        setOnClickListener(R.id.rl_cancel, R.id.rl_submit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_cancel:
                finish();
                break;
            case R.id.rl_submit:
                validateAndSubmit();
                break;
        }
    }

    private void validateAndSubmit() {
        if (CollectionsUtils.isMapEmpty(idVsEmployeeMap)) {
            showCrouton(getString(R.string.no_recipient_selected));
            return;
        }
        String comment = etComment.getText().toString().trim();
        if (TextUtils.isEmpty(comment)) {
            showCrouton(getString(R.string.comment_required));
            return;
        }
        List<Integer> receiverIds = new ArrayList<>(idVsEmployeeMap.keySet());
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.sendStarRequest(receiverIds, comment);
        executeTask(AppConstants.TASK_CODES.SEND_STAR, httpParamObject);
        enableOrDisableButton(R.id.rl_submit, false);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchEmployeeList();
    }

    private void fetchEmployeeList() {
        HttpParamObject httpParamObject = ApiRequestGeneratorV2.getEmployees();
        executeTask(AppConstants.TASK_CODES.GET_EMPLOYEES, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_EMPLOYEES: {
                GetEmployeesApiResponse getEmployeesApiResponse = (GetEmployeesApiResponse) response;
                initiateList(getEmployeesApiResponse);
                break;
            }
            case AppConstants.TASK_CODES.SEND_STAR: {
                enableOrDisableButton(R.id.rl_submit, true);
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null) {
                    if (!TextUtils.isEmpty(baseApiResponse.getMsg()))
                        showToast(baseApiResponse.getMsg());
                    else
                        showToast("You have successfully given the star");
                    setResult(RESULT_OK);
                    finish();
                }
            }
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        enableOrDisableButton(R.id.rl_submit, true);
    }

    private void initiateList(GetEmployeesApiResponse getEmployeesApiResponse) {
        if (getEmployeesApiResponse != null && getEmployeesApiResponse.getEmployeeListContainer() != null) {
            List<Employee> employeeList = getEmployeesApiResponse.getEmployeeListContainer().getEmployeeList();
            if (CollectionsUtils.isNotEmpty(employeeList)) {
                this.employeeList.clear();
                UserSession session = UserSession.getSessionInstance();
                for (Employee e : employeeList) {
                    if (e.getId().compareTo(session.getId()) == 0) {
                    } else {
                        this.employeeList.add(e);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    public class ViewHolder {

        ImageView ivProfilePic;
        TextView tvEmployeeName;
        TextView tvEmployeeDesignation;
        TextView tvInitials;
        LinearLayout rlCross;

        public ViewHolder(View view) {
            ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_feed_logo);
            tvEmployeeName = (TextView) view.findViewById(R.id.tv_user_feed_user_name);
            tvEmployeeDesignation = (TextView) view.findViewById(R.id.tv_user_feed_designation);
            tvInitials = (TextView) view.findViewById(R.id.tv_user_feed_initial);
            rlCross = (LinearLayout) view.findViewById(R.id.lay_right);
            rlCross.setVisibility(View.GONE);
        }
    }


    @Override
    public void onTokenAdded(Employee employee) {
        tokenCounter++;
        Log.d("added token", "employee id " + employee.getId());
        idVsEmployeeMap.put(employee.getId(), employee);
        Iterator<Employee> employeeIterator = employeeList.iterator();
        while (employeeIterator.hasNext()) {
            int empId = employeeIterator.next().getId();
            if (idVsEmployeeMap.containsKey(empId)) {
                employeeIterator.remove();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenRemoved(Employee employee) {
        tokenCounter--;
        Log.d("removed token", "employee id " + employee.getId());
        idVsEmployeeMap.remove(employee.getId());
        if (employeeList != null) {
            employeeList.add(employee);
        }
        adapter.notifyDataSetChanged();


    }


}

