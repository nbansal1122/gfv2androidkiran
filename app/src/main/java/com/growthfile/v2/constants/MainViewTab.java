package com.growthfile.v2.constants;

import com.growthfile.R;

/**
 * Created by robin on 12/17/16.
 */

public enum MainViewTab {

    HOME(R.drawable.home,0),
    LOCATION(R.drawable.location_nav,1),
    COLLECTION(R.drawable.rupee_bottom,2),
    GROWTH_FILE(R.drawable.growthfile_nav,3),
    SMILEY(R.drawable.smiley_nav,4),
    MORE(R.drawable.more,5);


    int iconId;
    int position;

    public int getIconId() {
        return iconId;
    }

    public int getPosition() {
        return position;
    }
    private MainViewTab(int iconId, int position){
        this.iconId = iconId;
        this.position = position;
    }
    public static MainViewTab findTabByPosition(int position){
        for(MainViewTab mainViewTab : MainViewTab.values()){
            if(mainViewTab.position == position){
                return mainViewTab;
            }
        }
        return null;
    }
}
