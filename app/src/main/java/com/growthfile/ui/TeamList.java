package com.growthfile.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import simplifii.framework.rest.responses.GetTeamResponse;
import simplifii.framework.rest.responses.TeamResponseMangerPojo;

import com.growthfile.R;
import com.growthfile.adapter.BaseRecyclerAdapter;
import com.growthfile.database.DbHelper;
import com.growthfile.fragment.CreateRoleFragment;
import simplifii.framework.model.BaseRecyclerModel;
import simplifii.framework.model.CreateRoleModel;
import com.growthfile.model.ListCreateRoleModel;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;

import simplifii.framework.rest.responses.UserProfile;
import simplifii.framework.utility.Prefs;

import com.growthfile.adapter.TeamFragmentAdapter;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeamList extends Fragment implements View.OnClickListener {


    Prefs prefs;
    private RelativeLayout relative_myteam_offline, rl_no_team_member_found, progress_team_list, rl_add_role;
    private EditText et_local_team_search;
    private FrameLayout frag_container;
    private LinearLayout liner_teamoffline_setting, ll_cancel_team_search, ll_teamsearch;
    List<UserProfile> team;
    private List<CreateRoleModel> createRoleModelList = new ArrayList<>();
    private ArrayList<UserProfile> arrayListteam;
    private TeamFragmentAdapter teamFragmentAdapter;
    private BaseRecyclerAdapter baseRecyclerAdapter;
    private List<BaseRecyclerModel> recyclerModelList = new ArrayList<>();
    RecyclerView recyclerView;
    TextView tv_myteam_heading;
    ImageView team_retry;


    public TeamList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        ListCreateRoleModel.init(getActivity());
        prefs = new Prefs();
        View view = inflater.inflate(R.layout.fragment_team_list, container, false);

        arrayListteam = new ArrayList<>();
        team = new ArrayList<>();
        team_retry = (ImageView) view.findViewById(R.id.team_retry);
        recyclerView = (RecyclerView) view.findViewById(R.id.rw_teams);
        frag_container = (FrameLayout) view.findViewById(R.id.frag_container);
        rl_add_role = (RelativeLayout) view.findViewById(R.id.rel_applyleavecard_cross);
        progress_team_list = (RelativeLayout) view.findViewById(R.id.progress_bar);
        relative_myteam_offline = (RelativeLayout) view.findViewById(R.id.relative_myteam_offline);
        rl_no_team_member_found = (RelativeLayout) view.findViewById(R.id.rl_no_team_member_found);
        liner_teamoffline_setting = (LinearLayout) view.findViewById(R.id.liner_teamoffline_setting);
        ll_cancel_team_search = (LinearLayout) view.findViewById(R.id.ll_cancel_team_search);
        ll_teamsearch = (LinearLayout) view.findViewById(R.id.ll_search);
        ll_cancel_team_search.setOnClickListener(this);
        rl_add_role.setOnClickListener(this);
        et_local_team_search = (EditText) view.findViewById(R.id.et_local_team_search);
        tv_myteam_heading = (TextView) view.findViewById(R.id.tv_myteam_heading);

        et_local_team_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_local_team_search.getText().toString().length() > 0) {
                    getfilteredTeamList(et_local_team_search.getText().toString());
                    if (teamFragmentAdapter != null) {
                        teamFragmentAdapter.notifyDataSetChanged();
                    }
                } else {
                    getTeamData();
                    if (teamFragmentAdapter != null) {
                        teamFragmentAdapter.notifyDataSetChanged();

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*
         * CLICK ON RETRY BUTTON WHEN OFFLINE
         */
        liner_teamoffline_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Animation a = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in);
                a.setDuration(1000);
                team_retry.startAnimation(a);
                if (Helper.checkInternetConnection(getActivity())) {
                    relative_myteam_offline.setVisibility(View.GONE);
                    tv_myteam_heading.setText("Team");
                    progress_team_list.setVisibility(View.VISIBLE);
                    // TEAM API CALL
                    callTeamApi();
                } else {
                    tv_myteam_heading.setText("Offline");
                    relative_myteam_offline.setVisibility(View.VISIBLE);
                }

            }
        });

        /*
         * call team list api on fragment load
         */
       /* if (Helper.checkInternetConnection(getActivity())) {
            relative_myteam_offline.setVisibility(View.GONE);
            progress_team_list.setVisibility(View.VISIBLE);
            tv_myteam_heading.setText("Team");
            callTeamApi();
        } else {
            tv_myteam_heading.setText("Offline");
            relative_myteam_offline.setVisibility(View.VISIBLE);
        }*/
        relative_myteam_offline.setVisibility(View.GONE);
        progress_team_list.setVisibility(View.VISIBLE);
        tv_myteam_heading.setText("Team");
        callTeamApi();
        setRecyclerAdapter();
        return view;

    }

    private void setRecyclerAdapter() {
        baseRecyclerAdapter = new BaseRecyclerAdapter(recyclerModelList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(baseRecyclerAdapter);
    }

    public static TeamList newInstance() {
        return new TeamList();
    }

    /**
     * Method to call team api
     */
    void callTeamApi() {
        if (prefs == null) {
            prefs = new Prefs();
        }

        String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        Call<GetTeamResponse> call = restApis.getTeamInfo("bearer " + auth_token);
        call.enqueue(new Callback<GetTeamResponse>() {
            @Override
            public void onResponse(Call<GetTeamResponse> call, Response<GetTeamResponse> response) {
                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    if (response.raw().code() == 200) {
                        progress_team_list.setVisibility(View.GONE);

                        team = response.body().getResponse().getTeam();
                        if (team.size() > 0) {
                            ll_teamsearch.setVisibility(View.VISIBLE);
                        }
                        TeamResponseMangerPojo mng = response.body().getResponse().getManager();
                        getTeamData();

                        System.out.println("arrayListteam=" + arrayListteam.size());
                        if (arrayListteam.size() > 0 || !mng.getName().equals("")) {
                            rl_no_team_member_found.setVisibility(View.GONE);
                        } else {
                            rl_no_team_member_found.setVisibility(View.VISIBLE);
                        }

//                        teamFragmentAdapter = new TeamFragmentAdapter(getActivity(),arrayListteam, mng);
//                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                        recyclerView.setAdapter(teamFragmentAdapter);
                    } else if (response.raw().code() == 401) {
                        progress_team_list.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        logout();
                    } else {
                        progress_team_list.setVisibility(View.GONE);
                        String errorMsg;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<GetTeamResponse> call, Throwable t) {
                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    String errorMsg = t.getMessage();
                    progress_team_list.setVisibility(View.GONE);
                    tv_myteam_heading.setText("Offline");
                    relative_myteam_offline.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    @Override
    public void onStart() {

        super.onStart();

        //System.out.println("Team on start");

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            System.out.println("Team on start");
        } else {
            // Do your Work
        }
    }

    // get data from api to bind child
    private void getTeamData() {

        recyclerModelList.clear();
        recyclerModelList.addAll(team);
        List<CreateRoleModel> createRoleModels = ListCreateRoleModel.loadList();
        recyclerModelList.addAll(createRoleModels);
        baseRecyclerAdapter.notifyDataSetChanged();

    }


    /**
     * method to get filtered list after local search in team list
     *
     * @param constraint
     * @return
     */
    private void getfilteredTeamList(String constraint) {
        arrayListteam.clear();

        for (int i = 0; i < team.size(); i++) {
            if (team.get(i).getName().toLowerCase().contains(constraint.toLowerCase())) {
                arrayListteam.add(team.get(i));
            }
        }

        if (arrayListteam.size() > 0) {
            rl_no_team_member_found.setVisibility(View.GONE);
        } else {
            rl_no_team_member_found.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_cancel_team_search:
                callTeamApi();
                /*getTeamData();
                if (teamFragmentAdapter!=null){
                    teamFragmentAdapter.notifyDataSetChanged();

                }
                rl_no_team_member_found.setVisibility(View.GONE);*/
                // getTeamData();
                //teamFragmentAdapter.notifyDataSetChanged();
                et_local_team_search.setText("");
                et_local_team_search.clearFocus();
                break;

            case R.id.progress_bar:
                break;

            case R.id.rel_applyleavecard_cross:
                CreateRoleFragment createRole = CreateRoleFragment.getInstance();
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.add(R.id.frag_container, createRole, null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        progress_team_list.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progress_team_list.setVisibility(View.GONE);
    }

    void logout() {
        prefs.clearAppData(getActivity());
        DbHelper dbHelper = DbHelper.getInstance(getActivity());
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(getActivity(), Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        getActivity().finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

//    @Override
//    public void onCreateRole(CreateRoleModel createRole) {
//        recyclerModelList.add(createRole);
//        baseRecyclerAdapter.notifyDataSetChanged();
//        saveData(createRole);
//    }

    private void saveData(CreateRoleModel createRole) {
        ListCreateRoleModel listCreateRoleModel = new ListCreateRoleModel();
        List<CreateRoleModel> createRoleList = ListCreateRoleModel.loadList();
        createRoleList.add(createRole);
        listCreateRoleModel.setCreateRoleList(createRoleList);
        ListCreateRoleModel.saveList(listCreateRoleModel);
    }
}
