package com.growthfile.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.growthfile.R;

public class TcWebView extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tc_web_view);
        webView= (WebView) findViewById(R.id.wv_tc);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://www.google.com");

        findViewById(R.id.rl_webview_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
