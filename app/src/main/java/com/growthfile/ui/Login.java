package com.growthfile.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.growthfile.GCMIntentService;
import com.growthfile.Pojo.OtpPojo;
import com.growthfile.R;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Prefs;

import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.growthfile.webservices.SendOtpData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Login extends AppCompatActivity implements View.OnClickListener {


    private EditText et_number;
    private ImageView iv_get_otp;
    private LinearLayout ll_login_root;
    private RelativeLayout rl_progress_login;
    Prefs prefs;
    Retrofit retroFitMaker;
    private int READ_SMS = 2;
    private String[] PERMISSION_READ_SMS = {Manifest.permission.READ_SMS};
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = new Prefs();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            requestReadSmsPermission();
        }

        initUi();
        //Initializing our broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            //When the broadcast received
            //We are sending the broadcast from GCMRegistrationIntentService

            @Override
            public void onReceive(Context context, Intent intent) {
                //If the broadcast has received with success
                //that means device is registered successfully
                if (intent.getAction().equals(GCMIntentService.REGISTRATION_SUCCESS)) {
                    //Getting the registration token from the intent
                    String token = intent.getStringExtra("token");
                    prefs.setPreferencesString(Login.this, "token", token);
                    //Displaying the token as toast
                    //     Toast.makeText(getApplicationContext(), "Registration token:" + token, Toast.LENGTH_LONG).show();

                    //if the intent is not with success then displaying error messages
                } else if (intent.getAction().equals(GCMIntentService.REGISTRATION_ERROR)) {
                    Toast.makeText(getApplicationContext(), "GCM registration error!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Error occurred", Toast.LENGTH_LONG).show();
                }
            }
        };


        //Checking play service is available or not
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        //if play service is not available
        if (ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //Displaying message that play service is not installed
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                //If play service is not supported
                //Displaying an error message
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {
            //Starting intent to register device
            Intent itent = new Intent(this, GCMIntentService.class);
            startService(itent);
        }

        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (isOpen) {

                    ll_login_root.scrollTo(0, ll_login_root.getHeight() / 4);

                } else {
                    ll_login_root.scrollTo(0, 0);
                }
            }
        });

    }

    void initUi() {
        rl_progress_login = (RelativeLayout) findViewById(R.id.rl_progress_login);
        ll_login_root = (LinearLayout) findViewById(R.id.ll_login_root);
        et_number = (EditText) findViewById(R.id.et_number);
        iv_get_otp = (ImageView) findViewById(R.id.iv_get_otp);
        et_number.requestFocus();
        iv_get_otp.setOnClickListener(this);
        findViewById(R.id.rl_know_why).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_get_otp:

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_number.getWindowToken(), 0);
                if (et_number.getText().toString().trim().length() == 10) {

                    String phone = et_number.getText().toString().trim();
                    prefs.setPreferencesString(Login.this, "PhoneNo", phone);


                    if (Helper.checkInternetConnection(this)) {

                        API_Task(phone);
                        rl_progress_login.setVisibility(View.VISIBLE);
                    } else {
                        Intent offlineIntent = new Intent(Login.this, OfflineError.class);
                        startActivity(offlineIntent);
                    }

                } else {

                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(4500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(150)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(Login.this, getText(R.string.incorrect_phone_number), style);
                    //  Toast.makeText(Login.this, getText(R.string.incorrect_phone_number), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.rl_progress_login:
                break;
            case R.id.rl_know_why:
                showDialog();
                break;
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.not_login_know_why));
        builder.setTitle("Growth File");

        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

        builder.create().show();
    }


    private void API_Task(final String map) {

        SendOtpData sendOtpData = new SendOtpData();
        sendOtpData.mobile = map;
        //HashMap hashMap1 = map;
        try {

            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.BASE_URL);
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<OtpPojo> call = apiInterface.getOTP(sendOtpData);

            call.enqueue(new Callback<OtpPojo>() {
                @Override
                public void onResponse(Call<OtpPojo> call, Response<OtpPojo> response) {

                    if (response.raw().code() == 200) {
                        rl_progress_login.setVisibility(View.GONE);
                        askForOTPPermission(map);
                    } else {
                        rl_progress_login.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                            // name = sys.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(Login.this, name, Toast.LENGTH_LONG).show();
                        return;
                    }

                }

                @Override
                public void onFailure(Call<OtpPojo> call, Throwable t) {

                    String errorMsg = t.getMessage();
                    rl_progress_login.setVisibility(View.GONE);
                    Toast.makeText(Login.this, errorMsg, Toast.LENGTH_LONG).show();
                    Intent offlineIntent = new Intent(Login.this, OfflineError.class);
                    startActivity(offlineIntent);

                }
            });


        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private void moveToOTPScreen(String phone) {
        Intent intent = new Intent(Login.this, OtpVerification.class);
        intent.putExtra(AppConstants.BUNDLE_KEYS.PHONE, phone);
        startActivity(intent);
        finish();
    }

    private void askForOTPPermission(final String phone) {
        moveToOTPScreen(phone);
    }

    /**
     * method to request sms permission
     */
    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, PERMISSION_READ_SMS, READ_SMS);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(Login.this, getText(R.string.sms_permission_request), style);
                    //   Toast.makeText(Login.this,getText(R.string.sms_permission_request), Toast.LENGTH_LONG).show();
                    finish();
                }

                break;
        }
    }

    //Registering receiver on activity resume
    @Override
    protected void onResume() {
        super.onResume();
        // Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_ERROR));
    }


    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
        super.onPause();
        //   Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }
}
