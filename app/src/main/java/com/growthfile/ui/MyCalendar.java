package com.growthfile.ui;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.ApplySelfLeavePojo;
import com.growthfile.Pojo.SelfCalendarModel;
import com.growthfile.Pojo.SelfCalendarResponse;
import com.growthfile.R;
import com.growthfile.adapter.CalendarInfoListAdapter;
import com.growthfile.database.DbHelper;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Prefs;

import com.growthfile.customUi.TextViewWorkSansRegular;
import com.growthfile.webservices.ApplySelfLeaveRawData;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCalendar extends AppBaseFragment implements View.OnClickListener {
    private CaldroidFragment caldroidFragment;
    private Date dateFrom, dateTo, current_date;
    private String from_date, to_date;
    private List<String> selectedDate = new ArrayList<>();
    private List<Date> selectedDateList = new ArrayList<>();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
    private LinearLayout ll_mycalendar_retry, ll_mycalendar_offline;
    private RelativeLayout rl_apply_for_leave, rl_two_btn_container, rl_selected_date_events, rl_self_calendar_info,
            rl_selected_date_info, rl_two_btn_apply_leave, rl_two_btn_mark_present, rl_progress_my_calendar, rel_mainmycal, rl_header;
    private ImageView iv_punchin_out_my_calendar, iv_my_calendar_info_check, iv_symbol_info, img_mycal_scuess_done, mycal_retry;
    private TextView tv_mycal_successmsg, tv_selected_Date, tv_date_corresponding_info_second, tv_date_corresponding_info_first, txt_slelcted_date, tv_mycal_heading;
    private View calendar_event_color;
    private Prefs prefs;
    int[] month_day = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private List<String> datesFromApi, appliedBy, approvedBy;
    private SelfCalendarResponse calendarResponse;
    private SelfCalendarModel selfCalendarModel;
    HashMap<String, String> colorcode = new HashMap<>();
    Retrofit retroFitMaker;
    private Animation loader_animation;
    private RecyclerView rv_my_calendar_info;
    String f_date = "", t_date = "";
    MyCalendarUpdateCallback mCallback;
    int current_Month;

    public MyCalendar() {
        // Required empty public constructor
    }


    public static MyCalendar newInstance() {
        MyCalendar f = new MyCalendar();
        return f;
    }

    @Override
    public void initViews() {
        prefs = new Prefs();

        datesFromApi = new ArrayList<>();
        approvedBy = new ArrayList<>();
        appliedBy = new ArrayList<>();
        initUi();
        loader_animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotation);

        initializeCalendar();
        handleDateSelection(6);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_calendar;
    }

    @Override
    public void setUserVisibleHint(final boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible) {
            System.out.println("my cal visible");
            initializeCalendar();
        }
    }


    @Override
    public void onStart() {
        System.out.println("inside mycal onstart");
        super.onStart();

        if (prefs.getPreferencesString(getActivity(), "landingFlag").equals("Calander")) {
            prefs.setPreferencesString(getActivity(), "landingFlag", "");
            Configuration croutonConfiguration = new Configuration.Builder()
                    .setDuration(5500).build();
            // Define custom styles for crouton
            Style style = new Style.Builder()
                    .setBackgroundColorValue(Color.parseColor("#80000000"))
                    .setGravity(Gravity.CENTER_HORIZONTAL)
                    .setConfiguration(croutonConfiguration)
                    .setHeight(120)
                    .setTextColorValue(Color.parseColor("#ffffff")).build();
            // Display notice with custom style and configuration
            Crouton.showText(getActivity(), getText(R.string.punchin_sucess), style, rel_mainmycal);
        }
    }


    /**
     * method to set background of dates when multiple date selected
     */
    void setSelectedDateBackground() {
        List<Date> allSelectedDates = new ArrayList<>();
        if (dateTo.compareTo(dateFrom) > 0) {
            allSelectedDates = getDaysBetweenDates(dateFrom, dateTo);
        } else {
            allSelectedDates = getDaysBetweenDates(dateTo, dateFrom);
        }
        HashMap<Date, Drawable> selectedDateData = new HashMap<>();
        for (int i = 0; i < allSelectedDates.size(); i++) {
            selectedDateData.put(allSelectedDates.get(i), getResources().getDrawable(R.drawable.date_selected_bg));
        }
        HashMap<Date, Integer> selectedDateDataForTextBg = new HashMap<>();
        for (int i = 0; i < allSelectedDates.size(); i++) {
            selectedDateDataForTextBg.put(allSelectedDates.get(i), R.color.cyan_text);
        }
        caldroidFragment.setSelectedDates(dateFrom, dateTo);
        caldroidFragment.setBackgroundDrawableForDates(selectedDateData);
        caldroidFragment.setTextColorForDates(selectedDateDataForTextBg);

        if (!allSelectedDates.contains(new Date())) {
            caldroidFragment.setTextColorForDate(R.color.colorAccent, new Date());
        } else {
            caldroidFragment.setTextColorForDate(R.color.cyan_text, new Date());
        }
        caldroidFragment.refreshView();

    }


    /**
     * Method to get date between two date
     *
     * @param startdate
     * @param enddate
     * @return
     */
    public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>(25);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startdate);
        while (cal.getTime().before(enddate)) {
            cal.add(Calendar.DATE, 1);
            dates.add(cal.getTime());

            Calendar newCal = Calendar.getInstance();
            newCal.setTime(cal.getTime());
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;

            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }
        }
        if (!dates.contains(startdate)) {
            dates.add(startdate);
            Calendar newCal = Calendar.getInstance();
            newCal.setTime(startdate);
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;
            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }
        }

        if (!dates.contains(enddate)) {
            dates.add(enddate);
            Calendar newCal = Calendar.getInstance();
            newCal.setTime(enddate);
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;
            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }

        }
        selectedDateList = dates;
        return dates;
    }

    /**
     * method to clear background of dates
     */
    void clearAllSelectedDates() {
        caldroidFragment.clearSelectedDates();
        caldroidFragment.clearBackgroundDrawableForDates(selectedDateList);
        caldroidFragment.clearTextColorForDates(selectedDateList);
        caldroidFragment.refreshView();
        selectedDate.clear();
        selectedDateList.clear();
        dateTo = null;
        dateFrom = null;

        setCurrentDateTextColor(true);
        caldroidFragment.refreshView();
        setDefaultColors();
    }

    /**
     * Method to set text color for current date
     *
     * @param isColored
     */
    void setCurrentDateTextColor(boolean isColored) {

        Date date = new Date();
        dateFrom = date;
        caldroidFragment.refreshView();
        if (isColored) {
            caldroidFragment.setTextColorForDate(R.color.colorAccent, date);
        } else {
            caldroidFragment.setTextColorForDate(R.color.black, date);
        }

        caldroidFragment.refreshView();
    }

    void initUi() {
        rl_header = (RelativeLayout) findView(R.id.rl_header);
        mycal_retry = (ImageView) findView(R.id.mycal_retry);
        tv_mycal_successmsg = (TextView) findView(R.id.tv_mycal_successmsg);
        rel_mainmycal = (RelativeLayout) findView(R.id.rel_mainmycal);
        rel_mainmycal.setOnClickListener(this);
        // ll_mycalander_applysuccess=(LinearLayout)view.findViewById(R.id.ll_mycalander_applysuccess);
        tv_mycal_heading = (TextView) findView(R.id.tv_mycal_heading);
        tv_mycal_heading.setText("Calendar");

        img_mycal_scuess_done = (ImageView) findView(R.id.img_mycal_scuess_done);
        img_mycal_scuess_done.setOnClickListener(this);
        rv_my_calendar_info = (RecyclerView) findView(R.id.rv_my_calendar_info);
        rv_my_calendar_info.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_my_calendar_info.setAdapter(new CalendarInfoListAdapter(getActivity()));
        rl_self_calendar_info = (RelativeLayout) findView(R.id.rl_self_calendar_info);
        ll_mycalendar_offline = (LinearLayout) findView(R.id.ll_mycalendar_offline);
        ll_mycalendar_retry = (LinearLayout) findView(R.id.ll_mycalendar_retry);
        rl_two_btn_mark_present = (RelativeLayout) findView(R.id.rl_two_btn_mark_present);
        rl_progress_my_calendar = (RelativeLayout) findView(R.id.rl_progress_my_calendar);
        rl_two_btn_apply_leave = (RelativeLayout) findView(R.id.rl_two_btn_apply_leave);
        rl_apply_for_leave = (RelativeLayout) findView(R.id.rl_apply_for_leave);
        rl_two_btn_container = (RelativeLayout) findView(R.id.rl_two_btn_container);
        rl_selected_date_events = (RelativeLayout) findView(R.id.rl_selected_date_events);
        rl_selected_date_info = (RelativeLayout) findView(R.id.rl_selected_date_info);
        iv_punchin_out_my_calendar = (ImageView) findView(R.id.iv_punchin_out_my_calendar);
        iv_symbol_info = (ImageView) findView(R.id.iv_symbol_info);
        iv_my_calendar_info_check = (ImageView) findView(R.id.iv_my_calendar_info_check);
        txt_slelcted_date = (TextViewWorkSansRegular) findView(R.id.txt_slelcted_date);
        tv_selected_Date = (TextViewWorkSansRegular) findView(R.id.tv_selected_Date);
        tv_date_corresponding_info_second = (TextViewWorkSansRegular) findView(R.id.tv_date_corresponding_info_second);
        tv_date_corresponding_info_first = (TextViewWorkSansRegular) findView(R.id.tv_date_corresponding_info_first);
        calendar_event_color = findView(R.id.calendar_event_color);
        iv_symbol_info.setOnClickListener(this);
        ll_mycalendar_retry.setOnClickListener(this);
        rl_two_btn_mark_present.setOnClickListener(this);
        rl_two_btn_apply_leave.setOnClickListener(this);
        iv_punchin_out_my_calendar.setOnClickListener(this);
        rl_apply_for_leave.setOnClickListener(this);
    }

    /**
     * Method to handle buttons visibility according to dates selection
     *
     * @param type
     */
    void handleDateSelection(int type) {
        switch (type) {
            case 1:
                tv_selected_Date.setText(getTextForSingleDate(dateFrom));
                txt_slelcted_date.setText("Selected Date");
                iv_punchin_out_my_calendar.setVisibility(View.GONE);
                rl_selected_date_events.setVisibility(View.VISIBLE);
                rl_apply_for_leave.setVisibility(View.GONE);
                rl_selected_date_info.setVisibility(View.VISIBLE);
                rl_two_btn_container.setVisibility(View.VISIBLE);

                break;
            case 2:
                tv_selected_Date.setText(getTextForSingleDate(dateFrom));
                txt_slelcted_date.setText("Selected Date");
                iv_punchin_out_my_calendar.setVisibility(View.VISIBLE);
                rl_selected_date_events.setVisibility(View.VISIBLE);
                rl_apply_for_leave.setVisibility(View.VISIBLE);
                rl_selected_date_info.setVisibility(View.VISIBLE);
                rl_two_btn_container.setVisibility(View.GONE);
                break;
            case 3:
                tv_selected_Date.setText(getTextForSingleDate(dateFrom));
                txt_slelcted_date.setText("Selected Date");
                iv_punchin_out_my_calendar.setVisibility(View.GONE);
                rl_selected_date_events.setVisibility(View.VISIBLE);
                rl_apply_for_leave.setVisibility(View.VISIBLE);
                rl_selected_date_info.setVisibility(View.VISIBLE);
                rl_two_btn_container.setVisibility(View.GONE);
                break;
            case 4:

                String compStart = getTextForSingleDate(dateFrom);
                String compEnd = getTextForSingleDate(dateTo);
                int compareDtae = compStart.compareTo(compEnd);
                System.out.println(compareDtae);
                if (compareDtae >= 1) {
                    tv_selected_Date.setText(getTextForSingleDate(dateTo) + " to " + getTextForSingleDate(dateFrom));
                } else if (compareDtae < 0) {
                    tv_selected_Date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                } else if (compareDtae == 0) {
                    tv_selected_Date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }
                txt_slelcted_date.setText("Selected Dates");
                iv_punchin_out_my_calendar.setVisibility(View.GONE);
                rl_selected_date_events.setVisibility(View.GONE);
                rl_apply_for_leave.setVisibility(View.GONE);
                rl_selected_date_info.setVisibility(View.VISIBLE);
                rl_two_btn_container.setVisibility(View.VISIBLE);
                break;
            case 5:
                String compStart1 = getTextForSingleDate(dateFrom);
                String compEnd1 = getTextForSingleDate(dateTo);
                int compareDtae1 = compStart1.compareTo(compEnd1);
                System.out.println(compareDtae1);
                if (compareDtae1 >= 1) {
                    tv_selected_Date.setText(getTextForSingleDate(dateTo) + " to " + getTextForSingleDate(dateFrom));
                } else if (compareDtae1 < 0) {
                    tv_selected_Date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                } else if (compareDtae1 == 0) {
                    tv_selected_Date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }
                //    tv_selected_Date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                txt_slelcted_date.setText("Selected Dates");
                iv_punchin_out_my_calendar.setVisibility(View.GONE);
                rl_selected_date_events.setVisibility(View.GONE);
                rl_apply_for_leave.setVisibility(View.VISIBLE);
                rl_selected_date_info.setVisibility(View.VISIBLE);
                rl_two_btn_container.setVisibility(View.GONE);
                break;
            case 6:
                tv_selected_Date.setText("");
                iv_punchin_out_my_calendar.setVisibility(View.GONE);
                rl_selected_date_events.setVisibility(View.GONE);
                rl_apply_for_leave.setVisibility(View.GONE);
                rl_selected_date_info.setVisibility(View.GONE);
                rl_two_btn_container.setVisibility(View.GONE);
                break;
        }
    }


    /**
     * get text to show selected date
     *
     * @param date
     * @return
     */
    String getTextForSingleDate(Date date) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        int year = c.get(Calendar.YEAR);
        String month_name = new DateFormatSymbols().getMonths()[c.get(Calendar.MONTH)].substring(0, 3);

        return day + " " + month_name + ", " + year;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_apply_for_leave:

                if (rl_self_calendar_info.getVisibility() == View.INVISIBLE) {
                    String dateStart, dateEnd;
                    String formattedDateStart, formattedDateEnd;

                    if (dateTo == null) {
                        dateStart = getTextForSingleDate(dateFrom);
                        dateEnd = getTextForSingleDate(dateFrom);

                        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                        Date date1 = null;
                        Date date2 = null;
                        try {
                            date1 = originalFormat.parse(dateStart);
                            date2 = originalFormat.parse(dateEnd);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        formattedDateStart = targetFormat.format(date1);
                        formattedDateEnd = targetFormat.format(date2);

                    } else {
                        dateStart = getTextForSingleDate(dateFrom);
                        dateEnd = getTextForSingleDate(dateTo);

                        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                        Date date1 = null;
                        Date date2 = null;
                        try {
                            date1 = originalFormat.parse(dateStart);
                            date2 = originalFormat.parse(dateEnd);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        formattedDateStart = targetFormat.format(date1);
                        formattedDateEnd = targetFormat.format(date2);
                    }

                    Intent applyLeaveIntent = new Intent(getActivity(), ApplyLeave.class);
                    applyLeaveIntent.putExtra("formattedDateStart", formattedDateStart);
                    applyLeaveIntent.putExtra("formattedDateEnd", formattedDateEnd);
                    applyLeaveIntent.putExtra("FROM", "MYCAL");
                    startActivityForResult(applyLeaveIntent, AppConstants.REQUEST_CODES.APPLY_LEAVE);
                } else {
                    rl_self_calendar_info.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.iv_punchin_out_my_calendar:
                Intent punchIntent = new Intent(getActivity(), PunchInOutActivity.class);
                punchIntent.putExtra("ToPunch", "Calander");
                startActivityForResult(punchIntent, AppConstants.REQUEST_CODES.MARK_PUNCH);
                break;

            case R.id.rl_two_btn_apply_leave:

                String dateStartnxt, dateEndnxt;
                String formattedDateStartnxt, formattedDateEndnxt;

                if (dateTo == null) {
                    dateStartnxt = getTextForSingleDate(dateFrom);
                    dateEndnxt = getTextForSingleDate(dateFrom);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartnxt);
                        date2 = originalFormat.parse(dateEndnxt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartnxt = targetFormat.format(date1);
                    formattedDateEndnxt = targetFormat.format(date2);

                } else {

                    dateStartnxt = getTextForSingleDate(dateFrom);
                    dateEndnxt = getTextForSingleDate(dateTo);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartnxt);
                        date2 = originalFormat.parse(dateEndnxt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartnxt = targetFormat.format(date1);
                    formattedDateEndnxt = targetFormat.format(date2);
                }

                Intent applyLeaveIntent1 = new Intent(getActivity(), ApplyLeave.class);
                applyLeaveIntent1.putExtra("formattedDateStart", formattedDateStartnxt);
                applyLeaveIntent1.putExtra("formattedDateEnd", formattedDateEndnxt);
                applyLeaveIntent1.putExtra("FROM", "MYCAL");
                // applyLeaveIntent1.putExtra("UserId", 35);
                startActivity(applyLeaveIntent1);
                break;
            case R.id.rl_two_btn_mark_present:
                String dateStartMP, dateEndMP;
                String formattedDateStartMP = "", formattedDateEndMP = "";
                if (dateTo == null) {
                    dateStartMP = getTextForSingleDate(dateFrom);
                    dateEndMP = getTextForSingleDate(dateFrom);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartMP);
                        date2 = originalFormat.parse(dateEndMP);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartMP = targetFormat.format(date1);
                    formattedDateEndMP = targetFormat.format(date2);

                } else {
                    dateStartMP = getTextForSingleDate(dateFrom);
                    dateEndMP = getTextForSingleDate(dateTo);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartMP);
                        date2 = originalFormat.parse(dateEndMP);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartMP = targetFormat.format(date1);
                    formattedDateEndMP = targetFormat.format(date2);
                }

                if (Helper.checkInternetConnection(getActivity())) {
                    tv_mycal_heading.setText("Calendar");
                    rl_progress_my_calendar.setVisibility(View.VISIBLE);

                    callSelfLeaveApplyApi(formattedDateStartMP, formattedDateEndMP);
                } else {
                    ll_mycalendar_offline.setVisibility(View.VISIBLE);
                    tv_mycal_heading.setText("Offline");
                }


                break;

            case R.id.ll_mycalendar_retry:


                final Animation a = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in);
                a.setDuration(1000);
                mycal_retry.startAnimation(a);
                if (Helper.checkInternetConnection(getActivity())) {
                    tv_mycal_heading.setText("Calendar");
                    caldroidFragment.nextMonth();
                    caldroidFragment.prevMonth();
                } else {
                    tv_mycal_heading.setText("Offline");
                    ll_mycalendar_offline.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv_symbol_info:
                if (rl_self_calendar_info.getVisibility() == View.INVISIBLE) {
                    rl_self_calendar_info.setVisibility(View.VISIBLE);
                } else {
                    rl_self_calendar_info.setVisibility(View.INVISIBLE);
                }
                break;

          /*  case R.id.img_mycal_scuess_done:

                ll_mycalander_applysuccess.setVisibility(View.GONE);
                tv_mycal_heading.setText("Calendar");
                iv_symbol_info.setVisibility(View.VISIBLE);
                initializeCalendar();
                dateTo = null;
                dateFrom = null;
                selectedDateList.clear();
                selectedDate.clear();
                handleDateSelection(6);
                mCallback.changeTabToCalendarUpdate();
             //   getActivity().finish();
                break;
*/
            case R.id.rl_progress_my_calendar:
                break;

            case R.id.rel_mainmycal:
                if (rl_self_calendar_info.getVisibility() == View.VISIBLE) {
                    rl_self_calendar_info.setVisibility(View.INVISIBLE);
                }

                break;

        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (MyCalendarUpdateCallback) activity;
    }

    /**
     * Method to call self calendar api
     */
    void callSelfCalendarApi(String from_date, String to_date) {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        Call<SelfCalendarModel> call = restApis.getSelfCalendarInfo("bearer " + auth_token, from_date, to_date);
        call.enqueue(new Callback<SelfCalendarModel>() {
            @Override
            public void onResponse(Call<SelfCalendarModel> call, Response<SelfCalendarModel> response) {
                Activity activity = getActivity();
                if (isAdded() && activity != null) {

                    if (response.raw().code() == 200) {
                        rl_progress_my_calendar.setVisibility(View.GONE);
                        ll_mycalendar_offline.setVisibility(View.GONE);
                        calendarResponse = response.body().getResponse();
                        selfCalendarModel = response.body();
                        for (int i = 0; i < calendarResponse.getData().getCalendar().size(); i++) {

                            datesFromApi.add(i, calendarResponse.getData().getCalendar().get(i).getDate());
                            appliedBy.add(i, calendarResponse.getData().getCalendar().get(i).getAppliedBy().getUserName());
                            approvedBy.add(i, calendarResponse.getData().getCalendar().get(i).getApprovedBy().getUserName());

                            changeBgOfDate(calendarResponse.getData().getCalendar().get(i).getDate(), calendarResponse.getData().getCalendar().get(i).getType());
                        }
                        caldroidFragment.refreshView();
                    } else if (response.raw().code() == 401) {
                        rl_progress_my_calendar.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //   Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
                        logout();
                    } else {
                        rl_progress_my_calendar.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

//                            JSONObject reader = new JSONObject(errorMsg);
                            //                name = reader.getString("msg");
                            // name = sys.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //  Toast.makeText(getActivity(), name, Toast.LENGTH_LONG).show();
                        // Intent genericIntent=new Intent(getActivity(),FragmentGenericError.class);
                        // startActivity(genericIntent);
                    }
                }
            }

            @Override
            public void onFailure(Call<SelfCalendarModel> call, Throwable t) {
                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    rl_progress_my_calendar.setVisibility(View.GONE);
                    ll_mycalendar_offline.setVisibility(View.VISIBLE);
                    tv_mycal_heading.setText("Offline");
                    String msg = t.getMessage();
                }
            }
        });


    }

    /**
     * method to change color of particular date
     *
     * @param date_to_color
     * @param leave_type
     */
    void changeBgOfDate(String date_to_color, String leave_type) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date finaldate = null;
        try {
            finaldate = format.parse(date_to_color);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(getImageByLeaveType(leave_type)), finaldate);

    }


    /**
     * method to get background image for date
     *
     * @param leave_type
     * @return
     */
    int getImageByLeaveType(String leave_type) {
        int bg = 0;
        switch (leave_type) {

            case "AM":
                bg = R.drawable.green_border;
                break;
            case "PM":
                bg = R.drawable.green_border;
                break;
            case "EL":
                bg = R.drawable.red_border;
                break;
            case "SL":
                bg = R.drawable.red_border;
                break;
            case "CL":
                bg = R.drawable.red_border;
                break;
            case "OL":
                bg = R.drawable.red_border;
                break;
            case "LWP":
                bg = R.drawable.red_border;
                break;
            case "HOLIDAY":
                bg = R.drawable.orange_border;
                break;
            case "WEEKOFF":
                bg = R.drawable.grey_border;
                break;
            default:
                bg = R.color.caldroid_gray;
                break;
        }
        return bg;
    }


    /**
     * method to get color for event type
     *
     * @param leave_type
     * @return
     */
    int getColorByLeaveType(String leave_type) {
        int bg = 0;
        switch (leave_type) {

            case "AM":
                bg = R.color.AM;
                break;
            case "PM":
                bg = R.color.AM;
                break;
            case "EL":
                bg = R.color.EL;
                break;
            case "SL":
                bg = R.color.EL;
                break;
            case "CL":
                bg = R.color.EL;
                break;
            case "OL":
                bg = R.color.EL;
                break;
            case "LWP":
                bg = R.color.EL;
                break;
            case "HOLIDAY":
                bg = R.color.HOLIDAY;
                break;
            case "WEEKOFF":
                bg = R.color.colorPrimaryDark;
                break;
            default:
                bg = R.color.caldroid_gray;
                ;
                break;


        }
        return bg;
    }

    /**
     * method to set all default color after reselection of dates
     */
    void setDefaultColors() {
        for (int i = 0; i < calendarResponse.getData().getCalendar().size(); i++) {
            changeBgOfDate(calendarResponse.getData().getCalendar().get(i).getDate(), calendarResponse.getData().getCalendar().get(i).getType());
        }
        caldroidFragment.refreshView();
    }

    /**
     * method to get name label to display corresponding to date
     *
     * @param type
     * @return
     */
    String getLabelFromType(String type) {
        String label = "";
        switch (type) {

            case "AM":
                label = selfCalendarModel.getResponse().getLegend().getAM().getLabel();
                break;
            case "PM":
                label = selfCalendarModel.getResponse().getLegend().getPM().getLabel();
                break;
            case "EL":
                label = selfCalendarModel.getResponse().getLegend().getEL().getLabel();
                break;
            case "SL":
                label = selfCalendarModel.getResponse().getLegend().getSL().getLabel();
                break;
            case "CL":
                label = selfCalendarModel.getResponse().getLegend().getCL().getLabel();
                break;
            case "OL":
                label = selfCalendarModel.getResponse().getLegend().getOL().getLabel();
                break;
            case "LWP":
                label = selfCalendarModel.getResponse().getLegend().getLWP().getLabel();
                break;
            case "HOLIDAY":
                label = selfCalendarModel.getResponse().getLegend().getHOLIDAY().getLabel();
                break;
            case "WEEKOFF":
                label = selfCalendarModel.getResponse().getLegend().getWEEKOFF().getLabel();
                break;


        }
        return label;
    }

    private void refreshAllData() {
        clearAllSelectedDates();
        handleDateSelection(6);
        setDefaultColors();
        caldroidFragment.clearSelectedDates();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.APPLY_LEAVE:
                if (resultCode == Activity.RESULT_OK) {
                    refreshAllData();
                }
                break;
            case AppConstants.REQUEST_CODES.MARK_PUNCH:
                if (resultCode == Activity.RESULT_OK) {
                    refreshAllData();
                }
                break;

        }
    }

    private void callSelfLeaveApplyApi(String from, String to) {

        ApplySelfLeaveRawData applySelfLeaveRawData = null;
        int compareDtae = from.compareTo(to);
        System.out.println(compareDtae);
        if (compareDtae >= 1) {

            applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = to;
            applySelfLeaveRawData.to_date = from;
            applySelfLeaveRawData.comment = "";

        } else if (compareDtae < 0) {
            applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = from;
            applySelfLeaveRawData.to_date = to;
            applySelfLeaveRawData.comment = "";
        } else if (compareDtae == 0) {
            applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = from;
            applySelfLeaveRawData.to_date = to;
            applySelfLeaveRawData.comment = "";
        }
        //HashMap hashMap1 = map;
        try {
            String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
            String endpoint_url = "/api/v1/employee/applyleave";
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<ApplySelfLeavePojo> call = apiInterface.applySelfLeave(endpoint_url, applySelfLeaveRawData, "bearer " + auth_token);

            call.enqueue(new Callback<ApplySelfLeavePojo>() {
                @Override
                public void onResponse(Call<ApplySelfLeavePojo> call, Response<ApplySelfLeavePojo> response) {
                    Activity activity = getActivity();
                    if (isAdded() && activity != null) {
                        if (response.raw().code() == 200) {
                            rl_progress_my_calendar.setVisibility(View.GONE);

                            // Inflate any custom view
                            View customView = getActivity().getLayoutInflater().inflate(R.layout.custom_crouton_layout, null);
                            TextView msg = (TextView) customView.findViewById(R.id.tv_crouton_msg);
                            msg.setText(response.body().getMsg());
                            Crouton.show(getActivity(), customView, rel_mainmycal);

                            tv_mycal_heading.setText("Calendar");
                            iv_symbol_info.setVisibility(View.VISIBLE);
                            initializeCalendar();
                            dateTo = null;
                            dateFrom = null;
                            selectedDateList.clear();
                            selectedDate.clear();
                            handleDateSelection(6);
                            mCallback.changeTabToCalendarUpdate();
                        } else if (response.raw().code() == 401) {
                            rl_progress_my_calendar.setVisibility(View.GONE);
                            String errorMsg = null;
                            String name = null;
                            try {
                                errorMsg = response.errorBody().string();
                                JSONObject reader = new JSONObject(errorMsg);
                                name = reader.getString("msg");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //   Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
                            logout();
                        } else {
                            rl_progress_my_calendar.setVisibility(View.GONE);
                            String errorMsg = null;
                            String name = null;
                            try {
                                errorMsg = response.errorBody().string();
                                JSONObject reader = new JSONObject(errorMsg);
                                name = reader.getString("msg");
                                // name = sys.getString("msg");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Toast.makeText(getActivity(), name, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ApplySelfLeavePojo> call, Throwable t) {
                    Activity activity = getActivity();
                    if (isAdded() && activity != null) {
                        String errorMsg = t.getMessage();
                        rl_progress_my_calendar.setVisibility(View.GONE);
                        ll_mycalendar_offline.setVisibility(View.VISIBLE);
                        tv_mycal_heading.setText("Offline");
                    }
                }
            });
        } catch (Exception e) {

            e.printStackTrace();

        }
    }


    @Override
    public void onPause() {
        super.onPause();
        // rl_progress_my_calendar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //  rl_progress_my_calendar.setVisibility(View.GONE);
    }


    /**
     * method to initialize calendar
     */
    void initializeCalendar() {
        current_date = new Date();
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        final Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidCustomDefault);
        caldroidFragment.setArguments(args);

        setCurrentDateTextColor(true);

        final FragmentTransaction t = getChildFragmentManager().beginTransaction();
        t.replace(R.id.calendar_container, caldroidFragment);
        t.commit();
        caldroidFragment.setCaldroidListener(new CaldroidListener() {


            @Override
            public void onChangeMonth(int month, int year) {
                if (Calendar.getInstance().getTime().getMonth() != month) {

                }

                if (month < 10) {
                    f_date = year + "-0" + (month) + "-" + "01";
                    t_date = year + "-0" + (month) + "-" + month_day[month - 1];
                } else {
                    f_date = year + "-" + (month) + "-" + "01";
                    t_date = year + "-" + (month) + "-" + month_day[month - 1];
                }

                if (Helper.checkInternetConnection(getActivity())) {
//                    clearAllSelectedDates();
                    current_Month = month;
                    System.out.println("month--" + month);
                    callSelfCalendarApi(f_date, t_date);
                    //   selectedDate.clear();
                    handleDateSelection(6);
                    //  clearAllSelectedDates();
                    rl_progress_my_calendar.setVisibility(View.VISIBLE);
                    tv_mycal_heading.setText("Calendar");
                } else {
                    ll_mycalendar_offline.setVisibility(View.VISIBLE);
                    tv_mycal_heading.setText("Offline");
                }

            }

            @Override
            public void onSelectDate(Date date, View view) {

                if (rl_self_calendar_info.getVisibility() == View.INVISIBLE) {
                    Calendar newCal = Calendar.getInstance();
                    newCal.setTime(date);
                    int year = newCal.get(Calendar.YEAR);
                    int month = newCal.get(Calendar.MONTH);
                    int day = newCal.get(Calendar.DAY_OF_MONTH);
                    String clickedDate = year + "-" + month + "-" + day;
                    String dateToSearch;

                    if (month < 9) {
                        if (day < 10) {
                            dateToSearch = year + "-" + "0" + (month + 1) + "-" + "0" + day;
                        } else {
                            dateToSearch = year + "-" + "0" + (month + 1) + "-" + day;
                        }
                    } else {
                        if (day < 10) {
                            dateToSearch = year + "-" + (month + 1) + "-" + "0" + day;
                        } else {
                            dateToSearch = year + "-" + (month + 1) + "-" + day;
                        }
                    }

                    if (selectedDate.contains(clickedDate)) {
                        if (selectedDate.size() == 1) {
                            selectedDate.remove(clickedDate);

                            if (fmt.format(current_date).equals(fmt.format(date))) {
                                setCurrentDateTextColor(true);
                            } else {
                                ////compare between clickeddate and dateto search///////

                                String[] sepSearchdate = dateToSearch.split("-");

                                if (current_Month < Integer.parseInt(sepSearchdate[1])) {
                                    caldroidFragment.setTextColorForDate(R.color.caldroid_darker_gray, date);
                                } else {
                                    caldroidFragment.setTextColorForDate(R.color.black, date);
                                }
                            }
                            caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(R.drawable.not_selected), date);
                            caldroidFragment.refreshView();

                            handleDateSelection(6);
                            dateFrom = null;
                            dateTo = null;
                            setDefaultColors();
                        } else {
                            clearAllSelectedDates();
                            handleDateSelection(6);
                        }
                    } else {

                        switch (selectedDate.size()) {
                            case 0:
                                dateFrom = date;
                                if (fmt.format(current_date).equals(fmt.format(date))) {
                                    handleDateSelection(2);
                                } else {
                                    if (Calendar.getInstance().getTime().after(date)) {
                                        handleDateSelection(1);
                                    } else {
                                        handleDateSelection(3);
                                    }
                                }
                                selectedDate.add(clickedDate);
                                caldroidFragment.setTextColorForDate(R.color.cyan_text, date);
                                caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(R.drawable.date_selected_bg), date);
                                caldroidFragment.refreshView();

                                if (datesFromApi.contains(dateToSearch)) {
                                    String[] sepSearchdate = dateToSearch.split("-");
                                    if (current_Month > Integer.parseInt(sepSearchdate[1]) || current_Month < Integer.parseInt(sepSearchdate[1])) {

                                    } else if (current_Month == Integer.parseInt(sepSearchdate[1])) {
                                        rl_selected_date_events.setVisibility(View.VISIBLE);

                                        int pos = datesFromApi.indexOf(dateToSearch);
                                        if (calendarResponse.getData().getCalendar().get(pos).getType().equals("WEEKOFF") || calendarResponse.getData().getCalendar().get(pos).getType().equals("HOLIDAY")) {
                                            tv_date_corresponding_info_first.setText(getLabelFromType(calendarResponse.getData().getCalendar().get(pos).getType()));
                                            tv_date_corresponding_info_second.setText("Holiday: " + calendarResponse.getData().getCalendar().get(pos).getOffReason());

                                            iv_my_calendar_info_check.setVisibility(View.INVISIBLE);
                                            calendar_event_color.setBackgroundResource(getColorByLeaveType(calendarResponse.getData().getCalendar().get(pos).getType()));
                                        } else {
                                            tv_date_corresponding_info_first.setText(getLabelFromType(calendarResponse.getData().getCalendar().get(pos).getType()));
                                            iv_my_calendar_info_check.setVisibility(View.VISIBLE);
                                            calendar_event_color.setBackgroundResource(getColorByLeaveType(calendarResponse.getData().getCalendar().get(pos).getType()));

                                            if (!calendarResponse.getData().getCalendar().get(pos).getIsApproved().equals(null)) {
                                                if (calendarResponse.getData().getCalendar().get(pos).getIsApproved()) {
                                                    iv_my_calendar_info_check.setImageResource(R.drawable.card_check);
                                                    if (calendarResponse.getData().getCalendar().get(pos).getApprovedBy().getUserName() == null) {
                                                        //tv_date_corresponding_info_second.setVisibility(View.GONE);
                                                        tv_date_corresponding_info_second.setText("Approved");
                                                    } else {
                                                        // tv_date_corresponding_info_second.setVisibility(View.VISIBLE);
                                                        tv_date_corresponding_info_second.setText("Approved by " + calendarResponse.getData().getCalendar().get(pos).getApprovedBy().getUserName());
                                                    }
                                                } else {
                                                    // tv_date_corresponding_info_second.setVisibility(View.VISIBLE);
                                                    iv_my_calendar_info_check.setImageResource(R.drawable.card_pending);
                                                    tv_date_corresponding_info_second.setText("Applied by " + calendarResponse.getData().getCalendar().get(pos).getAppliedBy().getUserName());
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    rl_selected_date_events.setVisibility(View.GONE);
                                }
                                break;

                            case 1:
                                dateTo = date;
                                setSelectedDateBackground();

                                if (Calendar.getInstance().getTime().after(dateFrom) && Calendar.getInstance().getTime().after(dateTo)) {
                                    handleDateSelection(4);

                                } else {
                                    if (Calendar.getInstance().getTime().after(dateFrom) && Calendar.getInstance().getTime().equals(dateTo)) {
                                        handleDateSelection(4);
                                    } else {
                                        handleDateSelection(5);
                                    }
                                }
                                break;
                            default:
                                handleDateSelection(6);
                                clearAllSelectedDates();

                                dateTo = null;
                                dateFrom = null;
                                break;
                        }
                    }
                } else {
                    rl_self_calendar_info.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    interface MyCalendarUpdateCallback {
        public void changeTabToCalendarUpdate();
    }

    void logout() {
        prefs.clearAppData(getActivity());
        DbHelper dbHelper = DbHelper.getInstance(getActivity());
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(getActivity(), Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        getActivity().finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
