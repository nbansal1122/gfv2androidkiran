package com.growthfile.ui;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.growthfile.Pojo.FeedListAcceptPojo;
import com.growthfile.R;
import com.growthfile.adapter.HomePagerAdapter;
import com.growthfile.fragment.AppBaseFragment;
import com.growthfile.fragment.GrowthFileFragment;
import com.growthfile.fragment.MoreOptionFragment;
import com.growthfile.fragment.TeamListFragment;
import com.growthfile.fragment.UserFeedFragment;
import com.growthfile.services.MyAlarmService;
import com.growthfile.services.TapLockSyncService;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Foreground;
import com.growthfile.utility.Helper;
import com.growthfile.utility.LocationFinder;
import com.growthfile.utility.LocationFinderCallback;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Prefs;


public class MainActivity extends AppCompatActivity implements LocationFinderCallback, GrowthFileFragment.MyCalendarCallback, MyCalendar.MyCalendarUpdateCallback,
        TabLayout.OnTabSelectedListener, AppBaseFragment.OnUserLogoClickListener {

    private TabLayout tab_home;
    private ViewPager pager_home;
    private HomePagerAdapter homePagerAdapter;
    private int from_apply_leave = 0;
    private LocationFinder locationFinder;
    Prefs prefs;
    private PendingIntent pendingIntent;
    private Integer userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Foreground.get(this).addListener(myListener);

        prefs = new Prefs();
        locationFinder = new LocationFinder(this);
        from_apply_leave = getIntent().getIntExtra("FROM_APPLY_LEAVE", 0);

        initUi();

        //initialize universal image loader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        //start taplog sync service
        Intent tapSyncIntent = new Intent(this, TapLockSyncService.class);
        startService(tapSyncIntent);


        // intent from notification either accept or reject cases
        if (getIntent().getIntExtra("entityId", 0) == 0) {

        } else {

            int entityId = getIntent().getIntExtra("entityId", 0);
            int actionId = getIntent().getIntExtra("actionId", 0);
            String entityType = getIntent().getStringExtra("entityType");

            if (Helper.checkInternetConnection(this)) {
                callOtherLeaveBalApi(entityId, entityType, actionId);
            }
        }

       /* Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.DAY_OF_MONTH, 21);
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 04);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.AM_PM, Calendar.PM);
        Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);*/

       /* String jsonFavorites = prefs.getPreferencesString(MainActivity.this,"workingDay");
        Gson gson = new Gson();

        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> arrayList = gson.fromJson(jsonFavorites, type);
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        if (day==2 && arrayList.get(0).equals("1")) {
            forday(2);
        } else if (day==3 &&arrayList.get(1).equals("1")) {
            forday(3);
        } else if (day==4 &&arrayList.get(2).equals("1")) {
            forday(4);
        } else if (day==5 &&arrayList.get(3).equals("1")) {
            forday(5);
        } else if (day==6 &&arrayList.get(4).equals("1")) {
            forday(6);
        } else if (day==7 &&arrayList.get(5).equals("1")) {
            forday(7);
        } else if (day==1 &&arrayList.get(6).equals("1")) {
            forday(1);
        }*/
    }

    public void forday(int week) {

        String dt = prefs.getPreferencesString(MainActivity.this, "startWorkingTime");
        String[] septime;
        String localDateTime = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss+00:00");
            Date theDate = simpleDateFormat.parse(dt);
            localDateTime = getDateInTimeZone(theDate, TimeZone.getDefault().getID());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        septime = localDateTime.split(":");
        Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, week);
        //  calendar.set(Calendar.HOUR_OF_DAY,11);
        //  calendar.set(Calendar.MINUTE,28);

        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(septime[0]) + 45);
        calendar.set(Calendar.MINUTE, Integer.parseInt(septime[1]));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), pendingIntent);
    }

    /**
     * Method to initialize all ui component
     */
    void initUi() {
        pager_home = (ViewPager) findViewById(R.id.pager_home);
        tab_home = (TabLayout) findViewById(R.id.tab_home);
        homePagerAdapter = new HomePagerAdapter(this, getSupportFragmentManager());
        pager_home.setAdapter(homePagerAdapter);
        pager_home.setOffscreenPageLimit(0);
//        pager_home.setPagingEnabled(false);

        tab_home.setTabGravity(TabLayout.GRAVITY_FILL);
//        tab_home.setupWithViewPager(pager_home);

        tab_home.addTab(tab_home.newTab().setIcon(R.drawable.activity_selector).setText("ACTIVITY"));
        tab_home.addTab(tab_home.newTab().setIcon(R.drawable.calender_selector).setText("CALENDAR"));
        tab_home.addTab(tab_home.newTab().setIcon(R.drawable.myfile_selector).setText("GROWTH FILE"));
        tab_home.addTab(tab_home.newTab().setIcon(R.drawable.team_selector).setText("TEAM"));
        tab_home.addTab(tab_home.newTab().setIcon(R.drawable.more_selector).setText("MORE"));

        tab_home.setOnTabSelectedListener(this);
//        tab_home.getTabAt(0).setIcon(R.drawable.activity_selector).setText("ACTIVITY");
//        tab_home.getTabAt(1).setIcon(R.drawable.calender_selector).setText("CALENDAR");
//        tab_home.getTabAt(2).setIcon(R.drawable.myfile_selector).setText("GROWTH FILE");
//        tab_home.getTabAt(3).setIcon(R.drawable.team_selector).setText("TEAM");
//        tab_home.getTabAt(4).setIcon(R.drawable.more_selector).setText("MORE");

        //  tab_home.setOnTabSelectedListener(MainActivity.this);
        if (from_apply_leave == 1) {
            tab_home.getTabAt(1).select();
//            pager_home.setCurrentItem(1);
        } else {
            tab_home.getTabAt(0).select();
        }
    }

    private void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack("");
        }
        fragmentTransaction.replace(R.id.fl_home_tab,fragment).commit();
    }

    Foreground.Listener myListener = new Foreground.Listener() {
        public void onBecameForeground() {
            Intent intent = new Intent(MainActivity.this, PinSetup.class);
            startActivity(intent);
            finish();
        }

        public void onBecameBackground() {
            // ... whatever you want to do
        }
    };

    @Override
    public void locationFound() {

    }

    @Override
    public void changeTabToCalendar() {
        tab_home.getTabAt(1).select();
//        tab_home.getTabAt(1).select();
//        pager_home.setCurrentItem(1);
    }

    @Override
    public void changeTabToCalendarUpdate() {

        //  pager_home.setCurrentItem(0);
        System.out.println("inside refresh");
        //  FeedList.newInstance().apiREfresh();

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
//        pager_home.setCurrentItem(tab.getPosition());
        int position = tab.getPosition();

        switch (position){
            case 0:
                UserFeedFragment userFeedFragment = new UserFeedFragment();
                userFeedFragment.setOnUserLogoClickListener(this);
                addFragment(userFeedFragment,false);
                break;
            case 1:
                MyCalendar myCalendar = new MyCalendar();
                myCalendar.setOnUserLogoClickListener(this);
                addFragment(myCalendar,false);
                break;
            case 2:
                GrowthFileFragment newMyProfile = GrowthFileFragment.getInstance(userId);
                newMyProfile.setOnUserLogoClickListener(this);
                newMyProfile.setMyCalendarCallback(this);
                addFragment(newMyProfile,false);
                userId = null;
                break;
            case 3:
                TeamListFragment teamListFragment = new TeamListFragment();
                teamListFragment.setOnUserLogoClickListener(this);
                addFragment(teamListFragment,false);
                break;
            case 4:
                addFragment(new MoreOptionFragment(),false);
                break;
        }

        if (tab.getPosition() == 0) {
            System.out.println("fgdgvgfuvduhfvvvggggggggggggggggggggggggggggggggggggggggggggggggggggg");

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    void callOtherLeaveBalApi(int entityId, String entityType, int actionId) {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(MainActivity.this, Preferences.KEY_AUTH_TOKEN);
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        // String endpoint_url = "/api/v1/userleaves/" + user_id;
        String endpoint = "/api/v1/action/" + entityType + "/" + entityId + "/" + actionId;
        RestApis restApis = retrofit.create(RestApis.class);
        Call<FeedListAcceptPojo> call = restApis.applyFeedAcept(endpoint, "bearer " + auth_token);
        call.enqueue(new Callback<FeedListAcceptPojo>() {
            @Override
            public void onResponse(Call<FeedListAcceptPojo> call, Response<FeedListAcceptPojo> response) {
                if (response.raw().code() == 200) {

                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);

                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120).setTileEnabled(true)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(MainActivity.this, response.body().getMsg(), style, FeedListFragment.previous1);
                    //   FeedList.ll_feeds_applysuccess.setVisibility(View.VISIBLE);
                    //  FeedList.tv_feedsucess_msg.setText(response.body().getMsg());
                    FeedListFragment.tv_feedmain_welcome.setText("");

                } else {
                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, name, Toast.LENGTH_LONG).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<FeedListAcceptPojo> call, Throwable t) {
                FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                String msg = t.getMessage();
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void openEmployeeProfile(Integer employeeId) {
        userId = employeeId;
        tab_home.getTabAt(2).select();
//        homePagerAdapter.setBundle(bundle);
//        pager_home.setCurrentItem(2);
    }

    public static class MyReceiver extends BroadcastReceiver {
        public NotificationManager mManager;

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent service1 = new Intent(context, MyAlarmService.class);
            context.startService(service1);
        }
    }

    public String getDateInTimeZone(Date currentDate, String timeZoneId) {

        //String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";
        DateFormat targetFormat = new SimpleDateFormat("hh:mm");
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);

        Date localDateTime = new Date(currentDate.getTime() + timeZone.getOffset(currentDate.getTime()));
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(localDateTime.getTime());
        if (timeZone.useDaylightTime()) {
            // time zone uses Daylight Saving
            cal.add(Calendar.MILLISECOND, timeZone.getDSTSavings() * -1);// in milliseconds
        }
        return targetFormat.format(cal.getTime());
    }

}
