package com.growthfile.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.growthfile.GCMIntentService;
import com.growthfile.Pojo.OtpPojo;
import com.growthfile.Pojo.SelfProfilePojo;

import me.philio.pinentry.PinEntryView;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.responses.BaseResponse;
import simplifii.framework.rest.responses.UserProfile;

import com.growthfile.R;
import com.growthfile.util.SMSListener;
import com.growthfile.utility.ApiRequestGenerator;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import com.growthfile.utility.LocationFinder;
import com.growthfile.utility.LocationFinderCallback;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Prefs;

import com.growthfile.webservices.LoginRawData;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.growthfile.webservices.SendOtpData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class OtpVerification extends BaseActivity implements View.OnClickListener, LocationFinderCallback {

    ImageView iv_verify_otp;

    String[] otp_code = new String[6];
    String activationCode = "", composed_otp;
    private LocationFinder locationFinder;
    Prefs prefs;
    private LinearLayout ll_otp_input;
    Retrofit retroFitMaker;
    private RelativeLayout rel_otpback, rl_otv_verify_root, rel_resend, rl_progress_otp;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private PinEntryView pinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        locationFinder = new LocationFinder(this);
        prefs = new Prefs();
        initUi();


        if (prefs.getPreferencesString(OtpVerification.this, "token").equals("") || prefs.getPreferencesString(OtpVerification.this, "token").equals(null)) {

            //Initializing our broadcast receiver
            mRegistrationBroadcastReceiver = new BroadcastReceiver() {

                //When the broadcast received
                //We are sending the broadcast from GCMRegistrationIntentService

                @Override
                public void onReceive(Context context, Intent intent) {
                    //If the broadcast has received with success
                    //that means device is registered successfully
                    if (intent.getAction().equals(GCMIntentService.REGISTRATION_SUCCESS)) {
                        //Getting the registration token from the intent
                        String token = intent.getStringExtra("token");
                        prefs.setPreferencesString(OtpVerification.this, "token", token);
                        //Displaying the token as toast
                        //     Toast.makeText(getApplicationContext(), "Registration token:" + token, Toast.LENGTH_LONG).show();

                        //if the intent is not with success then displaying error messages
                    } else if (intent.getAction().equals(GCMIntentService.REGISTRATION_ERROR)) {
                        Toast.makeText(getApplicationContext(), "GCM registration error!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error occurred", Toast.LENGTH_LONG).show();
                    }
                }
            };


            //Checking play service is available or not
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

            //if play service is not available
            if (ConnectionResult.SUCCESS != resultCode) {
                //If play service is supported but not installed
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    //Displaying message that play service is not installed
                    Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                    GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                    //If play service is not supported
                    //Displaying an error message
                } else {
                    Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
                }

                //If play service is available
            } else {
                //Starting intent to register device
                Intent itent = new Intent(this, GCMIntentService.class);
                startService(itent);
            }
        }

        for (int x = 0; x < 6; x++) {
            otp_code[x] = "";
        }

        InputMethodManager imm = (InputMethodManager)
                OtpVerification.this.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (isOpen) {
                    rl_otv_verify_root.scrollTo(0, rl_otv_verify_root.getHeight() / 4);
                } else {
                    rl_otv_verify_root.scrollTo(0, 0);
                }
            }
        });

        registerOTPReceiver();
    }


    /**
     * Method to initialize ui component
     */
    private void initUi() {

        rl_otv_verify_root = (RelativeLayout) findViewById(R.id.rl_otv_verify_root);
        rl_progress_otp = (RelativeLayout) findViewById(R.id.rl_progress_otp);

        pinView = (PinEntryView) findViewById(R.id.pinView);
        ll_otp_input = (LinearLayout) findViewById(R.id.ll_otp_input);
        iv_verify_otp = (ImageView) findViewById(R.id.iv_verify_otp);
        iv_verify_otp.setOnClickListener(this);

        rel_otpback = (RelativeLayout) findViewById(R.id.rel_otpback);
        rel_otpback.setOnClickListener(this);

        rel_resend = (RelativeLayout) findViewById(R.id.rel_resend);
        rel_resend.setOnClickListener(this);
        findViewById(R.id.rl_resend_otp).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_verify_otp:
                composed_otp = pinView.getText().toString();
                if (composed_otp.length() == 6) {

                    if (Helper.checkInternetConnection(OtpVerification.this)) {
                        rl_progress_otp.setVisibility(View.VISIBLE);
                        API_TaskOTP(composed_otp);
                    } else {

                        Configuration croutonConfiguration = new Configuration.Builder()
                                .setDuration(4500).build();
                        // Define custom styles for crouton
                        Style style = new Style.Builder()
                                .setBackgroundColorValue(Color.parseColor("#80000000"))
                                .setGravity(Gravity.CENTER_HORIZONTAL)
                                .setConfiguration(croutonConfiguration)
                                .setHeight(150)
                                .setTextColorValue(Color.parseColor("#ffffff")).build();
                        // Display notice with custom style and configuration
                        Crouton.showText(OtpVerification.this, getText(R.string.internet_check), style);
                        /// Toast.makeText(OtpVerification.this,getText(R.string.internet_check), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(4500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(150)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(OtpVerification.this, getText(R.string.otp_count_error), style);
                    // Toast.makeText(OtpVerification.this,getText(R.string.otp_count_error), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.rel_resend:

                //  prefs.setPreferencesString(OtpVerification.this, "PhoneNo", phone);
                prefs.getPreferencesString(OtpVerification.this, "PhoneNo");

                rl_progress_otp.setVisibility(View.VISIBLE);

                API_Task(prefs.getPreferencesString(OtpVerification.this, "PhoneNo"));
                break;

            case R.id.rel_otpback:
                Intent intent = new Intent(OtpVerification.this, Login.class);
                startActivity(intent);
                finish();
                break;

            case R.id.rl_progress_otp:
                break;
            case R.id.rl_resend_otp:
                resendOTP();
                break;
        }
    }

    private void resendOTP() {
        HttpParamObject obj = ApiRequestGenerator.getDefaultHttpPost(AppConstants.PAGE_URL.RESEND_OTP);
        obj.setJson(getJsonToResendOtp());
        obj.setClassType(BaseResponse.class);
        executeTask(AppConstants.TASK_CODES.RESEND_OTP, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.RESEND_OTP:
                if (response != null) {
                    BaseResponse baseResponse = (BaseResponse) response;
                    if (!TextUtils.isEmpty(baseResponse.getMsg())) {
                        showToast(baseResponse.getMsg());
                    }
                }
                break;
        }
    }

    private SMSListener smsListener;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (smsListener != null) {
            OtpVerification.this.unregisterReceiver(smsListener);
        }
    }

    private IntentFilter getTelephonyFilter() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        return filter;
    }

    private void registerOTPReceiver() {
        if (isPermissionGranted()) {
            if (smsListener == null) {
                smsListener = new SMSListener(new SMSListener.OTPCallback() {
                    @Override
                    public void onOTPReceived(String otp) {
                        pinView.setText((otp));
                        API_TaskOTP(otp);
                    }
                });
            }
            OtpVerification.this.registerReceiver(smsListener, getTelephonyFilter());
        }
    }

    private boolean isPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private String getJsonToResendOtp() {
        JSONObject obj = new JSONObject();
        String phone = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.PHONE);
        try {
            obj.put("mobile", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    /**
     * Method to compose activation code from stored code in array
     *
     * @return activation code
     */
    private String composeActivatioCode() {
        String code = "";

        for (int j = 0; j < 6; j++) {
            code = code + otp_code[j];
        }
        return code;
    }

    private LoginRawData getLoginRawData() {
        String deviceId = Util.getAndroidId(this);
        String model = Build.MODEL;
        String version = Build.VERSION.RELEASE;

        LoginRawData loginRawData = new LoginRawData();
        loginRawData.device_id = deviceId;
        loginRawData.device_model = "5s";
        loginRawData.os_type = "ANDROID";
        loginRawData.os_version = version;
        return loginRawData;
    }

    private void API_TaskOTP(String otp) {
        try {

            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.BASE_URL);
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            String number = prefs.getPreferencesString(OtpVerification.this, "PhoneNo");
            LoginRawData loginRawData = getLoginRawData();
            loginRawData.mobile = number;
            loginRawData.otp = otp;
            loginRawData.app_id = prefs.getPreferencesString(OtpVerification.this, "token");

            Call<SelfProfilePojo> call = apiInterface.verifyOTP(loginRawData);

            call.enqueue(new Callback<SelfProfilePojo>() {
                @Override
                public void onResponse(Call<SelfProfilePojo> call, Response<SelfProfilePojo> response) {
                    if (response.raw().code() == 200) {
                        prefs.setPreferencesString(OtpVerification.this, Preferences.KEY_AUTH_TOKEN, response.raw().headers().get("Auth-Token"));
                        prefs.setPreferencesString(OtpVerification.this, "Verified", "true");
                        saveLoginDataToPref(response.body().getResponse().getProfile());
                        rl_progress_otp.setVisibility(View.GONE);

                        prefs.setPreferencesInt(OtpVerification.this, Preferences.USER_ID, response.body().getResponse().getProfile().getId());
                        prefs.setPreferencesString(OtpVerification.this, "pinnane", response.body().getResponse().getProfile().getName());
                        prefs.setPreferencesString(OtpVerification.this, "moreMobile", response.body().getResponse().getProfile().getMobile());
                        prefs.setPreferencesString(OtpVerification.this, "moreemail", response.body().getResponse().getProfile().getEmail());
                        prefs.setPreferencesString(OtpVerification.this, "moreimage", response.body().getResponse().getProfile().getAvatar());


                        ArrayList<Integer> workingDay = new ArrayList<Integer>();
                        workingDay.add(response.body().getResponse().getCenter().getIsMonWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsTueWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsWedWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsThuWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsFriWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsSatWorking());
                        workingDay.add(response.body().getResponse().getCenter().getIsSunWorking());

                        Gson gson = new Gson();
                        String jsonFavorites = gson.toJson(workingDay);

                        prefs.setPreferencesString(OtpVerification.this, "workingDay", jsonFavorites);
                        prefs.setPreferencesString(OtpVerification.this, "startWorkingTime", response.body().getResponse().getCenter().getWorkTimingFrom());
                        prefs.setPreferencesString(OtpVerification.this, "EndWorkingTime", response.body().getResponse().getCenter().getWorkTimingTill());

                        Intent intent = new Intent(OtpVerification.this, FirstTimePinSetup.class);
                        startActivity(intent);
                        finish();

                    } else {
                        rl_progress_otp.setVisibility(View.GONE);
                      /* prefs.setPreferencesString(OtpVerification.this, "AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM1LCJpc3MiOiJodHRwOlwvXC9nZmlsZS1hcGkubG9jYWxcL2FwaVwvdjFcL3VzZXJcL2F1dGhlbnRpY2F0ZSIsImlhdCI6MTQ3ODA3OTY3NSwiZXhwIjoxNDc4Njg0NDc1LCJuYmYiOjE0NzgwNzk2NzUsImp0aSI6IjUzZDgwNjc3ODdiNjAyMDEyZTZiOTE0Mjg2Njc0MjIyIn0.-0Xhyo1iUrtNCDz2sEdqYuxKzkfa2YUNmBMLwLPD36Q");
                        Intent intent = new Intent(OtpVerification.this, FirstTimePinSetup.class);
                        startActivity(intent);
                        finish();*/
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Configuration croutonConfiguration = new Configuration.Builder()
                                .setDuration(4500).build();
                        // Define custom styles for crouton
                        Style style = new Style.Builder()
                                .setBackgroundColorValue(Color.parseColor("#80000000"))
                                .setGravity(Gravity.CENTER_HORIZONTAL)
                                .setConfiguration(croutonConfiguration)
                                .setHeight(150)
                                .setTextColorValue(Color.parseColor("#ffffff")).build();
                        // Display notice with custom style and configuration
                        Crouton.showText(OtpVerification.this, name, style);
                        // Toast.makeText(OtpVerification.this, name, Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                @Override
                public void onFailure(Call<SelfProfilePojo> call, Throwable t) {
                    String errorMsg = t.getMessage();
                    Toast.makeText(OtpVerification.this, "retrofit fail", Toast.LENGTH_LONG).show();
                    rl_progress_otp.setVisibility(View.GONE);
                    return;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void API_Task(String map) {

        SendOtpData sendOtpData = new SendOtpData();
        sendOtpData.mobile = map;
        try {

            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<OtpPojo> call = apiInterface.getOTP(sendOtpData);

            call.enqueue(new Callback<OtpPojo>() {
                @Override
                public void onResponse(Call<OtpPojo> call, Response<OtpPojo> response) {

                    if (response.raw().code() == 200) {
                        rl_progress_otp.setVisibility(View.GONE);
                    } else {
                        rl_progress_otp.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Configuration croutonConfiguration = new Configuration.Builder()
                                .setDuration(4500).build();
                        // Define custom styles for crouton
                        Style style = new Style.Builder()
                                .setBackgroundColorValue(Color.parseColor("#80000000"))
                                .setGravity(Gravity.CENTER_HORIZONTAL)
                                .setConfiguration(croutonConfiguration)
                                .setHeight(150)
                                .setTextColorValue(Color.parseColor("#ffffff")).build();
                        // Display notice with custom style and configuration
                        Crouton.showText(OtpVerification.this, name, style);
                        // Toast.makeText(OtpVerification.this, name, Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                @Override
                public void onFailure(Call<OtpPojo> call, Throwable t) {

                }

                ;
            });
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    /**
     * method to save user profile in to shared prefrences
     *
     * @param data
     */
    void saveLoginDataToPref(UserProfile data) {
        String json = JsonUtil.toJson(data);
        Preferences.saveData(Preferences.USER_PROFILE, json);
    }

    @Override
    public void locationFound() {
    }

    public static void updateNumber(String value) {
//ToDo -- Fill Text
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OtpVerification.this, Login.class);
        startActivity(intent);
        finish();
    }

    //Registering receiver on activity resume
    @Override
    protected void onResume() {
        super.onResume();
        // Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_ERROR));
    }

    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
        super.onPause();
        //   Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }
}
