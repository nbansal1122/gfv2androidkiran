package com.growthfile.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.R;
import com.growthfile.utility.LocationFinder;
import com.growthfile.utility.LocationFinderCallback;

import me.philio.pinentry.PinEntryView;
import simplifii.framework.utility.Prefs;

import com.growthfile.data.PunchLogData;
import com.growthfile.database.DbHelper;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class PinSetup extends AppCompatActivity implements View.OnClickListener, LocationFinderCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    //    EditText et_pin_1, et_pin_2, et_pin_3, et_pin_4;// et_pin_5;
    TextView txt_pin_name;
    ImageView iv_pinverify_otp;
    String[] otp_code = new String[4];
    String activationCode = "", composed_otp;
    private LinearLayout ll_pinsetup_root;
    View.OnKeyListener keyListener;
    private LocationFinder locationFinder;
    Prefs prefs;

    private PinEntryView pinView;
    private int REQUEST_LOCATION = 1;

    private String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinsetup);

        //check location and sms permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        }

        pinView = (PinEntryView) findViewById(R.id.pinView);

        prefs = new Prefs();
        locationFinder = new LocationFinder(this);
        initUi();

        for (int x = 0; x < 4; x++) {
            otp_code[x] = "";
        }
        InputMethodManager imm = (InputMethodManager)
                PinSetup.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }


        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (isOpen) {
                    ll_pinsetup_root.scrollTo(0, ll_pinsetup_root.getHeight() / 5);
                } else {
                    ll_pinsetup_root.scrollTo(0, 0);
                }
            }
        });
    }

    /**
     * Method to initialize ui component
     */
    private void initUi() {

        txt_pin_name = (TextView) findViewById(R.id.txt_pin_name);
        txt_pin_name.setText("Welcome " + prefs.getPreferencesString(PinSetup.this, "pinnane"));
        ll_pinsetup_root = (LinearLayout) findViewById(R.id.ll_pinsetup_root);
        iv_pinverify_otp = (ImageView) findViewById(R.id.iv_pinverify_otp);
        iv_pinverify_otp.setOnClickListener(this);
    }

    /**
     * Method to check activation code length is matching or not
     */
    void checkActivationCode() {

        int count = 0;
        for (int i = 0; i < 4; i++) {
            if (!otp_code[i].isEmpty()) {
                count = count + 1;
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_pinverify_otp:
//                composed_otp = composePinCode();
                composed_otp = pinView.getText().toString();
                if (composed_otp.toString().trim().length() == 4) {

                    String pinUser = prefs.getPreferencesString(PinSetup.this, "GfilePIN");
                    if (pinUser != "") {
                        if (composed_otp.equals(pinUser)) {
                            getAndSaveAllLogData();
                           /* if (Helper.checkInternetConnection(PinSetup.this)){
                                Intent tapSyncIntent = new Intent(PinSetup.this, TapLockSyncService.class);
                                startService(tapSyncIntent);
                            }*/
                            Intent intent = new Intent(PinSetup.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(PinSetup.this, getText(R.string.incorrect_pin), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        getAndSaveAllLogData();
                        prefs.setPreferencesString(PinSetup.this, "GfilePIN", composed_otp);
                       /* if (Helper.checkInternetConnection(PinSetup.this)){
                            Intent tapSyncIntent = new Intent(PinSetup.this, TapLockSyncService.class);
                            startService(tapSyncIntent);
                        }*/
                        Intent intent = new Intent(PinSetup.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(PinSetup.this, getText(R.string.pin_count_error), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Method to compose activation code from stored code in array
     *
     * @return activation code
     */
    private String composePinCode() {
        String code = "";

        for (int j = 0; j < 4; j++) {
            code = code + otp_code[j];
        }
        return code;
    }

    void getAndSaveAllLogData() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        //final String utcTime = dateFormat.format(new Date());

        System.out.println("DATE--------" + dateFormat.format(cal.getTime()) + "+00:00");
        String time_stamp = dateFormat.format(cal.getTime()) + "+00:00";

        double c_lat = locationFinder.getLatitude();
        double c_lon = locationFinder.getLongitude();

        if (c_lat == 0.0 || c_lon == 0.0) {

        } else {
            PunchLogData punchLogData = new PunchLogData();
            punchLogData.current_lat = c_lat;
            punchLogData.current_lon = c_lon;
            punchLogData.is_attendence = 0;
            punchLogData.sync_status = 0;
            punchLogData.timestamp = time_stamp;

            DbHelper.getInstance(PinSetup.this).insertPunchLog(punchLogData);
        }
    }

    @Override
    public void locationFound() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {
                    Toast.makeText(PinSetup.this, getText(R.string.location_permission_request), Toast.LENGTH_LONG).show();
                    finish();
                }

                break;


        }
    }

    /**
     * method to request location permission
     */
    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
    }
}
