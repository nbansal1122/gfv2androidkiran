package com.growthfile.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.R;
import com.growthfile.util.AppUtil;
import com.growthfile.utility.Helper;
import com.growthfile.utility.LocationData;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Prefs;

import com.growthfile.data.PunchLogData;
import com.growthfile.database.DbHelper;
import com.growthfile.services.TapLockSyncService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class PunchInOutActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback {

    private TextView tv_punchinout_time, tv_punchinout_date, tv_punchinout_ampm;
    private RelativeLayout rel_punchcard_cross, mainrelpunch;
    private ImageView iv_punchinout_confirm, iv_punchinout_loct_time;
    // private LocationFinder locationFinder;
    private boolean isGpsOn;
    private Prefs prefs;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

    // Google Map
    private GoogleMap googleMap;
    double latitude = 0.00;
    double longitude = 0.00;

    LocationData locationData;
    MapFragment mapFragment;
    Bundle bundle;
    String From;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_punchscreen);

        /*mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        //checking for gps
        LocationManager manager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        isGpsOn = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        prefs = new Prefs();

        Intent intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null)
            From = bundle.getString("ToPunch");

        tv_punchinout_time = (TextView) findViewById(R.id.tv_punchinout_time);
        tv_punchinout_date = (TextView) findViewById(R.id.tv_punchinout_date);
        tv_punchinout_ampm = (TextView) findViewById(R.id.tv_punchinout_ampm);
        mainrelpunch = (RelativeLayout) findViewById(R.id.mainrelpunch);
        iv_punchinout_confirm = (ImageView) findViewById(R.id.iv_punchinout_confirm);
        iv_punchinout_confirm.setOnClickListener(this);

        iv_punchinout_loct_time = (ImageView) findViewById(R.id.iv_punchinout_loct_time);
        iv_punchinout_loct_time.setBackgroundResource(getWeatherIcon());
        rel_punchcard_cross = (RelativeLayout) findViewById(R.id.rel_punchcard_cross);
        rel_punchcard_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (isGpsOn) {

            try {
                // Loading map
                initilizeMap();

            } catch (Exception e) {
                e.printStackTrace();
            }
            /*locationFinder = new LocationFinder(this);
            initiateImageLoader();
            String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + locationFinder.getLatitude() + "," + locationFinder.getLatitude() + "&zoom=12&size=100x100&maptype=roadmap&format=png";
            imageLoader.displayImage(url, iv_static_map, options);*/
        } else {
            //   Intent gpsIntent = new Intent(this, GPSError.class);
            //   startActivity(gpsIntent);
        }


        setCurrentTimeAndDate();

    }


    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initilizeMap() {
        if (mapFragment == null) {
            //   SupportMapFragment supportMapFragment = null;
            if (Build.VERSION.SDK_INT < 21) {
                // mapFragment = (SupportMapFragment)this.getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            } else {
                // mapFragment = (SupportMapFragment)this.getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            }
            mapFragment.getMapAsync(this);

            // check if map is created successfully or not
            if (mapFragment == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager manager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        isGpsOn = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGpsOn) {
            //locationFinder = new LocationFinder(this);
            try {
                // Loading map
                //  initilizeMap();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Intent gpsIntent = new Intent(this, GPSError.class);
            startActivity(gpsIntent);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_punchinout_confirm:
                if (Helper.checkGps(PunchInOutActivity.this)) {
                    checkPermissionForFileStorage();
                } else {
                    Intent gpsIntent = new Intent(this, GPSError.class);
                    startActivity(gpsIntent);
                }
                break;
        }
    }

    private void checkPermissionForFileStorage() {
        if (AppUtil.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            savePunchedInData();
        } else {
            requestWriteStoragePermission();
        }
    }

    private static final int WRITE_STORAGE = 2;
    private String[] PERMISSION_WRITE_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    /**
     * method to request sms permission
     */
    private void requestWriteStoragePermission() {
        ActivityCompat.requestPermissions(this, PERMISSION_WRITE_STORAGE, WRITE_STORAGE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_STORAGE:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    showCrouton(getString(R.string.permission_punch_in));
                    //   Toast.makeText(Login.this,getText(R.string.sms_permission_request), Toast.LENGTH_LONG).show();
                }else{
                    savePunchedInData();
                }

                break;
        }
    }

    private void showCrouton(String msg) {
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(5500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(120)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        Crouton.showText(this, msg, style);
    }


    private void savePunchedInData() {
        getAndSaveAllPunchInData();

        if (From.equals("Adapter")) {
            prefs.setPreferencesString(PunchInOutActivity.this, "landingFlag", "Main");
        } else {
            prefs.setPreferencesString(PunchInOutActivity.this, "landingFlag", "Calander");
        }
                   /* Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(PunchInOut.this,getText(R.string.punchin_sucess), style, mainrelpunch);*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time_stamp = dateFormat.format(cal.getTime());
        prefs.setPreferencesString(PunchInOutActivity.this, "MapFlagTime", time_stamp);
        prefs.setPreferencesString(PunchInOutActivity.this, "MapFlag", "punched");
        if (Helper.checkInternetConnection(PunchInOutActivity.this)) {
            Intent tapSyncIntent = new Intent(PunchInOutActivity.this, TapLockSyncService.class);
            startService(tapSyncIntent);
        }

        //Current date//

        setResult(RESULT_OK);
        finish();
    }

    /**
     * method to get all punch data and save it to database
     */
    void getAndSaveAllPunchInData() {

        locationData = new LocationData(PunchInOutActivity.this);
        Location location = locationData.getLocation();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        //final String utcTime = dateFormat.format(new Date());

        System.out.println("DATE--------" + dateFormat.format(cal.getTime()) + "+00:00");
        String time_stamp = dateFormat.format(cal.getTime()) + "+00:00";

        // String time_stamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        double c_lat = location.getLatitude();
        double c_lon = location.getLongitude();

        if (c_lat == 0.0 || c_lon == 0.0) {

            Intent gpsIntent = new Intent(this, GPSError.class);
            startActivity(gpsIntent);
        } else {

            PunchLogData punchLogData = new PunchLogData();
            punchLogData.current_lat = c_lat;
            punchLogData.current_lon = c_lon;
            punchLogData.is_attendence = 1;
            punchLogData.sync_status = 0;
            punchLogData.timestamp = time_stamp;

            DbHelper.getInstance(PunchInOutActivity.this).insertPunchLog(punchLogData);
            DbHelper.getInstance(PunchInOutActivity.this).copyDBToPhoneSD1();

            //save punch date to refresh feedlist
            prefs.setPreferencesInt(PunchInOutActivity.this, "PUNCH_IN_DATE", Calendar.getInstance().getTime().getDate());
        }
    }


    void initiateImageLoader() {
        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.map_placeholder)
                .showImageForEmptyUri(R.drawable.map_placeholder)
                .showImageOnFail(R.drawable.map_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
/*

    @Override
    public void locationFound() {
      */
/*  String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + locationFinder.getLatitude() + "," + locationFinder.getLongitude() + "&zoom=15&size=250x300&maptype=roadmap&format=png";

        if (imageLoader != null) {
            imageLoader.displayImage(url, iv_static_map, options);
        } else {
            initiateImageLoader();
            imageLoader.displayImage(url, iv_static_map, options);
        }*//*

    }
*/

    /**
     * method to set current date and time to screen
     */
    void setCurrentTimeAndDate() {
        String current_time;
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        int year = c.get(Calendar.YEAR);
        String month_name = new DateFormatSymbols().getMonths()[c.get(Calendar.MONTH)].substring(0, 3);
        String day_name = days[c.get(Calendar.DAY_OF_WEEK) - 1];

        tv_punchinout_date.setText(day_name.subSequence(0, 3) + ", " + month_name + " " + day + ", " + year);
        if (String.valueOf(c.get(Calendar.MINUTE)).length() == 2) {
            if (String.valueOf(c.get(Calendar.HOUR)).equals("0")) {
                current_time = "12" + ":" + String.valueOf(c.get(Calendar.MINUTE));
            } else {
                current_time = String.valueOf(c.get(Calendar.HOUR)) + ":" + String.valueOf(c.get(Calendar.MINUTE));
            }
        } else {
            if (String.valueOf(c.get(Calendar.HOUR)).equals("0")) {
                current_time = "12:0" + String.valueOf(c.get(Calendar.MINUTE));
            } else {
                current_time = String.valueOf(c.get(Calendar.HOUR)) + ":0" + String.valueOf(c.get(Calendar.MINUTE));
            }
        }


        tv_punchinout_time.setText(current_time);

        /*int hr = c.get(Calendar.HOUR_OF_DAY);
        if (hr <=11) {
            tv_punchinout_ampm.setText("am");
        } else {
            tv_punchinout_ampm.setText("pm");
        }
*/
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            tv_punchinout_ampm.setText("am");
        } else if (timeOfDay >= 12 && timeOfDay < 24) {
            tv_punchinout_ampm.setText("pm");
        }


    }


    /**
     * method to get wish text
     *
     * @return
     */
    int getWeatherIcon() {
        int icon = 0;

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            icon = R.drawable.day;

        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            icon = R.drawable.day;

        } else if (timeOfDay >= 16 && timeOfDay < 24) {
            icon = R.drawable.night;

        }
        return icon;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;
        locationData = new LocationData(PunchInOutActivity.this);
        Location location = locationData.getLocation();

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            if (ActivityCompat.checkSelfPermission(PunchInOutActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PunchInOutActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // Toast.makeText(PunchInOut.this, "Bolt Riders App does not have permission to access your GPS. Please verify the settings of your device.", Toast.LENGTH_LONG).show();
                return;
            }
            LatLng latLng = new LatLng(latitude, longitude);

            //   googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setZoomGesturesEnabled(false);


        }
    }
}
