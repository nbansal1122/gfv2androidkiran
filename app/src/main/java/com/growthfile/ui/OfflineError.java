package com.growthfile.ui;


import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growthfile.R;
import com.growthfile.utility.Helper;


public class OfflineError extends AppCompatActivity {

    LinearLayout liner_offline_setting;
    RelativeLayout rel_offlineerror_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_offlineerror);

        liner_offline_setting = (LinearLayout) findViewById(R.id.liner_offline_setting);

        liner_offline_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callGPSSettingIntent = new Intent(
                        Settings.ACTION_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });

        rel_offlineerror_back = (RelativeLayout) findViewById(R.id.rel_offlineerror_back);
        rel_offlineerror_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Helper.checkInternetConnection(this)) {
            finish();
        }
    }


}
