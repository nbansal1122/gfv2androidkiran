package com.growthfile.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growthfile.R;

public class TimeMismatch extends AppCompatActivity {

    LinearLayout linear_time_setting;
    RelativeLayout rel_timeerror_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_timemismatcherror);

        linear_time_setting=(LinearLayout)findViewById(R.id.linear_time_setting);
        linear_time_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
            }
        });

        rel_timeerror_back=(RelativeLayout)findViewById(R.id.rel_timeerror_back);
        rel_timeerror_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
