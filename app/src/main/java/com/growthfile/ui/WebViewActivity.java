package com.growthfile.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.growthfile.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class WebViewActivity extends BaseActivity {

    private String url;
    private WebView webView;

    public static void startActivity(Context ctx, String url, String title) {
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.KEY_URL, url);
        b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, title);
        Intent i = new Intent(ctx, WebViewActivity.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        setText(getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_TITLE), R.id.tv_title);
        url = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_URL);
        webView = (WebView) findViewById(R.id.wv_tc);
        webView.setWebViewClient(new MyClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        showProgressDialog();
        if (url.contains("http"))
            webView.loadUrl(url);
        else
            webView.loadUrl("http://" + url);
        setOnClickListener(R.id.rl_webview_back);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        onBackPressed();
    }

    class MyClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgressBar();
        }
    }
}