package com.growthfile.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.SelfProfilePojo;
import com.growthfile.Pojo.SelfProfileResponsePojo;
import simplifii.framework.rest.responses.UserProfile;
import com.growthfile.R;
import com.growthfile.adapter.OtherProfileAdapter;
import com.growthfile.database.DbHelper;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import simplifii.framework.utility.Prefs;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OtherProfile extends AppCompatActivity implements View.OnClickListener {

    LinearLayout  ll_op_retry, ll_op_offline;
    RelativeLayout rel_profileback,progress_otherprofile_lay;
    private int user_id;
    TextView  tv_otherprofile_title;
    Prefs prefs;
    private SelfProfileResponsePojo profilePojo;
    private UserProfile userProfilePojo;
    RecyclerView rv_otherprofile;
    private OtherProfileAdapter myProfileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        prefs = new Prefs();
        initUi();
        user_id=getIntent().getIntExtra("USER_ID",0);

        /*
        * API FOR OTHER USER PROFILE DATA
         */
       /* if (Helper.checkInternetConnection(this)) {

            tv_otherprofile_title.setText("Profile_OLd");
            progress_otherprofile_lay.setVisibility(View.VISIBLE);
            callotherProfileApi();
        } else {
            progress_otherprofile_lay.setVisibility(View.GONE);
            ll_op_offline.setVisibility(View.VISIBLE);
            tv_otherprofile_title.setText("Offline");
        }*/

        tv_otherprofile_title.setText("OtherProfile");
        progress_otherprofile_lay.setVisibility(View.VISIBLE);
        callotherProfileApi();


    }

    void initUi() {

        rv_otherprofile = (RecyclerView) findViewById(R.id.rv_otherprofile);
        progress_otherprofile_lay= (RelativeLayout) findViewById(R.id.progress_otherprofile_lay);
        ll_op_offline = (LinearLayout) findViewById(R.id.ll_op_offline);
        ll_op_retry = (LinearLayout) findViewById(R.id.ll_op_retry);
        ll_op_retry.setOnClickListener(this);
      //  linear_profile_cal = (LinearLayout) findViewById(R.id.linear_profile_cal);
        rel_profileback = (RelativeLayout) findViewById(R.id.rel_profileback);
        tv_otherprofile_title = (TextView) findViewById(R.id.tv_otherprofile_title);

       /* linear_profile_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (profilePojo.getPermissions().getCanViewCalender()){
                   Intent intent = new Intent(OtherProfile.this, OthersCalendar.class);
                   prefs.setPreferencesString(OtherProfile.this,"Othr_USER_NAME",userProfilePojo.getName());
                   prefs.setPreferencesInt(OtherProfile.this,"Othr_USER_ID",userProfilePojo.getId());
                    startActivity(intent);
               }else {
                   Toast.makeText(OtherProfile.this,"You don't have permission to view "+profilePojo.getProfile().getName()+"'s calendar",Toast.LENGTH_SHORT).show();
               }

            }
        });*/

        rel_profileback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * Method to call OTHER profile api
     */
    void callotherProfileApi() {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(OtherProfile.this, "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        String end_url="/api/v2/userprofile/"+user_id;
        Call<SelfProfilePojo> call = restApis.getOtherProfileInfo(end_url,"bearer " + auth_token);
        call.enqueue(new Callback<SelfProfilePojo>() {
            @Override
            public void onResponse(Call<SelfProfilePojo> call, Response<SelfProfilePojo> response) {
                if (response.raw().code() == 200) {

                    progress_otherprofile_lay.setVisibility(View.GONE);
                    ll_op_offline.setVisibility(View.GONE);
                    profilePojo = response.body().getResponse();
                    userProfilePojo = profilePojo.getProfile();

                    myProfileAdapter = new OtherProfileAdapter(OtherProfile.this,userProfilePojo,profilePojo);
                    rv_otherprofile.setLayoutManager(new LinearLayoutManager(OtherProfile.this));
                    rv_otherprofile.setAdapter(myProfileAdapter);
                } else if (response.raw().code() == 401) {
                    progress_otherprofile_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    logout();
                }
                else {
                    progress_otherprofile_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }

            @Override
            public void onFailure(Call<SelfProfilePojo> call, Throwable t) {
                progress_otherprofile_lay.setVisibility(View.GONE);
                ll_op_offline.setVisibility(View.VISIBLE);
                tv_otherprofile_title.setText("Offline");
                String msg = t.getMessage();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_op_retry:
                if (Helper.checkInternetConnection(this)) {
                    progress_otherprofile_lay.setVisibility(View.VISIBLE);
                    tv_otherprofile_title.setText("OtherProfile");
                    callotherProfileApi();
                } else {
                    progress_otherprofile_lay.setVisibility(View.GONE);
                    ll_op_offline.setVisibility(View.VISIBLE);
                    tv_otherprofile_title.setText("Offline");
                }
                break;

            case R.id.progress_otherprofile_lay:
                break;
        }
    }

    void logout() {
        prefs.clearAppData(OtherProfile.this);
        DbHelper dbHelper = DbHelper.getInstance(OtherProfile.this);
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(OtherProfile.this, Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());

    }
}
