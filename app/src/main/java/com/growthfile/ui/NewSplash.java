package com.growthfile.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growthfile.R;
import com.growthfile.customUi.TextViewWorkSansRegular;
import com.growthfile.database.DbHelper;

import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Prefs;

public class NewSplash extends AppCompatActivity {

    private RelativeLayout rl_tc,rl_splash_bottom;
    private DbHelper dbHelper;
    private TextViewWorkSansRegular tv_growthfile_tc;
    Prefs prefs;
    LinearLayout liner_splash_terms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        Preferences.initSharedPreferences(this);
        setContentView(R.layout.activity_newsplash);

        prefs= new Prefs();
        dbHelper=DbHelper.getInstance(getApplicationContext());
        Boolean isDBCreated = prefs.getPreferencesBoolean(getApplicationContext(), "DB_CREATED");
        if (isDBCreated) {
            dbHelper.createDb();
        } else {
            dbHelper.copyRawDataBase();
        }

        if(prefs.getPreferencesString(NewSplash.this,"Verified").equals("") || prefs.getPreferencesString(NewSplash.this,"Verified").equals(null)) {
            Intent intent = new Intent(NewSplash.this, Splash.class);
            startActivity(intent);
            finish();
        }else{
            String pinUser = prefs.getPreferencesString(NewSplash.this, "GfilePIN");
            if (pinUser.equals("")) {
                Intent intent = new Intent(NewSplash.this, FirstTimePinSetup.class);
                startActivity(intent);
                finish();
            }else {
                Intent intent = new Intent(NewSplash.this, PinSetup.class);
                startActivity(intent);
                finish();
            }
        }
    }


}
