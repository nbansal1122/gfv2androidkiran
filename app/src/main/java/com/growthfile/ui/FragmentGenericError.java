package com.growthfile.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.growthfile.R;
import com.growthfile.database.DbHelper;
import simplifii.framework.utility.Prefs;

public class FragmentGenericError extends AppCompatActivity {
    private RelativeLayout rel_frag_genericeroor_back;
    private DbHelper dbHelper;
    Prefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_generic_error);

        prefs= new Prefs();
        dbHelper=DbHelper.getInstance(this);
        rel_frag_genericeroor_back= (RelativeLayout) findViewById(R.id.rel_frag_genericeroor_back);
        rel_frag_genericeroor_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        prefs.clearAppData(FragmentGenericError.this);
        dbHelper.deleteDb();
        Intent splashIntent=new Intent(FragmentGenericError.this,Splash.class);
        startActivity(splashIntent);
        finish();

    }
}
