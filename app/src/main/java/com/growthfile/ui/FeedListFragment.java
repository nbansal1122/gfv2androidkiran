package com.growthfile.ui;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growthfile.Pojo.FeedListAcceptPojo;

import com.growthfile.Pojo.FeedListCard;
import com.growthfile.Pojo.FeedListModel;
import com.growthfile.Pojo.OtpVerifyUserProfilePojo;
import com.growthfile.R;
import com.growthfile.adapter.FeedCardFragmentAdapter;
import com.growthfile.database.DbHelper;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import com.growthfile.utility.LocationFinder;
import com.growthfile.utility.LocationFinderCallback;
import simplifii.framework.utility.Prefs;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.google.gson.Gson;

import org.json.JSONObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedListFragment extends Fragment implements LocationFinderCallback, SwipeRefreshLayout.OnRefreshListener, FeedCardFragmentAdapter.AdapterCallback {

    RecyclerView rw_feeds;
    public static TextView tv_feedmain_welcome,tv_feedsucess_msg;
    public static LinearLayout  linear_feedmain_withdraw;
    ImageView iv_feed_confirmPunchin,iv_retry;
    ImageView img_feedscuess_done;
    public  static  TextView tv_feedlist_withdrawtext;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM_readSingle = 1;
    private static final int TYPE_ITEM_readMultiple = 2;
    private static final int TYPE_ITEM_actionSingle = 3;
    private static final int TYPE_ITEM_actionMultiple = 4;
    private int total_card_count = 0,page_count=1;
    private LocationFinder locationFinder;
    private double lat, lon;
    private OtpVerifyUserProfilePojo userProfilePojo;
    private LinearLayoutManager layoutManager;
    Prefs prefs;
    public static LinearLayout rl_withdraw_feed_list;
    LinearLayout liner_feedoffline_setting;
    RelativeLayout rel_feedslist_offline,rl_feedlist_found;
    public static RelativeLayout previous1;
    private SwipeRefreshLayout swipeContainer;
    public static RelativeLayout rl_progress_feed_list;
    List<FeedListCard> cards;
    ArrayList<FeedListCard> feedListCards;
    private Animation loader_animation;
    static int enityID, actionID;
    static String entityTYPE;
    boolean headerFlag= false;

    public FeedListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationFinder = new LocationFinder(getActivity());
        // getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_list, container, false);

        prefs = new Prefs();
        userProfilePojo = getLoginDataFromPref();
        lat = locationFinder.getLatitude();
        lon = locationFinder.getLongitude();

        loader_animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotation);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        rl_feedlist_found = (RelativeLayout) view.findViewById(R.id.rl_feedlist_found);
        iv_retry = (ImageView) view.findViewById(R.id.iv_retry);
        tv_feedlist_withdrawtext = (TextView) view.findViewById(R.id.tv_feedlist_withdrawtext);
        linear_feedmain_withdraw = (LinearLayout) view.findViewById(R.id.linear_feedmain_withdraw);
        rl_withdraw_feed_list = (LinearLayout) view.findViewById(R.id.rl_withdraw_feed_list);
        rl_withdraw_feed_list.setVisibility(View.GONE);
        rl_progress_feed_list = (RelativeLayout) view.findViewById(R.id.rl_progress_feed_list);
    //    ll_feeds_applysuccess = (LinearLayout) view.findViewById(R.id.ll_feeds_applysuccess);
        rel_feedslist_offline = (RelativeLayout) view.findViewById(R.id.rel_feedslist_offline);
        liner_feedoffline_setting = (LinearLayout) view.findViewById(R.id.liner_feedoffline_setting);
        iv_feed_confirmPunchin = (ImageView) view.findViewById(R.id.iv_feed_confirmPunchin);
     //   ll_feeds_applysuccess = (LinearLayout) view.findViewById(R.id.ll_feeds_applysuccess);
        liner_feedoffline_setting = (LinearLayout) view.findViewById(R.id.liner_feedoffline_setting);
        rw_feeds = (RecyclerView) view.findViewById(R.id.rw_feeds);
        tv_feedmain_welcome = (TextView) view.findViewById(R.id.tv_feedmain_welcome);
        tv_feedmain_welcome.setText("Activity");
        previous1 = (RelativeLayout) view.findViewById(R.id.previous1);
        tv_feedsucess_msg = (TextView) view.findViewById(R.id.tv_feedsucess_msg);

        layoutManager = new LinearLayoutManager(getActivity());
        cards = new ArrayList<FeedListCard>();
        rw_feeds.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (layoutManager.findLastVisibleItemPosition() <= cards.size() - 3) {

                    if (cards.size() < total_card_count) {
                        page_count = page_count + 1;
                        getPunchData();
                        callFeedListApi();
                    }
                }
            }
        });

       // String wish_text = "";
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {

            if (!prefs.getPreferencesString(getActivity(), "saluteFlag").equals("morning")) {
                Configuration croutonConfiguration = new Configuration.Builder()
                        .setDuration(5500).build();
                // Define custom styles for crouton
                Style style = new Style.Builder()
                        .setBackgroundColorValue(Color.parseColor("#80000000"))
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                        .setConfiguration(croutonConfiguration)
                        .setHeight(120)
                        .setTextColorValue(Color.parseColor("#ffffff")).build();
                // Display notice with custom style and configuration
               Crouton.showText(getActivity(), getWishText() + " " + userProfilePojo.getName(), style, previous1);
            }else
            {

            }
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            if (!prefs.getPreferencesString(getActivity(), "saluteFlag").equals("afternoon")) {
                Configuration croutonConfiguration = new Configuration.Builder()
                        .setDuration(5500).build();
                // Define custom styles for crouton
                Style style = new Style.Builder()
                        .setBackgroundColorValue(Color.parseColor("#80000000"))
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                        .setConfiguration(croutonConfiguration)
                        .setHeight(120)
                        .setTextColorValue(Color.parseColor("#ffffff")).build();
                // Display notice with custom style and configuration
                Crouton.showText(getActivity(), getWishText() + " " + userProfilePojo.getName(), style, previous1);

            }else
            {

            }
        }else if(timeOfDay >= 16 && timeOfDay < 24){
            if (!prefs.getPreferencesString(getActivity(), "saluteFlag").equals("evening")) {
                Configuration croutonConfiguration = new Configuration.Builder()
                        .setDuration(5500).build();
                // Define custom styles for crouton
                Style style = new Style.Builder()
                        .setBackgroundColorValue(Color.parseColor("#80000000"))
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                        .setConfiguration(croutonConfiguration)
                        .setHeight(120)
                        .setTextColorValue(Color.parseColor("#ffffff")).build();
                // Display notice with custom style and configuration
                Crouton.showText(getActivity(), getWishText() + " " + userProfilePojo.getName(), style, previous1);
            }else
            {

            }
        }
        /*img_feedscuess_done = (ImageView) view.findViewById(R.id.img_feedscuess_done);
        img_feedscuess_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_feeds_applysuccess.setVisibility(View.GONE);
                tv_feedmain_welcome.setText("Activity");
                if (Helper.checkInternetConnection(getActivity())) {
                    cards.clear();
                    page_count = 1;
                    getPunchData();
                    callFeedListApi();
                    rl_progress_feed_list.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Activity");


                } else {
                    rel_feedslist_offline.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Offline");
                }
            }
        });*/
        liner_feedoffline_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Animation a = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in);
                a.setDuration(1000);
                iv_retry.startAnimation(a);
                if (Helper.checkInternetConnection(getActivity())) {
                    getPunchData();
                    callFeedListApi();
                    rl_progress_feed_list.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Activity");

                } else {
                    rel_feedslist_offline.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Offline");
                }
            }
        });

        rl_progress_feed_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        iv_feed_confirmPunchin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PunchInOutActivity.class);
                getActivity().startActivity(intent);
            }
        });

        linear_feedmain_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_progress_feed_list.setVisibility(View.VISIBLE);
                callOtherLeaveBalApi(enityID, entityTYPE, actionID);

            }
        });
        rl_withdraw_feed_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                rl_withdraw_feed_list.setVisibility(View.GONE);

                return false;
            }
        });
        return view;
    }


    @Override
    public void setUserVisibleHint(final boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible) {

            if(prefs==null){

            }else {
                System.out.println("my feedlist visible");
                rel_feedslist_offline.setVisibility(View.GONE);
                rl_progress_feed_list.setVisibility(View.VISIBLE);
                tv_feedmain_welcome.setText("Activity");
                cards.clear();
                page_count=1;
                getPunchData();
                callFeedListApi();
            }
        }
    }
    public static FeedListFragment newInstance() {
        FeedListFragment f = new FeedListFragment();
        return f;
    }

    @Override
    public void locationFound() {

    }
    /**
     * Fetch Store and User data from SharedPrefrences
     *
     * @return
     */
    OtpVerifyUserProfilePojo getLoginDataFromPref() {
        Gson gson = new Gson();
        String json = prefs.getPreferencesString(getActivity(), "USER_PROFILE");
        OtpVerifyUserProfilePojo obj = gson.fromJson(json, OtpVerifyUserProfilePojo.class);
        return obj;
    }

    @Override
    public void onStart() {
        System.out.println("inside feedlist");
        super.onStart();
        rel_feedslist_offline.setVisibility(View.GONE);
        rl_progress_feed_list.setVisibility(View.VISIBLE);
        tv_feedmain_welcome.setText("Activity");
        cards.clear();
        page_count=1;
        getPunchData();
        callFeedListApi();

        if(prefs.getPreferencesString(getActivity(),"landingFlag").equals("Main")){
            prefs.setPreferencesString(getActivity(),"landingFlag","");
            Configuration croutonConfiguration = new Configuration.Builder()
                    .setDuration(5500).build();
            // Define custom styles for crouton
            Style style = new Style.Builder()
                    .setBackgroundColorValue(Color.parseColor("#80000000"))
                    .setGravity(Gravity.CENTER_HORIZONTAL)
                    .setConfiguration(croutonConfiguration)
                    .setHeight(120)
                    .setTextColorValue(Color.parseColor("#ffffff")).build();
            // Display notice with custom style and configuration
            Crouton.showText(getActivity(),getText(R.string.punchin_sucess), style, previous1);
        }
    }


    /**
     * method to hit feed list api
     */
    void callFeedListApi() {
        String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        String url = "/api/v1/feed?offset="+page_count;
        Call<FeedListModel> call = restApis.getFeedListData(url, "bearer " + auth_token);

        call.enqueue(new Callback<FeedListModel>() {
            @Override
            public void onResponse(Call<FeedListModel> call, Response<FeedListModel> response) {
                rel_feedslist_offline.setVisibility(View.GONE);
                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    swipeContainer.setRefreshing(false);
                    if (response.raw().code() == 200) {

                        String timestamp= response.body().getResponse().getTimestamp();

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Calendar cal = Calendar.getInstance();
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        String time_stamp =dateFormat.format(cal.getTime())+"+00:00";

                        long diff = 0;
                        try {
                            diff = dateFormat.parse(time_stamp).getTime() - dateFormat.parse(timestamp).getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diffMinutes = diff / (60 * 1000) % 60;
                        if(diffMinutes>=0 && diffMinutes<10) {

                            total_card_count = response.body().getResponse().getTotal();
                            rel_feedslist_offline.setVisibility(View.GONE);

                            if(cards.size()<total_card_count) {
                                System.out.println("inside card if");
                                cards.addAll(response.body().getResponse().getCards());
                            }

                            if (cards.size() > 0) {
                                rl_feedlist_found.setVisibility(View.GONE);
                            } else {
                                if(headerFlag==false){
                                    rl_feedlist_found.setVisibility(View.GONE);
                                }else {
                                    rl_feedlist_found.setVisibility(View.VISIBLE);
                                }
                            }

                            FeedCardFragmentAdapter  rv_adapter = new FeedCardFragmentAdapter(getTeamData(), getActivity(), LocationFinder.c_lat, LocationFinder.c_lon, headerFlag,FeedListFragment.this);
                           // rw_feeds.setHasFixedSize(true);
                            rw_feeds.setLayoutManager(layoutManager);
                            rw_feeds.setAdapter(rv_adapter);
                         //   rv_adapter.notifyDataSetChanged();
                            rl_progress_feed_list.setVisibility(View.GONE);
                        }else
                        {
                            Intent intent = new Intent(getActivity(), TimeMismatch.class);
                            startActivity(intent);
                        }

                    } else if (response.raw().code() == 401) {
                        rel_feedslist_offline.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                      //  Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
                        logout();
                    } else {
                        rl_progress_feed_list.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Configuration croutonConfiguration = new Configuration.Builder()
                                .setDuration(5500).build();
                        // Define custom styles for crouton
                        Style style = new Style.Builder()
                                .setBackgroundColorValue(Color.parseColor("#80000000"))
                                .setGravity(Gravity.CENTER_HORIZONTAL)
                                .setConfiguration(croutonConfiguration)
                                .setHeight(120)
                                .setTextColorValue(Color.parseColor("#ffffff")).build();
                        // Display notice with custom style and configuration
                        if(!TextUtils.isEmpty(name))
                            Crouton.showText(getActivity(),name, style, previous1);

                    }
                }
            }

            @Override
            public void onFailure(Call<FeedListModel> call, Throwable t) {

                Activity activity = getActivity();
                if (isAdded() && activity != null) {
                    swipeContainer.setRefreshing(false);
                    rl_progress_feed_list.setVisibility(View.GONE);
                    rel_feedslist_offline.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Offline");
                }
            }
        });

    }

    // get data from api to bind child
    public ArrayList<FeedListCard> getTeamData() {

        int size = cards.size();
        feedListCards = new ArrayList<FeedListCard>();

        for (int i = 0; i < size; i++) {
            feedListCards.add(cards.get(i));
        }
        return feedListCards;
    }

    @Override
    public void onPause() {
        super.onPause();
      //  rl_progress_feed_list.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // rl_progress_feed_list.setVisibility(View.GONE);
    }


    /**
     * method to get wish text
     *
     * @return
     */
    String getWishText() {
        String wish_text = "";
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            prefs.setPreferencesString(getActivity(),"saluteFlag","morning");
            wish_text = "Good Morning";

        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            prefs.setPreferencesString(getActivity(),"saluteFlag","afternoon");
            wish_text = "Good Afternoon";

        } else if (timeOfDay >= 16 && timeOfDay < 24) {
            prefs.setPreferencesString(getActivity(),"saluteFlag","evening");
            wish_text = "Good Evening";

        }
        return wish_text;

    }

    public static void withdrawProcess(int entityId, String entityType, int actionId,String text) {

        rl_withdraw_feed_list.setVisibility(View.VISIBLE);
        tv_feedlist_withdrawtext.setText(text);
        //rl_withdraw_feed_list.setVisibility(View.VISIBLE);
        enityID = entityId;
        actionID = actionId;
        entityTYPE = entityType;
    }

    void callOtherLeaveBalApi(int entityId, String entityType, int actionId) {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        // String endpoint_url = "/api/v1/userleaves/" + user_id;
        String endpoint = "/api/v1/action/" + entityType + "/" + entityId + "/" + actionId;
        RestApis restApis = retrofit.create(RestApis.class);
        Call<FeedListAcceptPojo> call = restApis.applyFeedAcept(endpoint, "bearer " + auth_token);
        call.enqueue(new Callback<FeedListAcceptPojo>() {
            @Override
            public void onResponse(Call<FeedListAcceptPojo> call, Response<FeedListAcceptPojo> response) {
                if (response.raw().code() == 200) {

                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                    rl_withdraw_feed_list.setVisibility(View.GONE);
                    // Inflate any custom view
                    View customView = getActivity().getLayoutInflater().inflate(R.layout.custom_crouton_layout, null);
                    TextView msg=(TextView) customView.findViewById(R.id.tv_crouton_msg);
                    msg.setText(response.body().getMsg());
                    Crouton.show(getActivity(), customView,previous1);

                    cards.clear();
                    page_count = 1;
                    getPunchData();
                    callFeedListApi();
                    rl_progress_feed_list.setVisibility(View.VISIBLE);
                    tv_feedmain_welcome.setText("Activity");

                }else if (response.raw().code() == 401) {
                    rel_feedslist_offline.setVisibility(View.GONE);
                    rl_withdraw_feed_list.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                //    Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
                    logout();
                } else {
                    FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                    rl_withdraw_feed_list.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(5500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(120)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(getActivity(), name, style, previous1);
                   // Toast.makeText(getActivity(), name, Toast.LENGTH_LONG).show();
                    return;
                }
            }
            @Override
            public void onFailure(Call<FeedListAcceptPojo> call, Throwable t) {
                FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
                rl_withdraw_feed_list.setVisibility(View.GONE);
                rel_feedslist_offline.setVisibility(View.VISIBLE);
                String msg = t.getMessage();
               // Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

  public  void logout() {
        prefs.clearAppData(getActivity());
        DbHelper dbHelper = DbHelper.getInstance(getActivity());
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(getActivity(), Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        getActivity().finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    @Override
    public void onRefresh() {
        rl_withdraw_feed_list.setVisibility(View.GONE);
        page_count=1;
        getPunchData();
        callFeedListApi();
    }
    void getPunchData(){

        ArrayList<String> currentAttendance = new ArrayList<>();
        currentAttendance=DbHelper.getInstance(getActivity()).getAttendance();
        //Current date//
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time_stamp =dateFormat.format(cal.getTime());
        //Last index//
        if(currentAttendance.size()>0) {
            String lastIndx = currentAttendance.get(currentAttendance.size() - 1);
            String[] sep = lastIndx.split("T");

            String lastDate = sep[0];
            if (time_stamp.equals(lastDate)) {
                headerFlag = true;
            } else {
                if(prefs.getPreferencesString(getActivity(),"MapFlag").equals("punched") && prefs.getPreferencesString(getActivity(),"MapFlagTime").equals(time_stamp)) {
                    headerFlag = true;
                }else {
                    headerFlag=false;
                }
            }
        }else{
            if(prefs.getPreferencesString(getActivity(),"MapFlag").equals("punched")) {
                headerFlag = true;
            }else {
                headerFlag=false;
            }
        }
    }



    @Override
    public void onMethodCallback(String str) {
        // do something

        FeedListFragment.rl_progress_feed_list.setVisibility(View.GONE);
        rl_withdraw_feed_list.setVisibility(View.GONE);
        // Inflate any custom view
        View customView = getActivity().getLayoutInflater().inflate(R.layout.custom_crouton_layout, null);
        TextView msg=(TextView) customView.findViewById(R.id.tv_crouton_msg);
        msg.setText(str);
        Crouton.show(getActivity(), customView,previous1);

        cards.clear();
        page_count = 1;
        getPunchData();
        callFeedListApi();
        rl_progress_feed_list.setVisibility(View.VISIBLE);
        tv_feedmain_welcome.setText("Activity");

    }

}

