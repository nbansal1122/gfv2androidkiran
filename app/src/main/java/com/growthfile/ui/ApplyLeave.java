package com.growthfile.ui;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.Pojo.ApplySelfLeavePojo;
import com.growthfile.Pojo.SelfLeaveBalPojo;
import com.growthfile.R;
import com.growthfile.database.DbHelper;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;

import simplifii.framework.utility.Prefs;

import com.growthfile.webservices.ApplySelfLeaveRawData;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;

import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ApplyLeave extends AppCompatActivity implements View.OnClickListener {

    TextView tv_leave_1, tv_leave_2, tv_leave_3, tv_leave_4, tv_applyleave_successmsg;
    RelativeLayout rel_applyleavecard_cross, progress_applyleave_lay,
            rl_listdouble, rl_listsingle;
    TextView tv_applyleave_leavebal, tv_applyleave_causallv, tv_applyleave_earnedlv, tv_applyleave_medicallv,
            tv_applyleave_leaveto, tv_applyleave_leavefrom, tv_applyleave_leaveToMon, tv_applyleave_leavefromMon,
            tv_applyleave_heading, tv_applyleave_singleleavefrom, tv_applyleave_singleleavefromMon;

    EditText ed_applycard_comment;
    LinearLayout linear_applycard_confirm;
    RelativeLayout rl_applylleavemain;
    ImageView applyleave_retry;
    Retrofit retroFitMaker;
    String leaveType;
    private LinearLayout ll_apply_leave_offline, ll_apply_leave_retry;
    ImageView img_scuess_done;
    Prefs prefs;
    ProgressDialog progressDialog;
    String Sdate, Edate;
    String Smonth, Emonth;
    String formattedDateStart, formattedDateEnd, From;
    int OtherUserId;
    String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String comment;
    Bundle bundle;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applyleave);

        prefs = new Prefs();
        Intent intent = getIntent();
        bundle = intent.getExtras();
        From = bundle.getString("FROM");
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        rl_listdouble = (RelativeLayout) findViewById(R.id.rl_listdouble);
        rl_listsingle = (RelativeLayout) findViewById(R.id.rl_listsingle);
        applyleave_retry = (ImageView) findViewById(R.id.applyleave_retry);

        //// CHECK FROM WHERE THE PAGE LANDED////
        if (From.equals("MYCAL")) {
            String compStart = bundle.getString("formattedDateStart");
            String compEnd = bundle.getString("formattedDateEnd");
            int compareDtae = compStart.compareTo(compEnd);
            System.out.println(compareDtae);
            if (compareDtae >= 1) {
                rl_listdouble.setVisibility(View.VISIBLE);
                rl_listsingle.setVisibility(View.GONE);
                formattedDateStart = bundle.getString("formattedDateEnd");
                formattedDateEnd = bundle.getString("formattedDateStart");
            } else if (compareDtae < 0) {
                rl_listdouble.setVisibility(View.VISIBLE);
                rl_listsingle.setVisibility(View.GONE);
                formattedDateStart = bundle.getString("formattedDateStart");
                formattedDateEnd = bundle.getString("formattedDateEnd");
            } else if (compareDtae == 0) {
                rl_listdouble.setVisibility(View.GONE);
                rl_listsingle.setVisibility(View.VISIBLE);
                formattedDateStart = bundle.getString("formattedDateStart");
                formattedDateEnd = bundle.getString("formattedDateEnd");
            }
            String[] sep = formattedDateStart.split("-");
            Sdate = sep[2];
            String[] sepE = formattedDateEnd.split("-");
            Edate = sepE[2];
            Smonth = monthNames[Integer.parseInt(sep[1]) - 1];
            Emonth = monthNames[Integer.parseInt(sepE[1]) - 1];
            intui();
            /*
                API CALL FOR SELF LEAVE BALANCE
             */
            if (Helper.checkInternetConnection(this)) {
                ll_apply_leave_offline.setVisibility(View.GONE);
                tv_applyleave_heading.setText("Apply for a leave");
                progress_applyleave_lay.setVisibility(View.VISIBLE);
                callSelfLeaveBalApi();
            } else {
                progress_applyleave_lay.setVisibility(View.GONE);
                tv_applyleave_heading.setText("Offline");
                ll_apply_leave_offline.setVisibility(View.VISIBLE);
            }
        } else {

            String compStart = bundle.getString("formattedDateStart");
            String compEnd = bundle.getString("formattedDateEnd");
            int compareDtae = compStart.compareTo(compEnd);
            System.out.println(compareDtae);

            if (compareDtae >= 1) {
                rl_listdouble.setVisibility(View.VISIBLE);
                rl_listsingle.setVisibility(View.GONE);
                formattedDateStart = bundle.getString("formattedDateEnd");
                formattedDateEnd = bundle.getString("formattedDateStart");

            } else if (compareDtae < 0) {
                rl_listdouble.setVisibility(View.VISIBLE);
                rl_listsingle.setVisibility(View.GONE);
                formattedDateStart = bundle.getString("formattedDateStart");
                formattedDateEnd = bundle.getString("formattedDateEnd");

            } else if (compareDtae == 0) {
                rl_listdouble.setVisibility(View.GONE);
                rl_listsingle.setVisibility(View.VISIBLE);
                formattedDateStart = bundle.getString("formattedDateStart");
                formattedDateEnd = bundle.getString("formattedDateEnd");
            }
            OtherUserId = bundle.getInt("UserId");

            String[] sep = formattedDateStart.split("-");
            Sdate = sep[2];
            String[] sepE = formattedDateEnd.split("-");
            Edate = sepE[2];
            Smonth = monthNames[Integer.parseInt(sep[1]) - 1];
            Emonth = monthNames[Integer.parseInt(sepE[1]) - 1];
            intui();
            /*
                API CALL FOR OTHER LEAVE BALANCE
             */
            if (Helper.checkInternetConnection(this)) {
                ll_apply_leave_offline.setVisibility(View.GONE);
                progress_applyleave_lay.setVisibility(View.VISIBLE);
                tv_applyleave_heading.setText("Apply for a leave");
                callOtherLeaveBalApi(OtherUserId);
            } else {
                progress_applyleave_lay.setVisibility(View.GONE);
                ll_apply_leave_offline.setVisibility(View.VISIBLE);
                tv_applyleave_heading.setText("Offline");
            }

        }
    }

    void intui() {


        tv_applyleave_heading = (TextView) findViewById(R.id.tv_applyleave_heading);
        tv_applyleave_heading.setText("Apply for a leave");
        img_scuess_done = (ImageView) findViewById(R.id.img_scuess_done);
        img_scuess_done.setOnClickListener(this);
        //   ll_apply_leave_applysuccess=(LinearLayout)findViewById(R.id.ll_apply_leave_applysuccess);
        tv_applyleave_successmsg = (TextView) findViewById(R.id.tv_applyleave_successmsg);
        progress_applyleave_lay = (RelativeLayout) findViewById(R.id.progress_applyleave_lay);
        ll_apply_leave_offline = (LinearLayout) findViewById(R.id.ll_apply_leave_offline);
        ll_apply_leave_retry = (LinearLayout) findViewById(R.id.ll_apply_leave_retry);
        tv_leave_1 = (TextView) findViewById(R.id.tv_leave_1);
        tv_leave_2 = (TextView) findViewById(R.id.tv_leave_2);
        tv_leave_3 = (TextView) findViewById(R.id.tv_leave_3);
        tv_leave_4 = (TextView) findViewById(R.id.tv_leave_4);

        tv_applyleave_leavebal = (TextView) findViewById(R.id.tv_applyleave_leavebal);
        tv_applyleave_causallv = (TextView) findViewById(R.id.tv_applyleave_causallv);
        tv_applyleave_earnedlv = (TextView) findViewById(R.id.tv_applyleave_earnedlv);
        tv_applyleave_medicallv = (TextView) findViewById(R.id.tv_applyleave_medicallv);
        tv_applyleave_leaveto = (TextView) findViewById(R.id.tv_applyleave_leaveto);
        tv_applyleave_leavefrom = (TextView) findViewById(R.id.tv_applyleave_leavefrom);
        tv_applyleave_leaveToMon = (TextView) findViewById(R.id.tv_applyleave_leaveToMon);
        tv_applyleave_leavefromMon = (TextView) findViewById(R.id.tv_applyleave_leavefromMon);
        rl_applylleavemain = (RelativeLayout) findViewById(R.id.rl_applylleavemain);

        tv_applyleave_singleleavefromMon = (TextView) findViewById(R.id.tv_applyleave_singleleavefromMon);
        tv_applyleave_singleleavefrom = (TextView) findViewById(R.id.tv_applyleave_singleleavefrom);

        tv_applyleave_singleleavefromMon.setText(Emonth + ",2016");
        tv_applyleave_singleleavefrom.setText(Sdate);

        tv_applyleave_leavefrom.setText(Sdate);
        tv_applyleave_leaveto.setText(Edate);
        tv_applyleave_leaveToMon.setText(Emonth + ",2016");
        tv_applyleave_leavefromMon.setText(Smonth + ",2016");

        ed_applycard_comment = (EditText) findViewById(R.id.ed_applycard_comment);
        linear_applycard_confirm = (LinearLayout) findViewById(R.id.linear_applycard_confirm);
        linear_applycard_confirm.setOnClickListener(this);
        rel_applyleavecard_cross = (RelativeLayout) findViewById(R.id.rel_applyleavecard_cross);
        rel_applyleavecard_cross.setOnClickListener(this);
        ll_apply_leave_retry.setOnClickListener(this);

        tv_leave_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                leaveType = "EL";
                tv_leave_1.setBackgroundResource(R.drawable.leave_clickable);
                tv_leave_2.setBackgroundResource(R.color.leave);
                tv_leave_3.setBackgroundResource(R.color.leave);
                tv_leave_4.setBackgroundResource(R.color.leave);
                tv_leave_1.setTextColor(Color.parseColor("#ffffff"));
                tv_leave_2.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_3.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_4.setTextColor(Color.parseColor("#6c747c"));
            }
        });
        tv_leave_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                leaveType = "CL";
                tv_leave_1.setBackgroundResource(R.color.leave);
                tv_leave_2.setBackgroundResource(R.drawable.leave_clickable);
                tv_leave_3.setBackgroundResource(R.color.leave);
                tv_leave_4.setBackgroundResource(R.color.leave);
                tv_leave_1.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_2.setTextColor(Color.parseColor("#ffffff"));
                tv_leave_3.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_4.setTextColor(Color.parseColor("#6c747c"));
            }
        });
        tv_leave_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                leaveType = "SL";
                tv_leave_1.setBackgroundResource(R.color.leave);
                tv_leave_2.setBackgroundResource(R.color.leave);
                tv_leave_3.setBackgroundResource(R.drawable.leave_clickable);
                tv_leave_4.setBackgroundResource(R.color.leave);
                tv_leave_1.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_2.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_3.setTextColor(Color.parseColor("#ffffff"));
                tv_leave_4.setTextColor(Color.parseColor("#6c747c"));
            }
        });
        tv_leave_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                leaveType = "LWP";
                tv_leave_1.setBackgroundResource(R.color.leave);
                tv_leave_2.setBackgroundResource(R.color.leave);
                tv_leave_3.setBackgroundResource(R.color.leave);
                tv_leave_4.setBackgroundResource(R.drawable.leave_clickable);
                tv_leave_1.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_2.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_3.setTextColor(Color.parseColor("#6c747c"));
                tv_leave_4.setTextColor(Color.parseColor("#ffffff"));
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rel_applyleavecard_cross:

                if (From.equals("MYCAL")) {
                    finish();
                } else {
                    Intent intent = new Intent(ApplyLeave.this, OthersCalendar.class);
                    startActivity(intent);
                    finish();
                }

                break;

            case R.id.linear_applycard_confirm:

                if (From.equals("MYCAL")) {
                    if (leaveType == null) {
                        Toast.makeText(ApplyLeave.this, getText(R.string.select_leave_type), Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        /*
                            API CALL FOR APPLY SELF LEAVE
                         */

                        comment = ed_applycard_comment.getText().toString();
                        if (Helper.checkInternetConnection(this)) {
                            ll_apply_leave_offline.setVisibility(View.GONE);
                            progress_applyleave_lay.setVisibility(View.VISIBLE);
                            tv_applyleave_heading.setText("Apply for a leave");
                            callSelfLeaveApplyApi();

                        } else {
                            progress_applyleave_lay.setVisibility(View.GONE);
                            ll_apply_leave_offline.setVisibility(View.VISIBLE);
                            tv_applyleave_heading.setText("Offline");
                        }
                    }
                } else {
                    if (leaveType == null) {
                        Toast.makeText(ApplyLeave.this, getText(R.string.select_leave_type), Toast.LENGTH_LONG).show();
                        return;
                    } else {
                         /*
                            API CALL FOR APPLY OTHER LEAVE
                         */
                        comment = ed_applycard_comment.getText().toString();
                        if (Helper.checkInternetConnection(this)) {
                            ll_apply_leave_offline.setVisibility(View.GONE);
                            tv_applyleave_heading.setText("Apply for a leave");
                            progress_applyleave_lay.setVisibility(View.VISIBLE);
                            callOtherLeaveApplyApi(OtherUserId);

                        } else {
                            progress_applyleave_lay.setVisibility(View.GONE);
                            tv_applyleave_heading.setText("Offline");
                            ll_apply_leave_offline.setVisibility(View.VISIBLE);
                        }

                    }
                }
                break;

            case R.id.ll_apply_leave_retry:

                final Animation a = AnimationUtils.loadAnimation(ApplyLeave.this,
                        R.anim.slide_in);
                a.setDuration(1000);
                applyleave_retry.startAnimation(a);
                if (From.equals("MYCAL")) {
                    formattedDateStart = bundle.getString("formattedDateStart");
                    formattedDateEnd = bundle.getString("formattedDateEnd");
                    String[] sep = formattedDateStart.split("-");
                    Sdate = sep[2];
                    String[] sepE = formattedDateEnd.split("-");
                    Edate = sepE[2];
                    Smonth = monthNames[Integer.parseInt(sep[1]) - 1];
                    Emonth = monthNames[Integer.parseInt(sepE[1]) - 1];
            /*
                API CALL FOR SELF LEAVE BALANCE
             */
                    if (Helper.checkInternetConnection(this)) {

                        ll_apply_leave_offline.setVisibility(View.GONE);
                        tv_applyleave_heading.setText("Apply for a leave");
                        progress_applyleave_lay.setVisibility(View.VISIBLE);
                        callSelfLeaveBalApi();
                    } else {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        tv_applyleave_heading.setText("Offline");
                        ll_apply_leave_offline.setVisibility(View.VISIBLE);
                    }
                } else {
                    formattedDateStart = bundle.getString("formattedDateStart");
                    formattedDateEnd = bundle.getString("formattedDateEnd");
                    OtherUserId = bundle.getInt("UserId");

                    String[] sep = formattedDateStart.split("-");
                    Sdate = sep[2];
                    String[] sepE = formattedDateEnd.split("-");
                    Edate = sepE[2];
                    Smonth = monthNames[Integer.parseInt(sep[1]) - 1];
                    Emonth = monthNames[Integer.parseInt(sepE[1]) - 1];

            /*
                API CALL FOR OTHER LEAVE BALANCE
             */
                    if (Helper.checkInternetConnection(this)) {
                        ll_apply_leave_offline.setVisibility(View.GONE);
                        tv_applyleave_heading.setText("Apply for a leave");
                        progress_applyleave_lay.setVisibility(View.VISIBLE);
                        callOtherLeaveBalApi(OtherUserId);
                    } else {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        tv_applyleave_heading.setText("Offline");
                        ll_apply_leave_offline.setVisibility(View.VISIBLE);
                    }

                }
                break;

            case R.id.progress_applyleave_lay:

                break;

        }

    }

    /**
     * Method to call self leave bal api
     */
    void callSelfLeaveBalApi() {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(ApplyLeave.this, "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        Call<SelfLeaveBalPojo> call = restApis.getSelfLeaveInfo("bearer " + auth_token);
        call.enqueue(new Callback<SelfLeaveBalPojo>() {
            @Override
            public void onResponse(Call<SelfLeaveBalPojo> call, Response<SelfLeaveBalPojo> response) {
                if (response.raw().code() == 200) {

                    progress_applyleave_lay.setVisibility(View.GONE);
                    tv_applyleave_causallv.setText("" + response.body().getResponse().getLeaves().getCL());
                    tv_applyleave_earnedlv.setText("" + response.body().getResponse().getLeaves().getEL());
                    tv_applyleave_medicallv.setText("" + response.body().getResponse().getLeaves().getSL());

                    float totalLeave = response.body().getResponse().getLeaves().getCL() +
                            response.body().getResponse().getLeaves().getEL() +
                            response.body().getResponse().getLeaves().getSL();

                    String rounftotal = String.format("%.2f", totalLeave);
                    tv_applyleave_leavebal.setText("" + rounftotal);

                } else if (response.raw().code() == 401) {
                    progress_applyleave_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    logout();
                } else {
                    progress_applyleave_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();

                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    showCrouton(name);
                    return;
                }
            }

            @Override
            public void onFailure(Call<SelfLeaveBalPojo> call, Throwable t) {
                progress_applyleave_lay.setVisibility(View.GONE);
                //progress_applyleave_lay.setVisibility(View.GONE);
                tv_applyleave_heading.setText("Offline");
                ll_apply_leave_offline.setVisibility(View.VISIBLE);
                String msg = t.getMessage();
            }
        });
    }

    /**
     * Method to call apply self leave api
     */
    private void callSelfLeaveApplyApi() {

        ApplySelfLeaveRawData applySelfLeaveRawData = new ApplySelfLeaveRawData();
        applySelfLeaveRawData.leave_type = leaveType;
        applySelfLeaveRawData.from_date = formattedDateStart;
        applySelfLeaveRawData.to_date = formattedDateEnd;
        applySelfLeaveRawData.comment = comment;
        //HashMap hashMap1 = map;
        try {
            String auth_token = prefs.getPreferencesString(ApplyLeave.this, "AUTH_TOKEN");
            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
            String endpoint_url = "/api/v1/employee/applyleave";
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<ApplySelfLeavePojo> call = apiInterface.applySelfLeave(endpoint_url, applySelfLeaveRawData, "bearer " + auth_token);

            call.enqueue(new Callback<ApplySelfLeavePojo>() {
                @Override
                public void onResponse(Call<ApplySelfLeavePojo> call, Response<ApplySelfLeavePojo> response) {

                    if (response.raw().code() == 200) {
                        progress_applyleave_lay.setVisibility(View.GONE);


                        // Inflate any custom view
                        View customView = getLayoutInflater().inflate(R.layout.custom_crouton_layout, null);
                        TextView msg = (TextView) customView.findViewById(R.id.tv_crouton_msg);
                        msg.setText(response.body().getMsg());
                        Crouton.show(ApplyLeave.this, customView, rl_applylleavemain);
                        Toast.makeText(ApplyLeave.this, R.string.success_leave, Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();

                       /* if (From.equals("MYCAL")) {

                            Intent mycal_intent = new Intent(ApplyLeave.this, MainActivity.class);
                            mycal_intent.putExtra("FROM_APPLY_LEAVE",1);
                            mycal_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mycal_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mycal_intent);
                            finish();
                        }else
                        {
                            Intent intent = new Intent(ApplyLeave.this, OthersCalendar.class);
                            startActivity(intent);
                            finish();
                        }*/

                    } else if (response.raw().code() == 401) {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        logout();
                    } else {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                            // name = sys.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        showCrouton(name);
                        return;
                    }

                }

                @Override
                public void onFailure(Call<ApplySelfLeavePojo> call, Throwable t) {

                    String errorMsg = t.getMessage();
                    progress_applyleave_lay.setVisibility(View.GONE);
                    tv_applyleave_heading.setText("Offline");
                    ll_apply_leave_offline.setVisibility(View.VISIBLE);
                    return;
                }
            });


        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private void showCrouton(String name) {
        scrollView.smoothScrollTo(0, 0);
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(4500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(150)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        Crouton.showText(ApplyLeave.this, name, style, rl_applylleavemain);
    }


    /**
     * Method to call other leave bal api
     */
    void callOtherLeaveBalApi(int user_id) {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(ApplyLeave.this, "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        String endpoint_url = "/api/v1/userleaves/" + user_id;
        RestApis restApis = retrofit.create(RestApis.class);
        Call<SelfLeaveBalPojo> call = restApis.getOtherLeaveInfo(endpoint_url, "bearer " + auth_token);
        call.enqueue(new Callback<SelfLeaveBalPojo>() {
            @Override
            public void onResponse(Call<SelfLeaveBalPojo> call, Response<SelfLeaveBalPojo> response) {
                if (response.raw().code() == 200) {

                    progress_applyleave_lay.setVisibility(View.GONE);
                    tv_applyleave_causallv.setText("" + response.body().getResponse().getLeaves().getCL());
                    tv_applyleave_earnedlv.setText("" + response.body().getResponse().getLeaves().getEL());
                    tv_applyleave_medicallv.setText("" + response.body().getResponse().getLeaves().getSL());

                    Float totalLeave = response.body().getResponse().getLeaves().getCL() +
                            response.body().getResponse().getLeaves().getEL() +
                            response.body().getResponse().getLeaves().getSL();

                    tv_applyleave_leavebal.setText("" + totalLeave);


                } else if (response.raw().code() == 401) {
                    progress_applyleave_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    logout();
                } else {
                    progress_applyleave_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();

                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    showCrouton(name);
                    return;
                }
            }

            @Override
            public void onFailure(Call<SelfLeaveBalPojo> call, Throwable t) {
                progress_applyleave_lay.setVisibility(View.GONE);
                tv_applyleave_heading.setText("Offline");
                ll_apply_leave_offline.setVisibility(View.VISIBLE);
                String msg = t.getMessage();
            }
        });
    }


    /**
     * Method to call apply other leave api
     */
    private void callOtherLeaveApplyApi(int id) {

        ApplySelfLeaveRawData applySelfLeaveRawData = new ApplySelfLeaveRawData();
        applySelfLeaveRawData.leave_type = leaveType;
        applySelfLeaveRawData.from_date = formattedDateStart;
        applySelfLeaveRawData.to_date = formattedDateEnd;
        applySelfLeaveRawData.comment = comment;
        //HashMap hashMap1 = map;
        try {

            String auth_token = prefs.getPreferencesString(ApplyLeave.this, "AUTH_TOKEN");
            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
            String endpoint_url = "/api/v1/employee/applyleave/" + id;
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<ApplySelfLeavePojo> call = apiInterface.applySelfLeave(endpoint_url, applySelfLeaveRawData, "bearer " + auth_token);

            call.enqueue(new Callback<ApplySelfLeavePojo>() {
                @Override
                public void onResponse(Call<ApplySelfLeavePojo> call, Response<ApplySelfLeavePojo> response) {

                    if (response.raw().code() == 200) {

                        progress_applyleave_lay.setVisibility(View.GONE);
                        View customView = getLayoutInflater().inflate(R.layout.custom_crouton_layout, null);
                        TextView msg = (TextView) customView.findViewById(R.id.tv_crouton_msg);
                        msg.setText(response.body().getMsg());
                        Crouton.show(ApplyLeave.this, customView, rl_applylleavemain);

                        /*if (From.equals("MYCAL")) {

                            Intent mycal_intent = new Intent(ApplyLeave.this, MainActivity.class);
                            mycal_intent.putExtra("FROM_APPLY_LEAVE",1);
                            startActivity(mycal_intent);
                            finish();
                        }else
                        {
                            Intent intent = new Intent(ApplyLeave.this, OthersCalendar.class);
                            startActivity(intent);
                            finish();
                        }
*/
                        //ll_apply_leave_applysuccess.setVisibility(View.VISIBLE);
                        //   tv_applyleave_successmsg.setText(response.body().getMsg());
                        //  tv_applyleave_heading.setText("");
                        // rel_applyleavecard_cross.setVisibility(View.GONE);
                        // finish();
                    } else if (response.raw().code() == 401) {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        logout();
                    } else {
                        progress_applyleave_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                            // name = sys.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        showCrouton(name);
                        //return;
                    }
                }

                @Override
                public void onFailure(Call<ApplySelfLeavePojo> call, Throwable t) {
                    String errorMsg = t.getMessage();
                    progress_applyleave_lay.setVisibility(View.GONE);
                    tv_applyleave_heading.setText("Offline");
                    ll_apply_leave_offline.setVisibility(View.VISIBLE);
                    // return;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void logout() {
        prefs.clearAppData(ApplyLeave.this);
        DbHelper dbHelper = DbHelper.getInstance(ApplyLeave.this);
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(ApplyLeave.this, Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
