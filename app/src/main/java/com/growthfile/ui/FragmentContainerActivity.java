package com.growthfile.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.growthfile.R;
import com.growthfile.fragment.ChangeCostCenterFragment;
import com.growthfile.fragment.ChangeDepartmentFragment;
import com.growthfile.fragment.ChangeDesignationFragment;
import com.growthfile.fragment.ChangeNumberFragment;
import com.growthfile.fragment.ChangeReportingManagerFragment;
import com.growthfile.fragment.CreateRoleFragment;
import com.growthfile.fragment.EditProfileFragment;
import com.growthfile.fragment.AssignRoleFragment;
import com.growthfile.fragment.EditRoleFragment;
import com.growthfile.fragment.GiveSmileyFragment;
import com.growthfile.fragment.GiveStarFragment;
import com.growthfile.fragment.ResignCalenderFragment;
import com.growthfile.fragment.SubmitResignFragment;
import com.growthfile.fragment.SelectCostCenterFragment;
import com.growthfile.fragment.SelectDepartmentFragment;
import com.growthfile.fragment.SelectOfficeFragment;
import com.growthfile.fragment.TransferFragment;
import com.growthfile.v2.fragments.GrowthFileFragmentV2;
import com.growthfile.v2.fragments.SearchEmployeeFragment;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nitin on 30/10/15.
 */
public class FragmentContainerActivity extends BaseActivity {

    public static void startActivity(Context ctx, int fragmentType, Bundle extraBundle) {
        Intent i = new Intent(ctx, FragmentContainerActivity.class);
        if (extraBundle != null) {
            i.putExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE, extraBundle);
        }
        i.putExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        ctx.startActivity(i);
    }

    public static void startActivityForResult(Context ctx, int fragmentType, Bundle extraBundle, int requestCode, Fragment fragment) {
        Intent i = new Intent(ctx, FragmentContainerActivity.class);
        if (extraBundle != null) {
            i.putExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE, extraBundle);
        }
        i.putExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        fragment.startActivityForResult(i, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, getFragment(getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE))).commit();
    }

    private Fragment getFragment(int fragmentType) {
        Fragment fragment = null;
        switch (fragmentType) {
            case AppConstants.FRAGMENT_TYPE.EDIT_PROFILE:
                fragment = new EditProfileFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_MOBILE_NUMBER:
                fragment = new ChangeNumberFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_COST_CENTER:
                fragment = new ChangeCostCenterFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_DEPARTMENT:
                fragment = new ChangeDepartmentFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_DESIGNATION:
                fragment = new ChangeDesignationFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_REPORTING_MANAGER:
                fragment = new ChangeReportingManagerFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.RESIGN:
                fragment = new ResignCalenderFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.TRANSFER:
                fragment = new TransferFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.GIVE_SMILEY:
                fragment = new GiveSmileyFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.GIVE_STAR:
                fragment = new GiveStarFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CREATE_ROLE:
                fragment = new CreateRoleFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.ASSIGN_ROLE:
                fragment = new AssignRoleFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.SELECT_CENTER:
                fragment = new SelectOfficeFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.SELECT_COST_CENTER:
                fragment = new SelectCostCenterFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.SELECT_DEPARTMENT:
                fragment = new SelectDepartmentFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CHANGE_EDIT_ROLE:
                fragment = new EditRoleFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.SUBMIT_RESIGN_FRAGMENT:
                fragment = new SubmitResignFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.OPEN_EMPLOYEE_PROFILE:
                fragment = new GrowthFileFragmentV2();
                break;
            case AppConstants.FRAGMENT_TYPE.SEARCH_EMPLOYEE:
                fragment = new SearchEmployeeFragment();
                break;
        }
        if(fragment!=null){
            fragment.setArguments(getIntent().getBundleExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE));
        }
        return fragment;
    }

}
