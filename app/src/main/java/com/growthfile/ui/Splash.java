package com.growthfile.ui;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growthfile.R;
import com.growthfile.utility.Helper;
import simplifii.framework.utility.Prefs;
import com.growthfile.customUi.TextViewWorkSansRegular;
import com.growthfile.database.DbHelper;

public class Splash extends AppCompatActivity {

    private RelativeLayout rl_tc,rl_splash_bottom;
    private DbHelper dbHelper;
    private TextViewWorkSansRegular tv_growthfile_tc;
    Prefs prefs;
    LinearLayout liner_splash_terms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);


        tv_growthfile_tc= (TextViewWorkSansRegular) findViewById(R.id.tv_growthfile_tc);
        rl_splash_bottom=(RelativeLayout) findViewById(R.id.rl_splash_bottom);
        liner_splash_terms=(LinearLayout) findViewById(R.id.liner_splash_terms);

      /*  if(prefs.getPreferencesString(Splash.this,"Verified").equals("") || prefs.getPreferencesString(Splash.this,"Verified").equals(null)) {
            rl_splash_bottom.setVisibility(View.INVISIBLE);
            liner_splash_terms.setVisibility(View.INVISIBLE);
        }else{

            rl_splash_bottom.setVisibility(View.VISIBLE);
            liner_splash_terms.setVisibility(View.VISIBLE);
        }*/
        prefs= new Prefs();

        String text = "<font color=#cccccc>Growthfile</font> <font color=#5ab2e0> Terms of Service and Privacy Policy</font>";
        tv_growthfile_tc.setText(Html.fromHtml(text));
        tv_growthfile_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                    startActivity(browserIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(Splash.this, "No application can handle this request."
                            + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }*/

                if (Helper.checkInternetConnection(Splash.this)) {

                    Intent webIntent=new Intent(Splash.this,TcWebView.class);
                    startActivity(webIntent);
                } else {
                    Intent offlineIntent = new Intent(Splash.this, OfflineError.class);
                    startActivity(offlineIntent);
                }

            }
        });

        dbHelper=DbHelper.getInstance(getApplicationContext());
        Boolean isDBCreated = prefs.getPreferencesBoolean(getApplicationContext(), "DB_CREATED");
        if (isDBCreated) {
            dbHelper.createDb();
        } else {
            dbHelper.copyRawDataBase();
        }




        rl_tc= (RelativeLayout) findViewById(R.id.rl_tc);
        rl_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(prefs.getPreferencesString(Splash.this,"Verified").equals("") || prefs.getPreferencesString(Splash.this,"Verified").equals(null)) {
                    Intent intent = new Intent(Splash.this, Login.class);
                    startActivity(intent);
                    finish();
                }else{

                    String pinUser = prefs.getPreferencesString(Splash.this, "GfilePIN");
                    if (pinUser.equals("")) {
                        Intent intent = new Intent(Splash.this, FirstTimePinSetup.class);
                        startActivity(intent);
                        finish();
                    }else
                    {
                        Intent intent = new Intent(Splash.this, PinSetup.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });


    }


}
