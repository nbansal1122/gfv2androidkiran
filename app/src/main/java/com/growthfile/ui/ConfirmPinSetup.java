package com.growthfile.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growthfile.Pojo.OtpVerifyUserProfilePojo;
import com.growthfile.R;
import com.growthfile.data.PunchLogData;
import com.growthfile.database.DbHelper;
import com.growthfile.utility.LocationFinder;
import com.growthfile.utility.LocationFinderCallback;

import me.philio.pinentry.PinEntryView;
import simplifii.framework.utility.Prefs;

import com.google.gson.Gson;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ConfirmPinSetup extends AppCompatActivity implements View.OnClickListener, LocationFinderCallback, ActivityCompat.OnRequestPermissionsResultCallback {


    // EditText et_pin_1, et_pin_2, et_pin_3, et_pin_4;
    ImageView iv_confirmverify_otp;
    TextView txt_otpconfirm_name;
    //  String[] otp_code = new String[4];
    String[] otp_code_confirm = new String[4];
    private LinearLayout ll_confirm_pinsetup_root;
    String activationCode = "", composed_otpConfirm;
    View.OnKeyListener keyListener;
    private LocationFinder locationFinder;
    private OtpVerifyUserProfilePojo userProfilePojo;
    Prefs prefs;

    private int REQUEST_LOCATION = 1;
    private String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    private PinEntryView pinEntryView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pinsetup);

        //check location and sms permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        }
        prefs = new Prefs();
        locationFinder = new LocationFinder(this);
        userProfilePojo = getLoginDataFromPref();
        initUi();

        for (int x = 0; x < 4; x++) {
            //  otp_code[x] = "";
            otp_code_confirm[x] = "";
        }

        InputMethodManager imm = (InputMethodManager)
                ConfirmPinSetup.this.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {

                if (isOpen) {

                    ll_confirm_pinsetup_root.scrollTo(0, ll_confirm_pinsetup_root.getHeight() / 4);

                } else {
                    ll_confirm_pinsetup_root.scrollTo(0, 0);
                }
            }
        });
        findViewById(R.id.tv_start_again).setOnClickListener(this);
    }

    /**
     * Method to initialize ui component
     */
    private void initUi() {

        ll_confirm_pinsetup_root = (LinearLayout) findViewById(R.id.ll_confirm_pinsetup_root);
       /* et_pin_1 = (EditText) findViewById(R.id.et_pin_1);
        et_pin_2 = (EditText) findViewById(R.id.et_pin_2);
        et_pin_3 = (EditText) findViewById(R.id.et_pin_3);
        et_pin_4 = (EditText) findViewById(R.id.et_pin_4);
      */  //  et_pin_5 = (EditText) findViewById(R.id.et_pin_5);

        // et_conpin_5 = (EditText) findViewById(R.id.et_conpin_5);

        iv_confirmverify_otp = (ImageView) findViewById(R.id.iv_confirmverify_otp);
        iv_confirmverify_otp.setOnClickListener(this);

        txt_otpconfirm_name = (TextView) findViewById(R.id.txt_otpconfirm_name);
        txt_otpconfirm_name.setText("Welcome " + userProfilePojo.getName());

        pinEntryView = (PinEntryView) findViewById(R.id.pinView);

        ///////////////////confirm otp///////


    }


    /**
     * Method to check activation code length is matching or not
     */
    void checkActivationCode() {

        int count = 0;
        for (int i = 0; i < 4; i++) {
            if (!otp_code_confirm[i].isEmpty()) {
                count = count + 1;
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_confirmverify_otp:
                //composed_otp = composePinCode();
                composed_otpConfirm = pinEntryView.getText().toString();
                if (composed_otpConfirm.toString().trim().length() == 4) {

                    String pinUser = prefs.getPreferencesString(ConfirmPinSetup.this, "GfilePIN");
                    if (composed_otpConfirm.equals(pinUser)) {

                        getAndSaveAllLogData();
                        prefs.setPreferencesString(ConfirmPinSetup.this, "GfilePIN", pinUser);
                        Intent intent = new Intent(ConfirmPinSetup.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {

                        Configuration croutonConfiguration = new Configuration.Builder()
                                .setDuration(4500).build();
                        // Define custom styles for crouton
                        Style style = new Style.Builder()
                                .setBackgroundColorValue(Color.parseColor("#80000000"))
                                .setGravity(Gravity.CENTER_HORIZONTAL)
                                .setConfiguration(croutonConfiguration)
                                .setHeight(150)
                                .setTextColorValue(Color.parseColor("#ffffff")).build();
                        // Display notice with custom style and configuration
                        Crouton.showText(ConfirmPinSetup.this, getText(R.string.incorrect_pin), style);
                        //  Toast.makeText(FirstTimePinSetup.this,getText(R.string.incorrect_pin), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(4500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(150)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(ConfirmPinSetup.this, getText(R.string.pin_count_error), style);
                    //  Toast.makeText(FirstTimePinSetup.this, getText(R.string.otp_count_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            case R.id.tv_start_again:
                startAgain();
                break;
        }
    }

    private void startAgain() {
        startActivity(new Intent(this, FirstTimePinSetup.class));
        finish();
    }

    /**
     * Method to compose activation code from stored code in array
     *
     * @return activation code
     */
    /*private String composePinCode() {
        String code = "";

        for (int j = 0; j < 4; j++) {
            code = code + otp_code[j];
        }
        return code;
    }*/


    /**
     * Method to compose confirm activation code from stored code in array
     *
     * @return activation code
     */
    private String composePinCodeConfirm() {
        String code = "";

        for (int j = 0; j < 4; j++) {
            code = code + otp_code_confirm[j];
        }
        return code;
    }


    void getAndSaveAllLogData() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        //final String utcTime = dateFormat.format(new Date());

        System.out.println("DATE--------" + dateFormat.format(cal.getTime()) + "+00:00");
        String time_stamp = dateFormat.format(cal.getTime()) + "+00:00";

        // String time_stamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        double c_lat = locationFinder.getLatitude();
        double c_lon = locationFinder.getLongitude();

        if (c_lat == 0.0 || c_lon == 0.0) {

        } else {
            PunchLogData punchLogData = new PunchLogData();
            punchLogData.current_lat = c_lat;
            punchLogData.current_lon = c_lon;
            punchLogData.is_attendence = 0;
            punchLogData.sync_status = 0;
            punchLogData.timestamp = time_stamp;

            DbHelper.getInstance(ConfirmPinSetup.this).insertPunchLog(punchLogData);
            // DbHelper.getInstance(FirstTimePinSetup.this).copyDBToPhoneSD1();
        }
    }

    @Override
    public void locationFound() {

    }

    /**
     * Fetch Store and User data from SharedPrefrences
     *
     * @return
     */
    OtpVerifyUserProfilePojo getLoginDataFromPref() {
        Gson gson = new Gson();
        String json = prefs.getPreferencesString(ConfirmPinSetup.this, "USER_PROFILE");
        OtpVerifyUserProfilePojo obj = gson.fromJson(json, OtpVerifyUserProfilePojo.class);

        return obj;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    Configuration croutonConfiguration = new Configuration.Builder()
                            .setDuration(4500).build();
                    // Define custom styles for crouton
                    Style style = new Style.Builder()
                            .setBackgroundColorValue(Color.parseColor("#80000000"))
                            .setGravity(Gravity.CENTER_HORIZONTAL)
                            .setConfiguration(croutonConfiguration)
                            .setHeight(150)
                            .setTextColorValue(Color.parseColor("#ffffff")).build();
                    // Display notice with custom style and configuration
                    Crouton.showText(ConfirmPinSetup.this, getText(R.string.location_permission_request), style);
                    // Toast.makeText(FirstTimePinSetup.this,getText(R.string.location_permission_request), Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    /**
     * method to request location permission
     */
    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
    }
}
