package com.growthfile.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.growthfile.Pojo.ApplySelfLeavePojo;
import com.growthfile.Pojo.SelfCalendarModel;
import com.growthfile.Pojo.SelfCalendarResponse;
import com.growthfile.R;
import com.growthfile.adapter.CalendarInfoListAdapter;
import com.growthfile.database.DbHelper;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import simplifii.framework.utility.Prefs;
import com.growthfile.webservices.ApplySelfLeaveRawData;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OthersCalendar extends AppCompatActivity implements View.OnClickListener {
    private CaldroidFragment caldroidFragment;
    private Date dateFrom, dateTo, current_date;
    private String  user_name;
    private List<String> selectedDate = new ArrayList<>();
    private List<Date> selectedDateList = new ArrayList<>();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
    private RelativeLayout rl_oc_apply_for_leave, rl_oc_selected_date_events, rl_oc_selected_date_info,
            rl_oc_two_btn_container, rl_oc_back, rl_oc_two_btn_mark_present,
            rl_oc_two_btn_apply_leave,rl_oc_info,rl_oc_symbol_info,progress_othercal_lay;
    private ImageView iv_punchin_oc, iv_other_calendar_info_check,iv_oc_symbol_info,othercal_retry;
    private TextView tv_othercal_successmsg,tv_oc_selected_date, tv_oc_date_corresponding_info_second, tv_oc_date_corresponding_info_first,
            tv_calendar_header,txt_oc_selected_date;
    private RecyclerView rv_oc_info;
    private View side_border_oc_event;
    private Prefs prefs;
    private List<String> datesFromApi, appliedBy, approvedBy;
    private SelfCalendarResponse calendarResponse;
    private SelfCalendarModel selfCalendarModel;
    private HashMap<String, String> colorcode = new HashMap<>();
    private LinearLayout ll_oc_offline, ll_oc_retry,ll_othercalander_applysuccess;
    ImageView img_othercal_scuess_done;
    Retrofit retroFitMaker;
    private int user_id;
    int[] month_day = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int current_Month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_calendar);

        prefs = new Prefs();
        user_name= prefs.getPreferencesString(OthersCalendar.this,"Othr_USER_NAME");
        user_id=prefs.getPreferencesInt(OthersCalendar.this,"Othr_USER_ID");

        datesFromApi = new ArrayList<>();
        approvedBy = new ArrayList<>();
        appliedBy = new ArrayList<>();
        initUi();

        initializeCalendar();
    }


    /**
     * Method to initialize all ui component of calendar fragment
     */
    void initUi() {
        othercal_retry=(ImageView)findViewById(R.id.othercal_retry);
        ll_othercalander_applysuccess=(LinearLayout)findViewById(R.id.ll_othercalander_applysuccess);
        img_othercal_scuess_done=(ImageView)findViewById(R.id.img_othercal_scuess_done);
        img_othercal_scuess_done.setOnClickListener(this);

        progress_othercal_lay=(RelativeLayout)findViewById(R.id.progress_othercal_lay);
        tv_othercal_successmsg=(TextView) findViewById(R.id.tv_othercal_successmsg);
        rv_oc_info= (RecyclerView) findViewById(R.id.rv_oc_info);
        rv_oc_info.setLayoutManager(new LinearLayoutManager(OthersCalendar.this,LinearLayoutManager.VERTICAL,false));
        rv_oc_info.setAdapter(new CalendarInfoListAdapter(OthersCalendar.this));

      //  progressDialog = new ProgressDialog(this);

        ll_oc_offline = (LinearLayout) findViewById(R.id.ll_oc_offline);
        ll_oc_retry = (LinearLayout) findViewById(R.id.ll_oc_retry);
        rl_oc_two_btn_mark_present = (RelativeLayout) findViewById(R.id.rl_oc_two_btn_mark_present);
        rl_oc_two_btn_apply_leave = (RelativeLayout) findViewById(R.id.rl_oc_two_btn_apply_leave);
        rl_oc_back = (RelativeLayout) findViewById(R.id.rl_oc_back);
        rl_oc_info= (RelativeLayout) findViewById(R.id.rl_oc_info);
        rl_oc_apply_for_leave = (RelativeLayout) findViewById(R.id.rl_oc_apply_for_leave);
        rl_oc_two_btn_container = (RelativeLayout) findViewById(R.id.rl_oc_two_btn_container);
        rl_oc_selected_date_events = (RelativeLayout) findViewById(R.id.rl_oc_selected_date_events);
        rl_oc_selected_date_info = (RelativeLayout) findViewById(R.id.rl_oc_selected_date_info);
        iv_punchin_oc = (ImageView) findViewById(R.id.iv_punchin_oc);
        rl_oc_symbol_info= (RelativeLayout) findViewById(R.id.rl_oc_symbol_info);
        iv_other_calendar_info_check = (ImageView) findViewById(R.id.iv_other_calendar_info_check);
        tv_calendar_header = (TextView) findViewById(R.id.tv_calendar_header);
        txt_oc_selected_date= (TextView) findViewById(R.id.txt_oc_selected_date);
        tv_calendar_header.setText(user_name + "'s calendar");
        tv_oc_selected_date = (TextView) findViewById(R.id.tv_oc_selected_date);
        tv_oc_date_corresponding_info_second = (TextView) findViewById(R.id.tv_oc_date_corresponding_info_second);
        tv_oc_date_corresponding_info_first = (TextView) findViewById(R.id.tv_oc_date_corresponding_info_first);
        side_border_oc_event = findViewById(R.id.side_border_oc_event);
        rl_oc_symbol_info.setOnClickListener(this);
        rl_oc_back.setOnClickListener(this);
        ll_oc_retry.setOnClickListener(this);
        rl_oc_two_btn_mark_present.setOnClickListener(this);
        rl_oc_two_btn_apply_leave.setOnClickListener(this);
        iv_punchin_oc.setOnClickListener(this);
        rl_oc_apply_for_leave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_oc_apply_for_leave:
                String dateStart, dateEnd;
                String formattedDateStart, formattedDateEnd;

                if (dateTo == null) {
                    dateStart = getTextForSingleDate(dateFrom);
                    dateEnd = getTextForSingleDate(dateFrom);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStart);
                        date2 = originalFormat.parse(dateEnd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStart = targetFormat.format(date1);
                    formattedDateEnd = targetFormat.format(date2);

                } else {

                    dateStart = getTextForSingleDate(dateFrom);
                    dateEnd = getTextForSingleDate(dateTo);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStart);
                        date2 = originalFormat.parse(dateEnd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStart = targetFormat.format(date1);
                    formattedDateEnd = targetFormat.format(date2);
                }

                Intent applyLeaveIntent = new Intent(OthersCalendar.this, ApplyLeave.class);
                applyLeaveIntent.putExtra("formattedDateStart", formattedDateStart);
                applyLeaveIntent.putExtra("formattedDateEnd", formattedDateEnd);
                applyLeaveIntent.putExtra("FROM", "OtherCAL");
                applyLeaveIntent.putExtra("UserId", user_id);
                startActivity(applyLeaveIntent);
                finish();
                break;
            case R.id.iv_punchin_oc:
                Intent punchIntent = new Intent(OthersCalendar.this, PunchInOutActivity.class);
                startActivity(punchIntent);
                break;
            case R.id.rl_oc_back:
                finish();

                break;
            case R.id.rl_oc_two_btn_apply_leave:

                String dateStartnxt, dateEndnxt;
                String formattedDateStartnxt, formattedDateEndnxt;

                if (dateTo == null) {
                    dateStartnxt = getTextForSingleDate(dateFrom);
                    dateEndnxt = getTextForSingleDate(dateFrom);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartnxt);
                        date2 = originalFormat.parse(dateEndnxt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartnxt = targetFormat.format(date1);
                    formattedDateEndnxt = targetFormat.format(date2);

                } else {

                    dateStartnxt = getTextForSingleDate(dateFrom);
                    dateEndnxt = getTextForSingleDate(dateTo);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartnxt);
                        date2 = originalFormat.parse(dateEndnxt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartnxt = targetFormat.format(date1);
                    formattedDateEndnxt = targetFormat.format(date2);
                }

                Intent applyLeaveIntent1 = new Intent(OthersCalendar.this, ApplyLeave.class);
                applyLeaveIntent1.putExtra("formattedDateStart", formattedDateStartnxt);
                applyLeaveIntent1.putExtra("formattedDateEnd", formattedDateEndnxt);
                applyLeaveIntent1.putExtra("FROM", "OtherCAL");
                applyLeaveIntent1.putExtra("UserId", user_id);
                startActivity(applyLeaveIntent1);
                finish();
                break;
            case R.id.rl_oc_two_btn_mark_present:
                String dateStartMP, dateEndMP;
                String formattedDateStartMP = "", formattedDateEndMP = "";
                if (dateTo == null) {
                    dateStart = getTextForSingleDate(dateFrom);
                    dateEnd = getTextForSingleDate(dateFrom);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStart);
                        date2 = originalFormat.parse(dateEnd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartMP = targetFormat.format(date1);
                    formattedDateEndMP = targetFormat.format(date2);

                } else {
                    dateStartMP = getTextForSingleDate(dateFrom);
                    dateEndMP = getTextForSingleDate(dateTo);

                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat originalFormat = new SimpleDateFormat("dd MMM,yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = originalFormat.parse(dateStartMP);
                        date2 = originalFormat.parse(dateEndMP);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    formattedDateStartMP = targetFormat.format(date1);
                    formattedDateEndMP = targetFormat.format(date2);
                }
                if (Helper.checkInternetConnection(this)) {
                    progress_othercal_lay.setVisibility(View.VISIBLE);
                    ll_oc_offline.setVisibility(View.GONE);
                    tv_calendar_header.setText(user_name + "'s calendar");
                    callOtherLeaveApplyApi(formattedDateStartMP, formattedDateEndMP, user_id);


                } else {
                    progress_othercal_lay.setVisibility(View.GONE);
                    ll_oc_offline.setVisibility(View.VISIBLE);
                    tv_calendar_header.setText("Offline");
                }

                break;

            case R.id.ll_oc_retry:



                final Animation a = AnimationUtils.loadAnimation(OthersCalendar.this,
                        R.anim.slide_in);
                a.setDuration(1000);
                othercal_retry.startAnimation(a);
                if (Helper.checkInternetConnection(OthersCalendar.this)) {
                    ll_oc_offline.setVisibility(View.GONE);
                    tv_calendar_header.setText(user_name + "'s calendar");
                    caldroidFragment.nextMonth();
                    caldroidFragment.prevMonth();
                } else {
                    tv_calendar_header.setText("Offline");
                    ll_oc_offline.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.rl_oc_symbol_info:
                if (rl_oc_info.getVisibility()==View.INVISIBLE){
                    rl_oc_info.setVisibility(View.VISIBLE);
                }else {
                    rl_oc_info.setVisibility(View.INVISIBLE);
                }
                break;

            case R.id.img_othercal_scuess_done:
                ll_othercalander_applysuccess.setVisibility(View.GONE);
                rl_oc_symbol_info.setVisibility(View.VISIBLE);
                rl_oc_back.setVisibility(View.VISIBLE);
                tv_calendar_header.setText(user_name + "'s calendar");
                initializeCalendar();
                dateTo = null;
                dateFrom = null;
                selectedDateList.clear();
                selectedDate.clear();
                handleDateSelection(6);

                break;

            case R.id.progress_othercal_lay:
                break;

        }
    }

    /**
     * Method to set text color for current date
     *
     * @param isColored
     */
    void setCurrentDateTextColor(boolean isColored) {

        Date date = new Date();
        dateFrom = date;
        caldroidFragment.refreshView();
        if (isColored) {
            caldroidFragment.setTextColorForDate(R.color.colorAccent, date);
        } else {
            caldroidFragment.setTextColorForDate(R.color.black, date);
        }

        caldroidFragment.refreshView();
    }


    /**
     * Method to handle buttons visibility according to dates selection
     *
     * @param type
     */
    void handleDateSelection(int type) {
        switch (type) {
            case 1:
                tv_oc_selected_date.setText(getTextForSingleDate(dateFrom));
                txt_oc_selected_date.setText("Selected Date");
                iv_punchin_oc.setVisibility(View.GONE);
                rl_oc_selected_date_events.setVisibility(View.VISIBLE);
                rl_oc_apply_for_leave.setVisibility(View.GONE);
                rl_oc_selected_date_info.setVisibility(View.VISIBLE);
                rl_oc_two_btn_container.setVisibility(View.VISIBLE);

                break;
            case 2:
                tv_oc_selected_date.setText(getTextForSingleDate(dateFrom));
                txt_oc_selected_date.setText("Selected Date");
                // iv_punchin_oc.setVisibility(View.VISIBLE);
                rl_oc_selected_date_events.setVisibility(View.VISIBLE);
                rl_oc_apply_for_leave.setVisibility(View.VISIBLE);
                rl_oc_selected_date_info.setVisibility(View.VISIBLE);
                rl_oc_two_btn_container.setVisibility(View.GONE);
                break;
            case 3:
                tv_oc_selected_date.setText(getTextForSingleDate(dateFrom));
                txt_oc_selected_date.setText("Selected Date");
                iv_punchin_oc.setVisibility(View.GONE);
                rl_oc_selected_date_events.setVisibility(View.VISIBLE);
                rl_oc_apply_for_leave.setVisibility(View.VISIBLE);
                rl_oc_selected_date_info.setVisibility(View.VISIBLE);
                rl_oc_two_btn_container.setVisibility(View.GONE);
                break;
            case 4:

                String compStart=getTextForSingleDate(dateFrom);
                String compEnd= getTextForSingleDate(dateTo);
                int compareDtae=compStart.compareTo(compEnd);
                System.out.println(compareDtae);
                if(compareDtae>=1){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateTo) + " to " + getTextForSingleDate(dateFrom));
                }else if(compareDtae<0){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }else if(compareDtae==0){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }
               // tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                txt_oc_selected_date.setText("Selected Dates");
                iv_punchin_oc.setVisibility(View.GONE);
                rl_oc_selected_date_events.setVisibility(View.GONE);
                rl_oc_apply_for_leave.setVisibility(View.GONE);
                rl_oc_selected_date_info.setVisibility(View.VISIBLE);
                rl_oc_two_btn_container.setVisibility(View.VISIBLE);
                break;
            case 5:
                String compStart1=getTextForSingleDate(dateFrom);
                String compEnd1= getTextForSingleDate(dateTo);
                int compareDtae1=compStart1.compareTo(compEnd1);
                System.out.println(compareDtae1);
                if(compareDtae1>=1){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateTo) + " to " + getTextForSingleDate(dateFrom));
                }else if(compareDtae1<0){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }else if(compareDtae1==0){
                    tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                }
               // tv_oc_selected_date.setText(getTextForSingleDate(dateFrom) + " to " + getTextForSingleDate(dateTo));
                txt_oc_selected_date.setText("Selected Dates");
                iv_punchin_oc.setVisibility(View.GONE);
                rl_oc_selected_date_events.setVisibility(View.GONE);
                rl_oc_apply_for_leave.setVisibility(View.VISIBLE);
                rl_oc_selected_date_info.setVisibility(View.VISIBLE);
                rl_oc_two_btn_container.setVisibility(View.GONE);
                break;
            case 6:
                tv_oc_selected_date.setText("");
                iv_punchin_oc.setVisibility(View.GONE);
                rl_oc_selected_date_events.setVisibility(View.GONE);
                rl_oc_apply_for_leave.setVisibility(View.GONE);
                rl_oc_selected_date_info.setVisibility(View.GONE);
                rl_oc_two_btn_container.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * method to clear background of dates
     */
    void clearAllSelectedDates() {
        caldroidFragment.clearSelectedDates();
        caldroidFragment.clearBackgroundDrawableForDates(selectedDateList);
        caldroidFragment.clearTextColorForDates(selectedDateList);
        caldroidFragment.refreshView();
        selectedDate.clear();
        selectedDateList.clear();

        dateFrom=null;
        dateTo=null;
        setCurrentDateTextColor(true);
        caldroidFragment.refreshView();

        setDefaultColors();

    }

    /**
     * method to set all default color after reselection of dates
     */
    void setDefaultColors() {
        for (int i = 0; i < calendarResponse.getData().getCalendar().size(); i++) {
            changeBgOfDate(calendarResponse.getData().getCalendar().get(i).getDate(), calendarResponse.getData().getCalendar().get(i).getType());
        }
        caldroidFragment.refreshView();
    }

    /**
     * method to change color of particular date
     *
     * @param date_to_color
     * @param leave_type
     */
    void changeBgOfDate(String date_to_color, String leave_type) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date finaldate = null;
        try {
            finaldate = format.parse(date_to_color);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(getImageByLeaveType(leave_type)), finaldate);

    }

    /**
     * method to get background image for date
     *
     * @param leave_type
     * @return
     */
    int getImageByLeaveType(String leave_type) {
        int bg = 0;
        switch (leave_type) {

            case "AM":
                bg = R.drawable.green_border;
                break;
            case "PM":
                bg = R.drawable.green_border;
                break;
            case "EL":
                bg = R.drawable.red_border;
                break;
            case "SL":
                bg = R.drawable.red_border;
                break;
            case "CL":
                bg = R.drawable.red_border;
                break;
            case "OL":
                bg = R.drawable.red_border;
                break;
            case "LWP":
                bg = R.drawable.red_border;
                break;
            case "HOLIDAY":
                bg = R.drawable.orange_border;
                break;
            case "WEEKOFF":
                bg = R.drawable.grey_border;
                break;
            default:
                bg = R.color.caldroid_gray;
                ;
                break;

        }
        return bg;
    }

    /**
     * get text to show selected date
     *
     * @param date
     * @return
     */
    String getTextForSingleDate(Date date) {


        Calendar c = Calendar.getInstance();

        c.setTime(date);
        int day = c.get(Calendar.DATE);
        int year = c.get(Calendar.YEAR);
        String month_name = new DateFormatSymbols().getMonths()[c.get(Calendar.MONTH)].substring(0, 3);

        return day + " " + month_name + ", " + year;
    }

    /**
     * method to set background of dates when multiple date selected
     */
    void setSelectedDateBackground() {
        List<Date> allSelectedDates = new ArrayList<>();
        if (dateTo.compareTo(dateFrom) > 0) {
            allSelectedDates = getDaysBetweenDates(dateFrom, dateTo);
        } else {
            allSelectedDates = getDaysBetweenDates(dateTo, dateFrom);
        }


        HashMap<Date, Drawable> selectedDateData = new HashMap<>();
        for (int i = 0; i < allSelectedDates.size(); i++) {


            selectedDateData.put(allSelectedDates.get(i), getResources().getDrawable(R.drawable.date_selected_bg));

        }
        HashMap<Date, Integer> selectedDateDataForTextBg = new HashMap<>();
        for (int i = 0; i < allSelectedDates.size(); i++) {
            selectedDateDataForTextBg.put(allSelectedDates.get(i), R.color.white);
        }
        caldroidFragment.setSelectedDates(dateFrom, dateTo);
        caldroidFragment.setBackgroundDrawableForDates(selectedDateData);
        caldroidFragment.setTextColorForDates(selectedDateDataForTextBg);

        if (!allSelectedDates.contains(new Date())) {
            caldroidFragment.setTextColorForDate(R.color.colorAccent, new Date());
        } else {
            caldroidFragment.setTextColorForDate(R.color.white, new Date());
        }
        caldroidFragment.refreshView();

    }

    /**
     * Method to get date between two date
     *
     * @param startdate
     * @param enddate
     * @return
     */
    public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>(25);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startdate);
        while (cal.getTime().before(enddate)) {
            cal.add(Calendar.DATE, 1);
            dates.add(cal.getTime());


            Calendar newCal = Calendar.getInstance();
            newCal.setTime(cal.getTime());
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;

            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }


        }
        if (!dates.contains(startdate)) {
            dates.add(startdate);
            Calendar newCal = Calendar.getInstance();
            newCal.setTime(startdate);
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;
            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }
        }

        if (!dates.contains(enddate)) {
            dates.add(enddate);
            Calendar newCal = Calendar.getInstance();
            newCal.setTime(enddate);
            int year = newCal.get(Calendar.YEAR);
            int month = newCal.get(Calendar.MONTH);
            int day = newCal.get(Calendar.DAY_OF_MONTH);
            String dateBetween = year + "-" + month + "-" + day;
            if (!selectedDate.contains(dateBetween)) {
                selectedDate.add(dateBetween);
            }

        }
        selectedDateList = dates;
        return dates;
    }

    /**
     * Method to call others calendar api
     */
    void callOthersCalendarApi(int user_id, String f_date, String t_date) {

        if (prefs == null) {
            prefs = new Prefs();
        }
        //from_date=2016-08-01&to_date=2016-08-25
        String auth_token = prefs.getPreferencesString(OthersCalendar.this, "AUTH_TOKEN");
        String base_url = "http://api.gfile.canbrand.in/api/v1/usercalendar/" + user_id + "?";
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        String endpoint_url = "/api/v1/usercalendar/" + user_id + "?from_date="+f_date + "&to_date="+t_date;
        RestApis restApis = retrofit.create(RestApis.class);
        Call<SelfCalendarModel> call = restApis.getOthersCalendarInfo(endpoint_url, "bearer " + auth_token);
        call.enqueue(new Callback<SelfCalendarModel>() {
            @Override
            public void onResponse(Call<SelfCalendarModel> call, Response<SelfCalendarModel> response) {
                if (response.raw().code() == 200) {
                    progress_othercal_lay.setVisibility(View.GONE);
                    ll_oc_offline.setVisibility(View.GONE);
                    calendarResponse = response.body().getResponse();
                    selfCalendarModel = response.body();
                    for (int i = 0; i < calendarResponse.getData().getCalendar().size(); i++) {


                        datesFromApi.add(i, calendarResponse.getData().getCalendar().get(i).getDate());
                        appliedBy.add(i, calendarResponse.getData().getCalendar().get(i).getAppliedBy().getUserName());
                        approvedBy.add(i, calendarResponse.getData().getCalendar().get(i).getApprovedBy().getUserName());

                        changeBgOfDate(calendarResponse.getData().getCalendar().get(i).getDate(), calendarResponse.getData().getCalendar().get(i).getType());
                    }
                    caldroidFragment.refreshView();
                }else if (response.raw().code() == 401) {
                    progress_othercal_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();
                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(OthersCalendar.this,name,Toast.LENGTH_LONG).show();
                    logout();
                } else {
                    progress_othercal_lay.setVisibility(View.GONE);
                    String errorMsg = null;
                    String name = null;
                    try {
                        errorMsg = response.errorBody().string();

                        JSONObject reader = new JSONObject(errorMsg);
                        name = reader.getString("msg");
                        // name = sys.getString("msg");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(OthersCalendar.this, name, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SelfCalendarModel> call, Throwable t) {
                progress_othercal_lay.setVisibility(View.GONE);
                ll_oc_offline.setVisibility(View.VISIBLE);
                tv_calendar_header.setText("Offline");
                String msg = t.getMessage();
                Toast.makeText(OthersCalendar.this, msg, Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * method to get name label to display corresponding to date
     *
     * @param type
     * @return
     */
    String getLabelFromType(String type) {
        String label = "";
        switch (type) {
            case "AM":
                label = selfCalendarModel.getResponse().getLegend().getAM().getLabel();
                break;
            case "PM":
                label = selfCalendarModel.getResponse().getLegend().getPM().getLabel();
                break;
            case "EL":
                label = selfCalendarModel.getResponse().getLegend().getEL().getLabel();
                break;
            case "SL":
                label = selfCalendarModel.getResponse().getLegend().getSL().getLabel();
                break;
            case "CL":
                label = selfCalendarModel.getResponse().getLegend().getCL().getLabel();
                break;
            case "OL":
                label = selfCalendarModel.getResponse().getLegend().getOL().getLabel();
                break;
            case "LWP":
                label = selfCalendarModel.getResponse().getLegend().getLWP().getLabel();
                break;
            case "HOLIDAY":
                label = selfCalendarModel.getResponse().getLegend().getHOLIDAY().getLabel();
                break;
            case "WEEKOFF":
                label = selfCalendarModel.getResponse().getLegend().getWEEKOFF().getLabel();
                break;


        }
        return label;
    }


    /**
     * method to get color for event type
     *
     * @param leave_type
     * @return
     */
    int getColorByLeaveType(String leave_type) {
        int bg = 0;
        switch (leave_type) {

            case "AM":
                bg = R.color.AM;
                break;
            case "PM":
                bg = R.color.AM;
                break;
            case "EL":
                bg = R.color.EL;
                break;
            case "SL":
                bg = R.color.EL;
                break;
            case "CL":
                bg = R.color.EL;
                break;
            case "OL":
                bg = R.color.EL;
                break;
            case "LWP":
                bg = R.color.EL;
                break;
            case "HOLIDAY":
                bg = R.color.HOLIDAY;
                break;
            case "WEEKOFF":
                bg = R.color.colorPrimaryDark;
                break;
            default:
                bg = R.color.caldroid_gray;
                ;
                break;


        }
        return bg;
    }


    private void callOtherLeaveApplyApi(String from, String to, int id) {

        ApplySelfLeaveRawData applySelfLeaveRawData=null;

        int compareDtae=from.compareTo(to);
        System.out.println(compareDtae);
        if(compareDtae>=1){
             applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = to;
            applySelfLeaveRawData.to_date = from;
            applySelfLeaveRawData.comment = "";

        }else if(compareDtae<0){
             applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = from;
            applySelfLeaveRawData.to_date = to;
            applySelfLeaveRawData.comment = "";
        }else if(compareDtae==0){
             applySelfLeaveRawData = new ApplySelfLeaveRawData();
            applySelfLeaveRawData.leave_type = "PM";
            applySelfLeaveRawData.from_date = from;
            applySelfLeaveRawData.to_date = to;
            applySelfLeaveRawData.comment = "";
        }
        //HashMap hashMap1 = map;
        try {

            String auth_token = prefs.getPreferencesString(OthersCalendar.this, "AUTH_TOKEN");
            retroFitMaker = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
            String endpoint_url = "/api/v1/employee/applyleave/" + id;
            RestApis apiInterface = retroFitMaker.create(RestApis.class);
            Call<ApplySelfLeavePojo> call = apiInterface.applySelfLeave(endpoint_url,applySelfLeaveRawData, "bearer " + auth_token);

            call.enqueue(new Callback<ApplySelfLeavePojo>() {
                @Override
                public void onResponse(Call<ApplySelfLeavePojo> call, Response<ApplySelfLeavePojo> response) {

                    if (response.raw().code() == 200) {
                        progress_othercal_lay.setVisibility(View.GONE);
                        ll_othercalander_applysuccess.setVisibility(View.VISIBLE);
                        tv_othercal_successmsg.setText(response.body().getMsg());
                        tv_calendar_header.setText("");
                        rl_oc_symbol_info.setVisibility(View.GONE);
                        rl_oc_back.setVisibility(View.GONE);
                      //  Toast.makeText(OthersCalendar.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                    } else if (response.raw().code() == 401) {
                        progress_othercal_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(OthersCalendar.this,name,Toast.LENGTH_LONG).show();
                        logout();
                    }else {
                        progress_othercal_lay.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();

                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                            // name = sys.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(OthersCalendar.this, name, Toast.LENGTH_LONG).show();
                        return;
                    }

                }

                @Override
                public void onFailure(Call<ApplySelfLeavePojo> call, Throwable t) {

                    String errorMsg = t.getMessage();
                    progress_othercal_lay.setVisibility(View.GONE);
                    ll_oc_offline.setVisibility(View.VISIBLE);
                    tv_calendar_header.setText("Offline");
                    Toast.makeText(OthersCalendar.this, errorMsg, Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }


    /**
     * method to initialize calendar
     */
    private void initializeCalendar(){
        current_date = new Date();
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        final Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidCustomDefault);
        caldroidFragment.setArguments(args);


        setCurrentDateTextColor(true);


        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.other_calendar_container, caldroidFragment);
        t.commit();


        caldroidFragment.setCaldroidListener(new CaldroidListener() {


            @Override
            public void onChangeMonth(int month, int year) {
                String f_date = "", t_date = "";

                if (month < 10) {
                    f_date = year + "-0" + (month) + "-" + "01";
                    t_date = year + "-0" + (month) + "-" + month_day[month-1];
                } else {
                    f_date = year + "-" + (month) + "-" + "01";
                    t_date = year + "-" + (month) + "-" + month_day[month-1];
                }

                if (Helper.checkInternetConnection(OthersCalendar.this)) {
                    ll_oc_offline.setVisibility(View.GONE);
                    progress_othercal_lay.setVisibility(View.VISIBLE);
                    tv_calendar_header.setText(user_name + "'s calendar");
                    current_Month=month;
                    callOthersCalendarApi(user_id, f_date, t_date);

                } else {
                    progress_othercal_lay.setVisibility(View.GONE);
                    ll_oc_offline.setVisibility(View.VISIBLE);
                    tv_calendar_header.setText("Offline");
                }
//                ll_oc_offline.setVisibility(View.GONE);
//                progress_othercal_lay.setVisibility(View.VISIBLE);
//                tv_calendar_header.setText(user_name + "'s calendar");
//                callOthersCalendarApi(user_id, f_date, t_date);

            }


            @Override
            public void onSelectDate(Date date, View view) {

                if (rl_oc_info.getVisibility()==View.INVISIBLE){
                    Calendar newCal = Calendar.getInstance();
                    newCal.setTime(date);
                    int year = newCal.get(Calendar.YEAR);
                    int month = newCal.get(Calendar.MONTH);
                    int day = newCal.get(Calendar.DAY_OF_MONTH);
                    String clickedDate = year + "-" + month + "-" + day;
                    String dateToSearch;

                    if (month < 9) {
                        if (day < 10) {
                            dateToSearch = year + "-" + "0" + (month + 1) + "-" + "0" + day;
                        } else {
                            dateToSearch = year + "-" + "0" + (month + 1) + "-" + day;
                        }
                    } else {
                        if (day < 10) {
                            dateToSearch = year + "-" + (month + 1) + "-" + "0" + day;
                        } else {
                            dateToSearch = year + "-" + (month + 1) + "-" + day;
                        }
                    }


                    if (selectedDate.contains(clickedDate)) {


                        if (selectedDate.size() == 1) {
                            selectedDate.remove(clickedDate);

                            if (fmt.format(current_date).equals(fmt.format(date))) {
                                setCurrentDateTextColor(true);
                            } else {
                                String[] sepSearchdate= dateToSearch.split("-");

                                if(current_Month<Integer.parseInt(sepSearchdate[1])){
                                    caldroidFragment.setTextColorForDate(R.color.caldroid_darker_gray, date);
                                }else {
                                    caldroidFragment.setTextColorForDate(R.color.black, date);
                                }
                               // caldroidFragment.setTextColorForDate(R.color.black, date);
                            }

                            caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(R.drawable.not_selected), date);
                            caldroidFragment.refreshView();

                            handleDateSelection(6);
                            dateFrom = null;
                            dateTo = null;
                            setDefaultColors();
                        } else {
                            clearAllSelectedDates();
                            handleDateSelection(6);
                        }
                    } else {

                        switch (selectedDate.size()) {
                            case 0:
                                dateFrom = date;
                                if (fmt.format(current_date).equals(fmt.format(date))) {
                                    handleDateSelection(2);
                                } else {
                                    if (Calendar.getInstance().getTime().after(date)) {
                                        handleDateSelection(1);
                                    } else {
                                        handleDateSelection(3);
                                    }
                                }
                                selectedDate.add(clickedDate);
                                caldroidFragment.setTextColorForDate(R.color.white, date);
                                caldroidFragment.setBackgroundDrawableForDate(getResources().getDrawable(R.drawable.date_selected_bg), date);
                                caldroidFragment.refreshView();

                                if (datesFromApi.contains(dateToSearch)) {
                                    String[] sepSearchdate= dateToSearch.split("-");
                                    if(current_Month>Integer.parseInt(sepSearchdate[1]) || current_Month<Integer.parseInt(sepSearchdate[1])){

                                    }else if(current_Month==Integer.parseInt(sepSearchdate[1])){

                                        rl_oc_selected_date_events.setVisibility(View.VISIBLE);
                                    int pos = datesFromApi.indexOf(dateToSearch);
                                    if (calendarResponse.getData().getCalendar().get(pos).getType().equals("WEEKOFF") || calendarResponse.getData().getCalendar().get(pos).getType().equals("HOLIDAY")) {
                                        tv_oc_date_corresponding_info_first.setText(getLabelFromType(calendarResponse.getData().getCalendar().get(pos).getType()));
                                        tv_oc_date_corresponding_info_second.setText("Holiday: " + calendarResponse.getData().getCalendar().get(pos).getOffReason());
                                        side_border_oc_event.setBackgroundResource(getColorByLeaveType(calendarResponse.getData().getCalendar().get(pos).getType()));
                                        iv_other_calendar_info_check.setVisibility(View.INVISIBLE);
                                    } else {
                                        tv_oc_date_corresponding_info_first.setText(appliedBy.get(pos) + " applied for a " + calendarResponse.getData().getCalendar().get(pos).getType());
                                        iv_other_calendar_info_check.setVisibility(View.VISIBLE);
                                        side_border_oc_event.setBackgroundResource(getColorByLeaveType(calendarResponse.getData().getCalendar().get(pos).getType()));

                                        if (calendarResponse.getData().getCalendar().get(pos).getIsApproved()) {
                                            iv_other_calendar_info_check.setImageResource(R.drawable.card_check);
                                            if (calendarResponse.getData().getCalendar().get(pos).getApprovedBy().getUserName() == null) {
                                                tv_oc_date_corresponding_info_second.setText("Approved");
                                            } else {
                                                tv_oc_date_corresponding_info_second.setText("Approved by " + calendarResponse.getData().getCalendar().get(pos).getApprovedBy().getUserName());
                                            }
                                        } else {
                                            iv_other_calendar_info_check.setImageResource(R.drawable.card_pending);
                                            tv_oc_date_corresponding_info_second.setText("Applied by " + calendarResponse.getData().getCalendar().get(pos).getAppliedBy().getUserName());
                                        }
                                    }

                                } }else {
                                    rl_oc_selected_date_events.setVisibility(View.GONE);
                                }


                                break;
                            case 1:
                                dateTo = date;
                                setSelectedDateBackground();

                                if (Calendar.getInstance().getTime().after(dateFrom) && Calendar.getInstance().getTime().after(dateTo)) {
                                    handleDateSelection(4);

                                } else {
                                    if (Calendar.getInstance().getTime().after(dateFrom) && Calendar.getInstance().getTime().equals(dateTo)) {
                                        handleDateSelection(4);
                                    } else {
                                        handleDateSelection(5);
                                    }
                                }
                                break;
                            default:
                                handleDateSelection(6);
                                clearAllSelectedDates();

                                dateTo = null;
                                dateFrom = null;
                                break;
                        }


                    }
                }else {
                    rl_oc_info.setVisibility(View.INVISIBLE);
                }



            }
        });
    }
    void logout() {
        prefs.clearAppData(OthersCalendar.this);
        DbHelper dbHelper = DbHelper.getInstance(OthersCalendar.this);
        dbHelper.deleteDb();
        Intent splashIntent = new Intent(OthersCalendar.this, Splash.class);
        splashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}

