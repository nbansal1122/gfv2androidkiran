package com.growthfile.ui;


import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.growthfile.R;

import simplifii.framework.utility.AppConstants;

public class GPSError extends AppCompatActivity {

    LinearLayout liner_gps_setting;
    RelativeLayout rel_gpserror_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpserror);

        liner_gps_setting = (LinearLayout) findViewById(R.id.liner_gps_setting);

        liner_gps_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(callGPSSettingIntent, AppConstants.REQUEST_CODES.CHECK_GPS);
            }
        });

        rel_gpserror_back = (RelativeLayout) findViewById(R.id.rel_gpserror_back);
        rel_gpserror_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void checkGPSEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startActivity(new Intent(this, PunchInOutActivity.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.CHECK_GPS:
                checkGPSEnabled();
                break;
        }
    }
}
