package com.growthfile.ui;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.growthfile.Pojo.SelfProfilePojo;
import com.growthfile.Pojo.SelfProfileResponsePojo;
import simplifii.framework.rest.responses.UserProfile;
import com.growthfile.R;
import com.growthfile.utility.Constraint;
import com.growthfile.utility.Helper;
import simplifii.framework.utility.Prefs;
import com.growthfile.webservices.RestApis;
import com.growthfile.webservices.RetroFitMaker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfile extends Fragment implements View.OnClickListener {

    private Prefs prefs;
    private UserProfile userProfilePojo;
    private SelfProfileResponsePojo profilePojo;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private TextDrawable.IBuilder drawable1;
    MyCalendarCallback mCallback;
    ImageView iv_my_profile_pic;
    TextView tv_my_profile_name, tv_my_profile_designation, tv_my_profile_phone, tv_my_profile_email,
            tv_my_profile_joined, tv_my_profile_field, tv_my_profile_reporting,
            tv_my_profile_location,tv_myprofile_el, tv_myprofile_cl, tv_myprofile_ml,tv_myprofile_heading;

    private RelativeLayout relative_myprofile_offline,progress_my_profile;
    LinearLayout liner_offline_setting,linear_profile_cal;

    public MyProfile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        prefs = new Prefs();
        initUi(view);


        /*
        * API FOR SELF PROFILE DATA
         */
       if (Helper.checkInternetConnection(getActivity())) {
            relative_myprofile_offline.setVisibility(View.GONE);
            progress_my_profile.setVisibility(View.VISIBLE);
            tv_myprofile_heading.setText("My File");
            callSelfProfileApi();
        } else {
            relative_myprofile_offline.setVisibility(View.VISIBLE);
            tv_myprofile_heading.setText("Offline");
        }

       /* relative_myprofile_offline.setVisibility(View.GONE);
        progress_my_profile.setVisibility(View.VISIBLE);
        tv_myprofile_heading.setText("My File");
        callSelfProfileApi();*/
        return view;
    }

    void initUi(View view) {

        iv_my_profile_pic = (ImageView) view.findViewById(R.id.iv_my_profile_pic);
        tv_my_profile_name = (TextView) view.findViewById(R.id.tv_my_profile_name);
        tv_my_profile_designation = (TextView) view.findViewById(R.id.tv_my_profile_designation);
        tv_my_profile_phone = (TextView) view.findViewById(R.id.tv_my_profile_phone);
        tv_my_profile_email = (TextView) view.findViewById(R.id.tv_my_profile_email);
        tv_my_profile_joined = (TextView) view.findViewById(R.id.tv_my_profile_joined);
        tv_my_profile_field = (TextView) view.findViewById(R.id.tv_my_profile_field);
        tv_my_profile_reporting = (TextView) view.findViewById(R.id.tv_my_profile_reporting);
        tv_my_profile_location = (TextView) view.findViewById(R.id.tv_my_profile_location);
        tv_myprofile_el = (TextView) view.findViewById(R.id.tv_myprofile_el);
        tv_myprofile_cl = (TextView) view.findViewById(R.id.tv_myprofile_cl);
        tv_myprofile_ml = (TextView) view.findViewById(R.id.tv_myprofile_ml);
        progress_my_profile= (RelativeLayout) view.findViewById(R.id.progress_my_profile);
        relative_myprofile_offline = (RelativeLayout) view.findViewById(R.id.relative_myprofile_offline);
        liner_offline_setting = (LinearLayout) view.findViewById(R.id.liner_offline_setting);
        linear_profile_cal= (LinearLayout) view.findViewById(R.id.linear_profile_cal);
        linear_profile_cal.setOnClickListener(this);
        tv_myprofile_heading=(TextView)view.findViewById(R.id.tv_myprofile_heading);

        // Click over retry button when offline//////////
        liner_offline_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Helper.checkInternetConnection(getActivity())) {
                    relative_myprofile_offline.setVisibility(View.GONE);
                    progress_my_profile.setVisibility(View.VISIBLE);
                    tv_myprofile_heading.setText("My File");
                    // self profile api call
                    callSelfProfileApi();
                } else {

                    relative_myprofile_offline.setVisibility(View.VISIBLE);
                    tv_myprofile_heading.setText("Offline");
                }
            }
        });


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            System.out.println("profile on start");
        } else {
            // Do your Work
        }
    }
    @Override
    public void onStart() {
        super.onStart();

     //   System.out.println("profile on start");
    }

    public static MyProfile newInstance() {
        MyProfile f = new MyProfile();
        return f;
    }

    /**
     * Method to call self profile  api
     */
    void callSelfProfileApi() {
        if (prefs == null) {
            prefs = new Prefs();
        }
        String auth_token = prefs.getPreferencesString(getActivity(), "AUTH_TOKEN");
        Retrofit retrofit = new RetroFitMaker().instanceMaker(Constraint.staticBASE_URL);
        RestApis restApis = retrofit.create(RestApis.class);
        Call<SelfProfilePojo> call = restApis.getSelfProfileInfo("bearer " + auth_token);
        call.enqueue(new Callback<SelfProfilePojo>() {
            @Override
            public void onResponse(Call<SelfProfilePojo> call, Response<SelfProfilePojo> response) {


                Activity activity = getActivity();
                if(isAdded() && activity != null){
                    if (response.raw().code() == 200) {

                        progress_my_profile.setVisibility(View.GONE);
                        profilePojo = response.body().getResponse();
                        userProfilePojo = profilePojo.getProfile();

                        tv_my_profile_name.setText(userProfilePojo.getName());
                        tv_my_profile_designation.setText(userProfilePojo.getDesignation());
                        tv_my_profile_phone.setText("" + userProfilePojo.getMobile());
                        tv_my_profile_email.setText(userProfilePojo.getEmail());
                        tv_myprofile_el.setText("" + profilePojo.getLeaves().getEarnedLeaves());
                        tv_myprofile_cl.setText("" + profilePojo.getLeaves().getCasualLeaves());
                        tv_myprofile_ml.setText("" + profilePojo.getLeaves().getMedicalLeaves());

                        tv_my_profile_reporting.setText("Reporting to " + profilePojo.getReportsTo().getUserName());
                        tv_my_profile_location.setText(profilePojo.getCenter().getCode() + ", " + profilePojo.getCenter().getState());
                        tv_my_profile_field.setText(profilePojo.getDepartment().getDepartmentName());
                        String dt = userProfilePojo.getDateOfJoining();
                        String[] seperate = dt.split("T");

                        // the string representation of date (month/day/year)
                        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        DateFormat targetFormat = new SimpleDateFormat("dd MMM, yyyy");
                        Date date = null;
                        try {
                            date = originalFormat.parse(seperate[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String formattedDate = targetFormat.format(date);
                        tv_my_profile_joined.setText("Joined on " + formattedDate);

                        /////// CHECK WEATHER NAME CARD WAS SHOWN OR PROFILE PIC///////////
                        if (userProfilePojo.getAvatar().equals("") || userProfilePojo.equals(null)) {
                            String test = userProfilePojo.getName();
                            char first = test.charAt(0);

                            drawable1 = TextDrawable.builder().round();
                            TextDrawable drawable = TextDrawable.builder()
                                    .beginConfig()
                                    .textColor(Color.parseColor("#5ab2e0"))
                                    .useFont(Typeface.DEFAULT)
                                    .fontSize(66) /* size in px */
                                    .bold()
                                    .toUpperCase()
                                    .endConfig()
                                    .buildRound(String.valueOf(first), Color.parseColor("#ffffff"));
                            iv_my_profile_pic.setImageDrawable(drawable);

                        } else {
                            /// load profile pic
                            initiateImageLoader();
                            imageLoader.displayImage(userProfilePojo.getAvatar(), iv_my_profile_pic, options);
                        }
                    } else {
                        progress_my_profile.setVisibility(View.GONE);
                        String errorMsg = null;
                        String name = null;
                        try {
                            errorMsg = response.errorBody().string();
                            JSONObject reader = new JSONObject(errorMsg);
                            name = reader.getString("msg");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), name, Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<SelfProfilePojo> call, Throwable t) {

                Activity activity = getActivity();
                if(isAdded() && activity != null){
                    progress_my_profile.setVisibility(View.GONE);
                    relative_myprofile_offline.setVisibility(View.VISIBLE);
                    tv_myprofile_heading.setText("Offline");
                    String msg = t.getMessage();
                   // Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    /*
      method to load profile pic
     */
    void initiateImageLoader() {
        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_pic)
                .showImageForEmptyUri(R.drawable.profile_pic)
                .showImageOnFail(R.drawable.profile_pic)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (MyCalendarCallback) activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.linear_profile_cal:
                mCallback.changeTabToCalendar();
                break;
            case R.id.progress_my_profile:
                break;
        }
    }

    interface MyCalendarCallback {
        public void changeTabToCalendar();
    }

    @Override
    public void onPause() {
        super.onPause();
        progress_my_profile.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progress_my_profile.setVisibility(View.GONE);
    }
}
