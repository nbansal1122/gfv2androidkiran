package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyPojo {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private OtpVerifyResponsePojo response;

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The response
     */
    public OtpVerifyResponsePojo getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(OtpVerifyResponsePojo response) {
        this.response = response;
    }

}
