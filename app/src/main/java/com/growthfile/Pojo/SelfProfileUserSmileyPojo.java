package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserSmileyPojo implements Serializable{

    @SerializedName("received")
    @Expose
    private Integer received;
    @SerializedName("given")
    @Expose
    private Integer given;

    /**
     *
     * @return
     * The received
     */
    public Integer getReceived() {
        return received;
    }

    /**
     *
     * @param received
     * The received
     */
    public void setReceived(Integer received) {
        this.received = received;
    }

    /**
     *
     * @return
     * The given
     */
    public Integer getGiven() {
        return given;
    }

    /**
     *
     * @param given
     * The given
     */
    public void setGiven(Integer given) {
        this.given = given;
    }

}
