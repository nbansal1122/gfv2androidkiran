package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyOrgPojo {


    @SerializedName("org_id")
    @Expose
    private Integer orgId;
    @SerializedName("org_name")
    @Expose
    private String orgName;
    @SerializedName("entity_type")
    @Expose
    private String entityType;
    @SerializedName("logo")
    @Expose
    private String logo;

    /**
     *
     * @return
     * The orgId
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     *
     * @param orgId
     * The org_id
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     *
     * @return
     * The orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     *
     * @param orgName
     * The org_name
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     *
     * @return
     * The entityType
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     *
     * @param entityType
     * The entity_type
     */
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }
}