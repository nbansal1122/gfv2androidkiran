package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kundan on 9/7/2016.
 */
public class SelfCalendarData {
    @SerializedName("calendar")
    @Expose
    private List<SelfCalendarPojo> calendar = new ArrayList<SelfCalendarPojo>();

    /**
     *
     * @return
     * The calendar
     */
    public List<SelfCalendarPojo> getCalendar() {
        return calendar;
    }

    /**
     *
     * @param calendar
     * The calendar
     */
    public void setCalendar(List<SelfCalendarPojo> calendar) {
        this.calendar = calendar;
    }
}
