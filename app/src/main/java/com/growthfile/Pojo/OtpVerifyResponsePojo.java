package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyResponsePojo {

    @SerializedName("data")
    @Expose
    private OtpVerifyDataPojo data;

    /**
     *
     * @return
     * The data
     */
    public OtpVerifyDataPojo getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(OtpVerifyDataPojo data) {
        this.data = data;
    }
}