package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kundan on 9/19/2016.
 */
public class FeedListModel {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private FeedListResponse response;

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The response
     */
    public FeedListResponse getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(FeedListResponse response) {
        this.response = response;
    }
}
