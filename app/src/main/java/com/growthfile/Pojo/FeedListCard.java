package com.growthfile.Pojo;

import simplifii.framework.model.BaseRecyclerModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.AppConstants;

/**
 * Created by kundan on 9/19/2016.
 */
public class FeedListCard extends BaseRecyclerModel{

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.USER_FEED;
    }

    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("action_label")
    @Expose
    private String actionLabel;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("card_id")
    @Expose
    private Integer cardId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("card_type")
    @Expose
    private String cardType;
    @SerializedName("application_id")
    @Expose
    private Integer applicationId;
    @SerializedName("application_type")
    @Expose
    private String applicationType;
    @SerializedName("meta_data")
    @Expose
    private Object metaData;
    @SerializedName("applied_by")
    @Expose
    private FeedListAppliedBy appliedBy;
    @SerializedName("action_data")
    @Expose
    private FeedListActionData actionData;

    /**
     *
     * @return
     * The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     *
     * @param avatar
     * The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The actionLabel
     */
    public String getActionLabel() {
        return actionLabel;
    }

    /**
     *
     * @param actionLabel
     * The action_label
     */
    public void setActionLabel(String actionLabel) {
        this.actionLabel = actionLabel;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The cardId
     */
    public Integer getCardId() {
        return cardId;
    }

    /**
     *
     * @param cardId
     * The card_id
     */
    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     *
     * @param cardType
     * The card_type
     */
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    /**
     *
     * @return
     * The applicationId
     */
    public Integer getApplicationId() {
        return applicationId;
    }

    /**
     *
     * @param applicationId
     * The application_id
     */
    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    /**
     *
     * @return
     * The applicationType
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     *
     * @param applicationType
     * The application_type
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     *
     * @return
     * The metaData
     */
    public Object getMetaData() {
        return metaData;
    }

    /**
     *
     * @param metaData
     * The meta_data
     */
    public void setMetaData(Object metaData) {
        this.metaData = metaData;
    }

    /**
     *
     * @return
     * The appliedBy
     */
    public FeedListAppliedBy getAppliedBy() {
        return appliedBy;
    }

    /**
     *
     * @param appliedBy
     * The applied_by
     */
    public void setAppliedBy(FeedListAppliedBy appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     *
     * @return
     * The actionData
     */
    public FeedListActionData getActionData() {
        return actionData;
    }

    /**
     *
     * @param actionData
     * The action_data
     */
    public void setActionData(FeedListActionData actionData) {
        this.actionData = actionData;
    }

}
