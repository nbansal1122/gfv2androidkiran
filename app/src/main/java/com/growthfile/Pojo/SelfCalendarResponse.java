package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kundan on 9/7/2016.
 */
public class SelfCalendarResponse {
    @SerializedName("data")
    @Expose
    private SelfCalendarData data;
    @SerializedName("legend")
    @Expose
    private SelfCalendarLabel legend;

    /**
     *
     * @return
     * The data
     */
    public SelfCalendarData getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(SelfCalendarData data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The legend
     */
    public SelfCalendarLabel getLegend() {
        return legend;
    }

    /**
     *
     * @param legend
     * The legend
     */
    public void setLegend(SelfCalendarLabel legend) {
        this.legend = legend;
    }

}
