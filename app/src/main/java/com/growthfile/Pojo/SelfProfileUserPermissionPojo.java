package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserPermissionPojo implements Serializable {

    @SerializedName("can_view_calender")
    @Expose
    private Boolean canViewCalender;
    @SerializedName("can_view_stars")
    @Expose
    private Boolean canViewStars;
    @SerializedName("can_view_smiley")
    @Expose
    private Boolean canViewSmiley;
    @SerializedName("can_give_star")
    @Expose
    private Boolean canGiveStar;
    @SerializedName("can_give_smiley")
    @Expose
    private Boolean canGiveSmiley;
    @SerializedName("can_apply_leave")
    @Expose
    private Boolean canApplyLeave;

    @SerializedName("can_edit_profile")
    @Expose
    private Boolean canEditProfile;
    @SerializedName("can_edit_mobile")
    @Expose
    private Boolean canEditMobile;
    @SerializedName("can_edit_role")
    @Expose
    private Boolean canEditRole;
    @SerializedName("can_resign")
    @Expose
    private Boolean canResign;
    @SerializedName("can_terminate")
    @Expose
    private Boolean canTerminate;

    public Boolean getCanEditProfile() {
        return canEditProfile;
    }

    public void setCanEditProfile(Boolean canEditProfile) {
        this.canEditProfile = canEditProfile;
    }

    public Boolean getCanEditMobile() {
        return canEditMobile;
    }

    public void setCanEditMobile(Boolean canEditMobile) {
        this.canEditMobile = canEditMobile;
    }

    public Boolean getCanEditRole() {
        return canEditRole;
    }

    public void setCanEditRole(Boolean canEditRole) {
        this.canEditRole = canEditRole;
    }

    public Boolean getCanResign() {
        return canResign;
    }

    public void setCanResign(Boolean canResign) {
        this.canResign = canResign;
    }

    public Boolean getCanTerminate() {
        return canTerminate;
    }

    public void setCanTerminate(Boolean canTerminate) {
        this.canTerminate = canTerminate;
    }

    /**
     * @return The canViewCalender
     */
    public Boolean getCanViewCalender() {
        return canViewCalender;
    }

    /**
     * @param canViewCalender The can_view_calender
     */
    public void setCanViewCalender(Boolean canViewCalender) {
        this.canViewCalender = canViewCalender;
    }

    /**
     * @return The canViewStars
     */
    public Boolean getCanViewStars() {
        return canViewStars;
    }

    /**
     * @param canViewStars The can_view_stars
     */
    public void setCanViewStars(Boolean canViewStars) {
        this.canViewStars = canViewStars;
    }

    /**
     * @return The canViewSmiley
     */
    public Boolean getCanViewSmiley() {
        return canViewSmiley;
    }

    /**
     * @param canViewSmiley The can_view_smiley
     */
    public void setCanViewSmiley(Boolean canViewSmiley) {
        this.canViewSmiley = canViewSmiley;
    }

    /**
     * @return The canGiveStar
     */
    public Boolean getCanGiveStar() {
        return canGiveStar;
    }

    /**
     * @param canGiveStar The can_give_star
     */
    public void setCanGiveStar(Boolean canGiveStar) {
        this.canGiveStar = canGiveStar;
    }

    /**
     * @return The canGiveSmiley
     */
    public Boolean getCanGiveSmiley() {
        return canGiveSmiley;
    }

    /**
     * @param canGiveSmiley The can_give_smiley
     */
    public void setCanGiveSmiley(Boolean canGiveSmiley) {
        this.canGiveSmiley = canGiveSmiley;
    }

    /**
     * @return The canApplyLeave
     */
    public Boolean getCanApplyLeave() {
        return canApplyLeave;
    }

    /**
     * @param canApplyLeave The can_apply_leave
     */
    public void setCanApplyLeave(Boolean canApplyLeave) {
        this.canApplyLeave = canApplyLeave;
    }

}
