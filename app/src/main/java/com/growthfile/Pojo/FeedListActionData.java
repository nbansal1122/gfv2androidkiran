package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kundan on 9/19/2016.
 */
public class FeedListActionData {
    @SerializedName("is_readonly")
    @Expose
    private Boolean isReadonly;
    @SerializedName("status_type")
    @Expose
    private Integer statusType;
    @SerializedName("status_label")
    @Expose
    private String statusLabel;
    @SerializedName("status_user")
    @Expose
    private FeedListUserStatus statusUser;
    @SerializedName("actions")
    @Expose
    private List<FeedListAction> actions = new ArrayList<FeedListAction>();

    /**
     *
     * @return
     * The isReadonly
     */
    public Boolean getIsReadonly() {
        return isReadonly;
    }

    /**
     *
     * @param isReadonly
     * The is_readonly
     */
    public void setIsReadonly(Boolean isReadonly) {
        this.isReadonly = isReadonly;
    }

    /**
     *
     * @return
     * The statusType
     */
    public Integer getStatusType() {
        return statusType;
    }

    /**
     *
     * @param statusType
     * The status_type
     */
    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    /**
     *
     * @return
     * The statusLabel
     */
    public String getStatusLabel() {
        return statusLabel;
    }

    /**
     *
     * @param statusLabel
     * The status_label
     */
    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    /**
     *
     * @return
     * The statusUser
     */
    public FeedListUserStatus getStatusUser() {
        return statusUser;
    }

    /**
     *
     * @param statusUser
     * The status_user
     */
    public void setStatusUser(FeedListUserStatus statusUser) {
        this.statusUser = statusUser;
    }

    /**
     *
     * @return
     * The actions
     */
    public List<FeedListAction> getActions() {
        return actions;
    }

    /**
     *
     * @param actions
     * The actions
     */
    public void setActions(List<FeedListAction> actions) {
        this.actions = actions;
    }

}
