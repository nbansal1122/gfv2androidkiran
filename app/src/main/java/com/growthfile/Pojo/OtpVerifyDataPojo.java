package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyDataPojo {
    @SerializedName("user_profile")
    @Expose
    private OtpVerifyUserProfilePojo userProfile;

    /**
     *
     * @return
     * The userProfile
     */
    public OtpVerifyUserProfilePojo getUserProfile() {
        return userProfile;
    }

    /**
     *
     * @param userProfile
     * The user_profile
     */
    public void setUserProfile(OtpVerifyUserProfilePojo userProfile) {
        this.userProfile = userProfile;
    }
}