package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserDepartmentPojo implements Serializable{

    @SerializedName("department_id")
    @Expose
    private Integer departmentId;
    @SerializedName("department_name")
    @Expose
    private String departmentName;

    /**
     *
     * @return
     * The departmentId
     */
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**
     *
     * @param departmentId
     * The department_id
     */
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**
     *
     * @return
     * The departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     *
     * @param departmentName
     * The department_name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

}
