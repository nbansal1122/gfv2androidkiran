package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kundan on 9/19/2016.
 */
public class FeedListAction {
    @SerializedName("action_type")
    @Expose
    private Integer actionType;
    @SerializedName("action_label")
    @Expose
    private String actionLabel;

    /**
     *
     * @return
     * The actionType
     */
    public Integer getActionType() {
        return actionType;
    }

    /**
     *
     * @param actionType
     * The action_type
     */
    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    /**
     *
     * @return
     * The actionLabel
     */
    public String getActionLabel() {
        return actionLabel;
    }

    /**
     *
     * @param actionLabel
     * The action_label
     */
    public void setActionLabel(String actionLabel) {
        this.actionLabel = actionLabel;
    }

}
