package com.growthfile.Pojo;

import simplifii.framework.model.BaseRecyclerModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.responses.UserProfile;
import simplifii.framework.utility.AppConstants;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileResponsePojo extends BaseRecyclerModel implements Serializable{

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.USER_PROFILE_HEADER_INFO;
    }

    @SerializedName("profile")
    @Expose
    private UserProfile profile;
    @SerializedName("reports_to")
    @Expose
    private SelfProfileUserReportPojo reportsTo;
    @SerializedName("leaves")
    @Expose
    private SelfProfileUserLeavesPojo leaves;
    @SerializedName("department")
    @Expose
    private SelfProfileUserDepartmentPojo department;
    @SerializedName("center")
    @Expose
    private SelfProfileUserCenterPojo center;
    @SerializedName("org")
    @Expose
    private SelfProfileUserOrgPojo org;
    @SerializedName("permissions")
    @Expose
    private SelfProfileUserPermissionPojo permissions;
    @SerializedName("growthfile")
    @Expose
    private SelfProfileUserGrowthFilePojo growthfile;
    @SerializedName("smiley")
    @Expose
    private SelfProfileUserSmileyPojo smiley;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    /**
     *
     * @return
     * The profile
     */
    public UserProfile getProfile() {
        return profile;
    }

    /**
     *
     * @param profile
     * The profile
     */
    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    /**
     *
     * @return
     * The reportsTo
     */
    public SelfProfileUserReportPojo getReportsTo() {
        return reportsTo;
    }

    /**
     *
     * @param reportsTo
     * The reports_to
     */
    public void setReportsTo(SelfProfileUserReportPojo reportsTo) {
        this.reportsTo = reportsTo;
    }

    /**
     *
     * @return
     * The leaves
     */
    public SelfProfileUserLeavesPojo getLeaves() {
        return leaves;
    }

    /**
     *
     * @param leaves
     * The leaves
     */
    public void setLeaves(SelfProfileUserLeavesPojo leaves) {
        this.leaves = leaves;
    }

    /**
     *
     * @return
     * The department
     */
    public SelfProfileUserDepartmentPojo getDepartment() {
        return department;
    }

    /**
     *
     * @param department
     * The department
     */
    public void setDepartment(SelfProfileUserDepartmentPojo department) {
        this.department = department;
    }

    /**
     *
     * @return
     * The center
     */
    public SelfProfileUserCenterPojo getCenter() {
        return center;
    }

    /**
     *
     * @param center
     * The center
     */
    public void setCenter(SelfProfileUserCenterPojo center) {
        this.center = center;
    }

    /**
     *
     * @return
     * The org
     */
    public SelfProfileUserOrgPojo getOrg() {
        return org;
    }

    /**
     *
     * @param org
     * The org
     */
    public void setOrg(SelfProfileUserOrgPojo org) {
        this.org = org;
    }

    /**
     *
     * @return
     * The permissions
     */
    public SelfProfileUserPermissionPojo getPermissions() {
        return permissions;
    }

    /**
     *
     * @param permissions
     * The permissions
     */
    public void setPermissions(SelfProfileUserPermissionPojo permissions) {
        this.permissions = permissions;
    }

    /**
     *
     * @return
     * The growthfile
     */
    public SelfProfileUserGrowthFilePojo getGrowthfile() {
        return growthfile;
    }

    /**
     *
     * @param growthfile
     * The growthfile
     */
    public void setGrowthfile(SelfProfileUserGrowthFilePojo growthfile) {
        this.growthfile = growthfile;
    }

    /**
     *
     * @return
     * The smiley
     */
    public SelfProfileUserSmileyPojo getSmiley() {
        return smiley;
    }

    /**
     *
     * @param smiley
     * The smiley
     */
    public void setSmiley(SelfProfileUserSmileyPojo smiley) {
        this.smiley = smiley;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



}
