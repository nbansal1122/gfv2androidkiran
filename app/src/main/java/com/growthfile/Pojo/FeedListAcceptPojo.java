package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Neha on 9/19/2016.
 */
public class FeedListAcceptPojo {
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private FeedListAcceptResponsePojo response;

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The response
     */
    public FeedListAcceptResponsePojo getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(FeedListAcceptResponsePojo response) {
        this.response = response;
    }

}
