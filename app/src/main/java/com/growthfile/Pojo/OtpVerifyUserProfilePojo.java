package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyUserProfilePojo {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("avatar")
    @Expose
    private Object avatar;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("org_id")
    @Expose
    private Integer orgId;
    @SerializedName("reports_to")
    @Expose
    private Integer reportsTo;
    @SerializedName("work_timing_from")
    @Expose
    private String workTimingFrom;
    @SerializedName("work_timing_till")
    @Expose
    private String workTimingTill;
    @SerializedName("center")
    @Expose
    private OtpVerifyCentrePojo center;
    @SerializedName("org")
    @Expose
    private OtpVerifyOrgPojo org;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The avatar
     */
    public Object getAvatar() {
        return avatar;
    }

    /**
     *
     * @param avatar
     * The avatar
     */
    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     *
     * @param mobile
     * The mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     *
     * @return
     * The orgId
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     *
     * @param orgId
     * The org_id
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     *
     * @return
     * The reportsTo
     */
    public Integer getReportsTo() {
        return reportsTo;
    }

    /**
     *
     * @param reportsTo
     * The reports_to
     */
    public void setReportsTo(Integer reportsTo) {
        this.reportsTo = reportsTo;
    }

    /**
     *
     * @return
     * The workTimingFrom
     */
    public String getWorkTimingFrom() {
        return workTimingFrom;
    }

    /**
     *
     * @param workTimingFrom
     * The work_timing_from
     */
    public void setWorkTimingFrom(String workTimingFrom) {
        this.workTimingFrom = workTimingFrom;
    }

    /**
     *
     * @return
     * The workTimingTill
     */
    public String getWorkTimingTill() {
        return workTimingTill;
    }

    /**
     *
     * @param workTimingTill
     * The work_timing_till
     */
    public void setWorkTimingTill(String workTimingTill) {
        this.workTimingTill = workTimingTill;
    }

    /**
     *
     * @return
     * The center
     */
    public OtpVerifyCentrePojo getCenter() {
        return center;
    }

    /**
     *
     * @param center
     * The center
     */
    public void setCenter(OtpVerifyCentrePojo center) {
        this.center = center;
    }

    /**
     *
     * @return
     * The org
     */
    public OtpVerifyOrgPojo getOrg() {
        return org;
    }

    /**
     *
     * @param org
     * The org
     */
    public void setOrg(OtpVerifyOrgPojo org) {
        this.org = org;
    }

}