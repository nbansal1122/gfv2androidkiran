package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kundan on 9/8/2016.
 */
public class SelfCalendarPojo {
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_approved")
    @Expose
    private Boolean isApproved;
    @SerializedName("is_off")
    @Expose
    private Boolean isOff;
    @SerializedName("off_reason")
    @Expose
    private String offReason;
    @SerializedName("applied_on")
    @Expose
    private String appliedOn;
    @SerializedName("approved_on")
    @Expose
    private String approvedOn;
    @SerializedName("applied_by")
    @Expose
    private SelfCalendarAppliedBy appliedBy;
    @SerializedName("approved_by")
    @Expose
    private SelfCalendarApprovedBy approvedBy;

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The isApproved
     */
    public Boolean getIsApproved() {
        return isApproved;
    }

    /**
     *
     * @param isApproved
     * The is_approved
     */
    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }

    /**
     *
     * @return
     * The isOff
     */
    public Boolean getIsOff() {
        return isOff;
    }

    /**
     *
     * @param isOff
     * The is_off
     */
    public void setIsOff(Boolean isOff) {
        this.isOff = isOff;
    }

    /**
     *
     * @return
     * The offReason
     */
    public String getOffReason() {
        return offReason;
    }

    /**
     *
     * @param offReason
     * The off_reason
     */
    public void setOffReason(String offReason) {
        this.offReason = offReason;
    }

    /**
     *
     * @return
     * The appliedOn
     */
    public String getAppliedOn() {
        return appliedOn;
    }

    /**
     *
     * @param appliedOn
     * The applied_on
     */
    public void setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     *
     * @return
     * The approvedOn
     */
    public String getApprovedOn() {
        return approvedOn;
    }

    /**
     *
     * @param approvedOn
     * The approved_on
     */
    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     *
     * @return
     * The appliedBy
     */
    public SelfCalendarAppliedBy getAppliedBy() {
        return appliedBy;
    }

    /**
     *
     * @param appliedBy
     * The applied_by
     */
    public void setAppliedBy(SelfCalendarAppliedBy appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     *
     * @return
     * The approvedBy
     */
    public SelfCalendarApprovedBy getApprovedBy() {
        return approvedBy;
    }

    /**
     *
     * @param approvedBy
     * The approved_by
     */
    public void setApprovedBy(SelfCalendarApprovedBy approvedBy) {
        this.approvedBy = approvedBy;
    }
}
