package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserCenterPojo implements Serializable{

    @SerializedName("center_id")
    @Expose
    private Integer centerId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("lat")
    @Expose
    private Float lat;
    @SerializedName("lng")
    @Expose
    private Float lng;
    @SerializedName("is_mon_working")
    @Expose
    private Integer isMonWorking;
    @SerializedName("is_tue_working")
    @Expose
    private Integer isTueWorking;
    @SerializedName("is_wed_working")
    @Expose
    private Integer isWedWorking;
    @SerializedName("is_thu_working")
    @Expose
    private Integer isThuWorking;
    @SerializedName("is_fri_working")
    @Expose
    private Integer isFriWorking;
    @SerializedName("is_sat_working")
    @Expose
    private Integer isSatWorking;
    @SerializedName("is_sun_working")
    @Expose
    private Integer isSunWorking;
    @SerializedName("work_timing_from")
    @Expose
    private String workTimingFrom;
    @SerializedName("work_timing_till")
    @Expose
    private String workTimingTill;

    /**
     *
     * @return
     * The centerId
     */
    public Integer getCenterId() {
        return centerId;
    }

    /**
     *
     * @param centerId
     * The center_id
     */
    public void setCenterId(Integer centerId) {
        this.centerId = centerId;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The lat
     */
    public Float getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(Float lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lng
     */
    public Float getLng() {
        return lng;
    }

    /**
     *
     * @param lng
     * The lng
     */
    public void setLng(Float lng) {
        this.lng = lng;
    }

    /**
     *
     * @return
     * The isMonWorking
     */
    public Integer getIsMonWorking() {
        return isMonWorking;
    }

    /**
     *
     * @param isMonWorking
     * The is_mon_working
     */
    public void setIsMonWorking(Integer isMonWorking) {
        this.isMonWorking = isMonWorking;
    }

    /**
     *
     * @return
     * The isTueWorking
     */
    public Integer getIsTueWorking() {
        return isTueWorking;
    }

    /**
     *
     * @param isTueWorking
     * The is_tue_working
     */
    public void setIsTueWorking(Integer isTueWorking) {
        this.isTueWorking = isTueWorking;
    }

    /**
     *
     * @return
     * The isWedWorking
     */
    public Integer getIsWedWorking() {
        return isWedWorking;
    }

    /**
     *
     * @param isWedWorking
     * The is_wed_working
     */
    public void setIsWedWorking(Integer isWedWorking) {
        this.isWedWorking = isWedWorking;
    }

    /**
     *
     * @return
     * The isThuWorking
     */
    public Integer getIsThuWorking() {
        return isThuWorking;
    }

    /**
     *
     * @param isThuWorking
     * The is_thu_working
     */
    public void setIsThuWorking(Integer isThuWorking) {
        this.isThuWorking = isThuWorking;
    }

    /**
     *
     * @return
     * The isFriWorking
     */
    public Integer getIsFriWorking() {
        return isFriWorking;
    }

    /**
     *
     * @param isFriWorking
     * The is_fri_working
     */
    public void setIsFriWorking(Integer isFriWorking) {
        this.isFriWorking = isFriWorking;
    }

    /**
     *
     * @return
     * The isSatWorking
     */
    public Integer getIsSatWorking() {
        return isSatWorking;
    }

    /**
     *
     * @param isSatWorking
     * The is_sat_working
     */
    public void setIsSatWorking(Integer isSatWorking) {
        this.isSatWorking = isSatWorking;
    }

    /**
     *
     * @return
     * The isSunWorking
     */
    public Integer getIsSunWorking() {
        return isSunWorking;
    }

    /**
     *
     * @param isSunWorking
     * The is_sun_working
     */
    public void setIsSunWorking(Integer isSunWorking) {
        this.isSunWorking = isSunWorking;
    }

    /**
     *
     * @return
     * The workTimingFrom
     */
    public String getWorkTimingFrom() {
        return workTimingFrom;
    }

    /**
     *
     * @param workTimingFrom
     * The work_timing_from
     */
    public void setWorkTimingFrom(String workTimingFrom) {
        this.workTimingFrom = workTimingFrom;
    }

    /**
     *
     * @return
     * The workTimingTill
     */
    public String getWorkTimingTill() {
        return workTimingTill;
    }

    /**
     *
     * @param workTimingTill
     * The work_timing_till
     */
    public void setWorkTimingTill(String workTimingTill) {
        this.workTimingTill = workTimingTill;
    }
}
