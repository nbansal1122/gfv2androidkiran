package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserLeavesPojo implements Serializable{

    @SerializedName("earned_leaves")
    @Expose
    private Float earnedLeaves;
    @SerializedName("casual_leaves")
    @Expose
    private Float casualLeaves;
    @SerializedName("medical_leaves")
    @Expose
    private Float medicalLeaves;

    /**
     *
     * @return
     * The earnedLeaves
     */
    public Float getEarnedLeaves() {
        return earnedLeaves;
    }

    /**
     *
     * @param earnedLeaves
     * The earned_leaves
     */
    public void setEarnedLeaves(Float earnedLeaves) {
        this.earnedLeaves = earnedLeaves;
    }

    /**
     *
     * @return
     * The casualLeaves
     */
    public Float getCasualLeaves() {
        return casualLeaves;
    }

    /**
     *
     * @param casualLeaves
     * The casual_leaves
     */
    public void setCasualLeaves(Float casualLeaves) {
        this.casualLeaves = casualLeaves;
    }

    /**
     *
     * @return
     * The medicalLeaves
     */
    public Float getMedicalLeaves() {
        return medicalLeaves;
    }

    /**
     *
     * @param medicalLeaves
     * The medical_leaves
     */
    public void setMedicalLeaves(Float medicalLeaves) {
        this.medicalLeaves = medicalLeaves;
    }


}
