package com.growthfile.Pojo;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpPojo {

    private String msg;
    private OtpResponsePojo response;

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The response
     */
    public OtpResponsePojo getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(OtpResponsePojo response) {
        this.response = response;
    }

}
