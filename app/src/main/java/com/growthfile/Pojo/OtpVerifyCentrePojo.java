package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class OtpVerifyCentrePojo {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("is_mon_working")
    @Expose
    private Integer isMonWorking;
    @SerializedName("is_tue_working")
    @Expose
    private Integer isTueWorking;
    @SerializedName("is_wed_working")
    @Expose
    private Integer isWedWorking;
    @SerializedName("is_thu_working")
    @Expose
    private Integer isThuWorking;
    @SerializedName("is_fri_working")
    @Expose
    private Integer isFriWorking;
    @SerializedName("is_sat_working")
    @Expose
    private Integer isSatWorking;
    @SerializedName("is_sun_working")
    @Expose
    private Integer isSunWorking;

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The state
     */
    public Object getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(Object state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lng
     */
    public Double getLng() {
        return lng;
    }

    /**
     *
     * @param lng
     * The lng
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     *
     * @return
     * The isMonWorking
     */
    public Integer getIsMonWorking() {
        return isMonWorking;
    }

    /**
     *
     * @param isMonWorking
     * The is_mon_working
     */
    public void setIsMonWorking(Integer isMonWorking) {
        this.isMonWorking = isMonWorking;
    }

    /**
     *
     * @return
     * The isTueWorking
     */
    public Integer getIsTueWorking() {
        return isTueWorking;
    }

    /**
     *
     * @param isTueWorking
     * The is_tue_working
     */
    public void setIsTueWorking(Integer isTueWorking) {
        this.isTueWorking = isTueWorking;
    }

    /**
     *
     * @return
     * The isWedWorking
     */
    public Integer getIsWedWorking() {
        return isWedWorking;
    }

    /**
     *
     * @param isWedWorking
     * The is_wed_working
     */
    public void setIsWedWorking(Integer isWedWorking) {
        this.isWedWorking = isWedWorking;
    }

    /**
     *
     * @return
     * The isThuWorking
     */
    public Integer getIsThuWorking() {
        return isThuWorking;
    }

    /**
     *
     * @param isThuWorking
     * The is_thu_working
     */
    public void setIsThuWorking(Integer isThuWorking) {
        this.isThuWorking = isThuWorking;
    }

    /**
     *
     * @return
     * The isFriWorking
     */
    public Integer getIsFriWorking() {
        return isFriWorking;
    }

    /**
     *
     * @param isFriWorking
     * The is_fri_working
     */
    public void setIsFriWorking(Integer isFriWorking) {
        this.isFriWorking = isFriWorking;
    }

    /**
     *
     * @return
     * The isSatWorking
     */
    public Integer getIsSatWorking() {
        return isSatWorking;
    }

    /**
     *
     * @param isSatWorking
     * The is_sat_working
     */
    public void setIsSatWorking(Integer isSatWorking) {
        this.isSatWorking = isSatWorking;
    }

    /**
     *
     * @return
     * The isSunWorking
     */
    public Integer getIsSunWorking() {
        return isSunWorking;
    }

    /**
     *
     * @param isSunWorking
     * The is_sun_working
     */
    public void setIsSunWorking(Integer isSunWorking) {
        this.isSunWorking = isSunWorking;
    }

}