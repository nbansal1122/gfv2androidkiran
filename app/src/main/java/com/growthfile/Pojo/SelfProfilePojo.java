package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfilePojo {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private SelfProfileResponsePojo response;

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The response
     */
    public SelfProfileResponsePojo getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(SelfProfileResponsePojo response) {
        this.response = response;
    }
}
