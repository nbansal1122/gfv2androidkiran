package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfLeaveBalLeaveInfoPojo {

    @SerializedName("EL")
    @Expose
    private Float eL;
    @SerializedName("CL")
    @Expose
    private Float cL;
    @SerializedName("SL")
    @Expose
    private Float sL;

    /**
     *
     * @return
     * The eL
     */
    public Float getEL() {
        return eL;
    }

    /**
     *
     * @param eL
     * The EL
     */
    public void setEL(Float eL) {
        this.eL = eL;
    }

    /**
     *
     * @return
     * The cL
     */
    public Float getCL() {
        return cL;
    }

    /**
     *
     * @param cL
     * The CL
     */
    public void setCL(Float cL) {
        this.cL = cL;
    }

    /**
     *
     * @return
     * The sL
     */
    public Float getSL() {
        return sL;
    }

    /**
     *
     * @param sL
     * The SL
     */
    public void setSL(Float sL) {
        this.sL = sL;
    }

}
