package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfProfileUserGrowthFilePojo implements Serializable{

    @SerializedName("on_time")
    @Expose
    private int onTime;
    @SerializedName("attended")
    @Expose
    private int attended;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("leaves")
    @Expose
    private int leaves;
    @SerializedName("avg_days_taken")
    @Expose
    private int avgDaysTaken;

    /**
     *
     * @return
     * The onTime
     */
    public Integer getOnTime() {
        return onTime;
    }

    /**
     *
     * @param onTime
     * The on_time
     */
    public void setOnTime(Integer onTime) {
        this.onTime = onTime;
    }

    /**
     *
     * @return
     * The attended
     */
    public Integer getAttended() {
        return attended;
    }

    /**
     *
     * @param attended
     * The attended
     */
    public void setAttended(Integer attended) {
        this.attended = attended;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The leaves
     */
    public Integer getLeaves() {
        return leaves;
    }

    /**
     *
     * @param leaves
     * The leaves
     */
    public void setLeaves(Integer leaves) {
        this.leaves = leaves;
    }

    /**
     *
     * @return
     * The avgDaysTaken
     */
    public Integer getAvgDaysTaken() {
        return avgDaysTaken;
    }

    /**
     *
     * @param avgDaysTaken
     * The avg_days_taken
     */
    public void setAvgDaysTaken(Integer avgDaysTaken) {
        this.avgDaysTaken = avgDaysTaken;
    }

}
