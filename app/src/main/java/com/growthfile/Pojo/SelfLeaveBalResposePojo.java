package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CANBRAND on 07-Aug-15.
 */

public class SelfLeaveBalResposePojo {

    @SerializedName("leaves")
    @Expose
    private SelfLeaveBalLeaveInfoPojo leaves;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    /**
     *
     * @return
     * The leaves
     */
    public SelfLeaveBalLeaveInfoPojo getLeaves() {
        return leaves;
    }

    /**
     *
     * @param leaves
     * The leaves
     */
    public void setLeaves(SelfLeaveBalLeaveInfoPojo leaves) {
        this.leaves = leaves;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
