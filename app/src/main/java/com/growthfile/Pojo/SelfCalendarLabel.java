package com.growthfile.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kundan on 9/7/2016.
 */
public class SelfCalendarLabel {
    @SerializedName("AM")
    @Expose
    private SelfCalendarAm aM;
    @SerializedName("PM")
    @Expose
    private SelfCalendarPm pM;
    @SerializedName("EL")
    @Expose
    private SelfCalendarEl eL;
    @SerializedName("CL")
    @Expose
    private SelfCalendarCl cL;
    @SerializedName("SL")
    @Expose
    private SelfCalendarSl sL;
    @SerializedName("OL")
    @Expose
    private SelfCalendarOl oL;
    @SerializedName("LWP")
    @Expose
    private SelfCalendarLwp lWP;

    @SerializedName("HOLIDAY")
    @Expose
    private SelfCalendarHoliday hOLIDAY;
    @SerializedName("WEEKOFF")
    @Expose
    private SelfCalendarWeekoff wEEKOFF;

    /**
     *
     * @return
     * The aM
     */
    public SelfCalendarAm getAM() {
        return aM;
    }

    /**
     *
     * @param aM
     * The AM
     */
    public void setAM(SelfCalendarAm aM) {
        this.aM = aM;
    }

    /**
     *
     * @return
     * The pM
     */
    public SelfCalendarPm getPM() {
        return pM;
    }

    /**
     *
     * @param pM
     * The PM
     */
    public void setPM(SelfCalendarPm pM) {
        this.pM = pM;
    }

    /**
     *
     * @return
     * The eL
     */
    public SelfCalendarEl getEL() {
        return eL;
    }

    /**
     *
     * @param eL
     * The EL
     */
    public void setEL(SelfCalendarEl eL) {
        this.eL = eL;
    }

    /**
     *
     * @return
     * The cL
     */
    public SelfCalendarCl getCL() {
        return cL;
    }

    /**
     *
     * @param cL
     * The CL
     */
    public void setCL(SelfCalendarCl cL) {
        this.cL = cL;
    }

    /**
     *
     * @return
     * The sL
     */
    public SelfCalendarSl getSL() {
        return sL;
    }

    /**
     *
     * @param sL
     * The SL
     */
    public void setSL(SelfCalendarSl sL) {
        this.sL = sL;
    }

    /**
     *
     * @return
     * The oL
     */
    public SelfCalendarOl getOL() {
        return oL;
    }

    /**
     *
     * @param oL
     * The OL
     */
    public void setOL(SelfCalendarOl oL) {
        this.oL = oL;
    }

    /**
     *
     * @return
     * The lWP
     */
    public SelfCalendarLwp getLWP() {
        return lWP;
    }

    /**
     *
     * @param lWP
     * The LWP
     */
    public void setLWP(SelfCalendarLwp lWP) {
        this.lWP = lWP;
    }

    /**
     *
     * @return
     * The hOLIDAY
     */
    public SelfCalendarHoliday getHOLIDAY() {
        return hOLIDAY;
    }

    /**
     *
     * @param hOLIDAY
     * The HOLIDAY
     */
    public void setHOLIDAY(SelfCalendarHoliday hOLIDAY) {
        this.hOLIDAY = hOLIDAY;
    }

    /**
     *
     * @return
     * The wEEKOFF
     */
    public SelfCalendarWeekoff getWEEKOFF() {
        return wEEKOFF;
    }

    /**
     *
     * @param wEEKOFF
     * The WEEKOFF
     */
    public void setWEEKOFF(SelfCalendarWeekoff wEEKOFF) {
        this.wEEKOFF = wEEKOFF;
    }

}
