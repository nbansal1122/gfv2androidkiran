package simplifii.framework.asyncmanager;

import android.content.Context;

import simplifii.framework.utility.AppConstants;


public class ServiceFactory {

    public static Service getInstance(Context context, int taskCode) {
        Service service = null;
        switch (taskCode) {
            case AppConstants.TASK_CODES.UPLOAD_IMAGE:
                service = new FileUploadService();
                break;
            case AppConstants.TASK_CODES.FOLLOW_UP_ACTION:
            case AppConstants.TASK_CODES.VALIDATE_OTP:
            case AppConstants.TASK_CODES.APPLY_LEAVE:
            case AppConstants.TASK_CODES.BOOK_TRANSACTION:
            case AppConstants.TASK_CODES.MARK_LOCATION:
            case AppConstants.TASK_CODES.RESIGN_REQUEST:
                service = new MultipartService();
                break;
            case AppConstants.TASK_CODES.EDIT_PHOTO:
                service = new CloudinaryService();
                break;
            default:
                service = new OKHttpService();
                break;

        }
        return service;
    }

}
