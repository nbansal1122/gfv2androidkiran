package simplifii.framework.asyncmanager;

import android.graphics.Bitmap;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simplifii.framework.exceptionhandler.RestException;

/**
 * Created by nbansal2211 on 15/01/17.
 */

public class CloudinaryService extends GenericService {
    @Override
    public Object getData(Object... params) throws JSONException, SQLException, NullPointerException, RestException, ClassCastException, IOException {
        if (params != null && params.length == 1) {
            try {
                return uploadViaCloudinary((String) params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } else {
            throw new IllegalArgumentException("");
        }
    }

    private String uploadViaCloudinary(String filePath) throws Exception {
        Map config = new HashMap();
        config.put("cloud_name", "growthfile");
        config.put("api_key", "414334488441358");
        config.put("api_secret", "QMAHh5GTTLN1mA12jAw0VwcYz4o");
        Cloudinary cloudinary = new Cloudinary(config);
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        final Map map = cloudinary.uploader().upload(fileInputStream, ObjectUtils.emptyMap());
        if (map.containsKey("url")) {
            return map.get("url").toString();
        }
        throw new Exception();
    }
}
