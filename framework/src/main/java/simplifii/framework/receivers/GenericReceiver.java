package simplifii.framework.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by nitin on 04/12/15.
 */
public class GenericReceiver extends BroadcastReceiver {

    public static final String ACTION_REFRESH = "REFRESH";
    private IRefreshListener listener;

    public GenericReceiver(IRefreshListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_REFRESH)) {
            if (listener != null) {
                listener.onReceive(intent);
            }
        }
    }

    public static void sendBroadcast(Context context, String action, Bundle bundle) {
        Intent i = new Intent(action);
        if (bundle != null) {
            i.putExtras(bundle);
        }
        context.sendBroadcast(i);
    }

    public static interface IRefreshListener {
        public void onReceive(Intent intent);
    }
}
