package simplifii.framework.utility;

public interface AppConstants {

    public static final String DEF_REGULAR_FONT = "WorkSans-Regular.otf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    String LOCATION_TEXT = "Mark every meeting, market visit & client call instantly";
    String TRANSACTION_TEXT = "Book payments & collections with due date reminders";
    String SMILEY_TEXT = "Say thank you to each other privately";
    String STAR_TEXT = "Share credit for achievements and support them openly";

    interface REQUEST_CODES {

        int CREATE_ROLE = 1;
        int ASSIGN_ROLE = 2;
        int SELECT_DEPARTMENT = 3;
        int SELECT_CENTER = 4;
        int SELECT_COST_CENTER = 5;
        int SUBMIT_RESIGN = 6;
        int REQ_PICK_IMAGE = 7;
        int EDIT_PROFILE = 8;
        int CHANGE_NUMBER = 9;
        int CHANGE_REPORTING_MANAGER = 10;
        int CHANGE_DESIGNATION = 11;
        int CHANGE_DEPARTMENT = 12;
        int CHANGE_COST_CENTER = 13;
        int PUT_RESIGN = 14;
        int CHANGE_EDIT_ROLE = 15;
        int GIVE_SMILEY = 16;
        int GIVE_STAR = 17;
        int MARK_PUNCH = 18;
        int APPLY_LEAVE = 19;
        int CHECK_GPS = 20;
        int MARK_LOCATION = 21;
        int EDIT_TRANSACTION = 22;
        int MARK_PRESENT = 23;
    }


    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {

        String ABOUT_US = "http://www.google.com";
        String TnC = "http://www.google.com";
        String PRIVACY_POLICY = "http://www.google.com";


//        String BASE_URL = "http://aws1.simplifii.com/api";
//        String GFILE_BASE_URL = "http://gfile.simplifii.com/api";
//        String GFILE_BASE_URL = "https://backend.growthfile.com/api";
        String STAGING_SERVER_URL = "http://backend.growthfile.simplifii.com/api";
        String LIVE_SERVER_URL = "http://backend.growthfile.com/api";
        String BASE_URL = LIVE_SERVER_URL;
        String VERSION_V1 = "/v1";
        String VERSION_V2 = "/v2";

        String EMPLOYEES = "/employees";
        String ORGANISATION = "/org";
        String GET = "/get";
        String ACTION = "/action";
        String EDIT = "/edit";

        String CENTER_LIST = BASE_URL + VERSION_V1 + "/employee/centerlist";
        String USER_FEED = BASE_URL + VERSION_V1 + "/feed";
        String CHANGE_MOBILE = BASE_URL + VERSION_V1 + EMPLOYEES + "/changemobile";
        String GET_USER_PROFILE_SELF_V1 = BASE_URL + VERSION_V1 + "/userprofile";
        String CHANGE_REPORTING_MANAGER = BASE_URL + VERSION_V1 + EMPLOYEES + "/changerm";
        String RESIGN_REQUEST = BASE_URL + VERSION_V1 + EMPLOYEES + "/resign";
        String TERMINATE_REQUEST = BASE_URL + VERSION_V1 + EMPLOYEES + "/terminate";
        String GIVE_STAR_REQUEST = BASE_URL + VERSION_V1 + EMPLOYEES + "/stars";
        String GIVE_SMILEY_REQUEST = BASE_URL + VERSION_V1 + EMPLOYEES + "/smileys";
        String UPVOTE_STAR_REQUEST = BASE_URL + VERSION_V1 + EMPLOYEES + "/upvotestar";
        String ADD_POSITION_REQUEST = BASE_URL + VERSION_V1 + ORGANISATION + "/positions";
        String EDIT_POSITION_REQUEST = BASE_URL + VERSION_V1 + ORGANISATION + "/positions";
        String REVIEW_POSITION_REQUEST = EDIT_POSITION_REQUEST + "/review";
        String WITHDRAW_POSITION_REQUEST = EDIT_POSITION_REQUEST + "/withdraw";
        String EDIT_EMPLOYEE_DETAILS = BASE_URL + VERSION_V1 + EMPLOYEES + "/editprofile";
        String ACCEPT_REJECT_CHANGE_REPORTING_MANAGER = BASE_URL + VERSION_V1 + EMPLOYEES + "/reviewrmchange";
        String ACCEPT_REJECT_EMPLOYEE_POSITION_MAPPING = BASE_URL + VERSION_V1 + ORGANISATION + "/positions/reviewmapping";
        String MY_TEAM = BASE_URL + VERSION_V2 + "/myteam";
        String GET_DEPARTMENTS = BASE_URL + VERSION_V1 + GET + ORGANISATION + "/departments";
        String GET_CENTERS = BASE_URL + VERSION_V1 + GET + ORGANISATION + "/centers";
        String GET_COST_CENTERS = BASE_URL + VERSION_V1 + GET + ORGANISATION + "/costcenters";
        String MAP_EMPLOYEE = EDIT_POSITION_REQUEST + "/mapemployee";
        String WITHDRAW_RESIGNATION = BASE_URL + VERSION_V1 + EMPLOYEES + "/withdrawresignation";
        String ACTION_LEAVE = BASE_URL + VERSION_V1 + ACTION;
        String ACCEPT_REJECT_RESIGN = BASE_URL + VERSION_V1 + EMPLOYEES + "/reviewresignation";
        String ACCEPT_REJECT_TERMINATION = BASE_URL + VERSION_V1 + EMPLOYEES + "/reviewtermination";
        String GET_USER_PROFILE_OTHER_V1 = GET_USER_PROFILE_SELF_V1;
        String EDIT_SELF_PROFILE = BASE_URL + VERSION_V1 + EDIT + EMPLOYEES + "/profile";
        String RESEND_OTP = BASE_URL + VERSION_V1 + "/resendotp";
        String GET_EMPLOYEES = BASE_URL + VERSION_V1 + "/get/org/employees";
        String MARK_LOCATION = "";
        String UPDATE_PROFILE_PIC_V2 = BASE_URL + "/v2/employees/profileimage";
    }

    public static interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";


        String USER_ID = "user_id";
        String IS_LOGIN = "is_login";
        String USER_INSTANCE = "user_intance";
        String SELLER_NAME = "user_name";
        String SELLER_MOBILE = "Seller_mobile";
        String SELLER_EMAIL = "seller_email";
        String SELLER_INSTANCE = "seller Instance";
        String USER_TOKEN = "user_token";
        String USER_EMAIL = "user_email";
        String USER_NAME = "username";
        String OBJECT_ID = "object_id";
        String PAST_CURRENT_ILLNESS = "past_current_illness";
        String DIAGNOSIS = "diagnosis";
        String ALLERGY = "allergy";
        String SYSTEM_REVIEW = "system_review";
        String FURTHER_ANALIST = "ferther";
        String VITALS = "vitals";
        String MEDICINES = "medicine";
        String DICTIONARY_ADDED = "dictionary_added";
        String ADDING_SUGETIONS = "adding_suggetions";
        String DIAGNOSIS_DATA = "diagnosis_data";
        String CREATE_ROLE_INSTANCE = "role_instance";
        String USER_SESSION = "userSession";
        String FCM_TOKEN = "fcm_token";
        String KEY_EMP_LIST_JSON = "empListJson";
        String KEY_ACCOUNT_LIST_JSON = "accountList";
        String KEY_LAT = "Latitude";
        String KEY_LNG = "Longitude";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String SEARCH = "search";
        String CREATE_ROLE_DATA = "createRoleData";
        String SELECTED_DEPARTMENT = "selectedDepartment";
        String SELECTED_CENTER = "selectedCenter";
        String SELECTED_COST_CENTER = "selectedCostCenter";
        String USER_ID = "userId";
        String USER_NAME = "userName";
        String USER_PROFILE_RESPONSE = "userProfile";
        String DATE_API_FORMAT = "dateApiFormat";
        String DATE_UI_FORMAT = "dateUiFormat";
        String PHONE = "phone";
        String APPLICATION_ID = "applicationId";
        String IS_UPVOTE_STAR = "isUpvoteStar";
        String KEY_URL = "keyUrl";
        String KEY_TITLE = "keyTitle";
        String OTP = "otp";
        String CARD_ID = "cardId";
        java.lang.String NOTIF_TYPE = "notifType";
        String IS_FROM_PUSH = "isFromPush";
        String TOOLBAR_TITLE = "toolbarTitle";
        String LEAVE_TYPE = "leaveType";
        String EDIT_TRANSACTION = "editTransaction";
        String DUE_DATE = "dueDate";
        String AMOUNT = "amount";
        String COMMENT = "comment";
        String STATUS = "status";
        String ACCOUNT_ID = "accountId";
        String ACCOUNT_NAME = "accountName";
    }

    public static interface FRAGMENT_TYPE {
        int EDIT_PROFILE = 1;
        int CHANGE_MOBILE_NUMBER = 2;
        int CHANGE_REPORTING_MANAGER = 3;
        int TRANSFER = 4;
        int CHANGE_DESIGNATION = 5;
        int CHANGE_DEPARTMENT = 6;
        int CHANGE_COST_CENTER = 7;
        int RESIGN = 8;
        int GIVE_SMILEY = 9;
        int CREATE_ROLE = 10;
        int ASSIGN_ROLE = 11;
        int SELECT_DEPARTMENT = 12;
        int SELECT_CENTER = 13;
        int SELECT_COST_CENTER = 14;
        int GIVE_STAR = 15;
        int CHANGE_EDIT_ROLE = 16;
        int SUBMIT_RESIGN_FRAGMENT = 17;
        int OPEN_EMPLOYEE_PROFILE = 18;
        int SEARCH_EMPLOYEE = 19;
    }

    public static interface VIEW_TYPE {
        int TEAM_FRAGMENT_SELF = 0;
        int CREATED_ROLE = 1;
        int USER_PROFILE_META_INFO = 2;
        int USER_PROFILE_HEADER_INFO = 3;
        int USER_FEED = 4;
        int TEAM_FRAGMENT_OTHER = 5;
        int CARD_HEADER_MAP = 6;
        int GROWTH_FILE = 7;
        int STAR_IMAGE_HEADER = 8;
    }

    public interface TASK_CODES {

        int CHANGE_MOBILE_NUMBER = 1;
        int UPLOAD_IMAGE = 2;
        int GET_USER_PROFILE_SELF = 3;
        int MY_TEAM = 4;
        int GET_DEPARTMENTS = 5;
        int GET_CENTERS = 6;
        int GET_COST_CENTERS = 7;
        int ADD_POSITION = 8;
        int MAP_EMPLOYEE = 9;
        int GIVE_SMILEY = 10;
        int GIVE_STAR = 11;
        int EDIT_PROFILE = 12;
        int USER_PROFILE = 13;
        int USER_FEED = 14;
        int SUBMIT_RESIGN = 15;
        int WITHDRAW_RESIGNATION = 16;
        int WITHDRAW_LEAVE = 17;
        int ACCEPT_RESIGNATION = 18;
        int REJECT_RESIGNATION = 19;
        int ACCEPT_LEAVE = 20;
        int REJECT_LEAVE = 21;
        int WITHDRAW_POSITION = 22;
        int ACCEPT_TERMINATION = 23;
        int REJECT_TERMINATION = 24;
        int ACCEPT_POSITION = 25;
        int REJECT_POSITION = 26;
        int GET_USER_PROFILE_OTHER = 27;
        int EDIT_PROFILE_IMAGE = 28;
        int RESEND_OTP = 29;
        int GET_EMPLOYEES = 30;
        int CHANGE_REPORTING_MANAGER = 31;
        int USER_FEED_MORE = 32;
        int UPVOTE_STAR = 33;
        int GENERATE_OTP = 34;
        int VALIDATE_OTP = 35;
        int APPLY_LEAVE = 36;
        int MARK_LOCATION = 37;
        int SEND_SMILEY = 38;
        int SINGLE_CARD_DETAILS = 39;
        int FOLLOW_UP_ACTION = 40;
        int SEND_STAR = 41;
        int GET_ACCOUNTS = 42;
        int BOOK_TRANSACTION = 43;
        int RESIGN_REQUEST = 45;

        int EDIT_TRANSACTION = 44;

        int EDIT_PHOTO = 46;
        int LOGOUT = 47;
    }

    public static interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
    }

    public interface PARAMS {

        String LAT = "latitude";
        String LNG = "longitude";
    }

    public interface ACTION_TYPE {

        int ADD_TEAM_MEMBER = 1;
        int CAL_CLICKED = 2;
        int ACTION_FEED = 3;
        int OPEN_EMPLOYEE_PROFILE = 4;
        int GIVE_SMILEY = 5;
        int GIVE_STAR = 6;
        int MAP_HEADER_CLICK = 7;
        int EDIT_IMAGE = 8;
    }

    interface PAGE_URL_V2 {
        String BASE_URL = PAGE_URL.BASE_URL + PAGE_URL.VERSION_V2;
    }

    interface EMPLOYEES_PAGE_URL {
        String EMPLOYEES = "/employees";
        String EMPLOYEES_BASE_URL = PAGE_URL_V2.BASE_URL + EMPLOYEES;
        String GET_EMPLOYESS_BASE_URL = PAGE_URL_V2.BASE_URL + "/get" + EMPLOYEES;
        String LIST_EMPLOYEES_BASE_URL = PAGE_URL_V2.BASE_URL + "/get/list" + EMPLOYEES;
        String LOGOUT = PAGE_URL_V2.BASE_URL + "/logout";
        String GENERATE_OTP = EMPLOYEES_BASE_URL + "/generateotp";
        String VALIDATE_OTP = EMPLOYEES_BASE_URL + "/validateotp";
        String GET_CARDS = GET_EMPLOYESS_BASE_URL + "/cards";
        String APPLY_LEAVE = EMPLOYEES_BASE_URL + "/applyleave";
        String GET_GROWTH_FILE = EMPLOYEES_BASE_URL + "/growthfile";
        String LIST_EMPLOYEES = LIST_EMPLOYEES_BASE_URL;
        String MARK_LOCATION = EMPLOYEES_BASE_URL + "/marklocation";
        String SEND_SMILEY = EMPLOYEES_BASE_URL + "/sendsmiley";
        String SEND_STAR = EMPLOYEES_BASE_URL + "/sendstar";
        String SINGLE_CARD_DETAILS = GET_EMPLOYESS_BASE_URL + "/carddetails";
        String FOLLOW_UP_ACTION = EMPLOYEES_BASE_URL + "/followupaction";
        String GET_ACCOUNTS = PAGE_URL_V2.BASE_URL + "/get/list/accounts";
        String BOOK_TRANSACTION = EMPLOYEES_BASE_URL + "/booktransaction";
        String RESIGN_REQUEST = EMPLOYEES_BASE_URL + "/resign";
        String EDIT_TRANSACTION = EMPLOYEES_BASE_URL + "/transactions";
    }

}
