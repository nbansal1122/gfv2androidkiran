package simplifii.framework.rest.enums;

import simplifii.framework.model.CreateRoleModel;

/**
 * Created by robin on 11/28/16.
 */

public enum CreateRoleType {

    INTERN("INTERN"),
    PART_TIME("PART-TIME"),
    CONSULTANT("CONSULTANT"),
    FULL_TIME("FULL-TIME");

    private String value;

    public String getValue() {
        return value;
    }

    private CreateRoleType(String value){
        this.value = value;
    }

    public static CreateRoleType findByValue(String value){
        for(CreateRoleType createRoleType : CreateRoleType.values()){
            if(createRoleType.value.equalsIgnoreCase(value)){
                return createRoleType;
            }
        }
        return null;
    }
}
